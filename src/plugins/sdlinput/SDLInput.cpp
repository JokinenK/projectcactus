#include <iostream>
#include <cstring>
#include <SDL.h>
#include "SDLInput.h"
#include "core/events/KeyboardEvent.h"
#include "core/events/MouseEvent.h"
#include "core/MessageBus.h"
#include "support/Utils.h"

namespace
{
	static const struct {
		int source;
		KeyboardEvent::ModifierKey mapped;
	} mappings[] = {
			{ KMOD_LSHIFT, KeyboardEvent::LShift },
			{ KMOD_RSHIFT, KeyboardEvent::RShift },
			{ KMOD_LCTRL,  KeyboardEvent::LCtrl  },
			{ KMOD_RCTRL,  KeyboardEvent::RCtrl  },
			{ KMOD_LALT,   KeyboardEvent::LAlt   },
			{ KMOD_RALT,   KeyboardEvent::RAlt   },
			{ KMOD_LGUI,   KeyboardEvent::LWin   },
			{ KMOD_RGUI,   KeyboardEvent::RWin   }
	};

	int parseModifiers(int modifiers)
	{
		int result = 0;

		for (unsigned int i = 0; i < ARRAY_SIZE(mappings); i++) {
			if (modifiers & mappings[i].source) {
				result |= mappings[i].mapped;
			}
		};

		return result;
	}
}

SDLInput::SDLInput() :
		m_prevMouseX(0),
		m_prevMouseY(0),
		m_messageBus(0)
{
}

SDLInput::~SDLInput()
{
}

void SDLInput::init(MessageBus* messageBus)
{
	m_messageBus = messageBus;
	SDL_StartTextInput();
}

void SDLInput::update()
{
	SDL_Event event;

	while (SDL_PollEvent(&event)) {
		switch (event.type) {
			case SDL_KEYDOWN:
			case SDL_KEYUP: {
				KeyboardEvent::EventType type = (event.key.state == SDL_PRESSED)
											  ? KeyboardEvent::KeyPress
											  : KeyboardEvent::KeyRelease;

				int keySymbol = event.key.keysym.sym;
				int scanCode = event.key.keysym.scancode;
				int keyModifier = parseModifiers(event.key.keysym.mod);

				KeyboardEvent keyboardEvent(type, keySymbol, scanCode, keyModifier);
				m_messageBus->publish(&keyboardEvent);
			} break;

			case SDL_TEXTINPUT: {
				KeyboardEvent keyboardEvent(KeyboardEvent::TextInput, event.text.text[0], 0, 0);
				m_messageBus->publish(&keyboardEvent);
			} break;

			case SDL_MOUSEBUTTONUP:
			case SDL_MOUSEBUTTONDOWN: {
				int buttonMask = SDL_BUTTON(event.button.button);
				int stateMask = (event.button.state == SDL_PRESSED) ? buttonMask : 0x00;

				MouseEvent mouseEvent(MouseEvent::MouseButton, stateMask, event.button.x, event.button.y);
				m_messageBus->publish(&mouseEvent);

				m_prevMouseX = event.button.x;
				m_prevMouseY = event.button.y;
			} break;

			case SDL_MOUSEWHEEL: {
				MouseEvent mouseEvent(MouseEvent::MouseWheel, 0, m_prevMouseX, m_prevMouseY, event.wheel.x, event.wheel.y);
				m_messageBus->publish(&mouseEvent);
			} break;

			case SDL_MOUSEMOTION: {
				MouseEvent mouseEvent(MouseEvent::MouseMove, event.motion.state, event.motion.x, event.motion.y);
				m_messageBus->publish(&mouseEvent);

				m_prevMouseX = event.button.x;
				m_prevMouseY = event.button.y;
			} break;
		}
	}
}

IInput* PLUGIN_CreateInstance()
{
	return new SDLInput();
}

void PLUGIN_ReleaseInstance(IInput* instance)
{
	delete instance;
}

int PLUGIN_GetVersion()
{
	return 1;
}

const char* PLUGIN_GetName()
{
	return "Cactus SDL Input";
}
