cmake_minimum_required(VERSION 2.8.11)
project("CactusSDLInput")

find_package(SDL2 REQUIRED)

set(DIR ${CMAKE_CURRENT_SOURCE_DIR})

include_directories(
    ${CACTUS_ROOT}
    ${CACTUS_ROOT}/3rdparty/
    ${CACTUS_ROOT}/3rdparty/glm/
    ${SDL2_INCLUDE_DIR}
)

set(SOURCES
    ${DIR}/SDLInput.cpp
)

set(HEADERS
    ${DIR}/SDLInput.h
)

set(INTERFACES
)

set(LIBRARIES
    ${SDL2_LIBRARY}
)

add_library(${PROJECT_NAME} ${SOURCES})
set_target_properties(${PROJECT_NAME} PROPERTIES DEFINE_SYMBOL "PLUGIN_EXPORT")
target_link_libraries(${PROJECT_NAME} ${LIBRARIES})

install(TARGETS ${PROJECT_NAME} DESTINATION bin)