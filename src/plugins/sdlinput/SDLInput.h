#ifndef SDLINPUT_H_
#define SDLINPUT_H_

#include "core/IInput.h"
#include "core/PluginExports.h"

class MessageBus;

class SDLInput : public IInput
{
public:
	SDLInput();
    virtual ~SDLInput();

    void init(MessageBus* messageBus);
    void update();

private:
    short m_prevMouseX;
    short m_prevMouseY;

    MessageBus* m_messageBus;
};

extern PLUGIN_API IInput* PLUGIN_CreateInstance();
extern PLUGIN_API void PLUGIN_ReleaseInstance(IInput* instance);
extern PLUGIN_API int PLUGIN_GetVersion();
extern PLUGIN_API const char* PLUGIN_GetName();

#endif // SDLINPUT_H_
