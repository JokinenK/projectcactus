/*
 * OpenGLUniform.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_PLUGINS_OPENGLRENDERER_OPENGLUNIFORM_H_
#define PROJECTCACTUS_SRC_PLUGINS_OPENGLRENDERER_OPENGLUNIFORM_H_

#include <GL/glew.h>
#include "core/IUniform.h"

class OpenGLUniform : public IUniform
{
public:
	OpenGLUniform();
	OpenGLUniform(GLuint program, const std::string& uniformName);
	OpenGLUniform(const OpenGLUniform& other);

	virtual ~OpenGLUniform();

	void operator=(const OpenGLUniform& other);

    virtual void set(int x);
    virtual void set(int x, int y);
    virtual void set(int x, int y, int z);
    virtual void set(int x, int y, int z, int w);

    virtual void set(float x);
    virtual void set(float x, float y);
    virtual void set(float x, float y, float z);
    virtual void set(float x, float y, float z, float w);

    virtual void set(const glm::vec2& vec);
    virtual void set(const glm::vec3& vec);
    virtual void set(const glm::vec4& vec);
    virtual void set(const glm::mat4& mat);

    virtual void set(const std::vector<int>& arrInt);
	virtual void set(const std::vector<float>& arrFloat);
    virtual void set(const std::vector<glm::vec2>& arrVec);
	virtual void set(const std::vector<glm::vec3>& arrVec);
	virtual void set(const std::vector<glm::vec4>& arrVec);
	virtual void set(const std::vector<glm::mat4>& arrMat);

private:
    GLint m_handle;
};

#endif /* PROJECTCACTUS_SRC_PLUGINS_OPENGLRENDERER_OPENGLUNIFORM_H_ */
