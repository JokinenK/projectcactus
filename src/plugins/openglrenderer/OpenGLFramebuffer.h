/*
 * OpenGLFramebuffer.h
 *
 *  Created on: 24.1.2017
 *      Author: kalle
 */

#ifndef OPENGLFRAMEBUFFER_H_
#define OPENGLFRAMEBUFFER_H_

#include "core/IFramebuffer.h"

class OpenGLFramebuffer : public IFramebuffer
{
public:
	OpenGLFramebuffer();
	~OpenGLFramebuffer();

	void init(int width, int height);

	int getWidth() const;
	int getHeight() const;

	void bindWriteBuffer();
	void unbindWriteBuffer();

	void bindReadBuffer();
	void unbindReadBuffer();

	void clear();

	void attachRenderTarget(ITexture* texture, Attachment attachment);
	void drawBuffers(Attachment* attachments, int numAttachments);

	void copyColorToActiveFramebuffer();
	void copyDepthToActiveFramebuffer();

private:
	GLuint m_framebuffer;
	int m_width;
	int m_height;
};

#endif /* OPENGLFRAMEBUFFER_H_ */
