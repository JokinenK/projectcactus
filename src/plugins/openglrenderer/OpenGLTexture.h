#ifndef OPENGLTEXTURE_H
#define OPENGLTEXTURE_H

#include "core/ITexture.h"

class OpenGLTexture : public ITexture
{
public:
	OpenGLTexture();
    virtual ~OpenGLTexture();

    bool initTexture(
			int textureWidth,
			int textureHeight,
			StorageType storageType = UNSIGNED_BYTE,
			TextureType textureType = TEXTURE_2D);

	bool setTextureData(
			const unsigned char* textureData,
			DataFormat sourceFormat = RGBA,
			DataFormat targetFormat = RGBA,
			TextureType textureType = TEXTURE_SOURCE);

    bool updateTextureData(
    		int x,
			int y,
    		int width,
    		int height,
			const unsigned char* data,
			DataFormat sourceFormat = RGBA,
			TextureType textureType = TEXTURE_SOURCE);

    bool bind(int textureUnit);
    bool unbind(int textureUnit);

    int getWidth() const;
	int getHeight() const;

	bool setFilter(FilterType filterType, FilterValue filterValue);
	bool setFilterMin(FilterValue filterValue);
	bool setFilterMag(FilterValue filterValue);

	bool setWrap(WrapType wrapType, WrapValue wrapValue);
	bool setWrapS(WrapValue wrapValue);
	bool setWrapT(WrapValue wrapValue);
	bool setWrapR(WrapValue wrapValue);

	int getTextureHandle() const;
	TextureType getTextureType() const;

	static int mapFormat(DataFormat format);

private:
	bool checkRange(int x, int y, int width, int height) const;

	GLuint m_texture;
	TextureType m_textureType;
	int m_textureWidth;
	int m_textureHeight;
	DataFormat m_dataFormat;
	StorageType m_storageType;
};

#endif // OPENGLTEXTURE_H
