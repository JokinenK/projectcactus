/*
 * OpenGLModel.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include <vector>
#include <glm/glm.hpp>
#include "OpenGLModel.h"
#include "core/model/IndexedVertexModel.h"

OpenGLModel::OpenGLModel() :
		m_object(0),
		m_numIndices(0)
{
	glGenVertexArrays(1, &m_object);
	glBindVertexArray(m_object);
	glGenBuffers(NUM_BUFFERS, &m_buffers[0]);
	glBindVertexArray(0);
}

OpenGLModel::~OpenGLModel()
{
	glDeleteBuffers(NUM_BUFFERS, &m_buffers[0]);
	glDeleteVertexArrays(1, &m_object);
}

void OpenGLModel::setIndices(const std::vector<unsigned short>& indices)
{
	m_numIndices = indices.size();

	glBindVertexArray(m_object);
	initBuffer(BUFFER_INDICES, GL_ELEMENT_ARRAY_BUFFER, indices);
	glBindVertexArray(0);
}

void OpenGLModel::setPositions(const std::vector<glm::vec3>& positions)
{
	glBindVertexArray(m_object);
	initBuffer(BUFFER_POSITION, GL_ARRAY_BUFFER, positions);
	enableVertexAttributeArray(BUFFER_POSITION, 3);
	glBindVertexArray(0);
}

void OpenGLModel::setTexCoords(const std::vector<glm::vec2>& texCoords)
{
	glBindVertexArray(m_object);
	initBuffer(BUFFER_TEXCOORD, GL_ARRAY_BUFFER, texCoords);
	enableVertexAttributeArray(BUFFER_TEXCOORD, 2);
	glBindVertexArray(0);
}

void OpenGLModel::setNormals(const std::vector<glm::vec3>& normals)
{
	glBindVertexArray(m_object);
	initBuffer(BUFFER_NORMAL, GL_ARRAY_BUFFER, normals);
	enableVertexAttributeArray(BUFFER_NORMAL, 3);
	glBindVertexArray(0);
}

void OpenGLModel::setTangents(const std::vector<glm::vec4>& tangents)
{
	glBindVertexArray(m_object);
	initBuffer(BUFFER_TANGENT, GL_ARRAY_BUFFER, tangents);
	enableVertexAttributeArray(BUFFER_TANGENT, 4);
	glBindVertexArray(0);
}

void OpenGLModel::render()
{
	glBindVertexArray(m_object);
	glDrawElements(GL_TRIANGLES, m_numIndices, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
}

void OpenGLModel::enableVertexAttributeArray(BufferName bufferName, GLuint elementSize)
{
	glEnableVertexAttribArray(bufferName);
	glVertexAttribPointer(bufferName, elementSize, GL_FLOAT, GL_FALSE, 0, 0);
}
