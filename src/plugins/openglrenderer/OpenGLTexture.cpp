#include <GL/glew.h>
#include <iostream>
#include "OpenGLTexture.h"

namespace
{
	// Map the type enumerations to real OpenGL enumerations
	const static GLenum s_textureTypes[ITexture::NUM_TEXTURE_TYPES] = {
		GL_INVALID_ENUM,
		GL_TEXTURE_2D,
		GL_TEXTURE_CUBE_MAP,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
	};

	const static GLenum s_wrapTypes[ITexture::NUM_WRAP_TYPES] = {
		GL_TEXTURE_WRAP_S,
		GL_TEXTURE_WRAP_T,
		GL_TEXTURE_WRAP_R
	};

	const static GLenum s_wrapValues[ITexture::NUM_WRAP_VALUES] = {
		GL_REPEAT,
		GL_CLAMP_TO_EDGE,
		GL_MIRRORED_REPEAT,
		GL_MIRROR_CLAMP_TO_EDGE
	};

	const static GLenum s_filterTypes[ITexture::NUM_FILTER_TYPES] = {
		GL_TEXTURE_MIN_FILTER,
		GL_TEXTURE_MAG_FILTER
	};

	const static GLenum s_filterValues[ITexture::NUM_FILTER_VALUES] = {
		GL_NEAREST,
		GL_LINEAR,
		GL_NEAREST_MIPMAP_NEAREST,
		GL_LINEAR_MIPMAP_NEAREST,
		GL_NEAREST_MIPMAP_LINEAR,
		GL_LINEAR_MIPMAP_LINEAR
	};

	const static GLenum s_dataFormats[ITexture::NUM_DATA_FORMATS] = {
		GL_RGB,
		GL_RGB16F,
		GL_RGB32F,
		GL_RGBA,
		GL_RGBA16F,
		GL_RGBA32F,
		GL_RED,
		GL_RG,
		GL_LUMINANCE_ALPHA,
		GL_DEPTH_COMPONENT,
		GL_DEPTH_COMPONENT16,
		GL_DEPTH_COMPONENT24,
		GL_DEPTH_COMPONENT32,
		GL_DEPTH_COMPONENT32F,
		GL_DEPTH_STENCIL,
		GL_DEPTH24_STENCIL8
	};

	const static GLenum s_storageTypes[ITexture::NUM_DATA_TYPES] = {
		GL_BYTE,             /* BYTE,           */
		GL_UNSIGNED_BYTE,    /* UNSIGNED_BYTE,  */
		GL_SHORT,            /* SHORT,          */
		GL_UNSIGNED_SHORT,   /* UNSIGNED_SHORT, */
		GL_INT,              /* INT,            */
		GL_UNSIGNED_INT,     /* UNSIGNED_INT,   */
		GL_FLOAT,            /* FLOAT,          */
		GL_2_BYTES,          /* TWO_BYTES,      */
		GL_3_BYTES,          /* THREE_BYTES,    */
		GL_4_BYTES,          /* FOUR_BYTES,     */
		GL_DOUBLE,           /* DOUBLE,         */
		GL_UNSIGNED_INT_24_8 /* UNSIGNED_INT_24_8 */
	};
}

OpenGLTexture::OpenGLTexture() :
		m_texture(0),
		m_textureType(TEXTURE_SOURCE),
		m_textureWidth(0),
		m_textureHeight(0),
		m_dataFormat(RGBA),
		m_storageType(UNSIGNED_BYTE)
{
	glGenTextures(1, &m_texture);
}

OpenGLTexture::~OpenGLTexture()
{
	glDeleteTextures(1, &m_texture);
}

bool OpenGLTexture::initTexture(
		int textureWidth,
		int textureHeight,
		StorageType storageType,
		TextureType textureType)
{
	m_textureWidth = textureWidth;
	m_textureHeight = textureHeight;
	m_textureType = textureType;
	m_storageType = storageType;

	glBindTexture(s_textureTypes[textureType], m_texture);
	return true;
}

bool OpenGLTexture::setTextureData(
		const unsigned char* textureData,
		DataFormat sourceFormat,
		DataFormat targetFormat,
		TextureType textureType)
{
	glBindTexture(s_textureTypes[m_textureType], m_texture);

	if (textureType == TEXTURE_SOURCE) {
		textureType = m_textureType;
	}

	glTexImage2D(
			s_textureTypes[textureType],
			0,
			s_dataFormats[targetFormat],
			m_textureWidth,
			m_textureHeight,
			0,
			s_dataFormats[sourceFormat],
			s_storageTypes[m_storageType],
			textureData);

	return true;
}

bool OpenGLTexture::updateTextureData(
		int x,
		int y,
		int width,
		int height,
     	const unsigned char* data,
		DataFormat sourceFormat,
		TextureType textureType)
{
	if (!checkRange(x, y, width, height)) {
		std::cerr << "Sub image is out of range" << std::endl;
		return false;
	}

	glBindTexture(s_textureTypes[m_textureType], m_texture);

	if (textureType == TEXTURE_SOURCE) {
		textureType = m_textureType;
	}

	glTexSubImage2D(
			s_textureTypes[textureType],
			0,
			x,
			y,
			width,
			height,
			s_dataFormats[sourceFormat],
			s_storageTypes[m_storageType],
			data);

	return true;
}

bool OpenGLTexture::bind(int textureUnit)
{
    if (textureUnit < 0 || textureUnit > 31) {
    	std::cerr << "Bind failed, texture unit: " << textureUnit << " is out of range" << std::endl;
    	return false;
    }

    glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(s_textureTypes[m_textureType], m_texture);

	return true;
}

bool OpenGLTexture::unbind(int textureUnit)
{
    if (textureUnit < 0 || textureUnit > 31) {
    	std::cerr << "Unbind failed, texture unit: " << textureUnit << " is out of range" << std::endl;
    	return false;
    }

    glActiveTexture(GL_TEXTURE0 + textureUnit);
    glBindTexture(s_textureTypes[m_textureType], 0);

	return true;
}

int OpenGLTexture::getWidth() const
{
	return m_textureWidth;
}

int OpenGLTexture::getHeight() const
{
	return m_textureHeight;
}

bool OpenGLTexture::setFilter(FilterType filterType, FilterValue filterValue)
{
	glBindTexture(s_textureTypes[m_textureType], m_texture);

	bool success = false;
    GLuint filter = s_filterTypes[filterType];
    GLuint value = s_filterValues[filterValue];

    if (filter != GL_INVALID_ENUM && value != GL_INVALID_ENUM) {
        glTexParameterf(s_textureTypes[m_textureType], filter, value);
        success = true;
    }
    else {
    	std::cerr << "Unable to set filter, invalid values" << std::endl;
    }

    return success;
}

bool OpenGLTexture::setFilterMin(FilterValue filterValue)
{
	return setFilter(TEXTURE_MIN_FILTER, filterValue);
}

bool OpenGLTexture::setFilterMag(FilterValue filterValue)
{
	return setFilter(TEXTURE_MAG_FILTER, filterValue);
}

bool OpenGLTexture::setWrap(WrapType wrapType, WrapValue wrapValue)
{
	glBindTexture(s_textureTypes[m_textureType], m_texture);

	bool success = false;
    GLuint type = s_wrapTypes[wrapType];
    GLfloat value = s_wrapValues[wrapValue];

    if (type != GL_INVALID_ENUM && value != GL_INVALID_ENUM) {
        glTexParameterf(s_textureTypes[m_textureType], type, value);
        success = true;
    }
    else {
    	std::cerr << "Unable to set wrap, invalid values" << std::endl;
    }

    return success;
}

bool OpenGLTexture::setWrapS(WrapValue wrapValue)
{
	return setWrap(TEXTURE_WRAP_S, wrapValue);
}

bool OpenGLTexture::setWrapT(WrapValue wrapValue)
{
	return setWrap(TEXTURE_WRAP_T, wrapValue);
}

bool OpenGLTexture::setWrapR(WrapValue wrapValue)
{
	return setWrap(TEXTURE_WRAP_R, wrapValue);
}

int OpenGLTexture::getTextureHandle() const
{
	return m_texture;
}

ITexture::TextureType OpenGLTexture::getTextureType() const
{
	return m_textureType;
}

int OpenGLTexture::mapFormat(DataFormat format)
{
	return s_dataFormats[format];
}

bool OpenGLTexture::checkRange(int x, int y, int width, int height) const
{
	if ((x + width) < m_textureWidth || (y + height) < m_textureHeight) {
		return false;
	}

	return true;
}
