#ifndef OPENGLRENDERER_H
#define OPENGLRENDERER_H

#include <vector>
#include "core/IModel.h"
#include "core/IRenderer.h"
#include "core/PluginExports.h"

class OpenGLRenderer : public IRenderer
{
public:
    OpenGLRenderer();
    virtual ~OpenGLRenderer();

    void clear();
    void setClearColor(float r, float g, float b);

    void setBlendFunc(BlendFunc blendFuncSrc, BlendFunc blendFuncDst);
    BlendFunc getBlendFuncSrc() const;
    BlendFunc getBlendFuncDst() const;

    void setBlendEquation(BlendEquation blendEquation);
    BlendEquation getBlendEquation() const;

    void setDepthFunc(FuncType depthFunc);
    FuncType getDepthFunc() const;

    void setAlphaFunc(FuncType alphaFunc, float value);
    FuncType getAlphaFunc() const;

    void setCullFace(CullFace cullFace);
    CullFace getCullFace() const;

    void setScissorRectangle(float left, float top, float width, float height);
	void getScissorRectangle(float& left, float& top, float& width, float& height) const;

    void setDepthMask(bool enabled);

    bool isDepthTestEnabled() const;
    void setDepthTestEnabled(bool enabled);

    bool isDepthClampEnabled() const;
	void setDepthClampEnabled(bool enabled);

    bool isBlendEnabled() const;
    void setBlendEnabled(bool enabled);

    bool isAlphaTestEnabled() const;
    void setAlphaTestEnabled(bool enabled);

    bool isScissorTestEnabled() const;
    void setScissorTestEnabled(bool enabled);

    bool isCullFaceEnabled() const;
    void setCullFaceEnabled(bool enabled);

    IShader* createShader() const;
    void releaseShader(IShader* shader);

    ITexture* createTexture() const;
    void releaseTexture(ITexture* texture);

    IModel* createModel() const;
    void releaseModel(IModel* model);

    IFramebuffer* createFramebuffer() const;
    void releaseFramebuffer(IFramebuffer* framebuffer);

	void setColor(unsigned int rgb);
	unsigned int getColor() const;

    void setViewPort(int width, int height);
    void getViewPort(int& width, int& height) const;

private:
    void createEnumerationMappings();
    void addBlendFunc(BlendFunc blendFunc, GLenum glEnum);
    void addFuncType(FuncType funcType, GLenum glEnum);
    void addBlendEquation(BlendEquation blendEquation, GLenum glEnum);
    void addCullFace(CullFace cullFace, GLenum glEnum);

	int m_width;
	int m_height;

    bool m_depthMask;
    bool m_depthTestEnabled;
    bool m_depthClampEnabled;
    bool m_blendEnabled;
    bool m_alphaTestEnabled;
    bool m_scissorTestEnabled;
    bool m_cullFaceEnabled;

    struct {
    	float left;
    	float top;
    	float width;
    	float height;
    } m_scissorRectangle;

    struct {
    	int width;
    	int height;
    } m_viewPort;

	BlendFunc m_blendFuncSrc;
	BlendFunc m_blendFuncDst;
	BlendEquation m_blendEquation;
	FuncType m_depthFunc;
	FuncType m_alphaFunc;
	CullFace m_cullFace;

	std::vector<GLenum> m_blendFuncToNative;
	std::vector<GLenum> m_funcTypeToNative;
	std::vector<GLenum> m_blendEquationToNative;
	std::vector<GLenum> m_cullFaceToNative;
};

extern PLUGIN_API IRenderer* PLUGIN_CreateInstance();
extern PLUGIN_API void PLUGIN_ReleaseInstance(IRenderer* instance);
extern PLUGIN_API int PLUGIN_GetVersion();
extern PLUGIN_API const char* PLUGIN_GetName();

#endif // OPENGLRENDERER_H

