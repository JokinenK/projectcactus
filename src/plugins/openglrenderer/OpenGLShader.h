#ifndef OPENGLSHADER_H_
#define OPENGLSHADER_H_

#include <list>
#include <map>
#include "OpenGLUniform.h"
#include "core/IShader.h"

class IUniform;

class OpenGLShader : public IShader
{
public:
    OpenGLShader();
    virtual ~OpenGLShader();

    virtual void bind();
    virtual void unbind();

    virtual bool addVertexShader(const std::string& shaderSource);
    virtual bool addFragmentShader(const std::string& shaderSource);
    virtual bool addGeometryShader(const std::string& shaderSource);

    virtual bool compileShader();

    virtual void setAttribLocation(const std::string& attribName, int location);
    virtual IUniform* getUniform(const std::string& uniformName);

private:
    bool addShaderProgram(GLenum shaderType, const std::string& shaderSource);
	bool validateShader(GLuint shaderProgram, GLenum flag, bool isProgram, std::string* errorMessage = 0);

	GLuint m_program;
	std::list<GLuint> m_shaderPrograms;
	std::map<std::string, OpenGLUniform> m_shaderUniforms;
};

#endif // OPENGLSHADER_H_
