#include <map>
#include <vector>
#include <algorithm>
#include <iostream>
#include <GL/glew.h>
#include "OpenGLRenderer.h"
#include "OpenGLShader.h"
#include "OpenGLTexture.h"
#include "OpenGLFramebuffer.h"
#include "OpenGLModel.h"

OpenGLRenderer::OpenGLRenderer() :
		m_width(0),
		m_height(0),
		m_depthMask(false),
		m_depthTestEnabled(false),
		m_blendEnabled(false),
		m_alphaTestEnabled(false),
		m_cullFaceEnabled(false),
		m_blendFuncSrc(ONE),
		m_blendFuncDst(ONE),
		m_blendEquation(FUNC_ADD),
		m_depthFunc(LESS),
		m_alphaFunc(ALWAYS)
{
	createEnumerationMappings();

	GLenum status = glewInit();

	if (status == GLEW_OK) {
		glEnable(GL_DEPTH_CLAMP);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_CUBE_MAP);
	    glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);
	}
	else {
		std::cerr << "Glew failed to initialize, error: " << status << std::endl;
	}
}

OpenGLRenderer::~OpenGLRenderer()
{
}

void OpenGLRenderer::clear()
{
	// Ask GL to clear screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void OpenGLRenderer::setClearColor(float r, float g, float b)
{
	glClearColor(r, g, b, 1.0f);
}

void OpenGLRenderer::setBlendFunc(BlendFunc blendFuncSrc, BlendFunc blendFuncDst)
{
	m_blendFuncSrc = blendFuncSrc;
	m_blendFuncDst = blendFuncDst;

	glBlendFunc(m_blendFuncToNative[blendFuncSrc], m_blendFuncToNative[blendFuncDst]);
}

IRenderer::BlendFunc OpenGLRenderer::getBlendFuncSrc() const
{
	return m_blendFuncSrc;
}

IRenderer::BlendFunc OpenGLRenderer::getBlendFuncDst() const
{
	return m_blendFuncDst;
}

void OpenGLRenderer::setBlendEquation(BlendEquation blendEquation)
{
	m_blendEquation = blendEquation;

	glBlendEquation(m_blendEquationToNative[blendEquation]);
}

IRenderer::BlendEquation OpenGLRenderer::getBlendEquation() const
{
	return m_blendEquation;
}

void OpenGLRenderer::setDepthFunc(FuncType depthFunc)
{
	m_depthFunc = depthFunc;

	glDepthFunc(m_funcTypeToNative[depthFunc]);
}

IRenderer::FuncType OpenGLRenderer::getDepthFunc() const
{
	return m_depthFunc;
}

void OpenGLRenderer::setAlphaFunc(FuncType alphaFunc, float value)
{
	m_alphaFunc = alphaFunc;

	glAlphaFunc(m_funcTypeToNative[alphaFunc], value);
}

IRenderer::FuncType OpenGLRenderer::getAlphaFunc() const
{
	return m_alphaFunc;
}

void OpenGLRenderer::setCullFace(CullFace cullFace)
{
	m_cullFace = cullFace;

	glCullFace(m_cullFaceToNative[cullFace]);
}

IRenderer::CullFace OpenGLRenderer::getCullFace() const
{
	return m_cullFace;
}

void OpenGLRenderer::setScissorRectangle(float left, float top, float width, float height)
{
	m_scissorRectangle.left   = left;
	m_scissorRectangle.top    = top;
	m_scissorRectangle.width  = width;
	m_scissorRectangle.height = height;

	glScissor(left, top, width, height);
}

void OpenGLRenderer::getScissorRectangle(float& left, float& top, float& width, float& height) const
{
	left   = m_scissorRectangle.left;
	top    = m_scissorRectangle.top;
	width  = m_scissorRectangle.width;
	height = m_scissorRectangle.height;
}

void OpenGLRenderer::setDepthMask(bool enable)
{
    m_depthMask = enable;

	glDepthMask(enable ? GL_TRUE : GL_FALSE);
}

bool OpenGLRenderer::isDepthTestEnabled() const
{
	return m_depthTestEnabled;
}

void OpenGLRenderer::setDepthTestEnabled(bool enable)
{
	m_depthTestEnabled = enable;

	enable ? glEnable(GL_DEPTH_TEST)
		   : glDisable(GL_DEPTH_TEST);
}

bool OpenGLRenderer::isDepthClampEnabled() const
{
	return m_depthClampEnabled;
}

void OpenGLRenderer::setDepthClampEnabled(bool enable)
{
	m_depthClampEnabled = enable;

	enable ? glEnable(GL_DEPTH_CLAMP)
		   : glDisable(GL_DEPTH_CLAMP);
}

bool OpenGLRenderer::isBlendEnabled() const
{
	return m_blendEnabled;
}

void OpenGLRenderer::setBlendEnabled(bool enable)
{
	m_blendEnabled = enable;

	enable ? glEnable(GL_BLEND)
		   : glDisable(GL_BLEND);
}

bool OpenGLRenderer::isAlphaTestEnabled() const
{
	return m_alphaTestEnabled;
}

void OpenGLRenderer::setAlphaTestEnabled(bool enable)
{
	m_alphaTestEnabled = enable;

	enable ? glEnable(GL_ALPHA_TEST)
		   : glDisable(GL_ALPHA_TEST);
}

bool OpenGLRenderer::isScissorTestEnabled() const
{
	return m_scissorTestEnabled;
}

void OpenGLRenderer::setScissorTestEnabled(bool enable)
{
	m_scissorTestEnabled = enable;

	enable ? glEnable(GL_SCISSOR_TEST)
		   : glDisable(GL_SCISSOR_TEST);
}

bool OpenGLRenderer::isCullFaceEnabled() const
{
	return m_cullFaceEnabled;
}

void OpenGLRenderer::setCullFaceEnabled(bool enable)
{
	m_cullFaceEnabled = enable;

	enable ? glEnable(GL_CULL_FACE)
		   : glDisable(GL_CULL_FACE);
}

IShader* OpenGLRenderer::createShader() const
{
	return new OpenGLShader();
}

void OpenGLRenderer::releaseShader(IShader* shader)
{
	delete shader;
}

ITexture* OpenGLRenderer::createTexture() const
{
	return new OpenGLTexture();
}

void OpenGLRenderer::releaseTexture(ITexture* texture)
{
	delete texture;
}

IModel* OpenGLRenderer::createModel() const
{
	return new OpenGLModel();
}

void OpenGLRenderer::releaseModel(IModel* model)
{
	delete model;
}

IFramebuffer* OpenGLRenderer::createFramebuffer() const
{
	return new OpenGLFramebuffer();
}

void OpenGLRenderer::releaseFramebuffer(IFramebuffer* framebuffer)
{
	delete framebuffer;
}

void OpenGLRenderer::setViewPort(int width, int height)
{
	m_viewPort.width = width;
	m_viewPort.height = height;

	glViewport(0, 0, width, height);
}

void OpenGLRenderer::getViewPort(int& width, int& height) const
{
	width = m_viewPort.width;
	height = m_viewPort.height;
}

void OpenGLRenderer::createEnumerationMappings()
{
	m_blendFuncToNative.resize(NUM_BLEND_FUNCS);
	m_blendEquationToNative.resize(NUM_BLEND_EQUATIONS);
	m_funcTypeToNative.resize(NUM_FUNC_TYPES);
	m_cullFaceToNative.resize(NUM_CULL_FACES);

	addBlendFunc(ZERO, GL_ZERO);
	addBlendFunc(ONE, GL_ONE);
	addBlendFunc(SRC_COLOR, GL_SRC_COLOR);
	addBlendFunc(ONE_MINUS_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
	addBlendFunc(DST_COLOR, GL_DST_COLOR);
	addBlendFunc(ONE_MINUS_DST_COLOR, GL_ONE_MINUS_DST_COLOR);
	addBlendFunc(SRC_ALPHA, GL_SRC_ALPHA);
	addBlendFunc(ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	addBlendFunc(DST_ALPHA, GL_DST_ALPHA);
	addBlendFunc(ONE_MINUS_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);
	addBlendFunc(CONSTANT_COLOR, GL_CONSTANT_COLOR);
	addBlendFunc(ONE_MINUS_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR);
	addBlendFunc(CONSTANT_ALPHA, GL_CONSTANT_ALPHA);
	addBlendFunc(ONE_MINUS_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
	addBlendFunc(SRC_ALPHA_SATURATE, GL_SRC_ALPHA_SATURATE);
	addBlendFunc(SRC1_COLOR, GL_SRC1_COLOR);
	addBlendFunc(ONE_MINUS_SRC1_COLOR, GL_ONE_MINUS_SRC1_COLOR);
	addBlendFunc(SRC1_ALPHA, GL_SRC1_ALPHA);
	addBlendFunc(ONE_MINUS_SRC1_ALPHA, GL_ONE_MINUS_SRC1_ALPHA);

	addBlendEquation(FUNC_ADD, GL_FUNC_ADD);

	addFuncType(NEVER, GL_NEVER);
	addFuncType(LESS, GL_LESS);
	addFuncType(EQUAL, GL_EQUAL);
	addFuncType(LEQUAL, GL_LEQUAL);
	addFuncType(GREATER, GL_GREATER);
	addFuncType(NOTEQUAL, GL_NOTEQUAL);
	addFuncType(GEQUAL, GL_GEQUAL);
	addFuncType(ALWAYS, GL_ALWAYS);

	addCullFace(FRONT, GL_FRONT);
	addCullFace(BACK, GL_BACK);
}

void OpenGLRenderer::addBlendFunc(BlendFunc blendFunc, GLenum glEnum)
{
	m_blendFuncToNative[blendFunc] = glEnum;
}

void OpenGLRenderer::addFuncType(FuncType funcType, GLenum glEnum)
{
	m_funcTypeToNative[funcType] = glEnum;
}

void OpenGLRenderer::addBlendEquation(BlendEquation blendEquation, GLenum glEnum)
{
	m_blendEquationToNative[blendEquation] = glEnum;
}

void OpenGLRenderer::addCullFace(CullFace cullFace, GLenum glEnum)
{
	m_cullFaceToNative[cullFace] = glEnum;
}

/**
 * C methods for handling the plugin information and lifespan
 */

IRenderer* PLUGIN_CreateInstance()
{
	return new OpenGLRenderer();
}

void PLUGIN_ReleaseInstance(IRenderer* instance)
{
	delete instance;
}

int PLUGIN_GetVersion()
{
	return 1;
}

const char* PLUGIN_GetName()
{
	return "Cactus OpenGL Renderer";
}
