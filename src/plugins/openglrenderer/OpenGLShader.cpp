#include <string>
#include <iostream>
#include <GL/glew.h>
#include "OpenGLShader.h"
#include "OpenGLUniform.h"

OpenGLShader::OpenGLShader() :
		m_program(0)
{
	m_program = glCreateProgram();
}

OpenGLShader::~OpenGLShader()
{
	for (GLuint shaderProgram : m_shaderPrograms) {
		glDetachShader(m_program, shaderProgram);
		glDeleteShader(shaderProgram);
	}

	glDeleteProgram(m_program);
}

void OpenGLShader::bind()
{
	glUseProgram(m_program);
}

void OpenGLShader::unbind()
{
	glUseProgram(0);
}

bool OpenGLShader::addVertexShader(const std::string& shaderSource)
{
	return addShaderProgram(GL_VERTEX_SHADER, shaderSource);
}

bool OpenGLShader::addFragmentShader(const std::string& shaderSource)
{
	return addShaderProgram(GL_FRAGMENT_SHADER, shaderSource);
}

bool OpenGLShader::addGeometryShader(const std::string& shaderSource)
{
	return addShaderProgram(GL_GEOMETRY_SHADER, shaderSource);
}

bool OpenGLShader::addShaderProgram(GLenum shaderType, const std::string& shaderSource)
{
	bool success = false;
	GLuint shaderProgram = glCreateShader(shaderType);

	if (shaderProgram != 0) {
		const GLchar* sources[] = { shaderSource.c_str() };
		GLint lengths[] = { static_cast<GLint>(shaderSource.length()) };

		glShaderSource(shaderProgram, 1, sources, lengths);
		glCompileShader(shaderProgram);

		std::string errorMessage;
		if (validateShader(shaderProgram, GL_COMPILE_STATUS, false, &errorMessage)) {
			glAttachShader(m_program, shaderProgram);
			m_shaderPrograms.push_back(shaderProgram);
			success = true;
		}
		else {
			std::cerr << "Error compiling shader: " << errorMessage << std::endl;
		}
	}
	else {
		std::cerr << "Error compiling shader type " << shaderType << std::endl;
	}

	return success;
}

bool OpenGLShader::compileShader()
{
	bool success = true;
	std::string errorMessage;

	glLinkProgram(m_program);
	if (!validateShader(m_program, GL_LINK_STATUS, true, &errorMessage)) {
		std::cerr << "Error linking shader program: " << errorMessage << std::endl;
		success = false;
	}

	glValidateProgram(m_program);
	if (!validateShader(m_program, GL_LINK_STATUS, true, &errorMessage)) {
		std::cerr << "Error validating shader program: " << errorMessage << std::endl;
		success = false;
	}

	return success;
}

bool OpenGLShader::validateShader(
		GLuint shaderProgram,
		GLenum flag,
		bool isProgram,
		std::string* errorMessage)
{
	GLint success = 0;
	GLchar error[1024] = { 0 };

	if (isProgram) {
		glGetProgramiv(shaderProgram, flag, &success);
	}
	else {
		glGetShaderiv(shaderProgram, flag, &success);
	}

	if (success == GL_FALSE) {
		if (isProgram) {
			glGetProgramInfoLog(shaderProgram, sizeof(error), NULL, error);
		}
		else {
			glGetShaderInfoLog(shaderProgram, sizeof(error), NULL, error);
		}

		if (errorMessage) {
			*errorMessage = error;
		}
	}

	return success;
}

void OpenGLShader::setAttribLocation(const std::string& attribName, int location)
{
	glBindAttribLocation(m_program, location, attribName.c_str());
}

IUniform* OpenGLShader::getUniform(const std::string& uniformName)
{
	if (m_shaderUniforms.find(uniformName) == m_shaderUniforms.end()) {
		m_shaderUniforms[uniformName] = OpenGLUniform(m_program, uniformName);
	}

	return &m_shaderUniforms[uniformName];
}
