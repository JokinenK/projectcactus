/*
 * OpenGLFramebuffer.cpp
 *
 *  Created on: 24.1.2017
 *      Author: kalle
 */

#include <GL/glew.h>
#include <vector>
#include <iostream>
#include "OpenGLFramebuffer.h"
#include "core/ITexture.h"

namespace
{
	static const GLenum s_attachments[IFramebuffer::NUM_ATTACHMENTS] = {
		GL_NONE,
		GL_COLOR_ATTACHMENT0,
		GL_COLOR_ATTACHMENT1,
		GL_COLOR_ATTACHMENT2,
		GL_COLOR_ATTACHMENT3,
		GL_COLOR_ATTACHMENT4,
		GL_DEPTH_ATTACHMENT,
		GL_STENCIL_ATTACHMENT
	};
}

OpenGLFramebuffer::OpenGLFramebuffer() :
		m_framebuffer(0),
		m_width(0),
		m_height(0)
{
	glGenFramebuffers(1, &m_framebuffer);
}

OpenGLFramebuffer::~OpenGLFramebuffer()
{
	glDeleteFramebuffers(1, &m_framebuffer);
}

void OpenGLFramebuffer::init(int width, int height)
{
	m_width = width;
	m_height = height;
}

int OpenGLFramebuffer::getWidth() const
{
	return m_width;
}

int OpenGLFramebuffer::getHeight() const
{
	return m_height;
}

void OpenGLFramebuffer::bindWriteBuffer()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer);
}

void OpenGLFramebuffer::unbindWriteBuffer()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
}

void OpenGLFramebuffer::bindReadBuffer()
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebuffer);
}

void OpenGLFramebuffer::unbindReadBuffer()
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void OpenGLFramebuffer::clear()
{
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}

void OpenGLFramebuffer::attachRenderTarget(ITexture* texture, Attachment attachment)
{
	bindWriteBuffer();

	if (texture->getTextureType() == ITexture::TEXTURE_CUBE_MAP) {
		glFramebufferTexture(
				GL_FRAMEBUFFER,
				s_attachments[attachment],
				texture->getTextureHandle(),
				0);
	}
	else {
		glFramebufferTexture2D(
				GL_FRAMEBUFFER,
				s_attachments[attachment],
				GL_TEXTURE_2D,
				texture->getTextureHandle(),
				0);
	}

	GLuint status = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);

	if (status != GL_FRAMEBUFFER_COMPLETE) {
		std::cerr << "Trying to attach invalid framebuffer!" << std::endl;
	}

	unbindWriteBuffer();
}

void OpenGLFramebuffer::drawBuffers(Attachment* attachments, int numAttachments)
{
	std::vector<GLenum> parsedAttachments(numAttachments);

	for (int i = 0; i < numAttachments; i++) {
		parsedAttachments[i] = s_attachments[attachments[i]];
	}

	bindWriteBuffer();
	glDrawBuffers(numAttachments, parsedAttachments.data());
	unbindWriteBuffer();
}

void OpenGLFramebuffer::copyColorToActiveFramebuffer()
{
	bindReadBuffer();

	glBlitFramebuffer(
			0,
			0,
			m_width,
			m_height,
			0,
			0,
			m_width,
			m_height,
			GL_COLOR_BUFFER_BIT,
			GL_NEAREST
	);

	unbindReadBuffer();
}

void OpenGLFramebuffer::copyDepthToActiveFramebuffer()
{
	bindReadBuffer();

	glBlitFramebuffer(
			0,
			0,
			m_width,
			m_height,
			0,
			0,
			m_width,
			m_height,
			GL_DEPTH_BUFFER_BIT,
			GL_NEAREST
	);

	unbindReadBuffer();
}
