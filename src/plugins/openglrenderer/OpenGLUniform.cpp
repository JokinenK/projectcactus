/*
 * OpenGLUniform.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include <iostream>
#include "OpenGLUniform.h"

OpenGLUniform::OpenGLUniform() :
		m_handle(0)
{
}

OpenGLUniform::OpenGLUniform(GLuint program, const std::string& uniformName) :
		m_handle(0)
{
    m_handle = glGetUniformLocation(program, uniformName.c_str());

    if (m_handle == -1) {
    	std::cerr << "Unable to find uniform: " << uniformName << std::endl;
    }
}

OpenGLUniform::OpenGLUniform(const OpenGLUniform& other)
{
	operator=(other);
}

OpenGLUniform::~OpenGLUniform()
{
}

void OpenGLUniform::operator=(const OpenGLUniform& other)
{
	m_handle = other.m_handle;
}

void OpenGLUniform::set(int x)
{
	glUniform1i(m_handle, x);
}

void OpenGLUniform::set(int x, int y)
{
	glUniform2i(m_handle, x, y);
}

void OpenGLUniform::set(int x, int y, int z)
{
	glUniform3i(m_handle, x, y, z);
}

void OpenGLUniform::set(int x, int y, int z, int w)
{
	glUniform4i(m_handle, x, y, z, w);
}

void OpenGLUniform::set(float x)
{
	glUniform1f(m_handle, x);
}

void OpenGLUniform::set(float x, float y)
{
	glUniform2f(m_handle, x, y);
}

void OpenGLUniform::set(float x, float y, float z)
{
	glUniform3f(m_handle, x, y, z);
}

void OpenGLUniform::set(float x, float y, float z, float w)
{
	glUniform4f(m_handle, x, y, z, w);
}

void OpenGLUniform::set(const glm::vec2& vec)
{
	glUniform2fv(m_handle, 1, &vec[0]);
}

void OpenGLUniform::set(const glm::vec3& vec)
{
	glUniform3fv(m_handle, 1, &vec[0]);
}

void OpenGLUniform::set(const glm::vec4& vec)
{
	glUniform4fv(m_handle, 1, &vec[0]);
}

void OpenGLUniform::set(const glm::mat4& mat)
{
	glUniformMatrix4fv(m_handle, 1, GL_FALSE, &mat[0][0]);
}

void OpenGLUniform::set(const std::vector<int>& arrInt)
{
	glUniform1iv(m_handle, arrInt.size(), &arrInt[0]);
}

void OpenGLUniform::set(const std::vector<float>& arrFloat)
{
	glUniform1fv(m_handle, arrFloat.size(), &arrFloat[0]);
}

void OpenGLUniform::set(const std::vector<glm::vec2>& arrVec)
{
	glUniform2fv(m_handle, arrVec.size(), &arrVec[0][0]);
}

void OpenGLUniform::set(const std::vector<glm::vec3>& arrVec)
{
	glUniform3fv(m_handle, arrVec.size(), &arrVec[0][0]);
}

void OpenGLUniform::set(const std::vector<glm::vec4>& arrVec)
{
	glUniform4fv(m_handle, arrVec.size(), &arrVec[0][0]);
}

void OpenGLUniform::set(const std::vector<glm::mat4>& arrMat)
{
	glUniformMatrix4fv(m_handle, arrMat.size(), GL_FALSE, &arrMat[0][0][0]);
}
