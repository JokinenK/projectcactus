/*
 * OpenGLModel.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef OPENGLMODEL_H_
#define OPENGLMODEL_H_

#include <GL/glew.h>
#include "core/IModel.h"

class OpenGLModel : public IModel
{
public:
	OpenGLModel();
	virtual ~OpenGLModel();

	virtual void setIndices(const std::vector<unsigned short>& indices);
	virtual void setPositions(const std::vector<glm::vec3>& positions);
	virtual void setTexCoords(const std::vector<glm::vec2>& texCoords);
	virtual void setNormals(const std::vector<glm::vec3>& normals);
	virtual void setTangents(const std::vector<glm::vec4>& tangents);

	virtual void render();

private:
	void enableVertexAttributeArray(BufferName bufferName, GLuint elementSize);

	template <class T>
	void initBuffer(BufferName bufferName, GLenum bufferType, const std::vector<T>& data)
	{
		glBindBuffer(bufferType, m_buffers[bufferName]);
		glBufferData(bufferType, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);
	}

	GLuint m_object;
	size_t m_numIndices;
	GLuint m_buffers[NUM_BUFFERS];
};

#endif /* OPENGLMODEL_H_ */
