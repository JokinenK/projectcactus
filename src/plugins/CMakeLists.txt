cmake_minimum_required(VERSION 2.8.11)

add_subdirectory(openglrenderer)
add_subdirectory(sdlinput)
add_subdirectory(sdldisplay)