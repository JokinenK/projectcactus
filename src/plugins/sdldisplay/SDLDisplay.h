#ifndef SDLDISPLAY_H_
#define SDLDISPLAY_H_

#include <string>
#include "core/IDisplay.h"
#include "core/PluginExports.h"
#include "core/EventListener.h"

class SDLDisplay : public IDisplay
{
public:
	SDLDisplay();
	virtual ~SDLDisplay();

	void init(const std::string& title, short width, short height);
	short getWidth() const;
	short getHeight() const;
    float aspectRatio() const;
    bool isClosed() const;

    void setFullscreen(bool fullscreen);
    void captureMouse(bool visible);

    void swapBuffers();

private:
    bool m_closed;
	short m_width;
	short m_height;
	SDL_Window* m_window;
	SDL_GLContext m_context;
};

extern PLUGIN_API IDisplay* PLUGIN_CreateInstance();
extern PLUGIN_API void PLUGIN_ReleaseInstance(IDisplay* instance);
extern PLUGIN_API int PLUGIN_GetVersion();
extern PLUGIN_API const char* PLUGIN_GetName();


#endif // SDLDISPLAY_H_
