#include <SDL.h>
#include <iostream>
#include "SDLDisplay.h"
#include "core/MessageBus.h"

SDLDisplay::SDLDisplay() :
		m_closed(false),
		m_width(0),
		m_height(0),
		m_window(0),
		m_context(0)
{
}

SDLDisplay::~SDLDisplay()
{
	// Release context
	SDL_GL_DeleteContext(m_context);

	// Destroy created window.
	SDL_DestroyWindow(m_window);

	// Deinitialize SDL
	SDL_Quit();
}

void SDLDisplay::init(const std::string& title, short width, short height)
{
	m_width = width;
	m_height = height;
	m_closed = false;

	// Init SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	// Set attributes (8 bits per color, 32 total)
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);

	// Depth and stencil share the 32 bits total
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	// Use double buffering by default
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	// Create window
	m_window = SDL_CreateWindow(
		title.c_str(),
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		width,
		height,
		SDL_WINDOW_OPENGL);

	// Create GL context
	m_context = SDL_GL_CreateContext(m_window);
}

void SDLDisplay::setFullscreen(bool fullscreen)
{
	SDL_SetWindowFullscreen(m_window, fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
}

void SDLDisplay::captureMouse(bool capture)
{
	SDL_SetRelativeMouseMode(capture ? SDL_TRUE : SDL_FALSE);
}

void SDLDisplay::swapBuffers()
{
	// Swap buffers (double buffering)
	SDL_GL_SwapWindow(m_window);

	// Store event here
	SDL_Event evt;

	// Update events, look for quit event
	SDL_PumpEvents();
	SDL_PeepEvents(&evt, 1, SDL_PEEKEVENT, SDL_QUIT, SDL_QUIT);

	if (evt.type == SDL_QUIT) {
		// Quit requested
		m_closed = true;
	}
}

short SDLDisplay::getWidth() const
{
	return m_width;
}

short SDLDisplay::getHeight() const
{
	// Return height
	return m_height;
}

float SDLDisplay::aspectRatio() const
{
	// Return calculated aspect ratio
	return static_cast<float>(m_width) / static_cast<float>(m_height);
}

bool SDLDisplay::isClosed() const
{
	// Return boolean
	return m_closed;
}

IDisplay* PLUGIN_CreateInstance()
{
	return new SDLDisplay();
}

void PLUGIN_ReleaseInstance(IDisplay* instance)
{
	delete instance;
}

int PLUGIN_GetVersion()
{
	return 1;
}

const char* PLUGIN_GetName()
{
	return "Cactus SDL Display";
}
