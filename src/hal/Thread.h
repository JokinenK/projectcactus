/*
 * Thread.h
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#ifndef HAL_THREAD_H_
#define HAL_THREAD_H_

#include "hal/IRunnable.h"

namespace Hal
{

class Thread
{
public:
	Thread(IRunnable* runnable);
	~Thread();

	bool start();
	bool join();
	bool detach();

private:
	struct ThreadData;
	ThreadData* m_data;
};

}

#endif /* HAL_THREAD_H_ */
