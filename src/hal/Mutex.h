/*
 * Mutex.h
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#ifndef HAL_MUTEX_H_
#define HAL_MUTEX_H_

namespace Hal {

class Mutex {
public:
	Mutex();
	virtual ~Mutex();

	bool lock();
	bool tryLock();
	bool unlock();

private:
	struct MutexData;
	MutexData* m_data;
};

}

#endif /* HAL_MUTEX_H_ */
