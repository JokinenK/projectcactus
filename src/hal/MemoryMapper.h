/*
 * MemoryMap.h
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#ifndef HAL_MEMORYMAPPER_H_
#define HAL_MEMORYMAPPER_H_

#include <string>

namespace Hal
{

class MemoryMapper
{
public:
	typedef enum {
		MEMORYMAP_READONLY,
		MEMORYMAP_READWRITE,
		NUM_ATTRIBUTES
	} Attributes;

	static void* mapFile(const std::string& fileName, size_t& mapSize, Attributes attrs = MEMORYMAP_READONLY);
	static bool unmapFile(void* mappedData);
};

} // namespace

#endif /* HAL_MEMORYMAPPER_H_ */
