/*
 * File.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef HAL_FILE_H_
#define HAL_FILE_H_

#include <string>
#include <vector>

namespace Hal
{

class File
{
public:
	struct DirectoryEntry
	{
		std::string name;
		unsigned long long created;
		unsigned long long modified;
		unsigned long long accessed;
		size_t size;
	};

	struct DirectoryInfo
	{
		std::vector<DirectoryEntry> files;
		std::vector<DirectoryEntry> directories;
	};

	static std::string absolutePath(const std::string& relativePath);
	static std::string directoryName(const std::string& filePath);
	static std::string fileName(const std::string& filePath);
	static std::string fileExtension(const std::string& filePath);

	static bool fileExists(const std::string& filePath);
	static const std::string& pathSeparator();
	static const std::string& lineSeparator();

	static DirectoryInfo readDirectory(
			const std::string& directory,
			const std::string& extension = std::string());

	static std::string resolveFont(const std::string& fontName);
	static std::string defaultFont();

	static std::string homeDir();
	static std::string executableDir();
	static std::string currentDir();
};

}



#endif /* HAL_FILE_H_ */
