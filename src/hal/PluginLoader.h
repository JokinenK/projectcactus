/*
 * Plugin.h
 *
 *  Created on: 7.12.2016
 *      Author: kalle
 */

#ifndef PLUGINLOADER_H_
#define PLUGINLOADER_H_

#include <string>

namespace Hal
{

class PluginLoader
{
public:
	typedef enum {
		LOADFLAG_NONE,
		LOADFLAG_LAZYLOAD,
		NUM_LOADFLAGS
	} LoadFlags;

	PluginLoader(const std::string& fileName);
	virtual ~PluginLoader();

	void setLoadFlags(LoadFlags loadFlags);
	bool load();
	bool unload();
	void* symbol(const std::string& name);
	std::string error();

private:
	struct PluginData;
	PluginData* m_data;
};

}

#endif /* PLUGINLOADER_H_ */
