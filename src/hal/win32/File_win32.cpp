/*
 * File_win32.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include <Windows.h>
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <direct.h>
#include "hal/File.h"
#include "hal/win32/Common_win32.h"

namespace Hal
{

std::string File::absolutePath(const std::string& relativePath)
{
	TCHAR pathBuffer[MAX_PATH];
	GetFullPathName(TEXT(relativePath.c_str()), MAX_PATH, pathBuffer, NULL);
	return std::string(pathBuffer);
}

std::string File::directoryName(const std::string& filePath)
{
	 size_t lastSeparator = filePath.find_last_of("\\/");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(0, lastSeparator);
	 }

	 return std::string();
}

std::string File::fileName(const std::string& filePath)
{
	 size_t lastSeparator = filePath.find_last_of("\\/");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(lastSeparator + 1);
	 }

	 return filePath;
}

std::string File::fileExtension(const std::string& filePath)
{
	 size_t lastSeparator = filePath.find_last_of(".");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(lastSeparator + 1);
	 }

	 return std::string();
}

bool File::fileExists(const std::string& filePath)
{
	if(GetFileAttributes(filePath.c_str()) == INVALID_FILE_ATTRIBUTES
			&& GetLastError() == ERROR_FILE_NOT_FOUND) {

	    return false;
	}

	return true;
}

const std::string& File::pathSeparator()
{
	static std::string pathSeparator("\\");
	return pathSeparator;
}

const std::string& File::lineSeparator()
{
	static std::string lineSeparator("\r\n");
	return lineSeparator;
}

File::DirectoryInfo File::readDirectory(const std::string& directory, const std::string& extension)
{
	DirectoryInfo directoryInfo;

	std::stringstream pathBuilder;
	pathBuilder << absolutePath(directory);
	pathBuilder << pathSeparator();

	if (extension.empty()) {
		pathBuilder << "*";
	}
	else {
		pathBuilder << extension;
	}

	WIN32_FIND_DATA fileData;
	HANDLE handle = FindFirstFile(pathBuilder.str().c_str(), &fileData);

	if (handle != INVALID_HANDLE_VALUE) {
		do {
			DirectoryEntry entry;

			if (fileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) {
				continue;
			}

			entry.name = fileData.cFileName;

			if (entry.name.compare(".") == 0) {
				continue;
			}

			LARGE_INTEGER filesize;
			filesize.LowPart = fileData.nFileSizeLow;
			filesize.HighPart = fileData.nFileSizeHigh;

			entry.created = Win32Common::fileTimeToMillis(fileData.ftCreationTime);
			entry.modified = Win32Common::fileTimeToMillis(fileData.ftLastWriteTime);
			entry.accessed = Win32Common::fileTimeToMillis(fileData.ftLastAccessTime);
			entry.size = filesize.QuadPart;

			if (fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
				directoryInfo.directories.push_back(entry);
			}
			else {
				directoryInfo.files.push_back(entry);
			}
		} while (FindNextFile(handle, &fileData));

		FindClose(handle);
	}

	return directoryInfo;
}

std::string File::resolveFont(const std::string& fontName)
{
	std::stringstream path;
	path << getenv("WINDIR");
	path << pathSeparator();
	path << "Fonts";
	path << pathSeparator();
	path << fontName;
	path << ".ttf";

	return absolutePath(path.str());
}

std::string File::homeDir()
{
	return absolutePath(getenv("USERPROFILE"));
}

std::string File::executableDir()
{
	char buffer[MAX_PATH];
	memset(buffer, 0, MAX_PATH);

	if (GetModuleFileName(NULL, buffer, MAX_PATH) == 0) {
	    return std::string();
	}

	return directoryName(buffer);
}

std::string File::currentDir()
{
	char buffer[MAX_PATH];
	memset(buffer, 0, MAX_PATH);

	if (_getcwd(buffer, MAX_PATH) == NULL) {
		return std::string();
	}

	return directoryName(buffer);
}

std::string File::defaultFont()
{
	return std::string("arial");
}

}
