/*
 * PluginLoader_win32.cpp
 *
 *  Created on: 7.12.2016
 *      Author: kalle
 */

#include <windows.h>
#include <iostream>
#include "hal/PluginLoader.h"

namespace Hal
{

struct PluginLoader::PluginData
{
	HINSTANCE handle;
	int loadFlags;
	std::string fileName;
};

PluginLoader::PluginLoader(const std::string& fileName) :
		m_data(new PluginData)
{
	m_data->handle = 0;
	m_data->loadFlags = 0;
	m_data->fileName = fileName;
}

PluginLoader::~PluginLoader()
{
	unload();
	delete m_data;
}

void PluginLoader::setLoadFlags(LoadFlags loadFlags)
{
	(void)loadFlags;
	std::cout << "Plugin::setLoadFlags not implemented in win32" << std::endl;
}

bool PluginLoader::load()
{
	m_data->handle = LoadLibrary(m_data->fileName.c_str());
	return (m_data->handle != NULL);
}

bool PluginLoader::unload()
{
	if (m_data->handle && FreeLibrary(m_data->handle)) {
		m_data->handle = NULL;
	}

	return (m_data->handle == NULL);
}

void* PluginLoader::symbol(const std::string& name)
{
	void* symbol = (void*)(GetProcAddress(m_data->handle, name.c_str()));

	if (!symbol) {
		std::cerr << error() << std::endl;
	}

	return symbol;
}

std::string PluginLoader::error()
{
	std::string errorMessage;
    DWORD errorMessageId = GetLastError();

    if (errorMessageId != 0) {
		LPSTR errorBuffer = nullptr;

		size_t size = FormatMessageA(
				FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				errorMessageId,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPSTR) &errorBuffer,
				0,
				NULL);

		errorMessage = std::string(errorBuffer, size);
		LocalFree(errorBuffer);
    }

    return errorMessage;
}

}
