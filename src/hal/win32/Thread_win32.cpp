/*
 * Thread_win32.cpp
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#include <Windows.h>
#include "hal/Thread.h"

namespace Hal
{

static DWORD WINAPI runThread(void* object)
{
    IRunnable* runnable = static_cast<IRunnable*>(object);
    runnable->run();
    return 0;
}

struct Thread::ThreadData
{
	IRunnable* runnable;
	bool joined;
	bool detached;
	HANDLE thread;
	LPDWORD threadId;
};

Thread::Thread(IRunnable* runnable) :
    m_data(new ThreadData)
{
	m_data->runnable = runnable;
	m_data->joined = false;
	m_data->detached = false;
	m_data->thread = 0;
	m_data->threadId = 0;
}

Thread::~Thread()
{
    if (!m_data->detached) {
        join();
        CloseHandle(m_data->thread);
    }

    delete m_data;
}

bool Thread::start()
{
    m_data->thread = CreateThread(
    		0,
			0,
			runThread,
			m_data->runnable,
			0,
			m_data->threadId);

    return (m_data->thread != 0);
}

bool Thread::join()
{
    bool success = false;

    if (!m_data->joined && !m_data->detached) {
        m_data->joined = true;
        success = (WaitForSingleObject(m_data->thread, INFINITE) == 0);
    }

    return success;
}

bool Thread::detach()
{
    bool success = false;

    if (!m_data->joined && !m_data->detached) {
        m_data->detached = true;
        success = (CloseHandle(m_data->thread) != 0);
    }

    return success;
}

}
