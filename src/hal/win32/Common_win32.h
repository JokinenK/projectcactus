/*
 * Common_win32.h
 *
 *  Created on: 28.2.2017
 *      Author: kalle
 */

#ifndef COMMON_WIN32_H_
#define COMMON_WIN32_H_

namespace Hal
{

namespace Win32Common
{
	static const unsigned long long FILETIME_OFFSET_MS = 11644473600000LL;

	static unsigned long long fileTimeToMillis(const FILETIME& ft)
	{
		unsigned long long time =
					((LONGLONG)(ft.dwLowDateTime)) +
					((LONGLONG)(ft.dwHighDateTime) << 32LL);

		return (time * 1e-4) - FILETIME_OFFSET_MS;
	}
}

}

#endif /* COMMON_WIN32_H_ */
