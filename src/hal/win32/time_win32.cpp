/*
 * time_win32.cpp
 *
 *  Created on: 11.9.2016
 *      Author: kalle
 */

#include <Windows.h>
#include "hal/Time.h"
#include "hal/win32/Common_win32.h"

namespace Hal
{

unsigned long long Time::currentTimeMillis()
{
	FILETIME ft;
    GetSystemTimeAsFileTime(&ft);

    return Win32Common::fileTimeToMillis(ft);
}

}

