/*
 * Mutex.cpp
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#include <Windows.h>
#include "hal/Mutex.h"

namespace Hal {

struct Mutex::MutexData
{
	LONG counter;
	CRITICAL_SECTION criticalSection;
};

Mutex::Mutex() : m_data(new MutexData)
{
	m_data->counter = 0;
	InitializeCriticalSection(&m_data->criticalSection);
}

Mutex::~Mutex()
{
	DeleteCriticalSection(&m_data->criticalSection);
	delete m_data;
}

bool Mutex::lock()
{
	EnterCriticalSection(&m_data->criticalSection);
	m_data->counter++;
	return true;
}

bool Mutex::tryLock()
{
	long result = TryEnterCriticalSection(&m_data->criticalSection);

	if(result) {
		m_data->counter++;
	}

	return result;
}

bool Mutex::unlock()
{
	LeaveCriticalSection(&m_data->criticalSection);
    return true;
}

} /* namespace Hal */
