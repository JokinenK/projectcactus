/*
 * IRunnable.h
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#ifndef IRUNNABLE_H_
#define IRUNNABLE_H_

namespace Hal
{

class IRunnable
{
public:
	virtual ~IRunnable() {};
	virtual void run() = 0;
};

}

#endif /* IRUNNABLE_H_ */
