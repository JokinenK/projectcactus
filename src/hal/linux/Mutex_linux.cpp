/*
 * Mutex.cpp
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#include <pthread.h>
#include "hal/Mutex.h"

namespace Hal {

class Mutex::MutexData
{
public:
	pthread_mutex_t mutex;
};

Mutex::Mutex() :
		m_data(new MutexData)
{
	pthread_mutex_init(&m_data->mutex, 0);
}

Mutex::~Mutex()
{
	pthread_mutex_destroy(&m_data->mutex);
	delete m_data;
}

bool Mutex::lock()
{
	return (pthread_mutex_lock(&m_data->mutex) == 0);
}

bool Mutex::tryLock()
{
	return (pthread_mutex_trylock(&m_data->mutex) == 0);
}

bool Mutex::unlock()
{
	return (pthread_mutex_unlock(&m_data->mutex) == 0);
}

} /* namespace Hal */
