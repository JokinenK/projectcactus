/*
 * time_linux.cpp
 *
 *  Created on: 11.9.2016
 *      Author: kalle
 */

#include <sys/time.h>
#include "hal/Time.h"

namespace Hal
{

unsigned long long Time::currentTimeMillis()
{
    struct timeval tv;
    gettimeofday(&tv, 0);

    return ((unsigned long long)tv.tv_sec * 1000 +
    		(unsigned long long) tv.tv_usec / 1000);
}

}

