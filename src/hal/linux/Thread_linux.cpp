/*
 * thread_win32.cpp
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#include <pthread.h>
#include "hal/Thread.h"

namespace Hal
{

static void* runThread(void* object)
{
    IRunnable* runnable = static_cast<IRunnable*>(object);
    runnable->run();
    return 0;
}

struct Thread::ThreadData
{
	IRunnable* runnable;
	bool joined;
	bool detached;
	pthread_t thread;
};

Thread::Thread(IRunnable* runnable) :
		m_data(new ThreadData)
{
    m_data->runnable = runnable;
    m_data->joined = false;
    m_data->detached = false;
}

Thread::~Thread()
{
    if (!m_data->detached) {
        join();
    }

    delete m_data;
}

bool Thread::start()
{
    return (pthread_create(&m_data->thread, 0, runThread, m_data->runnable) == 0);
}

bool Thread::join()
{
    bool success = false;

    if (!m_data->joined && !m_data->detached) {
        m_data->joined = true;
        success = (pthread_join(m_data->thread, 0) == 0);
    }

    return success;
}

bool Thread::detach()
{
    bool success = false;
    if (!m_data->joined && !m_data->detached) {
        m_data->detached = true;
        success = (pthread_detach(m_data->thread) == 0);
    }

    return success;
}

}
