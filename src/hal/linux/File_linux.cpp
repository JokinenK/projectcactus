/*
 * File_linux.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include <limits.h>
#include <stdlib.h>
#include <sstream>
#include <dirent.h>
#include <unistd.h>
//#include <sys/types.h>
#include <time.h>
#include <sys/stat.h>
#include <pwd.h>
#include <linux/limits.h>
#include "hal/File.h"

namespace Hal
{

static bool isHidden(const std::string& fileName)
{
	if (fileName.compare("..") == 0) {
		return false;
	}

	if (fileName.at(0) != '.') {
		return false;
	}

	return true;
}

static bool extensionMatches(const std::string& fileName, const std::string& extension)
{
	if (extension.empty()) {
		return true;
	}

	auto iter = fileName.find(extension, fileName.length() - extension.length());

	if (iter != std::string::npos) {
		return true;
	}

	return false;
}

std::string File::absolutePath(const std::string& relativePath)
{
	char* pathBuffer = realpath(relativePath.c_str(), NULL);
	std::string absolutePath = std::string(pathBuffer);
	free(pathBuffer);

	return absolutePath;
}

std::string File::directoryName(const std::string& filePath)
{
	 size_t lastSeparator = filePath.find_last_of("\\/");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(0, lastSeparator);
	 }

	 return std::string();
}

std::string File::fileName(const std::string& filePath)
{
	 size_t lastSeparator = filePath.find_last_of("\\/");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(lastSeparator + 1);
	 }

	 return filePath;
}

std::string File::fileExtension(const std::string& filePath)
{
	 size_t lastSeparator = fileName(filePath).find_last_of(".");

	 if (lastSeparator != std::string::npos) {
		 return filePath.substr(lastSeparator + 1);
	 }

	 return std::string();
}


bool File::fileExists(const std::string& filePath) {
	struct stat buffer;
	return (stat (filePath.c_str(), &buffer) == 0);
}

const std::string& File::pathSeparator()
{
	static std::string pathSeparator("/");
	return pathSeparator;
}

File::DirectoryInfo File::readDirectory(const std::string& directory, const std::string& extension)
{
	DirectoryInfo directoryInfo;

	struct dirent* fileData;
	DIR* handle = opendir(absolutePath(directory).c_str());

	if (handle != NULL) {
		struct stat fileStat;

		while ((fileData = readdir(handle)) != NULL) {
			DirectoryEntry entry;

			entry.name = fileData->d_name;

			if (isHidden(entry.name)) {
				continue;
			}

			stat(fileData->d_name, &fileStat);
			entry.created = fileStat.st_ctime * 1000;
			entry.modified = fileStat.st_mtime * 1000;
			entry.accessed = fileStat.st_atime * 1000;
			entry.size = fileStat.st_size;

			if (S_ISDIR(fileStat.st_mode)) {
				directoryInfo.directories.push_back(entry);
			}
			else if (extensionMatches(entry.name, extension) && S_ISREG(fileStat.st_mode)) {
				directoryInfo.files.push_back(entry);
			}
		}

		closedir(handle);
	}

	return directoryInfo;
}

std::string File::resolveFont(const std::string& fontName)
{
	std::stringstream path;
	path << "/usr/share/fonts";
	path << pathSeparator();
	path << fontName;
	path << ".ttf";

	return absolutePath(path.str());
}

std::string File::defaultFont()
{
	return std::string("tahoma");
}

std::string File::homeDir()
{
	struct passwd* pw = getpwuid(getuid());
	return absolutePath(pw->pw_dir);
}

std::string File::executableDir()
{
	char buffer[MAX_PATH];
	memset(buffer, 0, MAX_PATH);

	char procPath[32];
	sprintf(procPath, "/proc/%d/exe", getpid());

	if (readlink(procPath, buffer, MAX_PATH) == 0) {
		return std::string();
	}

	return directoryName(buffer);
}

std::string File::currentDir()
{
	char buffer[MAX_PATH];
	memset(buffer, 0, MAX_PATH);

	if (getcwd(buffer, FILENAME_MAX) == NULL) {
		return std::string();
	}

	return directoryName(buffer);
}

}
