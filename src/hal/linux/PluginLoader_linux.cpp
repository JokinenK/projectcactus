/*
 * Plugin_linux.cpp
 *
 *  Created on: 7.12.2016
 *      Author: kalle
 */

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include "hal/PluginLoader.h"

namespace Hal
{

struct PluginLoader::PluginData
{
	void* handle;
	std::string fileName;
	int loadFlags;
};

static int f_pluginFlags[PluginLoader::NUM_LOADFLAGS] = {
		0,        /* LOADFLAG_NONE,     */
		RTLD_LAZY /* LOADFLAG_LAZYLOAD, */
};

PluginLoader::PluginLoader(const std::string& fileName) :
		m_data(new PluginData)
{
	m_data->handle = 0;
	m_data->fileName = fileName;
	m_data->loadFlags = 0;
}

PluginLoader::~PluginLoader()
{
	unload();
	delete m_data;
}

bool PluginLoader::load()
{
	m_data->handle = dlopen(
			m_data->fileName.c_str(),
			m_data->loadFlags);

	return (m_data->handle != NULL);
}

bool PluginLoader::unload()
{
	if (m_data->handle && dlclose(m_data->handle) == 0) {
		m_data->handle = NULL;
	}

	return (m_data->handle == NULL);
}

void* PluginLoader::symbol(const std::string& name)
{
	return dlsym(m_data->handle, name.c_str());
}

void PluginLoader::setLoadFlags(LoadFlags loadFlags)
{
	m_data->loadFlags = f_pluginFlags[loadFlags];
}

std::string PluginLoader::error()
{
	std::string errorMessage;
	char* error = dlerror();

	if (error != NULL) {
		errorMessage = std::string(error);
	}

	return errorMessage;
}

}
