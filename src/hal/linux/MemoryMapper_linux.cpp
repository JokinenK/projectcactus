/*
 * MemoryMap.h
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#include "hal/MemoryMapper.h"

namespace Hal
{

namespace
{

static long long s_fileAttributes[MemoryMapper::NUM_ATTRIBUTES] = {
//		GENERIC_READ,
//		GENERIC_READ | GENERIC_WRITE
};

static long long s_mapAttributes[MemoryMapper::NUM_ATTRIBUTES] = {
//		PAGE_READONLY,
//		PAGE_READWRITE
};

static long long s_viewAttributes[MemoryMapper::NUM_ATTRIBUTES] = {
//		FILE_MAP_READ,
//		FILE_MAP_READ | FILE_MAP_WRITE
};

}

void* MemoryMapper::mapFile(const std::string& fileName, size_t& mapSize, Attributes attrs)
{
	void* mappedData = 0;

//	HANDLE fileHandle = CreateFile(
//			fileName.c_str(),
//			s_fileAttributes[attrs],
//			FILE_SHARE_READ,
//	        NULL,
//			OPEN_ALWAYS,
//			FILE_ATTRIBUTE_NORMAL,
//			NULL);
//
//	if (fileHandle != 0) {
//		mapSize = GetFileSize(fileHandle, NULL);
//
//		HANDLE mapHandle  = CreateFileMapping(
//				fileHandle,
//				NULL,
//				s_mapAttributes[attrs],
//				0,
//				0,
//				NULL);
//
//		if (mapHandle != 0) {
//			mappedData = MapViewOfFile(
//					mapHandle,
//					s_viewAttributes[attrs],
//					0,
//					0,
//					0);
//
//			CloseHandle(mapHandle);
//		}
//
//		CloseHandle(fileHandle);
//	}

	return mappedData;
}

bool MemoryMapper::unmapFile(void* mappedData)
{
//	return (UnmapViewOfFile(mappedData) != 0);
	return true;
}

} // namespace
