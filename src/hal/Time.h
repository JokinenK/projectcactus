/*
 * Time.h
 *
 *  Created on: 11.9.2016
 *      Author: kalle
 */

#ifndef HAL_TIME_H_
#define HAL_TIME_H_

namespace Hal
{

class Time
{
public:
	static unsigned long long currentTimeMillis();
};

}

#endif /* HAL_TIME_H_ */
