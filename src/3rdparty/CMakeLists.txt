add_definitions(-DGLEW_STATIC)

get_filename_component(SDL2 "${CMAKE_CURRENT_SOURCE_DIR}/SDL2" ABSOLUTE)
set(SDL2 "${SDL2}" PARENT_SCOPE)

#add_subdirectory(glew-2.0.0)
add_subdirectory(tinyxml2)
add_subdirectory(stb_image)
add_subdirectory(freetype-2.7)