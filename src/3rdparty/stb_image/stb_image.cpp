/*
 * stb_image.cpp
 *
 *  Created on: 11.9.2016
 *      Author: kalle
 */

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_write.h"
