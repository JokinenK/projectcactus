cmake_minimum_required(VERSION 2.8.11)
project("stb_image")

include_directories(
    ${DIR}
)

set(SOURCES
    stb_image.cpp
)

set(HEADERS
    stb_image.h
)

set(INTERFACES
)

set(LIBRARIES
)

add_library(${PROJECT_NAME}-static STATIC ${SOURCES})
target_link_libraries(${PROJECT_NAME}-static ${LIBRARIES})