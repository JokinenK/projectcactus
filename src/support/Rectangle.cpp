/*
 * Rectangle.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <iostream>
#include "Rectangle.h"

const Rectangle Rectangle::EMPTY(0, 0, 0, 0);

Rectangle::Rectangle() :
		m_left(0),
		m_top(0),
		m_right(0),
		m_bottom(0)
{
}

Rectangle::Rectangle(float left, float top, float width, float height) :
		m_left(left),
		m_top(top),
		m_right(left + width),
		m_bottom(top + height)
{
}

Rectangle::Rectangle(const Point& position, const Dimensions& dimensions) :
		m_left(position.getX()),
		m_top(position.getY()),
		m_right(m_left + dimensions.getWidth()),
		m_bottom(m_top + dimensions.getHeight())
{
}

Rectangle::Rectangle(const Rectangle& other) :
		m_left(other.getLeft()),
		m_top(other.getTop()),
		m_right(other.getRight()),
		m_bottom(other.getBottom())
{
}

Rectangle::~Rectangle()
{
}

Rectangle& Rectangle::operator=(const Rectangle& other)
{
	setPosition(other.getPosition());
	setDimensions(other.getDimensions());

	return *this;
}

const Point Rectangle::getPosition() const
{
	return Point(m_left, m_top);
}

const Dimensions Rectangle::getDimensions() const
{
	return Dimensions(m_right - m_left, m_bottom - m_top);
}

Rectangle Rectangle::normalized() const
{
	return Rectangle(0, 0, getWidth(), getHeight());
}

void Rectangle::setLeft(float left)
{
	m_left = left;
}

float Rectangle::getLeft() const
{
	return m_left;
}

void Rectangle::setRight(float right)
{
	m_right = right;
}

float Rectangle::getRight() const
{
	return m_right;
}

void Rectangle::setTop(float top)
{
	m_top = top;
}

float Rectangle::getTop() const
{
	return m_top;
}

void Rectangle::setBottom(float bottom)
{
	m_bottom = bottom;
}

float Rectangle::getBottom() const
{
	return m_bottom;
}

void Rectangle::setSize(const Dimensions& dimensions)
{
	setSize(dimensions.getWidth(), dimensions.getHeight());
}

void Rectangle::setSize(float width, float height)
{
	setWidth(width);
	setHeight(height);
}

void Rectangle::adjustWidth(float deltaWidth)
{
	setWidth(getWidth() + deltaWidth);
}

void Rectangle::setWidth(float width)
{
	m_right = m_left + width;
}

float Rectangle::getWidth() const
{
	return m_right - m_left;
}

void Rectangle::adjustHeight(float deltaHeight)
{
	setHeight(getHeight() + deltaHeight);
}

void Rectangle::setHeight(float height)
{
	m_bottom = m_top + height;
}

float Rectangle::getHeight() const
{
	return m_bottom - m_top;
}

float Rectangle::getArea() const
{
	return getWidth() * getHeight();
}

void Rectangle::move(const Point& delta)
{
	move(delta.getX(), delta.getY());
}

void Rectangle::move(float dx, float dy)
{
	setPosition(m_left + dx, m_top + dy);
}

void Rectangle::setPosition(const Point& position)
{
	setPosition(position.getX(), position.getY());
}

void Rectangle::setPosition(float left, float top)
{
	float width = getWidth();
	float height = getHeight();

	m_left = left;
	m_top = top;
	m_right = m_left + width;
	m_bottom = m_top + height;
}

void Rectangle::setDimensions(const Dimensions& dimensions)
{
	setWidth(dimensions.getWidth());
	setHeight(dimensions.getHeight());
}

void Rectangle::setDimensions(float width, float height)
{
	setWidth(width);
	setHeight(height);;
}

bool Rectangle::contains(float x, float y) const
{
	// NOTE: Y coordinate grows towards bottom of screen

	if (x < getLeft()) {
		return false;
	}

	if (x > getRight()) {
		return false;
	}


	if (y > getBottom()) {
		return false;
	}

	if (y < getTop()) {
		return false;
	}

	return true;
}

bool Rectangle::contains(const Point& point) const
{
	return contains(point.getX(), point.getY());
}

bool Rectangle::isValid() const
{
	return (getWidth() != 0 && getHeight() != 0);
}

const glm::vec4 Rectangle::toVec4() const
{
	return glm::vec4(getLeft(), getTop(), getRight(), getBottom());
}
