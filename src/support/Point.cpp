/*
 * Point.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include "Point.h"

Point::Point()
{
	setPosition(0, 0);
}

Point::Point(float x, float y)
{
	setPosition(x, y);
}

Point::~Point()
{
}

Point& Point::operator=(const Point& other)
{
	setPosition(other.getX(), other.getY());
	return *this;
}

Point Point::operator-()
{
	return Point(-m_x, -m_y);
}

void Point::setX(float x)
{
	m_x = x;
}

float Point::getX() const
{
	return m_x;
}

void Point::setY(float y)
{
	m_y = y;
}

float Point::getY() const
{
	return m_y;
}

void Point::setPosition(float x, float y)
{
	setX(x);
	setY(y);
}

void Point::move(float dX, float dY)
{
	m_x += dX;
	m_y += dY;
}
