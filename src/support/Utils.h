#ifndef UTILS_H_
#define UTILS_H_

#include <algorithm>
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <sstream>
#include <cstring>
#include <glm/glm.hpp>

#define INVOKE_MEMBER_FN(instance, fnPtr) 	((instance)->*(fnPtr))
#define ARRAY_SIZE(x)						(sizeof(x) / sizeof(x[0]))
#define UNUSED(x)							((void)(x))

namespace Utils
{
    template <class T>
    inline static bool fromString(const std::string& input, T& result)
    {
    	if (!input.empty()) {
    		std::istringstream iss(input);

    		if (iss >> result) {
    			return true;
    		}
    	}

    	return false;
    }

    template <class T>
    inline static std::string toString(const T& value)
    {
    	std::ostringstream oss;
    	oss << value;
    	return oss.str();
    }

    template <class T>
    inline static bool stringToList(const std::string& input, T& results)
    {
		results.clear();

		typename T::value_type result;
		std::istringstream iss(input);

		while (iss >> result) {
			results.push_back(result);
		}

    	return !results.empty();
    }

    template <class T>
    inline static std::string listToString(const T& values)
	{
    	std::ostringstream oss;

    	for (const typename T::value_type& value : values) {
    		oss << value;
    	}

    	return oss.str();
	}

    inline static std::list<std::string> splitString(const std::string& source, const std::string& delimiter)
    {
    	std::list<std::string> results;

        size_t offset = 0;
        size_t pos = source.find(delimiter);

        while (pos != std::string::npos) {
        	results.push_back(source.substr(offset, pos - offset));
        	offset = pos + delimiter.length();
            pos = source.find(delimiter, offset);
        }

        results.push_back(source.substr(offset, source.length()));
    	return results;
    }

    inline static std::string& ltrim(std::string& str)
    {
        str.erase(
        		str.begin(),
    			std::find_if(
    					str.begin(),
    					str.end(),
    					std::not1(std::ptr_fun<int, int>(std::isspace))));

        return str;
    }

    inline static std::string& rtrim(std::string& str)
    {
        str.erase(
        		std::find_if(
        				str.rbegin(),
    					str.rend(),
    					std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
    			str.end());

        return str;
    }

    inline static std::string& trim(std::string& str)
    {
        return ltrim(rtrim(str));
    }

    inline static std::string ltrimmed(std::string str)
    {
    	return ltrim(str);
    }

    inline static std::string rtrimmed(std::string str)
    {
    	return rtrim(str);
    }

    inline static std::string trimmed(std::string str)
    {
    	return ltrim(rtrim(str));
    }

    inline static bool stringEquals(const std::string& lhs, const std::string& rhs) {
    	return (lhs.compare(rhs) == 0);
    }

    inline static bool stringBeginsWith(const std::string& lhs, const std::string& rhs) {
    	return (lhs.compare(0, rhs.length(), rhs) == 0);
    }

    inline static std::string readFile(const std::string& fileName)
    {
    	std::string contents;
    	FILE* handle = fopen(fileName.c_str(), "rb");

    	if (handle != 0) {
    		fseek(handle, 0, SEEK_END);
    		ssize_t fileSize = ftell(handle);
    		rewind(handle);

    		std::vector<char> buffer(fileSize + 1);
    		fread(buffer.data(), 1, fileSize, handle);

    		contents = std::string(buffer.data(), fileSize);
    		fclose(handle);
    	}
       	else {
			std::cerr << "Unable to read file: " << fileName << std::endl;
		}

    	return contents;
    }

    inline std::string readBetween(const std::string& str, const std::string& beginTag, const std::string& endTag)
    {
        std::string::size_type start = str.find(beginTag);

        if (start != str.npos) {
            std::string::size_type end = str.find(endTag, start + 1);

            if (end != str.npos) {
                ++start;
                std::string::size_type count = end - start;

                return str.substr(start, count);
            }
        }

        return std::string();
    }

    inline static float map(float x, float inMin, float inMax, float outMin, float outMax)
    {
		return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
    }

    template <class T>
    inline static T clamp(T value, T min, T max) {
    	if (value < min) {
    		return min;
    	}

    	if (value > max) {
    		return max;
    	}

    	return value;
    }

    // Specialize the fromString method because when it's called for string it would only take the first part
    template <>
    inline bool fromString<std::string>(const std::string& input, std::string& result)
    {
    	result = input;
    	return true;
    }

    template <>
    inline std::string toString<std::string>(const std::string& value)
    {
    	return value;
    }
}



#endif /* UTILS_H_ */
