/*
 * Dimensions.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef DIMENSIONS_H
#define DIMENSIONS_H

class Dimensions
{
public:
	Dimensions();
	Dimensions(float width, float height);
	~Dimensions();

	Dimensions& operator=(const Dimensions& other);

	void setWidth(float width);
	float getWidth() const;

	void setHeight(float height);
	float getHeight() const;

	float getArea() const;

	void setDimensions(float width, float height);

private:
	float m_width;
	float m_height;
};

#endif /* DIMENSIONS_H */
