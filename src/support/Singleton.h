#ifndef SINGLETON_H
#define SINGLETON_H

#include <typeinfo>
#include "core/PluginExports.h"

namespace SingletonStore
{
	extern PLUGIN_API void setInstance(const std::type_info& type, void* instance);
	extern PLUGIN_API void* getInstance(const std::type_info& type);
}

template <class T>
class Singleton
{
public:
    static T* instance()
    {
    	const std::type_info& typeInfo = typeid(T);
        T* instance = reinterpret_cast<T*>(SingletonStore::getInstance(typeInfo));

        if (!instance) {
            instance = new T();
            SingletonStore::setInstance(typeInfo, instance);
        }

        return instance;
    }

    static T& instanceRef()
    {
    	return *instance();
    }
};

#endif // SINGLETON_H
