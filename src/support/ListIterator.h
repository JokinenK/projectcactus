/*
 * ListIterator.h
 *
 *  Created on: 15.9.2016
 *      Author: kalle
 */

#ifndef LISTITERATOR_H_
#define LISTITERATOR_H_

#include <list>

template <class T>
class ListIterator
{
public:
	ListIterator(const std::list<T>& list) :
		m_list(list),
		m_iter(m_list.begin())
	{
	}

	bool hasNext() const
	{
		return (m_iter != m_list.end());
	}

	bool hasPrev() const
	{
		return (m_iter != m_list.begin());
	}

	const T& next()
	{
		return *(m_iter++);
	}

	const T& prev()
	{
		return *(m_iter--);
	}

	void toBegin()
	{
		m_iter = m_list.begin();
	}

	void toEnd()
	{
		m_iter = m_list.end();
	}

private:
	const std::list<T>& m_list;
	typename std::list<T>::const_iterator m_iter;
};

#endif /* LISTITERATOR_H_ */
