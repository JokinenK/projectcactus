/*
 * Color.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef COLOR_H_
#define COLOR_H_

#include <glm/glm.hpp>

class Color
{
public:
	Color();
    Color(int hexColor, bool hasAlpha = false);
    Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha = 0xFF);
    Color(float red, float green, float blue, float alpha = 1.0f);
    Color(const Color& other);
    virtual ~Color();

    Color& operator=(const Color& other);
    friend Color operator*(const Color& color, float scale);

    void clear();

    void setHexColor(int hexColor, bool setAlpha = false);
    int getHexColor(bool hasAlpha = false) const;

    void setRed(float red);
    void setRedWithByte(unsigned char red);
    float getRed() const;
    unsigned char getRedAsByte() const;
    
    void setGreen(float green);
    void setGreenWithByte(unsigned char green);
    float getGreen() const;
    unsigned char getGreenAsByte() const;
    
    void setBlue(float blue);
    void setBlueWithByte(unsigned char blue);
    float getBlue() const;
    unsigned char getBlueAsByte() const;

    void setAlpha(float alpha);
    void setAlphaWithByte(unsigned char alpha);
    float getAlpha() const;
    unsigned char getAlphaAsByte() const;

    float getMax() const;
    unsigned char getMaxAsByte() const;

    void printColor();

    const glm::vec4 toVec4() const;

private:
    static float toFloat(int value);
    static unsigned char fromFloat(float value);

    float m_red;
    float m_green;
    float m_blue;
    float m_alpha;
};

#endif // COLOR_H_
