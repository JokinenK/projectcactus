/*
 * BinaryTexturePacker.cpp
 *
 *  Created on: 30.9.2016
 *      Author: kalle
 */

#include "RectangleBinaryTree.h"

const Rectangle RectangleBinaryTree::s_invalidRectangle = Rectangle(0, 0, 0, 0);

RectangleBinaryTree::RectangleBinaryTree(int width, int height) :
	m_reserved(false),
	m_rectangle(0, 0, width, height),
	m_downNode(0),
	m_rightNode(0)
{
}

RectangleBinaryTree::RectangleBinaryTree(int x, int y, int width, int height) :
	m_reserved(false),
	m_downNode(0),
	m_rightNode(0)
{
	m_rectangle.setPosition(x, y);
	m_rectangle.setDimensions(width, height);
}

RectangleBinaryTree::~RectangleBinaryTree()
{
	delete m_downNode;
	delete m_rightNode;
}

RectangleBinaryTree* RectangleBinaryTree::findSlot(const Dimensions& dimensions)
{
	if (m_reserved) {
		RectangleBinaryTree* node;

		if ((node = m_rightNode->findSlot(dimensions)) != 0) {
			return node;
		}

		if ((node = m_downNode->findSlot(dimensions)) != 0) {
			return node;
		}
	}
	else if (rectangleFits(dimensions)) {
		return this;
	}

	return 0;
}

bool RectangleBinaryTree::rectangleFits(const Dimensions& dimensions) const
{
	if (dimensions.getWidth() > m_rectangle.getWidth()) {
		return false;
	}

	if (dimensions.getHeight() > m_rectangle.getHeight()) {
		return false;
	}

	return true;
}

const Rectangle& RectangleBinaryTree::splitSlot(const Dimensions& dimensions)
{
	int x = m_rectangle.getLeft();
	int y = m_rectangle.getTop();
	int w = m_rectangle.getWidth();
	int h = m_rectangle.getHeight();

	int width = dimensions.getWidth();
	int height = dimensions.getHeight();

	m_reserved = true;

	m_downNode = new RectangleBinaryTree(
			x,
			y + height,
			w,
			h - height);

	m_rightNode = new RectangleBinaryTree(
			x + width,
			y,
			w - width,
			height);

	m_rectangle.setWidth(width);
	m_rectangle.setHeight(height);

	return m_rectangle;
}

const Rectangle& RectangleBinaryTree::fit(const Dimensions& dimensions)
{
	RectangleBinaryTree* node = findSlot(dimensions);

	if (!node) {
		return s_invalidRectangle;
	}

	return node->splitSlot(dimensions);
}
