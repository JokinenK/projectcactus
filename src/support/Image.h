/*
 * Image.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#include <string>
#include <vector>
#include "support/Color.h"

class Image
{
public:
	typedef enum {
		DONT_FLIP = 0x00,
		FLIP_X    = 0x01,
		FLIP_Y    = 0x02
	} FlipMask;

	Image();
	Image(const std::string& fileName, int flipMask = DONT_FLIP);
	Image(int width, int height, int channels, unsigned char* buffer, int flipMask = DONT_FLIP);
	Image(const Image& other);
	~Image();

	Image& operator=(const Image& other);

	bool create(int width, int height, int channels);
	bool load(int width, int height, int channels, unsigned char* buffer, int flipMask = DONT_FLIP);
	bool load(const std::string& fileName, int flipMask = DONT_FLIP);
	bool save(const std::string& fileName = std::string());

	bool isValid() const;
	int getWidth() const;
	int getHeight() const;
	int getChannels() const;
	size_t getSize() const;

	const std::vector<unsigned char>& getData() const;
	const std::string& getFileName() const;

	void set(int x, int y, const Color& color);
	Color get(int x, int y) const;

	void flipImage(int flipMask);
	void setSubImage(int x, int y, const Image& other);

private:
	ssize_t calcOffset(int x, int y) const;

	int m_width;
	int m_height;
	int m_channels;
	std::vector<unsigned char> m_data;

	std::string m_fileName;
};


#endif /* IMAGE_H_ */
