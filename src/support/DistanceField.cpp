/*
 * DistanceField.cpp
 *
 *  Created on: 29.9.2016
 *      Author: kalle
 */

#include <vector>
#include <cmath>
#include <algorithm>
#include <iostream>
#include "DistanceField.h"

namespace
{
	template <class T>
	T clamp(T value, T low, T high)
	{
		if (value < low) { value = low; }
		if (value > high) { value = high; }
		return value;
	}

	bool isColorVisible(const Color& color, int numComponents)
	{
		bool visible = false;

		switch (numComponents) {
		case 1: {
			visible = (color.getRed() > 0.5f);
		} break;

		case 2: {
			visible = (color.getGreen() > 0.5f);
		} break;

		case 3: {
			visible = (color.getGreen() > 0.5f);
		} break;

		case 4: {
			visible = (color.getAlpha() > 0.5f);
		} break;
		}

		return visible;
	}

	class GridPoint {
	public:
		GridPoint() :
			dx(0),
			dy(0)
		{
		}

		GridPoint(int dx, int dy) :
			dx(dx),
			dy(dy)
		{
		}

		GridPoint(const GridPoint& other) :
			dx(other.dx),
			dy(other.dy)
		{
		}

		GridPoint& operator=(const GridPoint& other)
		{
			dx = other.dx;
			dy = other.dy;
			return *this;
		}

		int lengthSq() const
		{
			return dx * dx + dy * dy;
		}

		int dx;
		int dy;
	};

	static const GridPoint POINT_INSIDE = GridPoint(0, 0);
	static const GridPoint POINT_OUTSIZE = GridPoint(9999, 9999);

	class Grid
	{
	public:
		Grid(int width, int height) :
				m_width(width),
				m_height(height),
				m_realWidth(width + 2),
				m_realHeight(height + 2)
		{
			// Add 'safe zone' to each direction
			m_grid.resize(m_realWidth * m_realHeight);

			int x = 0;
			int firstRow = 0;
			int lastRow = (m_realHeight - 1);

			for (x = 0; x < m_realWidth; x++) {
				m_grid[firstRow * m_realWidth + x] = POINT_OUTSIZE;
				m_grid[lastRow * m_realWidth + x] = POINT_OUTSIZE;
			}

			int y = 0;
			int firstCol = 0;
			int lastCol = (m_realWidth - 1);

			for (y = 0; y < m_realHeight; y++) {
				m_grid[y * m_realHeight + firstCol] = POINT_OUTSIZE;
				m_grid[y * m_realHeight + lastCol] = POINT_OUTSIZE;
			}
		}

		size_t offset(int x, int y) const
		{
			return (++y) * m_realWidth + (++x);
		}

		GridPoint& get(int x, int y)
		{
			return m_grid[offset(x, y)];
		}

		void set(int x, int y, const GridPoint& point)
		{
			m_grid[offset(x, y)] = point;
		}

		void compare(GridPoint& point, int x, int y, int offsetX, int offsetY)
		{
			GridPoint other(get(x + offsetX, y + offsetY));
			other.dx += offsetX;
			other.dy += offsetY;

			if (other.lengthSq() < point.lengthSq()) {
				point = other;
			}
		}

		void propagate()
		{
			short x;
			short y;
			short lastCol = (m_width - 1);
			short lastRow = (m_height - 1);

			// Pass 0
			for (y = 0; y < m_height; y++) {
				for (x = 0; x < m_width; x++) {
					GridPoint& point = get(x, y);
					compare(point, x, y, -1,  0);
					compare(point, x, y,  0, -1);
					compare(point, x, y, -1, -1);
					compare(point, x, y,  1, -1);
				}

				for (x = lastCol; x >= 0; x--) {
					GridPoint& point = get(x, y);
					compare(point, x, y, 1, 0);
				}
			}

			// Pass 1
			for (y = lastRow; y >= 0; y--) {
				for (x = lastCol; x >= 0; x--) {
					GridPoint& point = get(x, y);
					compare(point, x, y,  1,  0);
					compare(point, x, y,  0,  1);
					compare(point, x, y, -1,  1);
					compare(point, x, y,  1,  1);
				}

				for (x = 0; x < m_width; x++) {
					GridPoint& point = get(x, y);
					compare(point, x, y, -1, 0);
				}
			}
		}

	private:
		short m_width;
		short m_height;
		short m_realWidth;
		short m_realHeight;
		std::vector<GridPoint> m_grid;
	};
}

Image DistanceField::create(const Image& source, int spread)
{
	int x;
	int y;

	int numComponents = source.getChannels();
	int width = source.getWidth();
	int height = source.getHeight();

	Image result;
	result.create(width, height, numComponents);

	std::cout << "Creating signed distance field font atlas..." << std::endl;

	Color color;
	Grid grid1 = Grid(width, height);
	Grid grid2 = Grid(width, height);

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			color.clear();
			color = source.get(x, y);

			// Points inside get marked with a dx/dy of zero.
			// Points outside get marked with an infinitely large distance.
			if (isColorVisible(color, numComponents)) {
				grid1.set(x, y, POINT_INSIDE);
				grid2.set(x, y, POINT_OUTSIZE);
			}
			else {
				grid1.set(x, y, POINT_OUTSIZE);
				grid2.set(x, y, POINT_INSIDE);
			}
		}
	}

	std::cout << "Propagating signed distance field font atlas..." << std::endl;

	grid1.propagate();
	grid2.propagate();

	int dist;
	int distPart1;
	int distPart2;
	int minDist = -spread;
	int maxDist = spread;
	unsigned char value;

	std::cout << "Normalizing signed distance field font atlas..." << std::endl;

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			// Map distance, 0 = max negative distance, 255 = max positive distance

			distPart1 = std::sqrt(grid1.get(x, y).lengthSq());
			distPart2 = std::sqrt(grid2.get(x, y).lengthSq());
			dist = (distPart2 - distPart1);

			if (dist < 0) {
				dist = -128 * (dist - minDist) / minDist;
			}
			else {
				dist = 128 + 128 * dist / maxDist;
			}

			value = clamp(dist, 0, 255);
			result.set(x, y, Color(value, value, value));
		}
	}

	std::cout << "Finished creating signed distance field font atlas..." << std::endl;

	return result;
}
