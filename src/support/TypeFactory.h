/*
 * Factory.h
 *
 *  Created on: 7.10.2016
 *      Author: kalle
 */

#ifndef FACTORY_H_
#define FACTORY_H_

#include <map>
#include <string>

template <typename Base>
class TypeFactory
{
public:
    template <typename Derived>
    void registerType(const std::string& name)
    {
        static_assert(
        		std::is_base_of<Base, Derived>::value,
				"Factory::registerType doesn't accept this type because doesn't derive from base class");

        m_constructors.insert(
        		std::make_pair(
        				name,
						&constructorFunc<Derived>));
    }

    Base* create(const std::string& name) const
    {
        typename std::map<std::string, Constructor>::const_iterator iter = m_constructors.find(name);

        if (iter != m_constructors.end()) {
            return iter->second();
        }

        return 0;
    }

private:
    template <typename Derived>
    static Base* constructorFunc()
    {
        return new Derived();
    }

    typedef Base* (*Constructor)();
    std::map<std::string, Constructor> m_constructors;
};

#endif /* FACTORY_H_ */
