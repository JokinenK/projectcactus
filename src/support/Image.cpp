/*
 * Image.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <vector>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include "support/Image.h"
#include "stb_image.h"
#include "stb_image_write.h"

Image::Image() :
		m_width(0),
		m_height(0),
		m_channels(0)
{
}

Image::Image(const std::string& fileName, int flipMask) :
		m_width(0),
		m_height(0),
		m_channels(0)
{
	load(fileName, flipMask);
}

Image::Image(int width, int height, int channels, unsigned char* buffer, int flipMask) :
		m_width(0),
		m_height(0),
		m_channels(0)
{
	load(width, height, channels, buffer, flipMask);
}

Image::Image(const Image& other) :
		m_width(other.getWidth()),
		m_height(other.getHeight()),
		m_channels(other.getChannels()),
		m_data(other.getData())
{
}

Image::~Image()
{
}

Image& Image::operator=(const Image& other)
{
	m_width = other.getWidth();
	m_height = other.getHeight();
	m_channels = other.getChannels();
	m_data = other.getData();

	return *this;
}

bool Image::create(int width, int height, int channels)
{
	m_width = width;
	m_height = height;
	m_channels = channels;
	m_data.resize(width * height * channels);

	return isValid();
}

bool Image::load(int width, int height, int channels, unsigned char* buffer, int flipMask)
{
	m_width = width;
	m_height = height;
	m_channels = channels;

	ssize_t size = width * height * channels;
	m_data.reserve(size);
	m_data.assign(buffer, buffer + size);

	flipImage(flipMask);
	return isValid();
}

bool Image::load(const std::string& fileName, int flipMask)
{
	bool success = false;

	int width;
	int height;
	int channels;

	unsigned char* buffer = stbi_load(
			fileName.c_str(),
			&width,
			&height,
			&channels, 0);

	if (buffer != 0) {
		success = load(
				width,
				height,
				channels,
				buffer,
				flipMask);

		stbi_image_free(buffer);
	}

	return success;
}

bool Image::save(const std::string& fileName)
{
	bool success = false;

	if (!fileName.empty()) {
		m_fileName = fileName;
	}

	if (!m_fileName.empty()) {
		stbi_write_png(m_fileName.c_str(), m_width, m_height, m_channels, m_data.data(), 0);
		success = true;
	}

	return success;
}

void Image::flipImage(int flipMask) {

	if (flipMask == DONT_FLIP) {
		return;
	}

	std::vector<unsigned char> copyOfData(m_data);

	int dstX;
	int dstY;
	bool flipX = ((flipMask & FLIP_X) == FLIP_X);
	bool flipY = ((flipMask & FLIP_Y) == FLIP_Y);

	for (int x = 0; x < m_width; x++) {
		for (int y = 0; y < m_height; y++) {
			dstX = flipX ? (m_width - x - 1) : x;
			dstY = flipY ? (m_height - y - 1) : y;

			size_t srcffset = calcOffset(x, y);
			size_t dstOffset = calcOffset(dstX, dstY);

			for (int comp = 0; comp < m_channels; comp++) {
				m_data[dstOffset++] = copyOfData[srcffset++];
			}
		}
	}
}

bool Image::isValid() const
{
	return (m_data.size() > 0);
}

int Image::getWidth() const
{
	return m_width;
}

int Image::getHeight() const
{
	return m_height;
}

int Image::getChannels() const
{
	return m_channels;
}

const std::vector<unsigned char>& Image::getData() const
{
	return m_data;
}

const std::string& Image::getFileName() const
{
	return m_fileName;
}

size_t Image::getSize() const
{
	return m_data.size();
}

ssize_t Image::calcOffset(int x, int y) const
{
	ssize_t offset = -1;

	if (x >= 0 && x <= m_width && y >= 0 && y <= m_height) {
		offset = (y * m_width + x) * m_channels;
	}

	return offset;
}

Color Image::get(int x, int y) const
{
	Color color;
	ssize_t offset = calcOffset(x, y);

	if (offset >= 0) {
		int i;
		for (i = 0; i < m_channels; i++) {
			switch (i) {
			case 0:
				color.setRedWithByte(m_data[offset + i]);
				break;
			case 1:
				color.setGreenWithByte(m_data[offset + i]);
				break;
			case 2:
				color.setBlueWithByte(m_data[offset + i]);
				break;
			case 3:
				color.setAlphaWithByte(m_data[offset + i]);
				break;
			}
		}
	}

	return color;
}

void Image::set(int x, int y, const Color& color)
{
	ssize_t offset = calcOffset(x, y);

	if (offset >= 0) {
		int i;
		for (i = 0; i < m_channels; i++) {
			switch (i) {
			case 0:
				m_data[offset + i] = color.getRedAsByte();
				break;
			case 1:
				m_data[offset + i] = color.getGreenAsByte();
				break;
			case 2:
				m_data[offset + i] = color.getBlueAsByte();
				break;
			case 3:
				m_data[offset + i] = color.getAlphaAsByte();
				break;
			}
		}
	}
}

void Image::setSubImage(int x, int y, const Image& other)
{
	int col;
	int row;

	for (col = 0; col < other.getWidth(); col++) {
		for (row = 0; row < other.getHeight(); row++) {
			set(col + x, row + y, other.get(col, row));
		}
	}
}
