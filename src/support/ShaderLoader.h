/*
 * ShaderLoader.h
 *
 *  Created on: 25.4.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_SHADER_SHADERLOADER_H_
#define PROJECTCACTUS_SRC_CORE_SHADER_SHADERLOADER_H_

#include <fstream>
#include "hal/File.h"
#include "support/Utils.h"

namespace ShaderLoader
{
	static std::string loadShader(const std::string& filePath)
	{
		std::string contents;
		std::string absolutePath = Hal::File::absolutePath(filePath);
		std::string sharedPath = Hal::File::directoryName(absolutePath);
		std::ifstream iss(absolutePath);

		std::cout << filePath << std::endl;

		if (iss.is_open()) {
			std::string line;

			while (std::getline(iss, line)) {
				if (Utils::stringBeginsWith(line, "#include")) {
					std::string includeFile = Utils::readBetween(line, "\"", "\"");
					contents += loadShader(sharedPath + Hal::File::pathSeparator() + includeFile);
				}
				else {
					contents += line + "\n";
				}
			}
		}
		else {
			std::cerr << "Unable to read file: " << absolutePath << std::endl;
		}

    	return contents;
	}
}

#endif /* PROJECTCACTUS_SRC_CORE_SHADER_SHADERLOADER_H_ */
