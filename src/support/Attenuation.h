#ifndef ATTENUATION_H
#define ATTENUATION_H

#include <glm/glm.hpp>

class Attenuation
{
public:
    Attenuation(float constant, float linear, float exponent);
    
    void setConstant(float constant);
    float getConstant() const;

    void setLinear(float linear);
    float getLinear() const;

    void setExponent(float exponent);
    float getExponent() const;

    const glm::vec3 toVec3() const;

private:
	float m_constant;
	float m_linear;
	float m_exponent;
};

#endif // ATTENUATION_H
