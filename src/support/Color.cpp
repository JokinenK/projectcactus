/*
 * Color.cpp
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#include <iostream>
#include <math.h>
#include "support/Color.h"

Color::Color()
{
	clear();
}

Color::Color(int hexColor, bool hasAlpha)
{
	if (!hasAlpha) {
		setAlpha(1.0f);
	}

	setHexColor(hexColor, hasAlpha);
}

Color::Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha)
{
	setAlphaWithByte(alpha);
	setRedWithByte(red);
	setGreenWithByte(green);
	setBlueWithByte(blue);
}

Color::Color(float red, float green, float blue, float alpha)
{
	setAlpha(alpha);
	setRed(red);
	setGreen(green);
	setBlue(blue);
}

Color::Color(const Color& other)
{
	setAlpha(other.getAlpha());
	setRed(other.getRed());
	setGreen(other.getGreen());
	setBlue(other.getBlue());
}

Color::~Color()
{
}

Color& Color::operator=(const Color& other)
{
	setAlpha(other.getAlpha());
	setRed(other.getRed());
	setGreen(other.getGreen());
	setBlue(other.getBlue());
	return *this;
}

void Color::clear()
{
	setAlpha(0.0f);
	setRed(0.0f);
	setGreen(0.0f);
	setBlue(0.0f);
}

void Color::setHexColor(int hexColor, bool hasAlpha)
{
	if (hasAlpha) {
		setAlphaWithByte((hexColor >> 24) & 0xFF);
	}

	setRedWithByte((hexColor >> 16) & 0xFF);
	setGreenWithByte((hexColor >> 8) & 0xFF);
	setBlueWithByte(hexColor & 0xFF);
}

int Color::getHexColor(bool hasAlpha) const
{
	int hexColor = 0;

	if (hasAlpha) {
		hexColor += (getAlphaAsByte() << 24);
	}

	hexColor += (getRedAsByte()   << 16);
    hexColor += (getGreenAsByte() <<  8);
    hexColor += (getBlueAsByte());

	return hexColor;
}

void Color::setRed(float red)
{
    m_red = red;
}

void Color::setRedWithByte(unsigned char red)
{
	m_red = toFloat(red);
}

float Color::getRed() const
{
    return m_red;
}

unsigned char Color::getRedAsByte() const
{
    return fromFloat(m_red);
}

void Color::setGreen(float green)
{
    m_green = green;
}

void Color::setGreenWithByte(unsigned char green)
{
	m_green = toFloat(green);
}

float Color::getGreen() const
{
    return m_green;
}

unsigned char Color::getGreenAsByte() const
{
    return fromFloat(m_green);
}

void Color::setBlue(float blue)
{
    m_blue = blue;
}

void Color::setBlueWithByte(unsigned char blue)
{
	m_blue = toFloat(blue);
}

float Color::getBlue() const
{
    return m_blue;
}

unsigned char Color::getBlueAsByte() const
{
    return fromFloat(m_blue);
}

void Color::setAlpha(float alpha)
{
	m_alpha = alpha;
}

void Color::setAlphaWithByte(unsigned char alpha)
{
	m_alpha = toFloat(alpha);
}

float Color::getAlpha() const
{
	return m_alpha;
}

unsigned char Color::getAlphaAsByte() const
{
	return fromFloat(m_alpha);
}

float Color::toFloat(int value)
{
    return static_cast<float>(value) / 0xFF;
}

unsigned char Color::fromFloat(float value)
{
    return static_cast<unsigned char>(value * 0xFF);
}

float Color::getMax() const
{
	return std::max(m_red, std::max(m_green, m_blue));
}

unsigned char Color::getMaxAsByte() const
{
	return fromFloat(getMax());
}

void Color::printColor()
{
	std::cout << "R: " << getRed()
			  << ", G: " << getGreen()
			  << ", B: " << getBlue()
			  << ", A: " << getAlpha()
			  << std::endl;
}

Color operator*(const Color& color, float scale)
{
	return Color(
			color.getRed() * scale,
			color.getGreen() * scale,
			color.getBlue() * scale,
			color.getAlpha() * scale);
}

const glm::vec4 Color::toVec4() const
{
	return glm::vec4(m_red, m_green, m_blue, m_alpha);
}
