#include "Attenuation.h"

Attenuation::Attenuation(float constant, float linear, float exponent) :
    m_constant(constant),
    m_linear(linear),
    m_exponent(exponent)
{
}

void Attenuation::setConstant(float constant)
{
    m_constant = constant;
}

float Attenuation::getConstant() const
{
    return m_constant;
}

void Attenuation::setLinear(float linear) 
{ 
    m_linear = linear; 
}

float Attenuation::getLinear() const 
{ 
    return m_linear; 
}

void Attenuation::setExponent(float exponent)
{ 
    m_exponent = exponent;
}

float Attenuation::getExponent() const
{ 
    return m_exponent;
}

const glm::vec3 Attenuation::toVec3() const
{
	return glm::vec3(m_constant, m_linear, m_exponent);
}
