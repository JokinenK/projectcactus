#include <map>
#include <string>
#include "support/Singleton.h"

namespace SingletonStore
{

static std::map<std::string, void*> f_singletons;

void setInstance(const std::type_info& type, void* instance)
{
	f_singletons[type.name()] = instance;
}

void* getInstance(const std::type_info& type)
{
	auto iter = f_singletons.find(type.name());

	if (iter != f_singletons.end()) {
		return iter->second;
	}

	return 0;
}

} // namespace
