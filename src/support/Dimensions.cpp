/*
 * Dimensions.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include "Dimensions.h"

Dimensions::Dimensions()
{
	setDimensions(0, 0);
}

Dimensions::Dimensions(float x, float y)
{
	setDimensions(x, y);
}

Dimensions::~Dimensions()
{
}

Dimensions& Dimensions::operator=(const Dimensions& other)
{
	setDimensions(other.getWidth(), other.getHeight());
	return *this;
}

void Dimensions::setWidth(float width)
{
	m_width = width;
}

float Dimensions::getWidth() const
{
	return m_width;
}

void Dimensions::setHeight(float height)
{
	m_height = height;
}

float Dimensions::getHeight() const
{
	return m_height;
}

float Dimensions::getArea() const
{
	return getWidth() * getHeight();
}

void Dimensions::setDimensions(float width, float height)
{
	setWidth(width);
	setHeight(height);
}
