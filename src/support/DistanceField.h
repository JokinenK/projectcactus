/*
 * DistanceField.h
 *
 *  Created on: 29.9.2016
 *      Author: kalle
 */

#ifndef DISTANCEFIELD_H_
#define DISTANCEFIELD_H_

#include "support/Image.h"

class DistanceField {
public:
	static Image create(const Image& source, int spread = 5);
};

#endif /* DISTANCEFIELD_H_ */
