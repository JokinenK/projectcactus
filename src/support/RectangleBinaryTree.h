/*
 * BinaryTexturePacker.h
 *
 *  Created on: 30.9.2016
 *      Author: kalle
 */

#ifndef RECTANGLEBINARYTREE_H_
#define RECTANGLEBINARYTREE_H_

#include "support/Rectangle.h"

struct RectangleBinaryTree
{
public:
	RectangleBinaryTree(int width, int height);
	~RectangleBinaryTree();

	const Rectangle& fit(const Dimensions& dimensions);

private:
	RectangleBinaryTree(int x, int y, int width, int height);

	bool rectangleFits(const Dimensions& dimensions) const;
	RectangleBinaryTree* findSlot(const Dimensions& dimensions);
	const Rectangle& splitSlot(const Dimensions& dimensions);

	bool m_reserved;
	Rectangle m_rectangle;
	RectangleBinaryTree* m_downNode;
	RectangleBinaryTree* m_rightNode;

	static const Rectangle s_invalidRectangle;
};

#endif /* RECTANGLEBINARYTREE_H_ */
