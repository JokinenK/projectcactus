/*
 * Point.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef POINT_H_
#define POINT_H_

class Point
{
public:
	Point();
	Point(float x, float y);
	~Point();

	Point& operator=(const Point& other);

	Point operator-();

	void setX(float x);
	float getX() const;

	void setY(float y);
	float getY() const;

	void setPosition(float x, float y);

	void move(float dX, float dY);

private:
	float m_x;
	float m_y;
};

#endif /* POINT_H_ */
