/*
 * Rectangle.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include <glm/glm.hpp>
#include "Point.h"
#include "Dimensions.h"

class Rectangle
{
public:
	Rectangle();
	Rectangle(float left, float top, float width, float height);
	Rectangle(const Point& position, const Dimensions& dimensions);
	Rectangle(const Rectangle& other);
	~Rectangle();

	Rectangle& operator=(const Rectangle& other);

	const Point getPosition() const;
	const Dimensions getDimensions() const;
	Rectangle normalized() const;

	void setLeft(float left);
	float getLeft() const;

	void setRight(float right);
	float getRight() const;

	void setTop(float top);
	float getTop() const;

	void setBottom(float bottom);
	float getBottom() const;

	void adjustWidth(float deltaWidth);
	void setWidth(float width);
	float getWidth() const;

	void adjustHeight(float deltaHeight);
	void setHeight(float height);
	float getHeight() const;

	void setSize(const Dimensions& dimensions);
	void setSize(float width, float height);

	void move(const Point& delta);
	void move(float dx, float dy);

	void setPosition(const Point& position);
	void setPosition(float left, float bottom);

	void setDimensions(const Dimensions& dimensions);
	void setDimensions(float width, float height);

	float getArea() const;

	bool contains(float x, float y) const;
	bool contains(const Point& point) const;
	bool isValid() const;

	const glm::vec4 toVec4() const;

	static const Rectangle EMPTY;

private:
	float m_left;
	float m_top;
	float m_right;
	float m_bottom;
};

#endif /* RECTANGLE_H_ */
