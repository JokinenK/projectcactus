/*
 * MapIterator.h
 *
 *  Created on: 15.9.2016
 *      Author: kalle
 */

#ifndef MAPITERATOR_H_
#define MAPITERATOR_H_

#include <map>

template <class Key, class T>
class MapIterator
{
public:
	MapIterator(const std::map<Key, T>& map) :
		m_map(map),
		m_iter(m_map.begin()),
		m_value(m_iter)
	{
	}

	bool hasNext() const
	{
		return (m_iter != m_map.end());
	}

	bool hasPrev() const
	{
		return (m_iter != m_map.begin());
	}

	typename std::map<Key, T>::const_iterator next()
	{
		m_value = m_iter++;
		return m_iter;
	}

	typename std::map<Key, T>::const_iterator prev()
	{
		m_value = m_iter--;
		return m_iter;
	}

	void toBegin()
	{
		m_iter = m_map.begin();
		m_value = m_iter;
	}

	void toEnd()
	{
		m_iter = m_map.end();
		m_value = m_iter;
	}

	const Key& key() const
	{
		return m_value->first;
	}

	const T& value() const
	{
		return m_value->second;
	}

private:
	const std::map<Key, T>& m_map;
	typename std::map<Key, T>::const_iterator m_iter;
	typename std::map<Key, T>::const_iterator m_value;
};



#endif /* MAPITERATOR_H_ */
