/*
 * Material.cpp
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#include "core/Material.h"
#include "core/TextureManager.h"

const Material Material::EMPTY;

namespace
{
	static Material::Id getNextMaterialId()
	{
		static Material::Id nextMaterialId = 0;
		return nextMaterialId++;
	}
}

Material::Material() :
		m_materialId(INVALID_ID),
		m_shininess(0.0f),
		m_dissolve(1.0f)
{
	for (int i = 0; i < NUM_TEXTURES; i++) {
		TextureName textureName = static_cast<TextureName>(i);
		setTexture(textureName, 0);
	}
}

Material::Material(const Material& other)
{
	operator=(other);
}

Material::~Material()
{
}

Material& Material::operator=(const Material& other)
{
	m_materialName = other.getMaterialName();
	m_materialId = other.getMaterialId();
	m_ambientColor = other.getAmbientColor();
	m_diffuseColor = other.getDiffuseColor();
	m_specularColor = other.getSpecularColor();
	m_shininess = other.getShininess();
	m_dissolve = other.getDissolve();

	for (int i = 0; i < NUM_TEXTURES; i++) {
		TextureName textureName = static_cast<TextureName>(i);
		setTexture(textureName, other.getTexture(textureName));
	}

	return *this;
}

void Material::clear()
{
	clearMaterialName();
	clearMaterialId();
	clearAmbientColor();
	clearDiffuseColor();
	clearSpecularColor();
	clearShininess();
	clearDissolve();
	clearAmbientTexture();
	clearDiffuseTexture();
	clearSpecularTexture();
	clearShininessTexture();
	clearDissolveTexture();
	clearDisplacementTexture();
	clearDisplacementTexture();

	setMaterialId(INVALID_ID);
}

void Material::finalize()
{
	if (m_materialId == INVALID_ID) {
		m_materialId = getNextMaterialId();
	}
}

bool Material::isValid() const
{
	return (m_materialId != INVALID_ID);
}

void Material::setMaterialName(const std::string& materialName)
{
	m_materialName = materialName;
}

const std::string& Material::getMaterialName() const
{
	return m_materialName;
}

void Material::clearMaterialName()
{
	m_materialName.clear();
}

void Material::setMaterialId(Material::Id materialId)
{
	m_materialId = materialId;
}

Material::Id Material::getMaterialId() const
{
	return m_materialId;
}

void Material::clearMaterialId()
{
	m_materialId = Material::INVALID_ID;
}

Material* Material::setAmbientColor(const Color& ambientColor)
{
	m_ambientColor = ambientColor;
	return this;
}

const Color& Material::getAmbientColor() const
{
	return m_ambientColor;
}

void Material::clearAmbientColor()
{
	m_ambientColor.clear();
}

Material* Material::setDiffuseColor(const Color& diffuseColor)
{
	m_diffuseColor = diffuseColor;
	return this;
}

const Color& Material::getDiffuseColor() const
{
	return m_diffuseColor;
}

void Material::clearDiffuseColor()
{
	m_diffuseColor.clear();
}

Material* Material::setSpecularColor(const Color& specularColor)
{
	m_specularColor = specularColor;
	return this;
}

const Color& Material::getSpecularColor() const
{
	return m_specularColor;
}

void Material::clearSpecularColor()
{
	m_specularColor.clear();
}

Material* Material::setShininess(float shininess)
{
	m_shininess = shininess;
	return this;
}

float Material::getShininess() const
{
	return m_shininess;
}

void Material::clearShininess()
{
	m_shininess = 0;
}

Material* Material::setDissolve(float dissolve)
{
	m_dissolve = dissolve;
	return this;
}

float Material::getDissolve() const
{
	return m_dissolve;
}

void Material::clearDissolve()
{
	m_dissolve = 0;
}

bool Material::hasAmbientTexture() const
{
	return hasTexture(TEXTURE_AMBIENT);
}

Material* Material::setAmbientTexture(const std::string& ambientTexture)
{
	return setTexture(TEXTURE_AMBIENT, TextureManager::instance()->getTexture2D(ambientTexture));
}

ITexture* Material::getAmbientTexture() const
{
	return getTexture(TEXTURE_AMBIENT);
}

void Material::clearAmbientTexture()
{
	setTexture(TEXTURE_AMBIENT, 0);
}

bool Material::hasDiffuseTexture() const
{
	return hasTexture(TEXTURE_DIFFUSE);
}

Material* Material::setDiffuseTexture(const std::string& diffuseTexture)
{
	return setTexture(TEXTURE_DIFFUSE, TextureManager::instance()->getTexture2D(diffuseTexture));
}

ITexture* Material::getDiffuseTexture() const
{
	return getTexture(TEXTURE_DIFFUSE);
}

void Material::clearDiffuseTexture()
{
	setTexture(TEXTURE_DIFFUSE, 0);
}

bool Material::hasSpecularTexture() const
{
	return hasTexture(TEXTURE_SPECULAR);
}

Material* Material::setSpecularTexture(const std::string& specularTexture)
{
	return setTexture(TEXTURE_SPECULAR, TextureManager::instance()->getTexture2D(specularTexture));
}

ITexture* Material::getSpecularTexture() const
{
	return getTexture(TEXTURE_SPECULAR);
}

void Material::clearSpecularTexture()
{
	setTexture(TEXTURE_SPECULAR, 0);
}

bool Material::hasShininessTexture() const
{
	return hasTexture(TEXTURE_SHININESS);
}

Material* Material::setShininessTexture(const std::string& shininessTexture)
{
	return setTexture(TEXTURE_SHININESS, TextureManager::instance()->getTexture2D(shininessTexture));
}

ITexture* Material::getShininessTexture() const
{
	return getTexture(TEXTURE_SHININESS);
}

void Material::clearShininessTexture()
{
	setTexture(TEXTURE_SHININESS, 0);
}

bool Material::hasDissolveTexture() const
{
	return hasTexture(TEXTURE_DISSOLVE);
}

Material* Material::setDissolveTexture(const std::string& dissolveTexture)
{
	return setTexture(TEXTURE_DISSOLVE, TextureManager::instance()->getTexture2D(dissolveTexture));
}

ITexture* Material::getDissolveTexture() const
{
	return getTexture(TEXTURE_DISSOLVE);
}

void Material::clearDissolveTexture()
{
	setTexture(TEXTURE_DISSOLVE, 0);
}

bool Material::hasNormalTexture() const
{
	return hasTexture(TEXTURE_NORMAL);
}

Material* Material::setNormalTexture(const std::string& normalTexture)
{
	return setTexture(TEXTURE_NORMAL, TextureManager::instance()->getTexture2D(normalTexture));
}

ITexture* Material::getNormalTexture() const
{
	return getTexture(TEXTURE_NORMAL);
}

void Material::clearNormalTexture()
{
	setTexture(TEXTURE_NORMAL, 0);
}

bool Material::hasDisplacementTexture() const
{
	return hasTexture(TEXTURE_DISPLACEMENT);
}

Material* Material::setDisplacementTexture(const std::string& displacementTexture)
{
	return setTexture(TEXTURE_DISPLACEMENT, TextureManager::instance()->getTexture2D(displacementTexture));
}

ITexture* Material::getDisplacementTexture() const
{
	return getTexture(TEXTURE_DISPLACEMENT);
}

void Material::clearDisplacementTexture()
{
	setTexture(TEXTURE_DISPLACEMENT, 0);
}

bool Material::hasTexture(TextureName textureName) const
{
	return (m_textures[textureName] != 0);
}

Material* Material::setTexture(TextureName textureName, ITexture* texture)
{
	m_textures[textureName] = texture;
	return this;
}

ITexture* Material::getTexture(TextureName textureName) const
{
	return m_textures[textureName];
}
