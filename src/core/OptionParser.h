/*
 * OptionParser.h
 *
 *  Created on: 23.3.2017
 *      Author: kalle
 */

#ifndef OPTIONPARSER_H_
#define OPTIONPARSER_H_

#include <vector>
#include <string>
#include <algorithm>
#include "support/Utils.h"

class OptionParser {
public:
	OptionParser(int argc, char** argv)
	{
		/* Skip the first argument that is called path */
		for (int i = 1; i < argc; i++) {
			m_tokens.push_back(argv[i]);
		}
	}

	virtual ~OptionParser()
	{

	}

	bool isDefined(const std::string& key)
	{
		auto iter = std::find(m_tokens.begin(), m_tokens.end(), key);

		if (iter == m_tokens.end()) {
			return false;
		}

		return true;
	}

	template <class T>
	T readIndex(int index, T fallback)
	{
		auto iter = m_tokens.begin();
		std::advance(iter, index);

		if (iter != m_tokens.end()) {
			T result;

			if (Utils::fromString<T>(*iter, result)) {
				return result;
			}
		}

		return fallback;
	}

	template <class T>
	T readValue(const std::string& key, T fallback)
	{
		auto iter = getValueIterator(key);

		if (iter != m_tokens.end()) {
			T result;

			if (Utils::fromString<T>(*iter, result)) {
				return result;
			}
		}

		return fallback;
	}

	ssize_t numTokens() const
	{
		return m_tokens.size();
	}

private:
	std::vector<std::string>::const_iterator getValueIterator(
			const std::string& key) const
	{
		auto iter = std::find(m_tokens.begin(), m_tokens.end(), key);

		if (iter != m_tokens.end()) {
			std::advance(iter, 1);
		}

		return iter;
	}

	std::vector<std::string> m_tokens;
};

#endif /* OPTIONPARSER_H_ */
