#ifndef IRENDERER_H_
#define IRENDERER_H_

#include "IFramebuffer.h"

class IShader;
class ITexture;
class IFramebuffer;
class IRendertarget;
class IModel;

class IRenderer
{
public:
	typedef enum {
		ZERO,
		ONE,
		SRC_COLOR,
		ONE_MINUS_SRC_COLOR,
		DST_COLOR,
		ONE_MINUS_DST_COLOR,
		SRC_ALPHA,
		ONE_MINUS_SRC_ALPHA,
		DST_ALPHA,
		ONE_MINUS_DST_ALPHA,
		CONSTANT_COLOR,
		ONE_MINUS_CONSTANT_COLOR,
		CONSTANT_ALPHA,
		ONE_MINUS_CONSTANT_ALPHA,
		SRC_ALPHA_SATURATE,
		SRC1_COLOR,
		ONE_MINUS_SRC1_COLOR,
		SRC1_ALPHA,
		ONE_MINUS_SRC1_ALPHA,
		NUM_BLEND_FUNCS
	} BlendFunc;

	typedef enum {
		FUNC_ADD,
		NUM_BLEND_EQUATIONS
	} BlendEquation;

	typedef enum {
		NEVER,
		LESS,
		EQUAL,
		LEQUAL,
		GREATER,
		NOTEQUAL,
		GEQUAL,
		ALWAYS,
		NUM_FUNC_TYPES
	} FuncType;

	typedef enum {
		 FRONT,
		 BACK,
		 NUM_CULL_FACES
	} CullFace;

    virtual ~IRenderer() {};

    virtual void clear() = 0;

    virtual void setClearColor(float r, float g, float b) = 0;

	virtual void setBlendFunc(BlendFunc blendFuncSrc, BlendFunc blendFuncDst) = 0;
    virtual BlendFunc getBlendFuncSrc() const = 0;
    virtual BlendFunc getBlendFuncDst() const = 0;

    virtual void setBlendEquation(BlendEquation blendEquation) = 0;
    virtual BlendEquation getBlendEquation() const = 0;

    virtual void setDepthFunc(FuncType depthFunc) = 0;
    virtual FuncType getDepthFunc() const = 0;

    virtual void setAlphaFunc(FuncType alphaFunc, float value) = 0;
    virtual FuncType getAlphaFunc() const = 0;

    virtual void setCullFace(CullFace cullFace) = 0;
	virtual CullFace getCullFace() const = 0;

    virtual void setScissorRectangle(float left, float top, float width, float height) = 0;
    virtual void getScissorRectangle(float& left, float& top, float& width, float& height) const = 0;

    virtual void setDepthMask(bool enable) = 0;

    virtual bool isDepthTestEnabled() const = 0;
    virtual void setDepthTestEnabled(bool enabled) = 0;

    virtual bool isDepthClampEnabled() const = 0;
    virtual void setDepthClampEnabled(bool enabled) = 0;

    virtual bool isBlendEnabled() const = 0;
    virtual void setBlendEnabled(bool enabled) = 0;

    virtual bool isAlphaTestEnabled() const = 0;
    virtual void setAlphaTestEnabled(bool enabled) = 0;

    virtual bool isScissorTestEnabled() const = 0;
    virtual void setScissorTestEnabled(bool enabled) = 0;

    virtual bool isCullFaceEnabled() const = 0;
    virtual void setCullFaceEnabled(bool enabled) = 0;

    virtual IShader* createShader() const = 0;
    virtual void releaseShader(IShader* shader) = 0;

    virtual ITexture* createTexture() const = 0;
    virtual void releaseTexture(ITexture* texture) = 0;

    virtual IModel* createModel() const = 0;
    virtual void releaseModel(IModel* model) = 0;

    virtual IFramebuffer* createFramebuffer() const = 0;
    virtual void releaseFramebuffer(IFramebuffer* framebuffer) = 0;

    virtual void setViewPort(int width, int height) = 0;
    virtual void getViewPort(int& width, int& height) const = 0;
};

#endif // IRENDERER_H_

