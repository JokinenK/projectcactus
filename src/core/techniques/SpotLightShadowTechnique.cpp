/*
 * SpotLightShadowTechnique.cpp
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#include <glm/gtx/transform.hpp>
#include "SpotLightShadowTechnique.h"
#include "core/IRenderer.h"
#include "core/IUniform.h"
#include "core/IFramebuffer.h"
#include "core/ITexture.h"
#include "core/RenderQueue.h"
#include "core/Camera.h"
#include "core/light/SpotLight.h"
#include "support/Utils.h"

SpotLightShadowTechnique::SpotLightShadowTechnique(IRenderer* renderer, float shadowTextureSize) :
		m_renderer(renderer),
		m_framebuffer(renderer->createFramebuffer()),
		m_texture(renderer->createTexture()),
		m_shadowTextureSize(shadowTextureSize),
		m_shader(
				"../res/shaders/shadow-spotlight.vs",
				"../res/shaders/shadow-spotlight.fs"),
		m_uniformMVP(0)
{
	m_framebuffer->init(shadowTextureSize, shadowTextureSize);

	m_texture->initTexture(shadowTextureSize, shadowTextureSize, ITexture::UNSIGNED_INT, ITexture::TEXTURE_2D);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24);

	m_texture->setWrapS(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapT(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapR(ITexture::CLAMP_TO_EDGE);
	m_texture->setFilterMin(ITexture::LINEAR);
	m_texture->setFilterMag(ITexture::LINEAR);

	m_framebuffer->attachRenderTarget(m_texture, IFramebuffer::DEPTH_ATTACHMENT);

    IFramebuffer::Attachment drawAttachments[] = { IFramebuffer::NONE };
    m_framebuffer->drawBuffers(drawAttachments, 1);

    m_uniformMVP = m_shader.getUniform("uniformMVP");
}

SpotLightShadowTechnique::~SpotLightShadowTechnique()
{
	m_renderer->releaseFramebuffer(m_framebuffer);
	m_renderer->releaseTexture(m_texture);
}

void SpotLightShadowTechnique::bind()
{
	m_shader.bind();
	m_framebuffer->bindWriteBuffer();

	m_renderer->setViewPort(m_shadowTextureSize, m_shadowTextureSize);
	m_renderer->setDepthMask(true);
	m_renderer->setDepthClampEnabled(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setBlendEnabled(false);
	m_renderer->setCullFace(IRenderer::BACK);
}

void SpotLightShadowTechnique::unbind()
{
	m_shader.unbind();
	m_framebuffer->unbindWriteBuffer();

	m_renderer->setDepthMask(false);
	m_renderer->setDepthClampEnabled(false);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setBlendEnabled(true);
	m_renderer->setCullFace(IRenderer::BACK);
}

void SpotLightShadowTechnique::render(RenderQueue& queue)
{
	m_framebuffer->clear();

	for (auto iter = queue.begin(); iter != queue.end(); ++iter) {
		const RenderQueue::Value& entry = iter->second;
		const glm::mat4& model = entry.transform->getTransformation();

		m_uniformMVP->set(m_lightViewProjection * model);

		entry.model->render();
	}
}

void SpotLightShadowTechnique::updateLightMatrices(const Camera& camera, const SpotLight& light)
{
	const glm::mat4& lightProjection = light.getProjection();
	const glm::vec3& lightPosition = light.getPosition();
	const glm::vec3& lightDirection = light.getDirection();

	glm::mat4 lightView = glm::lookAt(lightPosition, lightPosition + lightDirection, Camera::Up);

	m_lightViewProjection = lightProjection * lightView;
	m_shadowMatrix = camera.calculateShadowMatrix(m_lightViewProjection, Camera::SHADOW_PERSPECTIVE);
}

const glm::mat4& SpotLightShadowTechnique::getShadowMatrix() const
{
	return m_shadowMatrix;
}

ITexture* SpotLightShadowTechnique::getTexture() const
{
	return m_texture;
}

