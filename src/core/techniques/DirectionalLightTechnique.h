/*
 * LightTechnique.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTTECHNIQUE_H_

#include <vector>
#include "GBufferTechnique.h"
#include "DirectionalLightShadowTechnique.h"
#include "core/ITechnique.h"
#include "core/IUniform.h"
#include "core/IRenderer.h"
#include "core/Pipeline.h"
#include "core/BaseShader.h"
#include "core/uniform/UniformDirectionalLight.h"
#include "core/uniform/UniformPipeline.h"
#include "core/uniform/UniformCamera.h"

class Camera;
class DirectionalLight;

class DirectionalLightTechnique : public ITechnique
{
public:
	DirectionalLightTechnique(IRenderer* renderer, float screenWidth, float screenHeight, GBufferTechnique& gBufferTechnique, DirectionalLightShadowTechnique& shadowTechnique);
	virtual ~DirectionalLightTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void updateScreenSize(const glm::vec2& screenSize);
	void updateCamera(const Camera& camera);
	void updateDirectionalLight(const DirectionalLight& light);

private:
	IRenderer* m_renderer;

	float m_screenWidth;
	float m_screenHeight;
	int m_nextSampler;

	BaseShader m_shader;
	Pipeline m_pipeline;

	IUniform* m_uniformShadowMatrices;
	IUniform* m_uniformCascadeLengths;

	IUniform* m_uniformSamplerDiffuse;
	IUniform* m_uniformSamplerNormal;
	IUniform* m_uniformSamplerDepth;
	IUniform* m_uniformSamplerShadow;

	IUniform* m_uniformScreenSize;

	UniformDirectionalLight m_uniformDirectionalLight;
	UniformPipeline m_uniformPipeline;
	UniformCamera m_uniformCamera;

	GBufferTechnique& m_gBufferTechnique;
	DirectionalLightShadowTechnique& m_shadowTechnique;

	std::vector<int> m_shadowSamplers;
};

#endif /* PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTTECHNIQUE_H_ */
