#include "PointLightTechnique.h"
#include "core/RenderQueue.h"
#include "core/IUniform.h"
#include "core/IRenderer.h"
#include "core/Camera.h"

PointLightTechnique::PointLightTechnique(
		IRenderer* renderer,
		float screenWidth,
		float screenHeight,
		GBufferTechnique& gBufferTechnique,
		PointLightShadowTechnique& shadowTechnique) :
				m_renderer(renderer),
				m_screenWidth(screenWidth),
				m_screenHeight(screenHeight),
				m_nextSampler(0),
				m_shader(
						"../res/shaders/deferred-point.vs",
						"../res/shaders/deferred-point.fs"),
				m_gBufferTechnique(gBufferTechnique),
				m_shadowTechnique(shadowTechnique)
{
	m_uniformSamplerDiffuse = m_shader.getUniform("uniformSamplerDiffuse");
	m_uniformSamplerNormal = m_shader.getUniform("uniformSamplerNormal");
	m_uniformSamplerDepth = m_shader.getUniform("uniformSamplerDepth");

	m_uniformSamplerShadow = m_shader.getUniform("uniformSamplerShadow");
	m_uniformShadowMatrices = m_shader.getUniform("uniformShadowMatrices");

	m_uniformScreenSize = m_shader.getUniform("uniformScreenSize");

	m_uniformPointLight = UniformPointLight(&m_shader, "uniformPointLight");
	m_uniformPipeline = UniformPipeline(&m_shader, "uniformPipeline");
	m_uniformCamera = UniformCamera(&m_shader, "uniformCamera");

	m_shader.bind();

	m_uniformSamplerDiffuse->set(0);
	m_uniformSamplerNormal->set(1);
	m_uniformSamplerDepth->set(2);

	m_shader.unbind();
}

PointLightTechnique::~PointLightTechnique()
{
}

void PointLightTechnique::bind()
{
	m_shader.bind();

	m_renderer->setViewPort(m_screenWidth, m_screenHeight);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setDepthMask(false);
	m_renderer->setBlendEnabled(true);
	m_renderer->setCullFaceEnabled(true);

	m_renderer->setBlendEquation(IRenderer::FUNC_ADD);
	m_renderer->setBlendFunc(IRenderer::ONE, IRenderer::ONE);
	m_renderer->setCullFace(IRenderer::FRONT);

	for (ITexture* gBufferTexture : m_gBufferTechnique.getTextures()) {
		gBufferTexture->bind(m_nextSampler++);
	}

	int sampler = m_nextSampler++;
	m_shadowTechnique.getTexture()->bind(sampler);
	m_uniformSamplerShadow->set(sampler);
}

void PointLightTechnique::unbind()
{
	m_shader.unbind();

    m_renderer->setDepthMask(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setCullFace(IRenderer::BACK);

	for (ITexture* gBufferTexture : m_gBufferTechnique.getTextures()) {
		gBufferTexture->unbind(--m_nextSampler);
	}

	m_shadowTechnique.getTexture()->unbind(--m_nextSampler);
}

void PointLightTechnique::render(RenderQueue& queue)
{
	for (auto iter = queue.begin(); iter != queue.end(); iter++) {
		const RenderQueue::Value& entry = iter->second;

		m_pipeline.setModel(entry.transform->getTransformation());
		m_uniformPipeline.set(m_pipeline);

		entry.model->render();
	}
}

void PointLightTechnique::setPipeline(const Pipeline& pipeline)
{
	m_pipeline = pipeline;
}

void PointLightTechnique::updateCamera(const Camera& camera)
{
	m_pipeline.setView(camera.getView());
	m_pipeline.setProjection(camera.getProjection());

	m_uniformCamera.set(camera);
}

void PointLightTechnique::updateScreenSize(const glm::vec2& screenSize)
{
	m_uniformScreenSize->set(screenSize);
}

void PointLightTechnique::updatePointLight(const PointLight& light)
{
	m_uniformPointLight.set(light);
}

void PointLightTechnique::updateShadowMatrices(const std::vector<glm::mat4>& matrices)
{
	m_uniformShadowMatrices->set(matrices);
}
