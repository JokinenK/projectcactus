/*
 * GBufferTechnique.cpp
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#include "GBufferTechnique.h"
#include "core/IModel.h"
#include "core/IUniform.h"
#include "core/Material.h"
#include "core/MaterialManager.h"
#include "core/RenderQueue.h"
#include "core/Camera.h"
#include "support/Utils.h"

GBufferTechnique::GBufferTechnique(IRenderer* renderer, int screenWidth, int screenHeight) :
		m_screenWidth(screenWidth),
		m_screenHeight(screenHeight),
		m_renderer(renderer),
		m_framebuffer(renderer->createFramebuffer()),
		m_shader(
				"../res/shaders/deferred-geometry.vs",
				"../res/shaders/deferred-geometry.fs")
{
	m_textures.resize(NUM_TEXTURES);

	m_framebuffer->init(screenWidth, screenHeight);

	// Diffuse and specular intensity
	m_textures[TEXTURE_DIFFUSE] = renderer->createTexture();
	m_textures[TEXTURE_DIFFUSE]->initTexture(screenWidth, screenHeight, ITexture::UNSIGNED_BYTE);
	m_textures[TEXTURE_DIFFUSE]->setTextureData(0, ITexture::RGBA, ITexture::RGBA);
	setWrapAndFilter(TEXTURE_DIFFUSE);

    // Normals and specular power
	m_textures[TEXTURE_NORMAL] = renderer->createTexture();
	m_textures[TEXTURE_NORMAL]->initTexture(screenWidth, screenHeight, ITexture::UNSIGNED_BYTE);
	m_textures[TEXTURE_NORMAL]->setTextureData(0, ITexture::RGBA, ITexture::RGBA);
	setWrapAndFilter(TEXTURE_NORMAL);

    // Depth texture
    m_textures[TEXTURE_DEPTH] = renderer->createTexture();
    m_textures[TEXTURE_DEPTH]->initTexture(screenWidth, screenHeight, ITexture::UNSIGNED_INT_24_8);
    m_textures[TEXTURE_DEPTH]->setTextureData(0, ITexture::DEPTH_STENCIL, ITexture::DEPTH24_STENCIL8);
    setWrapAndFilter(TEXTURE_DEPTH);

    m_framebuffer->attachRenderTarget(m_textures[TEXTURE_DIFFUSE], IFramebuffer::COLOR_ATTACHMENT0);
    m_framebuffer->attachRenderTarget(m_textures[TEXTURE_NORMAL], IFramebuffer::COLOR_ATTACHMENT1);
    m_framebuffer->attachRenderTarget(m_textures[TEXTURE_DEPTH], IFramebuffer::DEPTH_ATTACHMENT);

    IFramebuffer::Attachment drawAttachmentsGBuffer[] = { IFramebuffer::COLOR_ATTACHMENT0, IFramebuffer::COLOR_ATTACHMENT1 };
    m_framebuffer->drawBuffers(drawAttachmentsGBuffer, ARRAY_SIZE(drawAttachmentsGBuffer));

	m_uniformMVP = m_shader.getUniform("uniformMVP");
	m_uniformScreenSize = m_shader.getUniform("uniformScreenSize");
	m_uniformNormalMatrix = m_shader.getUniform("uniformNormalMatrix");

	// Wrapper for multiple uniforms, can't use basic getUniform
	m_uniformMaterial = UniformMaterial(&m_shader, "uniformMaterial");
}

GBufferTechnique::~GBufferTechnique()
{
	for (int i = 0; i < NUM_TEXTURES; ++i) {
		m_renderer->releaseTexture(m_textures[i]);
	}

	m_renderer->releaseFramebuffer(m_framebuffer);
}

void GBufferTechnique::bind()
{
	m_shader.bind();
	m_framebuffer->bindWriteBuffer();
	m_framebuffer->clear();

	m_renderer->setViewPort(m_screenWidth, m_screenHeight);
	m_renderer->setDepthMask(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setBlendEnabled(false);
}

void GBufferTechnique::unbind()
{
	m_shader.unbind();
	m_framebuffer->unbindWriteBuffer();

	m_renderer->setDepthMask(false);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setBlendEnabled(true);
}

void GBufferTechnique::render(RenderQueue& queue)
{
	for (auto iter = queue.begin(); iter != queue.end(); ++iter) {
		const RenderQueue::Value& entry = iter->second;

		prepareMaterial(entry.material);

		m_pipeline.setModel(entry.transform->getTransformation());

		m_uniformMVP->set(m_pipeline.getMVP());
		m_uniformNormalMatrix->set(m_pipeline.getNormalMatrix());


		entry.model->render();

		releaseMaterial(entry.material);
	}
}

void GBufferTechnique::copyDepthToActiveFramebuffer()
{
	m_framebuffer->copyDepthToActiveFramebuffer();
}

const std::vector<ITexture*>& GBufferTechnique::getTextures() const
{
	return m_textures;
}

ITexture* GBufferTechnique::getTexture(TextureType textureType) const
{
	return m_textures[textureType];
}

void GBufferTechnique::updateCamera(const Camera& camera)
{
	m_pipeline.setView(camera.getView());
	m_pipeline.setProjection(camera.getProjection());
}

void GBufferTechnique::updateScreenSize(const glm::vec2& screenSize)
{
	m_uniformScreenSize->set(screenSize);
}

void GBufferTechnique::prepareMaterial(const Material* material)
{
	if (!material || !material->isValid()) {
		return;
	}

	for (int i = Material::FIRST_TEXTURE; i < Material::NUM_TEXTURES; ++i) {
		int textureUnit = i;
		Material::TextureName textureName = static_cast<Material::TextureName>(i);

		if (material->hasTexture(textureName)) {
			material->getTexture(textureName)->bind(textureUnit);
		}
	}

	m_uniformMaterial.set(*material);
}

void GBufferTechnique::releaseMaterial(const Material* material)
{
	if (!material || !material->isValid()) {
		return;
	}

	for (int i = Material::FIRST_TEXTURE; i < Material::NUM_TEXTURES; ++i) {
		int textureUnit = i;
		Material::TextureName textureName = static_cast<Material::TextureName>(i);

		if (material->hasTexture(textureName)) {
			material->getTexture(textureName)->unbind(textureUnit);
		}
	}
}

void GBufferTechnique::setWrapAndFilter(TextureType textureType)
{
    m_textures[textureType]->setWrapS(ITexture::CLAMP_TO_EDGE);
    m_textures[textureType]->setWrapT(ITexture::CLAMP_TO_EDGE);
    m_textures[textureType]->setWrapR(ITexture::CLAMP_TO_EDGE);
    m_textures[textureType]->setFilterMin(ITexture::LINEAR);
    m_textures[textureType]->setFilterMag(ITexture::LINEAR);
}
