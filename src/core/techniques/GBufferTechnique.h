/*
 * GBufferTechnique.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_GBUFFERTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_GBUFFERTECHNIQUE_H_

#include <glm/glm.hpp>
#include "core/IRenderer.h"
#include "core/ITechnique.h"
#include "core/IUniform.h"
#include "core/BaseShader.h"
#include "core/Pipeline.h"
#include "core/uniform/UniformMaterial.h"

class Camera;

class GBufferTechnique : public ITechnique
{
public:
	typedef enum {
		FIRST_TEXTURE,
		TEXTURE_DIFFUSE = FIRST_TEXTURE,
		TEXTURE_NORMAL,
		TEXTURE_DEPTH,
		NUM_TEXTURES
	} TextureType;

	GBufferTechnique(IRenderer* renderer, int screenWidth, int screenHeight);
	virtual ~GBufferTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void copyDepthToActiveFramebuffer();

	const std::vector<ITexture*>& getTextures() const;
	ITexture* getTexture(TextureType textureType) const;

	void updateCamera(const Camera& camera);
	void updateScreenSize(const glm::vec2& screenSize);

private:
	void setWrapAndFilter(TextureType textureType);
	void prepareMaterial(const Material* material);
	void releaseMaterial(const Material* material);

	int m_screenWidth;
	int m_screenHeight;

	IRenderer* m_renderer;
	IFramebuffer* m_framebuffer;
	std::vector<ITexture*> m_textures;

	Pipeline m_pipeline;
	BaseShader m_shader;

	IUniform* m_uniformMVP;
	IUniform* m_uniformScreenSize;
	IUniform* m_uniformNormalMatrix;

	UniformMaterial m_uniformMaterial;
};

#endif /* PROJECTCACTUS_SRC_CORE_TECHNIQUES_GBUFFERTECHNIQUE_H_ */
