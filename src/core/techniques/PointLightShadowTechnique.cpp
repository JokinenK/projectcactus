/*
 * PointLightShadowTechnique.cpp
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#include <glm/gtx/transform.hpp>
#include "PointLightShadowTechnique.h"
#include "core/IRenderer.h"
#include "core/IFramebuffer.h"
#include "core/ITexture.h"
#include "core/RenderQueue.h"
#include "core/Camera.h"
#include "core/light/PointLight.h"
#include "support/Utils.h"

PointLightShadowTechnique::PointLightShadowTechnique(IRenderer* renderer, float shadowTextureSize) :
		m_renderer(renderer),
		m_framebuffer(renderer->createFramebuffer()),
		m_texture(renderer->createTexture()),
		m_shadowTextureSize(shadowTextureSize),
		m_shader(
				"../res/shaders/shadow-pointlight.vs",
				"../res/shaders/shadow-pointlight.fs",
				"../res/shaders/shadow-pointlight.gs"),
		m_uniformModel(0),
		m_uniformShadowMatrices(0)
{
	m_framebuffer->init(shadowTextureSize, shadowTextureSize);

	m_shadowMatrices.resize(NUM_DIRECTIONS);

	m_texture->initTexture(shadowTextureSize, shadowTextureSize, ITexture::UNSIGNED_INT, ITexture::TEXTURE_CUBE_MAP);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_POSITIVE_X);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_NEGATIVE_X);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_POSITIVE_Y);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_NEGATIVE_Y);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_POSITIVE_Z);
	m_texture->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24, ITexture::TEXTURE_CUBE_MAP_NEGATIVE_Z);

	m_texture->setWrapS(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapT(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapR(ITexture::CLAMP_TO_EDGE);
	m_texture->setFilterMin(ITexture::LINEAR);
	m_texture->setFilterMag(ITexture::LINEAR);

	m_framebuffer->attachRenderTarget(m_texture, IFramebuffer::DEPTH_ATTACHMENT);

    IFramebuffer::Attachment drawAttachments[] = { IFramebuffer::NONE };
    m_framebuffer->drawBuffers(drawAttachments, 1);

    m_uniformModel = m_shader.getUniform("uniformModel");
    m_uniformShadowMatrices = m_shader.getUniform("uniformShadowMatrices");

    m_uniformCamera = UniformCamera(&m_shader, "uniformCamera");
    m_uniformPointLight = UniformPointLight(&m_shader, "uniformPointLight");
}

PointLightShadowTechnique::~PointLightShadowTechnique()
{
	m_renderer->releaseFramebuffer(m_framebuffer);
	m_renderer->releaseTexture(m_texture);
}

void PointLightShadowTechnique::bind()
{
	m_shader.bind();
	m_framebuffer->bindWriteBuffer();

	m_renderer->setViewPort(m_shadowTextureSize, m_shadowTextureSize);
	m_renderer->setDepthMask(true);
	m_renderer->setDepthClampEnabled(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setBlendEnabled(false);
	m_renderer->setCullFace(IRenderer::BACK);
}

void PointLightShadowTechnique::unbind()
{
	m_shader.unbind();
	m_framebuffer->unbindWriteBuffer();

	m_renderer->setDepthMask(false);
	m_renderer->setDepthClampEnabled(false);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setBlendEnabled(true);
	m_renderer->setCullFace(IRenderer::BACK);
}

void PointLightShadowTechnique::render(RenderQueue& queue)
{
	m_framebuffer->clear();

	for (auto iter = queue.begin(); iter != queue.end(); ++iter) {
		const RenderQueue::Value& entry = iter->second;
		const glm::mat4& model = entry.transform->getTransformation();

		m_uniformModel->set(model);
		m_uniformShadowMatrices->set(m_shadowMatrices);

		entry.model->render();
	}
}

void PointLightShadowTechnique::updateLightMatrices(const Camera& camera, const PointLight& light)
{
	UNUSED(camera);

	const glm::vec3& lightPosition = light.getPosition();
	const glm::mat4& lightProjection = light.getProjection();

	m_shadowMatrices[0] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Right,    Camera::Down);
	m_shadowMatrices[1] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Left,     Camera::Down);
	m_shadowMatrices[2] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Up,       Camera::Backward);
	m_shadowMatrices[3] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Down,     Camera::Forward);
	m_shadowMatrices[4] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Backward, Camera::Down);
	m_shadowMatrices[5] = lightProjection * glm::lookAt(lightPosition, lightPosition + Camera::Forward,  Camera::Down);
}

void PointLightShadowTechnique::updateCamera(const Camera& camera)
{
	m_uniformCamera.set(camera);
}

void PointLightShadowTechnique::updatePointLight(const PointLight& light)
{
	m_uniformPointLight.set(light);
}

const std::vector<glm::mat4>& PointLightShadowTechnique::getShadowMatrices() const
{
	return m_shadowMatrices;
}

ITexture* PointLightShadowTechnique::getTexture() const
{
	return m_texture;
}

