#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_SPOTLIGHTTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_SPOTLIGHTTECHNIQUE_H_

#include "GBufferTechnique.h"
#include "SpotLightShadowTechnique.h"
#include "core/ITechnique.h"
#include "core/IUniform.h"
#include "core/Pipeline.h"
#include "core/BaseShader.h"
#include "core/uniform/UniformSpotLight.h"
#include "core/uniform/UniformPipeline.h"
#include "core/uniform/UniformCamera.h"

class IRenderer;
class SpotLight;
class Camera;

class SpotLightTechnique : public ITechnique
{
public:
	SpotLightTechnique(
			IRenderer* renderer,
			float screenWidth,
			float screenHeight,
			GBufferTechnique& gBufferTechnique,
			SpotLightShadowTechnique& shadowTechnique);
	virtual ~SpotLightTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void updateScreenSize(const glm::vec2& screenSize);
	void updateCamera(const Camera& camera);
	void updateSpotLight(const SpotLight& light);

protected:
	IRenderer* m_renderer;

	float m_screenWidth;
	float m_screenHeight;
	int m_nextSampler;

	BaseShader m_shader;
	Pipeline m_pipeline;

	IUniform* m_uniformSamplerDiffuse;
	IUniform* m_uniformSamplerNormal;
	IUniform* m_uniformSamplerDepth;
	IUniform* m_uniformSamplerShadow;

	IUniform* m_uniformShadowMatrix;

	IUniform* m_uniformScreenSize;

	UniformSpotLight m_uniformSpotLight;
	UniformPipeline m_uniformPipeline;
	UniformCamera m_uniformCamera;

	GBufferTechnique& m_gBufferTechnique;
	SpotLightShadowTechnique& m_shadowTechnique;
};

#endif /* PROJECTCACTUS_SRC_CORE_TECHNIQUES_SPOTLIGHTTECHNIQUE_H_ */
