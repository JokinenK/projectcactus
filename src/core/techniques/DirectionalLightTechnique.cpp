/*
 * LightTechnique.cpp
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#include <sstream>
#include "DirectionalLightTechnique.h"
#include "core/light/DirectionalLight.h"
#include "core/IUniform.h"
#include "core/Camera.h"
#include "core/RenderQueue.h"

DirectionalLightTechnique::DirectionalLightTechnique(
		IRenderer* renderer,
		float screenWidth,
		float screenHeight,
		GBufferTechnique& gBufferTechnique,
		DirectionalLightShadowTechnique& shadowTechnique) :
				m_renderer(renderer),
				m_screenWidth(screenWidth),
				m_screenHeight(screenHeight),
				m_nextSampler(0),
				m_shader(
						"../res/shaders/deferred-directional.vs",
						"../res/shaders/deferred-directional.fs"),
				m_gBufferTechnique(gBufferTechnique),
				m_shadowTechnique(shadowTechnique)
{
	m_uniformShadowMatrices = m_shader.getUniform("uniformShadowMatrices");
	m_uniformSamplerShadow = m_shader.getUniform("uniformSamplerShadow");
	m_uniformCascadeLengths = m_shader.getUniform("uniformCascadeLengths");

	m_uniformSamplerDiffuse = m_shader.getUniform("uniformSamplerDiffuse");
	m_uniformSamplerNormal = m_shader.getUniform("uniformSamplerNormal");
	m_uniformSamplerDepth = m_shader.getUniform("uniformSamplerDepth");
	m_uniformScreenSize = m_shader.getUniform("uniformScreenSize");

	m_uniformDirectionalLight = UniformDirectionalLight(&m_shader, "uniformDirectionalLight");
	m_uniformPipeline = UniformPipeline(&m_shader, "uniformPipeline");
	m_uniformCamera = UniformCamera(&m_shader, "uniformCamera");

	m_shader.bind();

	m_uniformSamplerDiffuse->set(0);
	m_uniformSamplerNormal->set(1);
	m_uniformSamplerDepth->set(2);

	m_shader.unbind();
}

DirectionalLightTechnique::~DirectionalLightTechnique()
{
}

void DirectionalLightTechnique::bind()
{
	m_shader.bind();

	m_renderer->setViewPort(m_screenWidth, m_screenHeight);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setDepthMask(false);
	m_renderer->setBlendEnabled(true);
	m_renderer->setCullFaceEnabled(true);

	m_renderer->setBlendEquation(IRenderer::FUNC_ADD);
	m_renderer->setBlendFunc(IRenderer::ONE, IRenderer::ONE);
	m_renderer->setCullFace(IRenderer::BACK);

	m_uniformShadowMatrices->set(m_shadowTechnique.getShadowMatrices());

	for (ITexture* gBufferTexture : m_gBufferTechnique.getTextures()) {
		gBufferTexture->bind(m_nextSampler++);
	}

	for (ITexture* cascadeTexture : m_shadowTechnique.getTextures()) {
		int sampler = m_nextSampler++;

		m_shadowSamplers.push_back(sampler);
		cascadeTexture->bind(sampler);
	}

	m_uniformSamplerShadow->set(m_shadowSamplers);
}

void DirectionalLightTechnique::unbind()
{
	m_shader.unbind();

    m_renderer->setDepthMask(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setCullFace(IRenderer::BACK);

	for (ITexture* gBufferTexture : m_gBufferTechnique.getTextures()) {
		gBufferTexture->unbind(--m_nextSampler);
	}

	for (ITexture* cascadeTexture : m_shadowTechnique.getTextures()) {
		cascadeTexture->unbind(--m_nextSampler);
	}

	m_shadowSamplers.clear();
}

void DirectionalLightTechnique::render(RenderQueue& queue)
{
	for (auto iter = queue.begin(); iter != queue.end(); iter++) {
		const RenderQueue::Value& entry = iter->second;

		m_pipeline.setModel(entry.transform->getTransformation());
		m_uniformPipeline.set(m_pipeline);

		entry.model->render();
	}
}

void DirectionalLightTechnique::updateCamera(const Camera& camera)
{
	m_uniformCascadeLengths->set(camera.getFrustumEnds());
	m_uniformCamera.set(camera);
}

void DirectionalLightTechnique::updateScreenSize(const glm::vec2& screenSize)
{
	m_uniformScreenSize->set(screenSize);
}

void DirectionalLightTechnique::updateDirectionalLight(const DirectionalLight& light)
{
	m_uniformDirectionalLight.set(light);
}
