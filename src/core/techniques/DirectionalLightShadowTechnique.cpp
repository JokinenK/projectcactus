/*
 * DirectionalLightShadowTechnique.cpp
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#include <glm/gtx/transform.hpp>
#include "core/Camera.h"
#include "core/IRenderer.h"
#include "core/IUniform.h"
#include "core/IFramebuffer.h"
#include "core/RenderQueue.h"
#include "core/light/DirectionalLight.h"
#include "DirectionalLightShadowTechnique.h"

DirectionalLightShadowTechnique::DirectionalLightShadowTechnique(IRenderer* renderer, float shadowTextureSize, size_t numCascades) :
		m_renderer(renderer),
		m_framebuffer(renderer->createFramebuffer()),
		m_shadowTextureSize(shadowTextureSize),
		m_numCascades(numCascades),
		m_shader(
				"../res/shaders/shadow-directionallight.vs",
				"../res/shaders/shadow-directionallight.fs")
{
	m_framebuffer->init(shadowTextureSize, shadowTextureSize);

	m_textures.resize(numCascades);
	m_shadowMatrices.resize(numCascades);
	m_lightViewProjections.resize(numCascades);

	for (size_t cascadeIndex = 0; cascadeIndex < numCascades; ++cascadeIndex) {
		m_textures[cascadeIndex] = m_renderer->createTexture();
		m_textures[cascadeIndex]->initTexture(shadowTextureSize, shadowTextureSize, ITexture::UNSIGNED_INT);
		m_textures[cascadeIndex]->setTextureData(0, ITexture::DEPTH_COMPONENT, ITexture::DEPTH_COMPONENT24);

		m_textures[cascadeIndex]->setWrapS(ITexture::CLAMP_TO_EDGE);
		m_textures[cascadeIndex]->setWrapT(ITexture::CLAMP_TO_EDGE);
		m_textures[cascadeIndex]->setWrapR(ITexture::CLAMP_TO_EDGE);

		m_textures[cascadeIndex]->setFilterMin(ITexture::LINEAR);
		m_textures[cascadeIndex]->setFilterMag(ITexture::LINEAR);
	}

	IFramebuffer::Attachment drawAttachments[] = { IFramebuffer::NONE };
	m_framebuffer->drawBuffers(drawAttachments, 1);

	m_uniformMVP = m_shader.getUniform("uniformMVP");
}

DirectionalLightShadowTechnique::~DirectionalLightShadowTechnique()
{
	for (size_t cascadeIndex = 0; cascadeIndex < m_numCascades; ++cascadeIndex) {
		m_renderer->releaseTexture(m_textures[cascadeIndex]);
	}

	m_renderer->releaseFramebuffer(m_framebuffer);
}

void DirectionalLightShadowTechnique::bind()
{
	m_shader.bind();
	m_framebuffer->bindWriteBuffer();

	m_renderer->setViewPort(m_shadowTextureSize, m_shadowTextureSize);
	m_renderer->setDepthMask(true);
	m_renderer->setDepthClampEnabled(true);
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setBlendEnabled(false);
	m_renderer->setCullFace(IRenderer::FRONT);
}

void DirectionalLightShadowTechnique::unbind()
{
	m_shader.unbind();
	m_framebuffer->unbindWriteBuffer();

	m_renderer->setDepthMask(false);
	m_renderer->setDepthClampEnabled(false);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setBlendEnabled(false);
	m_renderer->setCullFace(IRenderer::BACK);
}

void DirectionalLightShadowTechnique::render(RenderQueue& queue)
{
	for (size_t cascadeIndex = 0; cascadeIndex < m_numCascades; ++cascadeIndex) {
		m_framebuffer->attachRenderTarget(m_textures[cascadeIndex], IFramebuffer::DEPTH_ATTACHMENT);
		m_framebuffer->bindWriteBuffer();
		m_framebuffer->clear();

		const glm::mat4& lightViewProjection = m_lightViewProjections[cascadeIndex];

		for (auto iter = queue.begin(); iter != queue.end(); ++iter) {
			const RenderQueue::Value& entry = iter->second;
			const glm::mat4& model = entry.transform->getTransformation();

			m_uniformMVP->set(lightViewProjection * model);
			entry.model->render();
		}
	}
}

void DirectionalLightShadowTechnique::updateLightMatrices(const Camera& camera, const DirectionalLight& light)
{
	const std::vector<ViewFrustum>& frustumSplits = camera.getFrustumSplits();

	for (size_t cascadeIndex = 0; cascadeIndex < m_numCascades; ++cascadeIndex) {
		const ViewFrustum& frustum = frustumSplits.at(cascadeIndex);
		const Sphere& boundingSphere = frustum.getBoundingSphere();

		glm::vec3 center = boundingSphere.getCenter();
		float radius = std::ceil(boundingSphere.getRadius());

		glm::vec3 min = center - glm::vec3(radius);
		glm::vec3 max = center + glm::vec3(radius);

		glm::vec3 lightPosition = center - (light.getDirection() * frustum.getFar());
		glm::mat4 lightView = glm::lookAt(lightPosition, center, Camera::Up);
		glm::mat4 lightProjection = glm::ortho(min.x, max.x, min.y, max.y, -max.z, -min.z);

		glm::mat4 shadowMatrix = lightProjection * lightView;
		glm::vec4 shadowOrigin = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		shadowOrigin = shadowMatrix * shadowOrigin;
		shadowOrigin = shadowOrigin * m_shadowTextureSize / 2.0f;

		glm::vec4 emptyVector(0);
		glm::vec4 shadowOffset = glm::round(shadowOrigin) - shadowOrigin;

		shadowOffset = shadowOffset * 2.0f / m_shadowTextureSize;
		shadowOffset.z = 0.0f;
		shadowOffset.w = 0.0f;

		lightProjection += glm::mat4(
				emptyVector,
				emptyVector,
				emptyVector,
				shadowOffset);

		glm::mat4 lightViewProjection = lightProjection * lightView;

		m_lightViewProjections[cascadeIndex] = lightViewProjection;
		m_shadowMatrices[cascadeIndex] = camera.calculateShadowMatrix(lightViewProjection, Camera::SHADOW_ORTHO);
	}
}

const std::vector<glm::mat4>& DirectionalLightShadowTechnique::getShadowMatrices() const
{
	return m_shadowMatrices;
}

const std::vector<ITexture*>& DirectionalLightShadowTechnique::getTextures() const
{
	return m_textures;
}
