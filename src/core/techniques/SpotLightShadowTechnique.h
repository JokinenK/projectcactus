/*
 * SpotLightShadowTechnique.h
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_SPOTLIGHTSHADOWTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_SPOTLIGHTSHADOWTECHNIQUE_H_

#include <glm/glm.hpp>
#include "core/ITechnique.h"
#include "core/BaseShader.h"

class IRenderer;
class IFramebuffer;
class ITexture;
class IUniform;
class Camera;
class SpotLight;

class SpotLightShadowTechnique : public ITechnique
{
public:
	SpotLightShadowTechnique(IRenderer* renderer, float shadowMapSize);
	virtual ~SpotLightShadowTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void updateLightMatrices(const Camera& camera, const SpotLight& light);

	const glm::mat4& getShadowMatrix() const;
	ITexture* getTexture() const;

private:
	IRenderer* m_renderer;
	IFramebuffer* m_framebuffer;
	ITexture* m_texture;

	glm::mat4 m_shadowMatrix;
	glm::mat4 m_lightViewProjection;

	float m_shadowTextureSize;

	BaseShader m_shader;
	IUniform* m_uniformMVP;
};

#endif /* PROJECTCACTUS_SRC_CORE_POINTLIGHTSHADOWTECHNIQUE_H_ */

