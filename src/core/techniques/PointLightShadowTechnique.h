/*
 * PointLightShadowTechnique.h
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_POINTLIGHTSHADOWTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_POINTLIGHTSHADOWTECHNIQUE_H_

#include <glm/glm.hpp>
#include "core/ITechnique.h"
#include "core/BaseShader.h"
#include "core/uniform/UniformCamera.h"
#include "core/uniform/UniformPointLight.h"

class IRenderer;
class IFramebuffer;
class ITexture;
class IUniform;
class Camera;
class PointLight;

class PointLightShadowTechnique : public ITechnique
{
public:
	enum {
		NUM_DIRECTIONS = 6
	};

	PointLightShadowTechnique(IRenderer* renderer, float shadowMapSize);
	virtual ~PointLightShadowTechnique();

	virtual void bind();
	virtual void unbind();

	virtual void render(RenderQueue& queue);

	void updateLightMatrices(const Camera& camera, const PointLight& light);

	void updateCamera(const Camera& camera);
	void updatePointLight(const PointLight& light);

	const std::vector<glm::mat4>& getShadowMatrices() const;
	ITexture* getTexture() const;

private:
	IRenderer* m_renderer;
	IFramebuffer* m_framebuffer;
	ITexture* m_texture;

	std::vector<glm::mat4> m_shadowMatrices;

	float m_shadowTextureSize;

	BaseShader m_shader;
	IUniform* m_uniformModel;
	IUniform* m_uniformShadowMatrices;

	UniformCamera m_uniformCamera;
	UniformPointLight m_uniformPointLight;
};

#endif /* PROJECTCACTUS_SRC_CORE_POINTLIGHTSHADOWTECHNIQUE_H_ */
