#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_POINTLIGHTTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_POINTLIGHTTECHNIQUE_H_

#include "GBufferTechnique.h"
#include "PointLightShadowTechnique.h"
#include "core/ITechnique.h"
#include "core/IUniform.h"
#include "core/Pipeline.h"
#include "core/BaseShader.h"
#include "core/uniform/UniformPointLight.h"
#include "core/uniform/UniformPipeline.h"
#include "core/uniform/UniformCamera.h"

class IRenderer;
class PointLight;
class Camera;

class PointLightTechnique : public ITechnique
{
public:
	PointLightTechnique(IRenderer* renderer, float screenWidth, float screenHeight, GBufferTechnique& gBufferTechnique, PointLightShadowTechnique& shadowTechnique);
	virtual ~PointLightTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void setPipeline(const Pipeline& pipeline);

	void updateScreenSize(const glm::vec2& screenSize);
	void updateCamera(const Camera& camera);
	void updatePointLight(const PointLight& light);
	void updateShadowMatrices(const std::vector<glm::mat4>& matrices);

protected:
	IRenderer* m_renderer;

	float m_screenWidth;
	float m_screenHeight;
	int m_nextSampler;

	BaseShader m_shader;
	Pipeline m_pipeline;

	IUniform* m_uniformSamplerDiffuse;
	IUniform* m_uniformSamplerNormal;
	IUniform* m_uniformSamplerDepth;

	IUniform* m_uniformSamplerShadow;
	IUniform* m_uniformShadowMatrices;

	IUniform* m_uniformScreenSize;

	UniformPointLight m_uniformPointLight;
	UniformPipeline m_uniformPipeline;
	UniformCamera m_uniformCamera;

	GBufferTechnique& m_gBufferTechnique;
	PointLightShadowTechnique& m_shadowTechnique;
};

#endif /* PROJECTCACTUS_SRC_CORE_TECHNIQUES_POINTLIGHTTECHNIQUE_H_ */
