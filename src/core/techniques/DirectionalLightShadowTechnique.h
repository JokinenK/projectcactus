/*
 * DirectionalLightShadowTechnique.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTSHADOWTECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTSHADOWTECHNIQUE_H_

#include <glm/glm.hpp>
#include "core/ITechnique.h"
#include "core/IUniform.h"
#include "core/BaseShader.h"
#include "core/Pipeline.h"

class IRenderer;
class IFramebuffer;
class Camera;
class DirectionalLight;

class DirectionalLightShadowTechnique : public ITechnique
{
public:
	DirectionalLightShadowTechnique(IRenderer* renderer, float shadowTextureSize, size_t numCascades = 4);
	virtual ~DirectionalLightShadowTechnique();

	virtual void bind();
	virtual void unbind();
	virtual void render(RenderQueue& queue);

	void updateLightMatrices(const Camera& camera, const DirectionalLight& light);

	const std::vector<glm::mat4>& getShadowMatrices() const;
	const std::vector<ITexture*>& getTextures() const;

private:
	IRenderer* m_renderer;
	IFramebuffer* m_framebuffer;

	std::vector<ITexture*> m_textures;
	std::vector<glm::mat4> m_shadowMatrices;
	std::vector<glm::mat4> m_lightViewProjections;

	float m_shadowTextureSize;
	size_t m_numCascades;

	BaseShader m_shader;

	IUniform* m_uniformMVP;
};

#endif /* PROJECTCACTUS_SRC_CORE_TECHNIQUES_DIRECTIONALLIGHTSHADOWTECHNIQUE_H_ */
