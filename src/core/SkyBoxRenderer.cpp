/*
 * SkyBoxRenderer.cpp
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#include <vector>
#include "SkyBoxRenderer.h"
#include "core/IRenderer.h"
#include "core/ITexture.h"
#include "core/IModel.h"
#include "core/IUniform.h"
#include "core/RendererFactory.h"
#include "core/Camera.h"
#include "core/model/VertexModel.h"
#include "core/model/IndexedVertexModel.h"
#include "support/Image.h"
#include "support/Utils.h"

SkyBoxRenderer::SkyBoxRenderer(
		IRenderer* renderer,
		float size,
		const std::string& fileBaseName,
		const std::string& fileExtension) :
				m_renderer(renderer),
				m_shader("../res/shaders/skybox.vs", "../res/shaders/skybox.fs")
{
	createModel(size);

	Image right(fileBaseName + "_right." + fileExtension);
	Image left(fileBaseName + "_left." + fileExtension);
	Image top(fileBaseName + "_top." + fileExtension);
	Image bottom(fileBaseName + "_bottom." + fileExtension);
	Image back(fileBaseName + "_back." + fileExtension);
	Image front(fileBaseName + "_front." + fileExtension);

	float width = right.getWidth();
	float height = right.getHeight();

	ITexture::DataFormat sourceFormat;
	ITexture::TextureType textureType;
	int typeIndex;
	const unsigned char* textureData;

	m_texture = renderer->createTexture();
	m_texture->initTexture(width, height, ITexture::UNSIGNED_BYTE, ITexture::TEXTURE_CUBE_MAP);

	m_texture->setWrapS(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapT(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapR(ITexture::CLAMP_TO_EDGE);

	m_texture->setFilterMin(ITexture::LINEAR);
	m_texture->setFilterMag(ITexture::LINEAR);

	Image* image;
	Image* images[] = {&right, &left, &top, &bottom, &back, &front};

	for (int i = 0; i < ARRAY_SIZE(images); ++i) {
		image        = images[i];
		typeIndex    = ITexture::TEXTURE_CUBE_MAP_POSITIVE_X + i;
		sourceFormat = ITexture::determineImageFormat(image->getChannels());
		textureType  = static_cast<ITexture::TextureType>(typeIndex);
		textureData  = image->getData().data();

		m_texture->setTextureData(
				textureData,
				sourceFormat,
				ITexture::RGBA,
				textureType);
	}

	m_shader.bind();
	m_shader.getUniform("uniformSamplerDiffuse")->set(0);
	m_shader.unbind();
}

SkyBoxRenderer::~SkyBoxRenderer()
{
}

void SkyBoxRenderer::createModel(float size)
{
	std::vector<glm::vec3> positions;

	const glm::vec3 FTL(-size,  size,  size);
	const glm::vec3 FTR( size,  size,  size);
	const glm::vec3 FBL(-size, -size,  size);
	const glm::vec3 FBR( size, -size,  size);
	const glm::vec3 BTL(-size,  size, -size);
	const glm::vec3 BTR( size,  size, -size);
	const glm::vec3 BBL(-size, -size, -size);
	const glm::vec3 BBR( size, -size, -size);

	// Back face
	positions.push_back(BTL);
	positions.push_back(BBL);
	positions.push_back(BBR);
	positions.push_back(BBR);
	positions.push_back(BTR);
	positions.push_back(BTL);

	// Left face
	positions.push_back(FBL);
	positions.push_back(BBL);
	positions.push_back(BTL);
	positions.push_back(BTL);
	positions.push_back(FTL);
	positions.push_back(FBL);

	// Right face
	positions.push_back(BBR);
	positions.push_back(FBR);
	positions.push_back(FTR);
	positions.push_back(FTR);
	positions.push_back(BTR);
	positions.push_back(BBR);

	// Front face
	positions.push_back(FBL);
	positions.push_back(FTL);
	positions.push_back(FTR);
	positions.push_back(FTR);
	positions.push_back(FBR);
	positions.push_back(FBL);

	// Top face
	positions.push_back(BTL);
	positions.push_back(BTR);
	positions.push_back(FTR);
	positions.push_back(FTR);
	positions.push_back(FTL);
	positions.push_back(BTL);

	// Bottom face
	positions.push_back(BBL);
	positions.push_back(FBL);
	positions.push_back(BBR);
	positions.push_back(BBR);
	positions.push_back(FBL);
	positions.push_back(FBR);

	IndexedVertexModel indexedVertexModel(positions);
	m_model = RendererFactory::instance()->createModel();
	m_model->setIndices(indexedVertexModel.getIndices());
	m_model->setPositions(indexedVertexModel.getPositions());
	m_model->setTexCoords(indexedVertexModel.getTexCoords());
	m_model->setNormals(indexedVertexModel.getNormals());
	m_model->setTangents(indexedVertexModel.getTangents());
}

void SkyBoxRenderer::render(Camera* camera)
{
	m_renderer->setDepthTestEnabled(true);
	m_renderer->setDepthMask(true);
	m_renderer->setBlendEnabled(false);

	glm::mat4 view = glm::mat4(glm::mat3(camera->getView()));

	m_texture->bind(0);

	m_shader.bind();
	m_shader.getUniform("uniformVP")->set(camera->getProjection() * view);

	m_model->render();

	m_shader.unbind();
	m_texture->unbind(0);

	m_renderer->setDepthMask(true);
	m_renderer->setDepthTestEnabled(true);
}
