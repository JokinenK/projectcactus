/*
 * Plugin.h
 *
 *  Created on: 7.12.2016
 *      Author: kalle
 */

#ifndef PLUGIN_H_
#define PLUGIN_H_

#include <iostream>
#include "hal/PluginLoader.h"

template <class T>
class Plugin
{
public:
	typedef T* (*PluginCreateInstanceFunc)(void);
	typedef bool (*PluginReleaseInstanceFunc)(T* instance);
	typedef int (*PluginVersionFunc)(void);
	typedef char* (*PluginNameFunc)(void);

	Plugin(const std::string& pluginPath) :
		m_pluginLoader(pluginPath),
		m_createFunc(0),
		m_releaseFunc(0),
		m_nameFunc(0),
		m_versionFunc(0)
	{
		bool success = m_pluginLoader.load();

		if (success) {
			*reinterpret_cast<void**>(&m_createFunc) =
					m_pluginLoader.symbol("PLUGIN_CreateInstance");

			*reinterpret_cast<void**>(&m_releaseFunc) =
					m_pluginLoader.symbol("PLUGIN_ReleaseInstance");

			*reinterpret_cast<void**>(&m_nameFunc) =
					m_pluginLoader.symbol("PLUGIN_GetName");

			*reinterpret_cast<void**>(&m_versionFunc) =
					m_pluginLoader.symbol("PLUGIN_GetVersion");

			success = (m_createFunc != NULL)
					&& (m_releaseFunc != NULL)
					&& (m_nameFunc != NULL)
					&& (m_versionFunc != NULL);
		}
		else {
			std::cerr << "PluginLoader load failed: " << m_pluginLoader.error() << std::endl;
		}

		if (!success) {
			std::cerr << "Unable to load plugin: " << pluginPath << std::endl;
		}
	}

	~Plugin()
	{
		m_pluginLoader.unload();
	}

	T* createInstance()
	{
		T* instance;

		if (m_createFunc) {
			instance = m_createFunc();
		}

		if (!instance) {
			std::cerr << "Unable to create instance of plugin" << std::endl;
		}

		return instance;
	}

	void releaseInstance(T*& instance)
	{
		m_releaseFunc(instance);
		instance = 0;
	}

	int getVersion() const
	{
		int pluginVersion = 0;

		if (m_versionFunc) {
			pluginVersion = m_versionFunc();
		}

		return pluginVersion;
	}

	std::string getName() const
	{
		std::string pluginName;

		if (m_nameFunc) {
			const char* namePtr = m_nameFunc();

			if (namePtr) {
				pluginName = std::string(namePtr);
			}
		}

		return pluginName;
	}

private:
	Hal::PluginLoader m_pluginLoader;
	PluginCreateInstanceFunc m_createFunc;
	PluginReleaseInstanceFunc m_releaseFunc;
	PluginNameFunc m_nameFunc;
	PluginVersionFunc m_versionFunc;
};

#endif /* PLUGIN_H_ */
