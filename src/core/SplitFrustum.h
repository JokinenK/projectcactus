/*
 * SplitFrustum.h
 *
 *  Created on: 3.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_SPLITFRUSTUM_H_
#define PROJECTCACTUS_SRC_CORE_SPLITFRUSTUM_H_

#include <vector>
#include <glm/glm.hpp>
#include "core/ViewFrustum.h"

class SplitFrustum
{
public:
	SplitFrustum(ViewFrustum& frustum, int numSplits = 4, float lambda = 0.95f, float overlap = 0.005f);
	virtual ~SplitFrustum();

	void setFov(float fovDegrees);
	float getFov() const;

	void setAspectRatio(float aspectRatio);
	float getAspectRatio() const;

	void setNear(float near);
	float getNear() const;

	void setFar(float far);
	float getFar() const;

	const glm::mat4& getProjection() const;

	void updateCorners(const glm::vec3& position, const glm::vec3& up, const glm::vec3& forward, const glm::vec3& right);

	const std::vector<ViewFrustum>& getFrustumSplits() const;
	const std::vector<float>& getFrustumEnds() const;

private:
	void splitFrustum();

	ViewFrustum& m_frustum;
	int m_numSplits;
	float m_lambda;
	float m_overlap;
	std::vector<ViewFrustum> m_frustumSplits;
	std::vector<float> m_frustumEnds;
};

#endif /* PROJECTCACTUS_SRC_CORE_SPLITFRUSTUM_H_ */
