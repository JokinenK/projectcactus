/*
 * DataBinding.h
 *
 *  Created on: 23.2.2017
 *      Author: kalle
 */

#ifndef DATABINDING_H_
#define DATABINDING_H_

#include <functional>
#include "support/Utils.h"

class IDataBinding
{
public:
	virtual ~IDataBinding() {};
	virtual void update() = 0;
};

template <class SourceClass, class Getter, class TargetClass, class Setter>
class DataBinding : public IDataBinding
{
public:
	DataBinding(SourceClass source, Getter getter, TargetClass target, Setter setter) :
		m_source(source),
		m_getter(getter),
		m_target(target),
		m_setter(setter)
	{
	}

	void update() {
		INVOKE_MEMBER_FN(&m_target, m_setter)(INVOKE_MEMBER_FN(&m_source, m_getter)());
	}

private:
	SourceClass m_source;
	TargetClass m_target;

	Getter m_getter;
	Setter m_setter;
};

template <class SourceClass, class Getter, class TargetClass, class Setter>
static IDataBinding* createDataBinding(SourceClass&& source, Getter&& getter, TargetClass&& target, Setter&& setter) {
	return new DataBinding<SourceClass, Getter, TargetClass, Setter>(
			std::forward<SourceClass>(source),
			std::forward<Getter>(getter),
			std::forward<TargetClass>(target),
			std::forward<Setter>(setter));
}

#endif /* DATABINDING_H_ */
