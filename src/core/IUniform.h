/*
 * IUniform.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_IUNIFORM_H_
#define PROJECTCACTUS_SRC_CORE_IUNIFORM_H_

#include <vector>
#include <glm/glm.hpp>

class IUniform
{
public:
	virtual ~IUniform() {};

    virtual void set(int x) = 0;
    virtual void set(int x, int y) = 0;
    virtual void set(int x, int y, int z) = 0;
    virtual void set(int x, int y, int z, int w) = 0;

    virtual void set(float x) = 0;
    virtual void set(float x, float y) = 0;
    virtual void set(float x, float y, float z) = 0;
    virtual void set(float x, float y, float z, float w) = 0;

    virtual void set(const glm::vec2& vec) = 0;
    virtual void set(const glm::vec3& vec) = 0;
    virtual void set(const glm::vec4& vec) = 0;
    virtual void set(const glm::mat4& mat) = 0;

    virtual void set(const std::vector<int>& arrInt) = 0;
	virtual void set(const std::vector<float>& arrFloat) = 0;
	virtual void set(const std::vector<glm::vec2>& arrVec) = 0;
	virtual void set(const std::vector<glm::vec3>& arrVec) = 0;
	virtual void set(const std::vector<glm::vec4>& arrVec) = 0;
	virtual void set(const std::vector<glm::mat4>& arrMat) = 0;
};

#endif /* PROJECTCACTUS_SRC_CORE_IUNIFORM_H_ */
