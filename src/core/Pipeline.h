/*
 * Pipeline.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_PIPELINE_H_
#define PROJECTCACTUS_SRC_CORE_PIPELINE_H_

#include <glm/glm.hpp>

class Pipeline {
public:
	Pipeline();
	Pipeline(const Pipeline& other);
	virtual ~Pipeline();

	Pipeline& operator=(const Pipeline& other);

	void setProjection(const glm::mat4& projection);
	const glm::mat4& getProjection() const;

	void setView(const glm::mat4& view);
	const glm::mat4& getView() const;

	void setModel(const glm::mat4& model);
	const glm::mat4& getModel() const;

	const glm::mat4& getVP() const;
	const glm::mat4& getMV() const;
	const glm::mat4& getMVP() const;
	const glm::mat4& getNormalMatrix() const;

	const glm::mat4& getInverseProjection() const;
	const glm::mat4& getInverseView() const;
	const glm::mat4& getInverseModel() const;
	const glm::mat4& getInverseVP() const;
	const glm::mat4& getInverseMV() const;
	const glm::mat4& getInverseMVP() const;

private:
	void update();

	bool m_projectionDirty;
	bool m_viewDirty;
	bool m_modelDirty;

	glm::mat4 m_projection;
	glm::mat4 m_projectionInverse;

	glm::mat4 m_view;
	glm::mat4 m_viewInverse;

	glm::mat4 m_model;
	glm::mat4 m_modelInverse;

	glm::mat4 m_vp;
	glm::mat4 m_vpInverse;

	glm::mat4 m_mv;
	glm::mat4 m_mvInverse;

	glm::mat4 m_mvp;
	glm::mat4 m_mvpInverse;

	glm::mat4 m_normalMatrix;
};

#endif /* PROJECTCACTUS_SRC_CORE_PIPELINE_H_ */
