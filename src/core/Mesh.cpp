#include <glm/glm.hpp>
#include "Mesh.h"
#include "core/IRenderer.h"
#include "core/IRenderPrerequisite.h"
#include "core/IModel.h"
#include "core/GameObject.h"
#include "core/RendererFactory.h"
#include "core/Material.h"
#include "core/BoundingBox.h"
#include "core/RenderQueue.h"


Mesh::Mesh(const VertexModel& modelData, Material* material) :
		m_material(material),
		m_transform(0),
		m_renderer(0),
		m_shouldDealloc(true)
{
	initFromIndexedVertexModel(modelData.toIndexedVertexModel());
}

Mesh::Mesh(const IndexedVertexModel& modelData, Material* material) :
		m_material(material),
		m_transform(0),
		m_renderer(0),
		m_shouldDealloc(true)
{
	initFromIndexedVertexModel(modelData);
}

Mesh::Mesh(IModel* model, Material* material) :
		m_model(model),
		m_material(material),
		m_transform(0),
		m_renderer(0),
		m_boundingBox(0),
		m_shouldDealloc(false)
{
}

Mesh::~Mesh()
{
	if (m_shouldDealloc) {
		RendererFactory::instance()->releaseModel(m_model);
	}
}

void Mesh::initFromIndexedVertexModel(const IndexedVertexModel& model)
{
	m_model = RendererFactory::instance()->createModel();

	m_model->setIndices(model.getIndices());
	m_model->setPositions(model.getPositions());
	m_model->setTexCoords(model.getTexCoords());
	m_model->setNormals(model.getNormals());
	m_model->setTangents(model.getTangents());

}

Material* Mesh::getMaterial() const
{
	return m_material;
}

IModel* Mesh::getModel() const
{
	return m_model;
}

void Mesh::start()
{
	GameObject* parent = getParent();

	m_renderer = parent->getRenderer();
	m_transform = parent->getTransform();

	std::list<BoundingBox*> boundingBoxes = parent->findComponents<BoundingBox>();

	if (!boundingBoxes.empty()) {
		m_boundingBox = boundingBoxes.front();
	}
}

void Mesh::stop()
{
}

void Mesh::addToRenderQueue(RenderQueue& queue)
{
	if (!queue.shouldRender(m_boundingBox)) {
		return;
	}

	RenderQueue::Value entry;

	entry.model = m_model;
	entry.transform = m_transform;
	entry.material = m_material;

	Material::Id materialId = Material::INVALID_ID;

	if (entry.material) {
		materialId = entry.material->getMaterialId();
	}

	queue.addToQueue(materialId, entry);
}
