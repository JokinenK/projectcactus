/*
 * SkyBoxRenderer.h
 *
 *  Created on: 4.10.2016
 *      Author: kalle
 */

#ifndef SKYBOXRENDERER_H_
#define SKYBOXRENDERER_H_

#include <string>
#include "core/BaseShader.h"

class IRenderer;
class IModel;
class ITexture;
class Camera;

class SkyBoxRenderer {
public:
	SkyBoxRenderer(
			IRenderer* renderer,
			float size,
			const std::string& fileBaseName,
			const std::string& fileExtension);
	virtual ~SkyBoxRenderer();

	void render(Camera* camera);
	void createModel(float size);

private:
	IRenderer* m_renderer;
	BaseShader m_shader;

	IModel* m_model;
	ITexture* m_texture;
};

#endif /* SKYBOXRENDERER_H_ */
