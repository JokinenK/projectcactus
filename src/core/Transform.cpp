#include <glm/gtx/transform.hpp>
#include "core/Transform.h"
#include "core/GameObject.h"
#include "core/Camera.h"

namespace
{
	static const glm::mat4 IDENTITY_MATRIX = glm::mat4(1);
	static const glm::quat IDENTITY_QUAT = glm::quat();
}

Transform::Transform(const glm::vec3& position, const glm::quat& rotation, const glm::vec3& scale) :
		m_parent(0),
		m_parentTransform(0),
		m_parentTransformation(IDENTITY_MATRIX),
		m_parentRotation(IDENTITY_QUAT)
{
	setPosition(position);
	setRotation(rotation);
	setScale(scale);
}

void Transform::update()
{
	if (m_dirty) {
		const glm::mat4& parentTransformation = getParentTransformation();
		const glm::quat& parentRotation =  getParentRotation();

		m_transformation = parentTransformation * m_translateMatrix * m_rotateMatrix * m_scaleMatrix;
		m_transformedPosition = glm::vec3(parentTransformation * glm::vec4(m_position, 1));
		m_transformedRotation = parentRotation * m_rotation;

		m_up = m_transformedRotation * Camera::Up;
	    m_right = m_transformedRotation * Camera::Right;
	    m_forward = m_transformedRotation * Camera::Forward;

	    m_dirty = false;
	}
}

void Transform::invalidate()
{
	m_dirty = true;

	for (Transform* child : m_children) {
		child->invalidate();
	}
}

void Transform::rotate(const glm::vec3& axis, float angleRadians)
{
	rotate(glm::angleAxis(angleRadians, axis));
}

void Transform::rotate(const glm::quat& rotation)
{
	setRotation(glm::conjugate(glm::normalize(rotation * m_rotation)));
}

void Transform::translate(const glm::vec3& dir, float amount)
{
	setPosition(m_position + dir * amount);
}

const glm::mat4& Transform::getTransformation() const
{
	return m_transformation;
}

const glm::mat4& Transform::getTranslateMatrix() const
{
	return m_translateMatrix;
}

const glm::mat4& Transform::getRotateMatrix() const
{
	return m_rotateMatrix;
}

const glm::mat4& Transform::getScaleMatrix() const
{
	return m_scaleMatrix;
}

void Transform::setParent(GameObject* parent)
{ 
	if (m_parentTransform) {
		m_parentTransform->removeChild(this);
	}

	m_parent = parent; 
	m_parentTransform = parent->getTransform();
	m_parentTransform->addChild(this);

	invalidate();
}

GameObject* Transform::getParent() const
{
	return m_parent;
}

void Transform::setPosition(const glm::vec3& position)
{
	m_position = position;
    m_translateMatrix = glm::translate(m_position);

    invalidate();
}

const glm::vec3& Transform::getPosition() const
{
	return m_position;
}

void Transform::setRotation(const glm::quat& rotation)
{
	m_rotation = rotation;
	m_rotateMatrix = glm::mat4_cast(m_rotation);

	invalidate();
}

const glm::quat& Transform::getRotation() const
{
	return m_rotation;
}

void Transform::setScale(const glm::vec3& scale)
{
	m_scale = scale;
	m_scaleMatrix = glm::scale(scale);

	invalidate();
}

const glm::vec3& Transform::getScale() const
{
	return m_scale;
}

const glm::vec3& Transform::getTransformedPosition()
{
	return m_transformedPosition;
}

const glm::quat& Transform::getTransformedRotation()
{
	return m_transformedRotation;
}

const glm::vec3& Transform::getUp()
{
	return m_up;
}

const glm::vec3& Transform::getRight()
{
	return m_right;
}

const glm::vec3& Transform::getForward()
{
	return m_forward;
}

const glm::mat4& Transform::getParentTransformation() const
{
	if (m_parentTransform) {
		return m_parentTransform->getTransformation();
	}

	return IDENTITY_MATRIX;
}

const glm::quat& Transform::getParentRotation() const
{
	if (m_parentTransform) {
		return m_parentTransform->getTransformedRotation();
	}

	return IDENTITY_QUAT;
}

void Transform::addChild(Transform* transform)
{
	m_children.push_back(transform);
}

void Transform::removeChild(Transform* transform)
{
	m_children.remove(transform);
}
