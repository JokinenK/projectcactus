/*
 * Time.cpp
 *
 *  Created on: 30.9.2016
 *      Author: kalle
 */

#include "hal/Time.h"
#include "Time.h"

Time::Time(int frameRate, int maxFrameSkip)
{
    setTargetFrameRate(frameRate);

    m_numSamples = 60;
	m_sampleIndex = 0;
	m_samplesTotal = 0;
    m_samples.resize(m_numSamples);

    unsigned int i;
    for (i = 0; i < m_numSamples; i++) {
		m_samples[i] = 0;
	}

    unsigned long long currentTime = Hal::Time::currentTimeMillis();

    m_maxFrameSkip = maxFrameSkip;
    m_prevTime_ms = currentTime;
    m_nextGameTick_ms = currentTime + m_targetFrameTime_ms;
    m_frameTime_ms = 0;
    m_loops = 0;
	m_t = 0.0f;
}

Time::~Time()
{
}

void Time::setTargetFrameRate(int frameRate)
{
    m_targetFrameTime_ms = (1000.0f / frameRate);
}

void Time::addSample(float sample)
{
	m_samplesTotal -= m_samples[m_sampleIndex];
	m_samplesTotal += sample;
	m_samples[m_sampleIndex] = sample;

    if (++m_sampleIndex == m_numSamples) {
		m_sampleIndex = 0;
	}
}

void Time::startFrame()
{
    m_loops = 0;
}

bool Time::doFixedUpdate()
{
	if (Hal::Time::currentTimeMillis() > m_nextGameTick_ms && m_loops < m_maxFrameSkip) {
		m_nextGameTick_ms += m_targetFrameTime_ms;
		m_loops++;
        return true;
	}

    return false;
}

void Time::endFrame()
{
	unsigned long long currentTime = Hal::Time::currentTimeMillis();

	m_frameTime_ms = currentTime - m_prevTime_ms;
	m_prevTime_ms = currentTime;
	m_t += m_frameTime_ms;

	addSample(1000.0f / m_frameTime_ms);
}

float Time::frameTime() const
{
	return (m_frameTime_ms / 1000.0f);
}

float Time::t() const
{
	return (m_t / 1000.0f);
}

float Time::interp() const
{
	return (static_cast<float>(Hal::Time::currentTimeMillis()) + m_targetFrameTime_ms - m_nextGameTick_ms) / m_targetFrameTime_ms;
}

float Time::fps() const
{
    return (m_samplesTotal / m_numSamples);
}
