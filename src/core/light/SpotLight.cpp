#include <glm/glm.hpp>
#include "support/Attenuation.h"
#include "core/Camera.h"
#include "core/light/SpotLight.h"

SpotLight::SpotLight(
		const Color& color,
		const Attenuation& attenuation,
		float intensity,
		float coneAngle) :
				PointLight(color, attenuation, intensity)
{
	setConeAngle(coneAngle);
}

SpotLight::~SpotLight()
{
}

void SpotLight::setConeAngle(float coneAngle)
{
	m_coneAngle = coneAngle;
    m_cutoff = std::cos(glm::radians(coneAngle) / 2);

    updateRadius(coneAngle);
}

float SpotLight::getConeAngle() const
{
    return m_coneAngle;
}

float SpotLight::getCutoff() const
{
	return m_cutoff;
}

const glm::vec3 SpotLight::getDirection() const
{
	return glm::normalize(getTransform()->getForward());
}
