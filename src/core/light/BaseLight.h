#ifndef BASELIGHT_H
#define BASELIGHT_H

#include <glm/glm.hpp>
#include <iostream>
#include "core/Component.h"
#include "support/Color.h"

class Transform;

class BaseLight : public Component
{
public:
    BaseLight(const Color& color, float intensity = 1.0f);
    virtual ~BaseLight();

    void start();

    void setColor(const Color& color);
    const Color& getColor() const;

    void setIntensity(float intensity);
    float getIntensity() const;

    void setShadowEnabled(bool shadowEnabled);
    bool isShadowEnabled() const;

    Transform* getTransform() const;

private:
    void releaseTransform();

    Color m_color;
    float m_intensity;
    bool m_shadowEnabled;
    bool m_shouldDeallocTransform;
    Transform* m_transform;
};

#endif // BASELIGHT_H

