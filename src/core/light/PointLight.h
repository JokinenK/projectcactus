#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "core/light/BaseLight.h"
#include "support/Attenuation.h"

class PointLight : public BaseLight
{
public:
    PointLight(const Color& color, const Attenuation& attenuation, float intensity = 1.0f);
    virtual ~PointLight();

	void setAttenuation(const Attenuation& attenuation);
    const Attenuation& getAttenuation() const;

    void setRange(float range);
    float getRadius() const;

    const glm::vec3& getPosition() const;
    const glm::mat4& getProjection() const;

protected:
    void updateRadius(float lightAngle = 90.0f);

private:
    Attenuation m_attenuation;
    float m_radius;
    glm::mat4 m_projection;
};

#endif // POINTLIGHT_H

