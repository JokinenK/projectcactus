#include "core/light/DirectionalLight.h"
#include "core/Transform.h"
#include "core/Material.h"
#include "core/Camera.h"
#include "support/Utils.h"

DirectionalLight::DirectionalLight(
		const Color& color,
		float intensity) :
				BaseLight(color, intensity)
{
}

DirectionalLight::~DirectionalLight()
{
}

const glm::vec3 DirectionalLight::getDirection() const
{
    return glm::normalize(getTransform()->getForward());
}
