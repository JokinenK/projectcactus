#include <glm/gtx/transform.hpp>
#include "support/Attenuation.h"
#include "core/light/PointLight.h"
#include "core/Transform.h"
#include "core/Material.h"

#define COLOR_DEPTH 	(256.0f)
#define MIN_LIGHT 		(5.0f)

PointLight::PointLight(
		const Color& color,
		const Attenuation& attenuation,
		float intensity) :
				BaseLight(color, intensity),
				m_attenuation(attenuation)
{
	updateRadius();
}

PointLight::~PointLight()
{
}

void PointLight::setAttenuation(const Attenuation& attenuation)
{
    m_attenuation = attenuation;
    updateRadius();
}

const Attenuation& PointLight::getAttenuation() const
{
    return m_attenuation;
}

float PointLight::getRadius() const
{
    return m_radius;
}

const glm::vec3& PointLight::getPosition() const
{
	return getTransform()->getTransformedPosition();
}

const glm::mat4& PointLight::getProjection() const
{
	return m_projection;
}

void PointLight::updateRadius(float lightAngle)
{
	float a = m_attenuation.getExponent();
	float b = m_attenuation.getLinear();
	float c = m_attenuation.getConstant();

	m_radius = (-b + std::sqrt(b * b - 4 * a * (c - (COLOR_DEPTH / MIN_LIGHT) * getIntensity()))) / (2 * a);
	m_projection = glm::perspective(glm::radians(lightAngle), 1.0f, 0.1f, m_radius);
}
