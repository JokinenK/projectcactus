#ifndef SPOTLIGHT_H
#define SPOTLIGHT_H

#include "core/light/PointLight.h"

class Attenuation;

class SpotLight : public PointLight
{
public:
    SpotLight(const Color& color, const Attenuation& attenuation, float intensity = 1.0f, float coneAngle = 45.0f);
    virtual ~SpotLight();

    void setConeAngle(float coneAngle);
    float getConeAngle() const;
    float getCutoff() const;

    const glm::vec3 getDirection() const;

private:
    float m_coneAngle;
    float m_cutoff;
};

#endif // SPOTLIGHT_H

