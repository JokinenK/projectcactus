#include "core/light/BaseLight.h"
#include "core/GameObject.h"

BaseLight::BaseLight(const Color& color, float intensity) :
		m_color(color),
		m_intensity(intensity),
		m_shadowEnabled(false),
		m_shouldDeallocTransform(true),
		m_transform(new Transform())
{
}

BaseLight::~BaseLight()
{
	releaseTransform();
}

void BaseLight::start()
{
	GameObject* parent = getParent();

	if (parent) {
		releaseTransform();
		m_transform = parent->getTransform();
	}
}

Transform* BaseLight::getTransform() const
{
	return m_transform;
}

const Color& BaseLight::getColor() const
{
    return m_color;
}

void BaseLight::setColor(const Color& color)
{
    m_color = color;
}

float BaseLight::getIntensity() const
{
    return m_intensity;
}

void BaseLight::setIntensity(float intensity)
{
    m_intensity = intensity;
}

void BaseLight::setShadowEnabled(bool shadowEnabled)
{
	m_shadowEnabled = shadowEnabled;
}

bool BaseLight::isShadowEnabled() const
{
	return m_shadowEnabled;
}

void BaseLight::releaseTransform()
{
	if (m_shouldDeallocTransform) {
		delete m_transform;
		m_transform = 0;
		m_shouldDeallocTransform = false;
	}
}
