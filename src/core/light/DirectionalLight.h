#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H

#include "core/light/BaseLight.h"

class Camera;

class DirectionalLight : public BaseLight
{
public:
    DirectionalLight(const Color& color, float intensity = 1.0f);
	virtual ~DirectionalLight();

	const glm::vec3 getDirection() const;
};

#endif // DIRECTIONALLIGHT_H

