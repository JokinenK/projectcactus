/*
 * MaterialManager.h
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_MATERIALMANAGER_H_
#define PROJECTCACTUS_SRC_CORE_MATERIALMANAGER_H_

#include <map>
#include <string>
#include "core/Material.h"
#include "support/Singleton.h"

class MaterialManager : public Singleton<MaterialManager>
{
public:
	MaterialManager();
	virtual ~MaterialManager();

	Material* cacheMaterial(const Material& material);
	Material* findMaterial(Material::Id materialId);

private:
	std::map<Material::Id, Material> m_materials;
};

#endif /* PROJECTCACTUS_SRC_CORE_MATERIALMANAGER_H_ */
