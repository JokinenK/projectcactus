/*
 * GameLoop.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef GAMELOOP_H_
#define GAMELOOP_H_

#include "core/EventListener.h"
#include "core/events/MouseEvent.h"
#include "core/events/KeyboardEvent.h"

class IRenderer;
class IInput;
class IDisplay;
class Camera;
class GameObject;
class SceneRenderer;
class SkyBoxRenderer;
class Time;
class MessageBus;

namespace Gui { class GuiRenderer; }

class GameLoop  : public EventListener<MouseEvent>,
			      public EventListener<KeyboardEvent>
{
public:
	GameLoop(
			IDisplay* display,
			IInput* input,
			SceneRenderer* sceneRenderer,
			SkyBoxRenderer* skyBoxRenderer,
			Gui::GuiRenderer* guiRenderer);

    virtual ~GameLoop();

    bool isRunning() const;

    void start();
    void stop();
    void fixedUpdate();
    void lateUpdate();
    void update();
    void pause();
    void unpause();

    Camera* getPrimaryCamera() const;

	bool receiveEvent(const MouseEvent* mouseEvent);
	bool receiveEvent(const KeyboardEvent* keyboardEvent);

private:
    void gameLoop();
    void initCameras();

    bool m_running;
    IDisplay* m_display;
    IRenderer* m_renderer;
    SceneRenderer* m_sceneRenderer;
    SkyBoxRenderer* m_skyBoxRenderer;
    Gui::GuiRenderer* m_guiRenderer;

    IInput* m_input;
    Time* m_time;

    Camera* m_primaryCamera;
	MessageBus* m_messageBus;
};

#endif /* GAMELOOP_H_ */
