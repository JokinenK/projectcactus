/*
 * ViewFrustum.h
 *
 *  Created on: 12.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_VIEWFRUSTUM_H_
#define PROJECTCACTUS_SRC_CORE_VIEWFRUSTUM_H_

#include "core/physics/Frustum.h"
#include "core/physics/AABB.h"
#include "core/physics/Sphere.h"

class ViewFrustum : public Frustum
{
public:
	ViewFrustum();
	ViewFrustum(float fovRadians, float aspectRatio, float near, float far);
	ViewFrustum(const ViewFrustum& other);
	virtual ~ViewFrustum();

	ViewFrustum& operator=(const ViewFrustum& other);

	void setFov(float fovRadians);
	float getFov() const;

	void setAspectRatio(float aspectRatio);
	float getAspectRatio() const;

	void setNear(float near);
	float getNear() const;

	void setFar(float far);
	float getFar() const;

	const glm::mat4& getProjection() const;

	void updateCorners(const glm::vec3& position, const glm::vec3& up, const glm::vec3& forward, const glm::vec3& right);

	const AABB& getBoundingBox() const;
	const Sphere& getBoundingSphere() const;

	bool isDirty() const;

private:
	void updateProjection();

	float m_fovRadians;
	float m_aspectRatio;
	float m_near;
	float m_far;

	float m_nearHeight;
	float m_nearWidth;

	float m_farHeight;
	float m_farWidth;

	float m_halfFarWidth;
	float m_halfFarHeight;

	float m_halfNearWidth;
	float m_halfNearHeight;

	bool m_dirty;

	glm::mat4 m_projection;

	AABB m_boundingBox;
	Sphere m_boundingSphere;
};

#endif /* PROJECTCACTUS_SRC_CORE_VIEWFRUSTUM_H_ */
