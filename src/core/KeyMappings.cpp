#include "KeyMappings.h"
#include <algorithm>
#include <list>

bool KeyMappings::s_initialized = false;
std::map<int, KeyCode> KeyMappings::s_scanCodeToKeyCode;
std::map<int, ScanCode> KeyMappings::s_keyCodeToScanCode;

void KeyMappings::initialize()
{
	if (s_initialized) {
		return;
	}

	std::list<std::pair<ScanCode, KeyCode> > pairs;
	pairs.push_back(std::make_pair(SCANCODE_0, KEYCODE_0));
	pairs.push_back(std::make_pair(SCANCODE_1, KEYCODE_1));
	pairs.push_back(std::make_pair(SCANCODE_2, KEYCODE_2));
	pairs.push_back(std::make_pair(SCANCODE_3, KEYCODE_3));
	pairs.push_back(std::make_pair(SCANCODE_4, KEYCODE_4));
	pairs.push_back(std::make_pair(SCANCODE_5, KEYCODE_5));
	pairs.push_back(std::make_pair(SCANCODE_6, KEYCODE_6));
	pairs.push_back(std::make_pair(SCANCODE_7, KEYCODE_7));
	pairs.push_back(std::make_pair(SCANCODE_8, KEYCODE_8));
	pairs.push_back(std::make_pair(SCANCODE_9, KEYCODE_9));
	pairs.push_back(std::make_pair(SCANCODE_A, KEYCODE_a));
	pairs.push_back(std::make_pair(SCANCODE_B, KEYCODE_b));
	pairs.push_back(std::make_pair(SCANCODE_C, KEYCODE_c));
	pairs.push_back(std::make_pair(SCANCODE_D, KEYCODE_d));
	pairs.push_back(std::make_pair(SCANCODE_E, KEYCODE_e));
	pairs.push_back(std::make_pair(SCANCODE_F, KEYCODE_f));
	pairs.push_back(std::make_pair(SCANCODE_G, KEYCODE_g));
	pairs.push_back(std::make_pair(SCANCODE_H, KEYCODE_h));
	pairs.push_back(std::make_pair(SCANCODE_I, KEYCODE_i));
	pairs.push_back(std::make_pair(SCANCODE_J, KEYCODE_j));
	pairs.push_back(std::make_pair(SCANCODE_K, KEYCODE_k));
	pairs.push_back(std::make_pair(SCANCODE_L, KEYCODE_l));
	pairs.push_back(std::make_pair(SCANCODE_M, KEYCODE_m));
	pairs.push_back(std::make_pair(SCANCODE_N, KEYCODE_n));
	pairs.push_back(std::make_pair(SCANCODE_O, KEYCODE_o));
	pairs.push_back(std::make_pair(SCANCODE_P, KEYCODE_p));
	pairs.push_back(std::make_pair(SCANCODE_Q, KEYCODE_q));
	pairs.push_back(std::make_pair(SCANCODE_R, KEYCODE_r));
	pairs.push_back(std::make_pair(SCANCODE_S, KEYCODE_s));
	pairs.push_back(std::make_pair(SCANCODE_T, KEYCODE_t));
	pairs.push_back(std::make_pair(SCANCODE_U, KEYCODE_u));
	pairs.push_back(std::make_pair(SCANCODE_V, KEYCODE_v));
	pairs.push_back(std::make_pair(SCANCODE_W, KEYCODE_w));
	pairs.push_back(std::make_pair(SCANCODE_X, KEYCODE_x));
	pairs.push_back(std::make_pair(SCANCODE_Y, KEYCODE_y));
	pairs.push_back(std::make_pair(SCANCODE_Z, KEYCODE_z));

	pairs.push_back(std::make_pair(SCANCODE_F1, KEYCODE_F1));
	pairs.push_back(std::make_pair(SCANCODE_F2, KEYCODE_F2));
	pairs.push_back(std::make_pair(SCANCODE_F3, KEYCODE_F3));
	pairs.push_back(std::make_pair(SCANCODE_F4, KEYCODE_F4));
	pairs.push_back(std::make_pair(SCANCODE_F5, KEYCODE_F5));
	pairs.push_back(std::make_pair(SCANCODE_F6, KEYCODE_F6));
	pairs.push_back(std::make_pair(SCANCODE_F7, KEYCODE_F7));
	pairs.push_back(std::make_pair(SCANCODE_F8, KEYCODE_F8));
	pairs.push_back(std::make_pair(SCANCODE_F9, KEYCODE_F9));
	pairs.push_back(std::make_pair(SCANCODE_F10, KEYCODE_F10));
	pairs.push_back(std::make_pair(SCANCODE_F11, KEYCODE_F11));
	pairs.push_back(std::make_pair(SCANCODE_F12, KEYCODE_F12));
	pairs.push_back(std::make_pair(SCANCODE_F13, KEYCODE_F13));
	pairs.push_back(std::make_pair(SCANCODE_F14, KEYCODE_F14));
	pairs.push_back(std::make_pair(SCANCODE_F15, KEYCODE_F15));
	pairs.push_back(std::make_pair(SCANCODE_F16, KEYCODE_F16));
	pairs.push_back(std::make_pair(SCANCODE_F17, KEYCODE_F17));
	pairs.push_back(std::make_pair(SCANCODE_F18, KEYCODE_F18));
	pairs.push_back(std::make_pair(SCANCODE_F19, KEYCODE_F19));
	pairs.push_back(std::make_pair(SCANCODE_F20, KEYCODE_F20));
	pairs.push_back(std::make_pair(SCANCODE_F21, KEYCODE_F21));
	pairs.push_back(std::make_pair(SCANCODE_F22, KEYCODE_F22));
	pairs.push_back(std::make_pair(SCANCODE_F23, KEYCODE_F23));
	pairs.push_back(std::make_pair(SCANCODE_F24, KEYCODE_F24));

	pairs.push_back(std::make_pair(SCANCODE_LALT, KEYCODE_LALT));
	pairs.push_back(std::make_pair(SCANCODE_RALT, KEYCODE_RALT));

	pairs.push_back(std::make_pair(SCANCODE_LCTRL, KEYCODE_LCTRL));
	pairs.push_back(std::make_pair(SCANCODE_RCTRL, KEYCODE_RCTRL));

	pairs.push_back(std::make_pair(SCANCODE_LSHIFT, KEYCODE_LSHIFT));
	pairs.push_back(std::make_pair(SCANCODE_RSHIFT, KEYCODE_RSHIFT));

	pairs.push_back(std::make_pair(SCANCODE_LGUI, KEYCODE_LGUI));
	pairs.push_back(std::make_pair(SCANCODE_RGUI, KEYCODE_RGUI));

	pairs.push_back(std::make_pair(SCANCODE_RETURN, KEYCODE_RETURN));
	pairs.push_back(std::make_pair(SCANCODE_RETURN2, KEYCODE_RETURN2));

	pairs.push_back(std::make_pair(SCANCODE_LEFT, KEYCODE_LEFT));
	pairs.push_back(std::make_pair(SCANCODE_RIGHT, KEYCODE_RIGHT));
	pairs.push_back(std::make_pair(SCANCODE_UP, KEYCODE_UP));
	pairs.push_back(std::make_pair(SCANCODE_DOWN, KEYCODE_DOWN));

	pairs.push_back(std::make_pair(SCANCODE_SPACE, KEYCODE_SPACE));
	pairs.push_back(std::make_pair(SCANCODE_BACKSPACE, KEYCODE_BACKSPACE));

	pairs.push_back(std::make_pair(SCANCODE_PAGEDOWN, KEYCODE_PAGEDOWN));
	pairs.push_back(std::make_pair(SCANCODE_PAGEUP, KEYCODE_PAGEUP));

	pairs.push_back(std::make_pair(SCANCODE_HOME, KEYCODE_HOME));
	pairs.push_back(std::make_pair(SCANCODE_END, KEYCODE_END));

	pairs.push_back(std::make_pair(SCANCODE_VOLUMEDOWN, KEYCODE_VOLUMEDOWN));
	pairs.push_back(std::make_pair(SCANCODE_VOLUMEUP, KEYCODE_VOLUMEUP));

	pairs.push_back(std::make_pair(SCANCODE_LEFTBRACKET, KEYCODE_LEFTBRACKET));
	pairs.push_back(std::make_pair(SCANCODE_RIGHTBRACKET, KEYCODE_RIGHTBRACKET));

	pairs.push_back(std::make_pair(SCANCODE_COMMA, KEYCODE_COMMA));
	pairs.push_back(std::make_pair(SCANCODE_PERIOD, KEYCODE_PERIOD));

	pairs.push_back(std::make_pair(SCANCODE_SEMICOLON, KEYCODE_SEMICOLON));
	pairs.push_back(std::make_pair(SCANCODE_SEPARATOR, KEYCODE_SEPARATOR));

	pairs.push_back(std::make_pair(SCANCODE_TAB, KEYCODE_TAB));
	pairs.push_back(std::make_pair(SCANCODE_CAPSLOCK, KEYCODE_CAPSLOCK));

	pairs.push_back(std::make_pair(SCANCODE_AC_BACK, KEYCODE_AC_BACK));
	pairs.push_back(std::make_pair(SCANCODE_AC_BOOKMARKS, KEYCODE_AC_BOOKMARKS));
	pairs.push_back(std::make_pair(SCANCODE_AC_FORWARD, KEYCODE_AC_FORWARD));
	pairs.push_back(std::make_pair(SCANCODE_AC_HOME, KEYCODE_AC_HOME));
	pairs.push_back(std::make_pair(SCANCODE_AC_REFRESH, KEYCODE_AC_REFRESH));
	pairs.push_back(std::make_pair(SCANCODE_AC_SEARCH, KEYCODE_AC_SEARCH));
	pairs.push_back(std::make_pair(SCANCODE_AC_STOP, KEYCODE_AC_STOP));
	pairs.push_back(std::make_pair(SCANCODE_AGAIN, KEYCODE_AGAIN));
	pairs.push_back(std::make_pair(SCANCODE_ALTERASE, KEYCODE_ALTERASE));
	pairs.push_back(std::make_pair(SCANCODE_APOSTROPHE, KEYCODE_QUOTE));
	pairs.push_back(std::make_pair(SCANCODE_APPLICATION, KEYCODE_APPLICATION));
	pairs.push_back(std::make_pair(SCANCODE_AUDIOMUTE, KEYCODE_AUDIOMUTE));
	pairs.push_back(std::make_pair(SCANCODE_AUDIONEXT, KEYCODE_AUDIONEXT));
	pairs.push_back(std::make_pair(SCANCODE_AUDIOPLAY, KEYCODE_AUDIOPLAY));
	pairs.push_back(std::make_pair(SCANCODE_AUDIOPREV, KEYCODE_AUDIOPREV));
	pairs.push_back(std::make_pair(SCANCODE_AUDIOSTOP, KEYCODE_AUDIOSTOP));
	pairs.push_back(std::make_pair(SCANCODE_BACKSLASH, KEYCODE_BACKSLASH));
	pairs.push_back(std::make_pair(SCANCODE_BRIGHTNESSDOWN, KEYCODE_BRIGHTNESSDOWN));
	pairs.push_back(std::make_pair(SCANCODE_BRIGHTNESSUP, KEYCODE_BRIGHTNESSUP));
	pairs.push_back(std::make_pair(SCANCODE_CALCULATOR, KEYCODE_CALCULATOR));
	pairs.push_back(std::make_pair(SCANCODE_CANCEL, KEYCODE_CANCEL));
	pairs.push_back(std::make_pair(SCANCODE_CLEAR, KEYCODE_CLEAR));
	pairs.push_back(std::make_pair(SCANCODE_CLEARAGAIN, KEYCODE_CLEARAGAIN));
	pairs.push_back(std::make_pair(SCANCODE_COMPUTER, KEYCODE_COMPUTER));
	pairs.push_back(std::make_pair(SCANCODE_COPY, KEYCODE_COPY));
	pairs.push_back(std::make_pair(SCANCODE_CRSEL, KEYCODE_CRSEL));
	pairs.push_back(std::make_pair(SCANCODE_CURRENCYSUBUNIT, KEYCODE_CURRENCYSUBUNIT));
	pairs.push_back(std::make_pair(SCANCODE_CURRENCYUNIT, KEYCODE_CURRENCYUNIT));
	pairs.push_back(std::make_pair(SCANCODE_CUT, KEYCODE_CUT));
	pairs.push_back(std::make_pair(SCANCODE_DECIMALSEPARATOR, KEYCODE_DECIMALSEPARATOR));
	pairs.push_back(std::make_pair(SCANCODE_DELETE, KEYCODE_DELETE));
	pairs.push_back(std::make_pair(SCANCODE_DISPLAYSWITCH, KEYCODE_DISPLAYSWITCH));
	pairs.push_back(std::make_pair(SCANCODE_EJECT, KEYCODE_EJECT));
	pairs.push_back(std::make_pair(SCANCODE_EQUALS, KEYCODE_EQUALS));
	pairs.push_back(std::make_pair(SCANCODE_ESCAPE, KEYCODE_ESCAPE));
	pairs.push_back(std::make_pair(SCANCODE_EXECUTE, KEYCODE_EXECUTE));
	pairs.push_back(std::make_pair(SCANCODE_EXSEL, KEYCODE_EXSEL));
	pairs.push_back(std::make_pair(SCANCODE_FIND, KEYCODE_FIND));
	pairs.push_back(std::make_pair(SCANCODE_GRAVE, KEYCODE_BACKQUOTE));
	pairs.push_back(std::make_pair(SCANCODE_HELP, KEYCODE_HELP));
	pairs.push_back(std::make_pair(SCANCODE_INSERT, KEYCODE_INSERT));
	pairs.push_back(std::make_pair(SCANCODE_KBDILLUMDOWN, KEYCODE_KBDILLUMDOWN));
	pairs.push_back(std::make_pair(SCANCODE_KBDILLUMTOGGLE, KEYCODE_KBDILLUMTOGGLE));
	pairs.push_back(std::make_pair(SCANCODE_KBDILLUMUP, KEYCODE_KBDILLUMUP));
	pairs.push_back(std::make_pair(SCANCODE_KP_0, KEYCODE_KP_0));
	pairs.push_back(std::make_pair(SCANCODE_KP_00, KEYCODE_KP_00));
	pairs.push_back(std::make_pair(SCANCODE_KP_000, KEYCODE_KP_000));
	pairs.push_back(std::make_pair(SCANCODE_KP_1, KEYCODE_KP_1));
	pairs.push_back(std::make_pair(SCANCODE_KP_2, KEYCODE_KP_2));
	pairs.push_back(std::make_pair(SCANCODE_KP_3, KEYCODE_KP_3));
	pairs.push_back(std::make_pair(SCANCODE_KP_4, KEYCODE_KP_4));
	pairs.push_back(std::make_pair(SCANCODE_KP_5, KEYCODE_KP_5));
	pairs.push_back(std::make_pair(SCANCODE_KP_6, KEYCODE_KP_6));
	pairs.push_back(std::make_pair(SCANCODE_KP_7, KEYCODE_KP_7));
	pairs.push_back(std::make_pair(SCANCODE_KP_8, KEYCODE_KP_8));
	pairs.push_back(std::make_pair(SCANCODE_KP_9, KEYCODE_KP_9));
	pairs.push_back(std::make_pair(SCANCODE_KP_A, KEYCODE_KP_A));
	pairs.push_back(std::make_pair(SCANCODE_KP_AMPERSAND, KEYCODE_KP_AMPERSAND));
	pairs.push_back(std::make_pair(SCANCODE_KP_AT, KEYCODE_KP_AT));
	pairs.push_back(std::make_pair(SCANCODE_KP_B, KEYCODE_KP_B));
	pairs.push_back(std::make_pair(SCANCODE_KP_BACKSPACE, KEYCODE_KP_BACKSPACE));
	pairs.push_back(std::make_pair(SCANCODE_KP_BINARY, KEYCODE_KP_BINARY));
	pairs.push_back(std::make_pair(SCANCODE_KP_C, KEYCODE_KP_C));
	pairs.push_back(std::make_pair(SCANCODE_KP_CLEAR, KEYCODE_KP_CLEAR));
	pairs.push_back(std::make_pair(SCANCODE_KP_CLEARENTRY, KEYCODE_KP_CLEARENTRY));
	pairs.push_back(std::make_pair(SCANCODE_KP_COLON, KEYCODE_KP_COLON));
	pairs.push_back(std::make_pair(SCANCODE_KP_COMMA, KEYCODE_KP_COMMA));
	pairs.push_back(std::make_pair(SCANCODE_KP_D, KEYCODE_KP_D));
	pairs.push_back(std::make_pair(SCANCODE_KP_DBLAMPERSAND, KEYCODE_KP_DBLAMPERSAND));
	pairs.push_back(std::make_pair(SCANCODE_KP_DBLVERTICALBAR, KEYCODE_KP_DBLVERTICALBAR));
	pairs.push_back(std::make_pair(SCANCODE_KP_DECIMAL, KEYCODE_KP_DECIMAL));
	pairs.push_back(std::make_pair(SCANCODE_KP_DIVIDE, KEYCODE_KP_DIVIDE));
	pairs.push_back(std::make_pair(SCANCODE_KP_E, KEYCODE_KP_E));
	pairs.push_back(std::make_pair(SCANCODE_KP_ENTER, KEYCODE_KP_ENTER));
	pairs.push_back(std::make_pair(SCANCODE_KP_EQUALS, KEYCODE_KP_EQUALS));
	pairs.push_back(std::make_pair(SCANCODE_KP_EQUALSAS400, KEYCODE_KP_EQUALSAS400));
	pairs.push_back(std::make_pair(SCANCODE_KP_EXCLAM, KEYCODE_KP_EXCLAM));
	pairs.push_back(std::make_pair(SCANCODE_KP_F, KEYCODE_KP_F));
	pairs.push_back(std::make_pair(SCANCODE_KP_GREATER, KEYCODE_KP_GREATER));
	pairs.push_back(std::make_pair(SCANCODE_KP_HASH, KEYCODE_KP_HASH));
	pairs.push_back(std::make_pair(SCANCODE_KP_HEXADECIMAL, KEYCODE_KP_HEXADECIMAL));
	pairs.push_back(std::make_pair(SCANCODE_KP_LEFTBRACE, KEYCODE_KP_LEFTBRACE));
	pairs.push_back(std::make_pair(SCANCODE_KP_LEFTPAREN, KEYCODE_KP_LEFTPAREN));
	pairs.push_back(std::make_pair(SCANCODE_KP_LESS, KEYCODE_KP_LESS));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMADD, KEYCODE_KP_MEMADD));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMCLEAR, KEYCODE_KP_MEMCLEAR));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMDIVIDE, KEYCODE_KP_MEMDIVIDE));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMMULTIPLY, KEYCODE_KP_MEMMULTIPLY));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMRECALL, KEYCODE_KP_MEMRECALL));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMSTORE, KEYCODE_KP_MEMSTORE));
	pairs.push_back(std::make_pair(SCANCODE_KP_MEMSUBTRACT, KEYCODE_KP_MEMSUBTRACT));
	pairs.push_back(std::make_pair(SCANCODE_KP_MINUS, KEYCODE_KP_MINUS));
	pairs.push_back(std::make_pair(SCANCODE_KP_MULTIPLY, KEYCODE_KP_MULTIPLY));
	pairs.push_back(std::make_pair(SCANCODE_KP_OCTAL, KEYCODE_KP_OCTAL));
	pairs.push_back(std::make_pair(SCANCODE_KP_PERCENT, KEYCODE_KP_PERCENT));
	pairs.push_back(std::make_pair(SCANCODE_KP_PERIOD, KEYCODE_KP_PERIOD));
	pairs.push_back(std::make_pair(SCANCODE_KP_PLUS, KEYCODE_KP_PLUS));
	pairs.push_back(std::make_pair(SCANCODE_KP_PLUSMINUS, KEYCODE_KP_PLUSMINUS));
	pairs.push_back(std::make_pair(SCANCODE_KP_POWER, KEYCODE_KP_POWER));
	pairs.push_back(std::make_pair(SCANCODE_KP_RIGHTBRACE, KEYCODE_KP_RIGHTBRACE));
	pairs.push_back(std::make_pair(SCANCODE_KP_RIGHTPAREN, KEYCODE_KP_RIGHTPAREN));
	pairs.push_back(std::make_pair(SCANCODE_KP_SPACE, KEYCODE_KP_SPACE));
	pairs.push_back(std::make_pair(SCANCODE_KP_TAB, KEYCODE_KP_TAB));
	pairs.push_back(std::make_pair(SCANCODE_KP_VERTICALBAR, KEYCODE_KP_VERTICALBAR));
	pairs.push_back(std::make_pair(SCANCODE_KP_XOR, KEYCODE_KP_XOR));
	pairs.push_back(std::make_pair(SCANCODE_MAIL, KEYCODE_MAIL));
	pairs.push_back(std::make_pair(SCANCODE_MEDIASELECT, KEYCODE_MEDIASELECT));
	pairs.push_back(std::make_pair(SCANCODE_MENU, KEYCODE_MENU));
	pairs.push_back(std::make_pair(SCANCODE_MINUS, KEYCODE_MINUS));
	pairs.push_back(std::make_pair(SCANCODE_MODE, KEYCODE_MODE));
	pairs.push_back(std::make_pair(SCANCODE_MUTE, KEYCODE_MUTE));
	pairs.push_back(std::make_pair(SCANCODE_NUMLOCKCLEAR, KEYCODE_NUMLOCKCLEAR));
	pairs.push_back(std::make_pair(SCANCODE_OPER, KEYCODE_OPER));
	pairs.push_back(std::make_pair(SCANCODE_OUT, KEYCODE_OUT));
	pairs.push_back(std::make_pair(SCANCODE_PASTE, KEYCODE_PASTE));
	pairs.push_back(std::make_pair(SCANCODE_PAUSE, KEYCODE_PAUSE));
	pairs.push_back(std::make_pair(SCANCODE_POWER, KEYCODE_POWER));
	pairs.push_back(std::make_pair(SCANCODE_PRINTSCREEN, KEYCODE_PRINTSCREEN));
	pairs.push_back(std::make_pair(SCANCODE_PRIOR, KEYCODE_PRIOR));
	pairs.push_back(std::make_pair(SCANCODE_SCROLLLOCK, KEYCODE_SCROLLLOCK));
	pairs.push_back(std::make_pair(SCANCODE_SELECT, KEYCODE_SELECT));
	pairs.push_back(std::make_pair(SCANCODE_SLASH, KEYCODE_SLASH));
	pairs.push_back(std::make_pair(SCANCODE_SLEEP, KEYCODE_SLEEP));
	pairs.push_back(std::make_pair(SCANCODE_STOP, KEYCODE_STOP));
	pairs.push_back(std::make_pair(SCANCODE_SYSREQ, KEYCODE_SYSREQ));
	pairs.push_back(std::make_pair(SCANCODE_THOUSANDSSEPARATOR, KEYCODE_THOUSANDSSEPARATOR));
	pairs.push_back(std::make_pair(SCANCODE_UNDO, KEYCODE_UNDO));
	pairs.push_back(std::make_pair(SCANCODE_WWW, KEYCODE_WWW));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL1, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL2, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL3, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL4, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL5, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL6, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL7, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL8, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_INTERNATIONAL9, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG1, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG2, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG3, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG4, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG5, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG6, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG7, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG8, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_LANG9, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_NONUSBACKSLASH, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_NONUSHASH, KEYCODE_UNKNOWN));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_AMPERSAND));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_ASTERISK));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_AT));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_CARET));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_COLON));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_DOLLAR));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_EXCLAM));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_GREATER));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_HASH));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_LEFTPAREN));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_LESS));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_PERCENT));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_PLUS));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_QUESTION));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_QUOTEDBL));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_RIGHTPAREN));
	pairs.push_back(std::make_pair(SCANCODE_UNKNOWN, KEYCODE_UNDERSCORE));

	for (std::pair<ScanCode, KeyCode> pair : pairs) {
		if (pair.first != SCANCODE_UNKNOWN) {
			s_scanCodeToKeyCode[pair.first] = pair.second;
		}

		if (pair.second != KEYCODE_UNKNOWN) {
			s_keyCodeToScanCode[pair.second] = pair.first;
		}
	}

	s_initialized = true;
}

KeyCode KeyMappings::scanCodeToKeyCode(int scanCode)
{
	if (!s_initialized) {
		initialize();
	}

	auto iter = s_scanCodeToKeyCode.find(scanCode);

	if (iter != s_scanCodeToKeyCode.end()) {
		return iter->second;
	}

	return KEYCODE_UNKNOWN;
}

ScanCode KeyMappings::keyCodeToScanCode(int keyCode)
{
	if (!s_initialized) {
		initialize();
	}

	auto iter = s_keyCodeToScanCode.find(keyCode);

	if (iter != s_keyCodeToScanCode.end()) {
		return iter->second;
	}

	return SCANCODE_UNKNOWN;
}
