#ifndef IINPUT_H_
#define IINPUT_H_

class MessageBus;

class IInput
{
public:
    virtual ~IInput() {}

    virtual void init(MessageBus* messageBus) = 0;
    virtual void update() = 0;
};

#endif /* IINPUT_H_ */
