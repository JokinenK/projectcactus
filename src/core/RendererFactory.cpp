/*
 * GraphicsFactory.cpp
 *
 *  Created on: 7.10.2016
 *      Author: kalle
 */

#include <vector>
#include "RendererFactory.h"
#include "core/IRenderer.h"
#include "core/ITexture.h"
#include "core/IShader.h"
#include "core/IModel.h"
#include "core/IFramebuffer.h"
#include "core/model/VertexModel.h"
#include "core/model/IndexedVertexModel.h"

RendererFactory::RendererFactory() :
		m_renderer(0)
{
}

RendererFactory::~RendererFactory()
{
}

bool RendererFactory::setRenderer(IRenderer* renderer)
{
	m_renderer = renderer;

	if (!m_renderer) {
		std::cerr << "Unable to load renderer plugin" << std::endl;
		return false;
	}

	return true;
}

IRenderer* RendererFactory::getRenderer() const
{
	return m_renderer;
}

IShader* RendererFactory::createShader() const
{
	IShader* shader = 0;

	if (m_renderer) {
		shader = m_renderer->createShader();
	}
	else {
		std::cerr << "Unable to create shader" << std::endl;
	}

	return shader;
}

void RendererFactory::releaseShader(IShader* shader)
{
	if (m_renderer) {
		m_renderer->releaseShader(shader);
	}
	else {
		std::cerr << "Unable to release shader" << std::endl;
	}
}

ITexture* RendererFactory::createTexture() const
{
	ITexture* texture = 0;

	if (m_renderer) {
		texture = m_renderer->createTexture();
	}
	else {
		std::cerr << "Unable to create texture" << std::endl;
	}

	return texture;
}

void RendererFactory::releaseTexture(ITexture* texture)
{
	if (m_renderer) {
		m_renderer->releaseTexture(texture);
	}
	else {
		std::cerr << "Unable to release texture" << std::endl;
	}
}

IModel* RendererFactory::createModel() const
{
	IModel* model = 0;

	if (m_renderer) {
		model = m_renderer->createModel();
	}
	else {
		std::cerr << "Unable to create model" << std::endl;
	}

	return model;
}

void RendererFactory::releaseModel(IModel* model)
{
	if (m_renderer) {
		m_renderer->releaseModel(model);
	}
	else {
		std::cerr << "Unable to release model" << std::endl;
	}
}

IFramebuffer* RendererFactory::createFramebuffer() const
{
	IFramebuffer* framebuffer = 0;

	if (m_renderer) {
		framebuffer = m_renderer->createFramebuffer();
	}
	else {
		std::cerr << "Unable to create framebuffer" << std::endl;
	}

	return framebuffer;
}

void RendererFactory::releaseFramebuffer(IFramebuffer* framebuffer)
{
	if (m_renderer) {
		m_renderer->releaseFramebuffer(framebuffer);
	}
	else {
		std::cerr << "Unable to release framebuffer" << std::endl;
	}
}

IModel* RendererFactory::createQuad(float left, float right, float top, float bottom, bool inverted)
{
	VertexModel modelData;

	glm::vec3 posTopLeft(left, top, 1);
	glm::vec3 posTopRight(right, top, 1);
	glm::vec3 posBottomLeft(left, bottom, 1);
	glm::vec3 posBottomRight(right, bottom, 1);

	if (!inverted) {
		modelData.addPosition(posBottomLeft);
		modelData.addPosition(posBottomRight);
		modelData.addPosition(posTopRight);
		modelData.addPosition(posBottomLeft);
		modelData.addPosition(posTopRight);
		modelData.addPosition(posTopLeft);
	}
	else {
		modelData.addPosition(posTopLeft);
		modelData.addPosition(posTopRight);
		modelData.addPosition(posBottomLeft);
		modelData.addPosition(posTopRight);
		modelData.addPosition(posBottomRight);
		modelData.addPosition(posBottomLeft);
	}

	IndexedVertexModel indexedVertexModel = modelData.toIndexedVertexModel();

	IModel* model = createModel();
	model->setIndices(indexedVertexModel.getIndices());
	model->setPositions(indexedVertexModel.getPositions());
	model->setTexCoords(indexedVertexModel.getTexCoords());
	model->setNormals(indexedVertexModel.getNormals());
	model->setTangents(indexedVertexModel.getTangents());
	return model;
}
