#ifndef IDISPLAY_H_
#define IDISPLAY_H_

#include <string>

class IDisplay
{
public:
	virtual ~IDisplay() {};

	virtual void init(const std::string& title, short width, short height) = 0;
	virtual short getWidth() const = 0;
	virtual short getHeight() const = 0;
	virtual float aspectRatio() const = 0;
	virtual bool isClosed() const = 0;

	virtual void setFullscreen(bool fullscreen) = 0;
	virtual void captureMouse(bool capture) = 0;
	virtual void swapBuffers() = 0;
};

#endif // IDISPLAY_H_
