/*
 * Material.h
 *
 *  Created on: 12.3.2016
 *      Author: Kalle Jokinen
 */

#ifndef MATERIAL_H
#define MATERIAL_H

#include <string>
#include "core/ITexture.h"
#include "support/Color.h"

class Material
{
public:
	typedef int Id;

	const static Material EMPTY;
	const static Material::Id INVALID_ID = -1;

	typedef enum {
		FIRST_TEXTURE,
	    TEXTURE_AMBIENT = FIRST_TEXTURE,
	    TEXTURE_DIFFUSE,
	    TEXTURE_SPECULAR,
	    TEXTURE_SHININESS,
	    TEXTURE_DISSOLVE,
	    TEXTURE_NORMAL,
	    TEXTURE_DISPLACEMENT,
		NUM_TEXTURES
	} TextureName;

	Material();
    Material(const Material& other);
    virtual ~Material();

    Material& operator=(const Material& other);

    void clear();
    void finalize();

    bool isValid() const;

	void setMaterialName(const std::string& materialName);
	const std::string& getMaterialName() const;
	void clearMaterialName();

    void setMaterialId(Material::Id materialId);
    Material::Id getMaterialId() const;
    void clearMaterialId();

    Material* setAmbientColor(const Color& ambientColor);
    const Color& getAmbientColor() const;
    void clearAmbientColor();

    Material* setDiffuseColor(const Color& diffuseColor);
    const Color& getDiffuseColor() const;
    void clearDiffuseColor();

    Material* setSpecularColor(const Color& specularColor);
    const Color& getSpecularColor() const;
    void clearSpecularColor();

    Material* setShininess(float shininess);
    float getShininess() const;
    void clearShininess();

	Material* setDissolve(float dissolve);
	float getDissolve() const;
	void clearDissolve();

    bool hasAmbientTexture() const;
    Material* setAmbientTexture(const std::string& ambientTexture);
	ITexture* getAmbientTexture() const;
	void clearAmbientTexture();

    bool hasDiffuseTexture() const;
    Material* setDiffuseTexture(const std::string& diffuseTexture);
    ITexture* getDiffuseTexture() const;
    void clearDiffuseTexture();

    bool hasSpecularTexture() const;
    Material* setSpecularTexture(const std::string& specularTexture);
	ITexture* getSpecularTexture() const;
	void clearSpecularTexture();

	bool hasShininessTexture() const;
	Material* setShininessTexture(const std::string& specularIntensityTexture);
	ITexture* getShininessTexture() const;
	void clearShininessTexture();

	bool hasDissolveTexture() const;
	Material* setDissolveTexture(const std::string& dissolveTexture);
	ITexture* getDissolveTexture() const;
	void clearDissolveTexture();

    bool hasNormalTexture() const;
    Material* setNormalTexture(const std::string& normalTexture);
    ITexture* getNormalTexture() const;
    void clearNormalTexture();

    bool hasDisplacementTexture() const;
    Material* setDisplacementTexture(const std::string& displacementTexture);
    ITexture* getDisplacementTexture() const;
    void clearDisplacementTexture();

    Material* setTexture(TextureName textureName, ITexture* texture);
	ITexture* getTexture(TextureName textureName) const;
	bool hasTexture(TextureName textureName) const;

private:
    std::string m_materialName;
    Material::Id m_materialId;

    Color m_ambientColor;
    Color m_diffuseColor;
    Color m_specularColor;
    float m_shininess;
    float m_dissolve;

    ITexture* m_textures[NUM_TEXTURES];
};

#endif // MATERIAL_H

