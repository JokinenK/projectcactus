/*
 * ITechnique.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_ITECHNIQUE_H_
#define PROJECTCACTUS_SRC_CORE_ITECHNIQUE_H_

#include "core/Transform.h"
#include "core/IModel.h"
#include "core/Material.h"

class Material;
class RenderQueue;

class ITechnique
{
public:
	virtual ~ITechnique() {};

	virtual void bind() = 0;
	virtual void unbind() = 0;

	virtual void render(RenderQueue& queue) = 0;
};



#endif /* PROJECTCACTUS_SRC_CORE_ITECHNIQUE_H_ */
