/*
 * TextureManager.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef TEXTUREMANAGER_H_
#define TEXTUREMANAGER_H_

#include <map>
#include <string>
#include "support/Singleton.h"
#include "support/Image.h"

class ITexture;

class TextureManager : public Singleton<TextureManager>
{
public:
	TextureManager();
	virtual ~TextureManager();

	ITexture* getTexture2D(const std::string& filePath, int flipMask = Image::FLIP_Y);

private:
	std::map<std::string, ITexture*> m_cachedTextures;
};

#endif /* TEXTUREMANAGER_H_ */
