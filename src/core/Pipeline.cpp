/*
 * Pipeline.cpp
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#include <iostream>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/orthonormalize.hpp>
#include "Pipeline.h"

Pipeline::Pipeline()
{
	setView(glm::mat4(1));
	setProjection(glm::mat4(1));
	setModel(glm::mat4(1));
}

Pipeline::Pipeline(const Pipeline& other)
{
	operator=(other);
}

Pipeline::~Pipeline()
{
}

Pipeline& Pipeline::operator=(const Pipeline& other)
{
	m_projectionDirty = other.m_projectionDirty;
	m_viewDirty = other.m_viewDirty;
	m_modelDirty = other.m_modelDirty;
 	m_projection = other.m_projection;
	m_projectionInverse = other.m_projectionInverse;
 	m_view = other.m_view;
	m_viewInverse = other.m_viewInverse;
 	m_model = other.m_model;
	m_modelInverse = other.m_modelInverse;
 	m_vp = other.m_vp;
	m_vpInverse = other.m_vpInverse;
 	m_mv = other.m_mv;
	m_mvInverse = other.m_mvInverse;
 	m_mvp = other.m_mvp;
	m_mvpInverse = other.m_mvpInverse;
	m_normalMatrix = other.m_normalMatrix;

	return *this;
}

void Pipeline::setProjection(const glm::mat4& projection)
{
	if (projection == m_projection) {
		return;
	}

	m_projection = projection;
	m_projectionInverse = glm::inverse(m_projection);
	m_projectionDirty = true;

	update();
}

const glm::mat4& Pipeline::getProjection() const
{
	return m_projection;
}

void Pipeline::setView(const glm::mat4& view)
{
	if (view == m_view) {
		return;
	}

	m_view = view;
	m_viewInverse = glm::inverse(m_view);
	m_viewDirty = true;

	update();
}

const glm::mat4& Pipeline::getView() const
{
	return m_view;
}

void Pipeline::setModel(const glm::mat4& model)
{
	if (model == m_model) {
		return;
	}

	m_model = model;
	m_modelInverse = glm::inverse(m_model);
	m_modelDirty = true;

	m_normalMatrix = glm::transpose(m_modelInverse);

	update();
}

const glm::mat4& Pipeline::getModel() const
{
	return m_model;
}

const glm::mat4& Pipeline::getInverseProjection() const
{
	return m_projectionInverse;
}

const glm::mat4& Pipeline::getInverseView() const
{
	return m_viewInverse;
}

const glm::mat4& Pipeline::getInverseModel() const
{
	return m_modelInverse;
}

const glm::mat4& Pipeline::getVP() const
{
	return m_vp;
}

const glm::mat4& Pipeline::getInverseVP() const
{
	return m_vpInverse;
}

const glm::mat4& Pipeline::getMV() const
{
	return m_mv;
}

const glm::mat4& Pipeline::getInverseMV() const
{
	return m_mvInverse;
}

const glm::mat4& Pipeline::getMVP() const
{
	return m_mvp;
}

const glm::mat4& Pipeline::getInverseMVP() const
{
	return m_mvpInverse;
}

const glm::mat4& Pipeline::getNormalMatrix() const
{
	return m_normalMatrix;
}

void Pipeline::update()
{
	if (m_viewDirty || m_projectionDirty) {
		m_vp = m_projection * m_view;
		m_vpInverse = m_projectionInverse * m_viewInverse;
	}

	if (m_viewDirty || m_modelDirty) {
		m_mv = m_view * m_model;
		m_mvInverse = m_viewInverse * m_modelInverse;
	}

	if (m_viewDirty || m_modelDirty || m_projectionDirty) {
		m_mvp = m_vp * m_model;
		m_mvpInverse = m_vpInverse * m_modelInverse;
	}

	m_modelDirty = false;
	m_viewDirty = false;
	m_projectionDirty = false;
}
