/*
 * RenderQueue.cpp
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#include "RenderQueue.h"
#include "core/BoundingBox.h"
#include "core/IRenderPrerequisite.h"

RenderQueue::RenderQueue(const IRenderPrerequisite& renderPrerequisite) :
		m_renderPrerequisite(renderPrerequisite)
{
}

RenderQueue::~RenderQueue()
{
}

bool RenderQueue::shouldRender(const BoundingBox* boundingBox)
{
	return m_renderPrerequisite.shouldRender(boundingBox);
}

void RenderQueue::addToQueue(Key key, Value entry)
{
	m_queue.insert(std::make_pair(key, entry));
}

void RenderQueue::sort()
{
}

void RenderQueue::clear()
{
	m_queue.clear();
}

RenderQueue::Iterator RenderQueue::begin()
{
	return m_queue.begin();
}

RenderQueue::Iterator RenderQueue::end()
{
	return m_queue.end();
}
