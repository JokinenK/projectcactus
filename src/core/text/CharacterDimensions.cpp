/*
 * CharacterRectangle.cpp
 *
 *  Created on: 1.10.2016
 *      Author: kalle
 */

#include "CharacterDimensions.h"

#include <algorithm>

CharacterDimensions::CharacterDimensions() :
	m_character(0)
{
}

CharacterDimensions::CharacterDimensions(unsigned long ch, const Dimensions& dimensions) :
	m_character(ch),
	m_dimensions(dimensions)
{
}

CharacterDimensions::~CharacterDimensions()
{
}

CharacterDimensions& CharacterDimensions::operator=(const CharacterDimensions& other)
{
	m_character = other.getCharacter();
	m_dimensions = other.getDimensions();
	return *this;
}

bool CharacterDimensions::operator<(const CharacterDimensions& other) const
{
	return m_dimensions.getHeight() < other.getDimensions().getHeight();
}

const Dimensions& CharacterDimensions::getDimensions() const
{
	return m_dimensions;
}

unsigned long CharacterDimensions::getCharacter() const
{
	return m_character;
}
