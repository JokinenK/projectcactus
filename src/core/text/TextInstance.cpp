/*
 * TextInstance.cpp
 *
 *  Created on: 1.10.2016
 *      Author: kalle
 */

#include "TextInstance.h"

TextInstance::TextInstance(
		Font* font,
		const std::string& text) :
				m_font(font),
				m_text(text)
{
}

TextInstance::~TextInstance()
{
}

void TextInstance::setText(const std::string& text)
{
	m_text = text;
}

const std::string& TextInstance::getText() const
{
	return m_text;
}

void TextInstance::setPosition(const Point& position)
{
	m_position = position;
}

const Point& TextInstance::getPosition() const
{
	return m_position;
}

void TextInstance::setLeft(float left)
{
	m_position.setX(left);
}

float TextInstance::getLeft() const
{
	return m_position.getX();
}

void TextInstance::setTop(float top)
{
	m_position.setY(top);
}

float TextInstance::getTop() const
{
	return m_position.getY();
}

void TextInstance::setFont(Font* font)
{
	m_font = font;
}

Font* TextInstance::getFont() const
{
	return m_font;
}

float TextInstance::getWidth() const
{
	return m_font->width(m_text);
}

float TextInstance::getHeight() const
{
	return m_font->height();
}

void TextInstance::render(IShader* shader, IModel* model) const
{
	m_font->render(shader, model, m_position, m_text);
}

