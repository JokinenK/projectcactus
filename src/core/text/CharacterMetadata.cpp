/*
 * Character.cpp
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#include "CharacterMetadata.h"

CharacterMetadata::CharacterMetadata() :
	m_boundingBox(0, 0, 0, 0),
	m_texCoords(0, 0, 0, 0),
	m_bearingX(0),
	m_bearingY(0),
	m_advance(0),
	m_valid(false)
{
}

CharacterMetadata::CharacterMetadata(
		const Rectangle& boundingBox,
		const Rectangle& texCoords,
		short bearingX,
		short bearingY,
		short advance) :
			m_boundingBox(boundingBox),
			m_texCoords(texCoords),
			m_bearingX(bearingX),
			m_bearingY(bearingY),
			m_advance(advance),
			m_valid(true)
{
}

CharacterMetadata::~CharacterMetadata()
{
}

CharacterMetadata& CharacterMetadata::operator=(const CharacterMetadata& other)
{
	m_boundingBox = other.getBoundingBox();
	m_texCoords = other.getTexCoords();
	m_bearingX = other.getBearingX();
	m_bearingY = other.getBearingY();
	m_advance = other.getAdvance();
	m_valid = other.isValid();

	return *this;
}

const Rectangle& CharacterMetadata::getBoundingBox() const
{
	return m_boundingBox;
}

const Rectangle& CharacterMetadata::getTexCoords() const
{
	return m_texCoords;
}

short CharacterMetadata::getBearingX() const
{
	return m_bearingX;
}

short CharacterMetadata::getBearingY() const
{
	return m_bearingY;
}

short CharacterMetadata::getAdvance() const
{
	return m_advance;
}

bool CharacterMetadata::isValid() const
{
	return m_valid;
}
