/*
 * Font.cpp
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#include "Font.h"
#include "FontAtlas.h"
#include "core/FontManager.h"
#include "core/IShader.h"
#include "core/IUniform.h"
#include "support/Utils.h"
#include "hal/File.h"

namespace
{
	const static std::string WHITESPACE = std::string(" ");
}

Font::Font(
		const std::string& fontName,
		float fontSize,
		Color textColor,
		Color borderColor) :
				m_fontAtlas(FontManager::instance()->getFontAtlas(fontName)),
                m_fontHeight(1.0f)
{
	setFontSize(fontSize);
	setTextColor(textColor);
	setBorderColor(borderColor);
}

Font::~Font()
{
}

void Font::setFontSize(float fontSize) {
	m_fontSize = fontSize;
	m_fontScale = fontSize / m_fontAtlas->fontSize();
}

float Font::getFontSize() const
{
	return m_fontSize;
}

void Font::setTextColor(const Color& textColor)
{
	m_textColor = textColor;
}

const Color& Font::getTextColor() const
{
	return m_textColor;
}

void Font::setBorderColor(const Color& borderColor)
{
	m_borderColor = borderColor;
}

const Color& Font::getBorderColor() const
{
	return m_borderColor;
}

void Font::render(
		IShader* shader,
		IModel* model,
		const Point& position,
		const std::string& text) const
{
	m_fontAtlas->getTexture()->bind(1);

	shader->getUniform("uniformTextColor")->set(m_textColor.toVec4());
	shader->getUniform("uniformOutlineColor")->set(m_borderColor.toVec4());
	shader->getUniform("uniformIsText")->set(true);

	float left = position.getX();
	float top = position.getY();

    std::string::const_iterator ch;
    for (ch = text.begin(); ch != text.end(); ++ch) {
    	m_fontAtlas->renderCharacter(shader, model, left, top, *ch, m_fontScale);
    }

    shader->getUniform("uniformIsText")->set(false);

    m_fontAtlas->getTexture()->unbind(1);
}

float Font::width(const std::string& text) const
{
	return m_fontAtlas->width(text) * m_fontScale;
}

float Font::height() const
{
	return m_fontAtlas->height() * m_fontScale;
}

Rectangle Font::boundingBox(const std::string& text, const Rectangle& target) const
{
	float fontHeight = height();
	float spaceWidth = width(WHITESPACE);

	float currentWidth = 0;
	float targetWidth = target.getWidth();

	float totalWidth = 0;
	float totalHeight = 0;

	std::list<std::string> words = Utils::splitString(text, WHITESPACE);

	for (const std::string& word : words) {
		float wordWidth = width(word);

		if (currentWidth + wordWidth > targetWidth) {
			totalWidth = std::max(totalWidth, currentWidth);
			totalHeight += fontHeight;
			currentWidth = wordWidth;
		}
		else {
			currentWidth += wordWidth + spaceWidth;
		}
	}

	Rectangle boundingBox;
	boundingBox.setLeft(target.getLeft());
	boundingBox.setTop(target.getTop());
	boundingBox.setWidth(totalWidth);
	boundingBox.setHeight(totalHeight);

	return boundingBox;
}


