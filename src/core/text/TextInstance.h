/*
 * TextInstance.h
 *
 *  Created on: 1.10.2016
 *      Author: kalle
 */

#ifndef TEXTINSTANCE_H_
#define TEXTINSTANCE_H_

#include <string>
#include "Font.h"
#include "support/Color.h"
#include "support/Point.h"

class Camera;

class TextInstance {
public:
	TextInstance(
			Font* font,
			const std::string& text);

	virtual ~TextInstance();

	void setText(const std::string& text);
	const std::string& getText() const;

	void setPosition(const Point& position);
	const Point& getPosition() const;

	void setLeft(float left);
	float getLeft() const;

	void setTop(float top);
	float getTop() const;

	void setFont(Font* font);
	Font* getFont() const;

	float getWidth() const;
	float getHeight() const;

	void render(IShader* shader, IModel* model) const;

private:
	Font* m_font;
	std::string m_text;
	Point m_position;
};

#endif /* TEXTINSTANCE_H_ */
