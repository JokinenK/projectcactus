/*
 * FontAtlas.cpp
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */
#include "FontAtlas.h"

#include <cmath>
#include <locale>
#include <algorithm>
#include <vector>
#include <glm/gtx/transform.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H
#include "core/IRenderer.h"
#include "core/ITexture.h"
#include "core/IModel.h"
#include "core/IUniform.h"
#include "core/IShader.h"
#include "core/RendererFactory.h"
#include "core/Transform.h"
#include "core/Camera.h"
#include "support/Image.h"
#include "support/Rectangle.h"
#include "support/DistanceField.h"
#include "support/RectangleBinaryTree.h"
#include "support/Utils.h"
#include "hal/File.h"
#include "CharacterDimensions.h"
#include "CharacterMetadata.h"

namespace
{
	static std::vector<CharacterDimensions> createCharacterBoundingBoxes(
			FT_Face face,
			unsigned long firstChar,
			unsigned long lastChar,
			int fontSpread,
			int& minBearingY)
	{
		std::vector<CharacterDimensions> results;

		int width;
		int height;
		int spreadOffset = (fontSpread * 2);

		for (unsigned long ch = firstChar; ch < lastChar; ch++) {
			if (FT_Load_Char(face, ch, FT_LOAD_RENDER) != 0) {
				std::cerr << "Failed to load glyph" << std::endl;
				continue;
			}

			width = (face->glyph->metrics.width >> 6) + spreadOffset;
			height = (face->glyph->metrics.height >> 6) + spreadOffset;

			minBearingY = std::max(minBearingY, face->glyph->bitmap_top);
			results.push_back(CharacterDimensions(ch, Dimensions(width, height)));
		}

		// Sort the characters by their maximum edge from largest to smallest
		std::sort(results.rbegin(), results.rend());
		return results;
	}

	glm::mat4 createModelMatrix(
			const CharacterMetadata& character,
			float posX,
			float posY,
			float fontScale)
	{
		const Rectangle& geometry = character.getBoundingBox();
		float width = geometry.getWidth() * fontScale;
		float height = geometry.getHeight() * fontScale;

		posX += (character.getBearingX() * fontScale);
		posY += (character.getBearingY() * fontScale);

		glm::mat4 modelMatrix = glm::scale(glm::vec3(width, height, 0));
		glm::mat4 translateMatrix = glm::translate(glm::vec3(posX, posY, 0));

		return translateMatrix * modelMatrix;
	}
}

FontAtlas::FontAtlas(
		const std::string& fontPath,
		int fontSize,
		int fontDpi,
		int fontSpread,
		unsigned long atlasFirstChar,
		unsigned long atlasLastChar,
		short atlasWidth,
		short atlasHeight) :
				m_atlasWidth(atlasWidth),
				m_atlasHeight(atlasHeight),
				m_atlasFirstChar(atlasFirstChar),
				m_atlasLastChar(atlasLastChar),
				m_fontSize(fontSize),
				m_fontDpi(fontDpi),
				m_fontSpread(fontSpread),
				m_fontHeight(0),
				m_fontPath(fontPath),
				m_texture(0)
{
	createFontAtlas(true);
}

FontAtlas::~FontAtlas()
{
}

void FontAtlas::createFontAtlas(bool generateSignedDistanceField)
{
	RectangleBinaryTree binaryTree(m_atlasWidth, m_atlasHeight);
	std::string atlasFilePath = "FontAtlas_" + Hal::File::fileName(m_fontPath) + ".png";

	Image bitmapFontAtlas;
	Image signedDistaneFieldFontAtlas;

	std::vector<CharacterDimensions> listCharBBoxes;
	int minBearingY = 0;

	FT_Library ft;
	FT_Face face;

	if (generateSignedDistanceField) {
		bitmapFontAtlas.create(m_atlasWidth, m_atlasHeight, 1);
	}

	if (FT_Init_FreeType(&ft) != 0) {
		std::cerr << "Couldn't init FreeType" << std::endl;
		goto ft_cleanup_library;
	}

	if (FT_New_Face(ft, m_fontPath.c_str(), 0, &face) != 0) {
		std::cerr << "Failed to load font" << std::endl;
		goto ft_cleanup_face;
	}

	FT_Select_Charmap(face , FT_ENCODING_UNICODE);
	FT_Set_Char_Size(face, 0, m_fontSize << 6, m_fontDpi, m_fontDpi);

	listCharBBoxes = createCharacterBoundingBoxes(
			face,
			m_atlasFirstChar,
			m_atlasLastChar,
			m_fontSpread,
			minBearingY);

	m_fontHeight = (face->size->metrics.height >> 6);

	for (const CharacterDimensions& charBBox : listCharBBoxes) {
		unsigned long ch = charBBox.getCharacter();
		unsigned int glyphIndex = FT_Get_Char_Index(face, ch);

		if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_RENDER) != 0) {
			std::cerr << "Failed to load glyph: " << static_cast<char>(ch) << std::endl;
			continue;
		}

		const FT_GlyphSlot& glyph = face->glyph;
		const Rectangle& target = binaryTree.fit(charBBox.getDimensions());

		if (target.isValid()) {
			const FT_Bitmap& bitmap = glyph->bitmap;
			Image charBitmap(bitmap.width, bitmap.rows, 1, bitmap.buffer);

			if (generateSignedDistanceField) {
				bitmapFontAtlas.setSubImage(
						target.getLeft() + m_fontSpread,
						target.getTop() + m_fontSpread,
						charBitmap);
			}
		}

		m_characterMap[ch] = CharacterMetadata(
				target,
				createTexCoords(target),
				glyph->bitmap_left,
				minBearingY - glyph->bitmap_top,
				glyph->advance.x >> 6);
	}

	if (generateSignedDistanceField) {
		signedDistaneFieldFontAtlas = DistanceField::create(bitmapFontAtlas, m_fontSpread);
		signedDistaneFieldFontAtlas.save(atlasFilePath);
	}
	else {
		signedDistaneFieldFontAtlas.load(atlasFilePath);
	}

	std::cout << "Finished loading font atlas..." << std::endl;

	if (createTexture(signedDistaneFieldFontAtlas)) {
		std::cout << "Initialized font atlas texture..." << std::endl;
	}
	else {
		std::cerr << "Failed to initialize font atlas texture" << std::endl;
	}

ft_cleanup_face:
	FT_Done_Face(face);

ft_cleanup_library:
	FT_Done_FreeType(ft);
}

Rectangle FontAtlas::createTexCoords(const Rectangle& atlasPosition) const
{
	Rectangle texCoords;

	texCoords.setLeft(atlasPosition.getLeft() / m_atlasWidth);
	texCoords.setTop(atlasPosition.getTop() / m_atlasHeight);
	texCoords.setRight(atlasPosition.getRight() / m_atlasWidth);
	texCoords.setBottom(atlasPosition.getBottom() / m_atlasHeight);

	return texCoords;
}

bool FontAtlas::createTexture(const Image& atlasImage)
{
	m_texture = RendererFactory::instance()->createTexture();

	m_texture->initTexture(
			atlasImage.getWidth(),
			atlasImage.getHeight(),
			ITexture::UNSIGNED_BYTE);

	m_texture->setWrapS(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapT(ITexture::CLAMP_TO_EDGE);
	m_texture->setWrapR(ITexture::CLAMP_TO_EDGE);

	m_texture->setFilterMin(ITexture::LINEAR);
	m_texture->setFilterMag(ITexture::LINEAR);

	m_texture->setTextureData(atlasImage.getData().data(), ITexture::RED, ITexture::RED);

	return true;
}

ITexture* FontAtlas::getTexture() const
{
	return m_texture;
}

void FontAtlas::renderCharacter(
		IShader* shader,
		IModel* model,
		float& left,
		float& top,
		unsigned long ch,
		float fontScale) const
{
    const CharacterMetadata& charMetadata = m_characterMap.at(ch);

	if (charMetadata.isValid() && charMetadata.getBoundingBox().isValid()) {
		shader->getUniform("uniformTexCoords")->set(charMetadata.getTexCoords().toVec4());
		shader->getUniform("uniformModel")->set(createModelMatrix(charMetadata, left, top, fontScale));
		model->render();
	}

	left += (charMetadata.getAdvance() * fontScale);
}

float FontAtlas::width(const std::string& text) const
{
	float width = 0;

	std::string::const_iterator ch;
	for (ch = text.begin(); ch != text.end(); ++ch) {
		const CharacterMetadata& charMetadata = m_characterMap.at(*ch);

		if (ch == text.begin()) {
			// TODO: This is to fix up the invalid width, find some better way.
			width += charMetadata.getBoundingBox().getWidth() + charMetadata.getBearingX();
		}
		else {
			width += charMetadata.getAdvance();
		}
	}

	return width;
}

float FontAtlas::height() const
{
	return m_fontHeight;
}

float FontAtlas::fontSize() const
{
	return m_fontSize;
}
