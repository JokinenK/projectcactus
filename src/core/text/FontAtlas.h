/*
 * FontAtlas.h
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#ifndef FONTATLAS_H_
#define FONTATLAS_H_

#include <map>
#include "CharacterMetadata.h"
#include "support/Rectangle.h"
#include "support/Color.h"

class ITexture;
class IRenderer;
class IModel;
class IShader;
class Image;
class TextShader;

class FontAtlas {
public:
	FontAtlas(
			const std::string& fontPath,
			int fontSize = 46,
			int fontDpi = 96,
			int fontSpread = 8,
			unsigned long atlasFirstChar = 32,
			unsigned long atlasLastChar = 255,
			short atlasWidth = 1024,
			short atlasHeight = 1024);

	virtual ~FontAtlas();

	void renderCharacter(
			IShader* shader,
			IModel* model,
			float& left,
			float& top,
			unsigned long ch,
			float fontScale) const;

	float width(const std::string& text) const;
	float height() const;
	float fontSize() const;

	ITexture* getTexture() const;

private:
	void createQuad();
	void createFontAtlas(bool generateSignedDistanceField);
	bool createTexture(const Image& atlasImage);
	Rectangle createTexCoords(const Rectangle& atlasPosition) const;

	short m_atlasWidth;
	short m_atlasHeight;

	unsigned long m_atlasFirstChar;
	unsigned long m_atlasLastChar;

	int m_fontSize;
	int m_fontDpi;
	int m_fontSpread;

	unsigned int m_fontHeight;

	std::string m_fontPath;
	ITexture* m_texture;
	std::map<char, CharacterMetadata> m_characterMap;
};

#endif /* FONTATLAS_H_ */
