/*
 * Character.h
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#ifndef CHARACTERTEXTURE_H_
#define CHARACTERTEXTURE_H_

#include "core/ITexture.h"
#include "support/Rectangle.h"

class CharacterMetadata {
public:
	CharacterMetadata();
	CharacterMetadata(
			const Rectangle& boundingBox,
			const Rectangle& texCoords,
			short bearingX,
			short bearingY,
			short advance);

	virtual ~CharacterMetadata();

	CharacterMetadata& operator=(const CharacterMetadata& other);

	const Rectangle& getBoundingBox() const;
	const Rectangle& getTexCoords() const;
	short getBearingX() const;
	short getBearingY() const;
	short getAdvance() const;
	bool isValid() const;

private:
	Rectangle m_boundingBox;
	Rectangle m_texCoords;
	short m_bearingX;
	short m_bearingY;
	short m_advance;
	bool m_valid;
};

#endif /* CHARACTERTEXTURE_H_ */
