/*
 * FontInstance.h
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#ifndef FONT_H_
#define FONT_H_

#include <string>
#include "CharacterMetadata.h"
#include "support/Rectangle.h"
#include "support/Color.h"

class ITexture;
class IRenderer;
class IModel;
class IShader;
class Image;
class TextShader;
class FontAtlas;

class Font {
public:
	Font(
			const std::string& fontName,
			float fontSize = 12,
			Color textColor = Color(0.0f, 0.0f, 0.0f, 1.0f),
			Color borderColor = Color(0.0f, 0.0f, 0.0f, 0.0f));

	virtual ~Font();

	void render(
			IShader* shader,
			IModel* model,
			const Point& position,
			const std::string& text) const;

	float width(const std::string& text) const;
	float height() const;

	Rectangle boundingBox(const std::string& text, const Rectangle& target) const;

	void setTextColor(const Color& textColor);
	const Color& getTextColor() const;

	void setBorderColor(const Color& borderColor);
	const Color& getBorderColor() const;

	void setFontSize(float fontSize);
	float getFontSize() const;

private:
	FontAtlas* m_fontAtlas;

	Color m_textColor;
	Color m_borderColor;

	float m_fontSize;
	float m_fontScale;
	float m_fontHeight;
};

#endif /* FONT_H_ */
