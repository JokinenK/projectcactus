/*
 * CharacterRectangle.h
 *
 *  Created on: 1.10.2016
 *      Author: kalle
 */

#ifndef CHARACTERDIMENSIONS_H_
#define CHARACTERDIMENSIONS_H_

#include "support/Dimensions.h"

class CharacterDimensions {
public:
	CharacterDimensions();
	CharacterDimensions(unsigned long ch, const Dimensions& dimensions);
	~CharacterDimensions();

	CharacterDimensions& operator=(const CharacterDimensions& other);
	bool operator<(const CharacterDimensions& other) const;

	const Dimensions& getDimensions() const;
	unsigned long getCharacter() const;

private:
	unsigned long m_character;
	Dimensions m_dimensions;
};

#endif /* CHARACTERDIMENSIONS_H_ */
