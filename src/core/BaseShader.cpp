/*
 * BaseShader.cpp
 *
 *  Created on: 7.10.2016
 *      Author: kalle
 */

#include <iostream>
#include "BaseShader.h"
#include "hal/File.h"
#include "core/RendererFactory.h"
#include "core/IRenderer.h"
#include "core/IModel.h"
#include "support/ShaderLoader.h"

BaseShader::BaseShader(
		const std::string& vertexShaderPath,
		const std::string& fragmentShaderPath,
		const std::string& geometryShaderPath)
{
	m_shader = RendererFactory::instance()->createShader();

	if (!vertexShaderPath.empty()) {
		std::string vertexShaderSource = ShaderLoader::loadShader(
				Hal::File::executableDir() + Hal::File::pathSeparator() + vertexShaderPath);

		addVertexShader(vertexShaderSource);
	}

	if (!fragmentShaderPath.empty()) {
		std::string fragmentShaderSource = ShaderLoader::loadShader(
				Hal::File::executableDir() + Hal::File::pathSeparator() + fragmentShaderPath);

		addFragmentShader(fragmentShaderSource);
	}

	if (!geometryShaderPath.empty()) {
		std::string geometryShaderSource = ShaderLoader::loadShader(
				Hal::File::executableDir() + Hal::File::pathSeparator() + geometryShaderPath);

		addGeometryShader(geometryShaderSource);
	}

	// Set attribute locations
	setAttribLocation("attrPosition", IModel::BUFFER_POSITION);
	setAttribLocation("attrTexCoord", IModel::BUFFER_TEXCOORD);
	setAttribLocation("attrNormal",   IModel::BUFFER_NORMAL);
	setAttribLocation("attrTangent",  IModel::BUFFER_TANGENT);

	compileShader();
}

BaseShader::~BaseShader()
{
	delete m_shader;
}

void BaseShader::bind()
{
	m_shader->bind();
}

void BaseShader::unbind()
{
	m_shader->unbind();
}

bool BaseShader::addVertexShader(const std::string& shaderSource)
{
	return m_shader->addVertexShader(shaderSource);
}

bool BaseShader::addFragmentShader(const std::string& shaderSource)
{
	return m_shader->addFragmentShader(shaderSource);
}

bool BaseShader::addGeometryShader(const std::string& shaderSource)
{
	return m_shader->addGeometryShader(shaderSource);
}

bool BaseShader::compileShader()
{
	return m_shader->compileShader();
}

void BaseShader::setAttribLocation(const std::string& attribName, int location)
{
	m_shader->setAttribLocation(attribName, location);
}

IUniform* BaseShader::getUniform(const std::string& uniformName)
{
	return m_shader->getUniform(uniformName);
}
