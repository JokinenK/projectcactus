/*
 * ViewFrustumRenderPrequisite.h
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_VIEWFRUSTUMRENDERPREREQUISITE_H_
#define PROJECTCACTUS_SRC_CORE_VIEWFRUSTUMRENDERPREREQUISITE_H_

#include "IRenderPrerequisite.h"

class Frustum;
class BoundingBox;

class ViewFrustumRenderPrerequisite : public IRenderPrerequisite
{
public:
	ViewFrustumRenderPrerequisite();
	ViewFrustumRenderPrerequisite(const Frustum& frustum);
	virtual ~ViewFrustumRenderPrerequisite();

	void setFrustum(const Frustum& frustum);

	virtual bool shouldRender(const BoundingBox* boundingBox) const;

private:
	const Frustum* m_frustum;
};

#endif /* PROJECTCACTUS_SRC_CORE_VIEWFRUSTUMRENDERPREREQUISITE_H_ */
