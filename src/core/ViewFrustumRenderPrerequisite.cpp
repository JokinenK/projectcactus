/*
 * ViewFrustumRenderPrequisite.cpp
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#include <iostream>
#include "ViewFrustumRenderPrerequisite.h"
#include "core/BoundingBox.h"
#include "core/ICollider.h"
#include "core/physics/Frustum.h"

ViewFrustumRenderPrerequisite::ViewFrustumRenderPrerequisite() :
		m_frustum(0)
{
}

ViewFrustumRenderPrerequisite::ViewFrustumRenderPrerequisite(const Frustum& frustum) :
		m_frustum(&frustum)
{
}

ViewFrustumRenderPrerequisite::~ViewFrustumRenderPrerequisite()
{
}

void ViewFrustumRenderPrerequisite::setFrustum(const Frustum& frustum)
{
	m_frustum = &frustum;
}

bool ViewFrustumRenderPrerequisite::shouldRender(const BoundingBox* boundingBox) const
{
	if (!m_frustum) {
		return false;
	}

	const AABB& bbox = boundingBox->getTransformedBoundingBox();
	ICollider::HalfSpace collision = m_frustum->intersectsAABB(bbox);

	if (collision == ICollider::NEGATIVE) {
		return false;
	}

	return true;
}
