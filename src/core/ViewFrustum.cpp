/*
 * ViewFrustum.cpp
 *
 *  Created on: 12.5.2017
 *      Author: kalle
 */

#include <glm/gtc/matrix_transform.hpp>
#include "ViewFrustum.h"

ViewFrustum::ViewFrustum() :
		m_fovRadians(0),
		m_aspectRatio(0),
		m_near(0),
		m_far(0),
		m_nearHeight(0),
		m_nearWidth(0),
		m_farHeight(0),
		m_farWidth(0),
		m_halfFarWidth(0),
		m_halfFarHeight(0),
		m_halfNearWidth(0),
		m_halfNearHeight(0),
		m_dirty(true)
{
}

ViewFrustum::ViewFrustum(float fovRadians, float aspectRatio, float near, float far) :
		m_fovRadians(fovRadians),
		m_aspectRatio(aspectRatio),
		m_near(near),
		m_far(far),
		m_nearHeight(0),
		m_nearWidth(0),
		m_farHeight(0),
		m_farWidth(0),
		m_halfFarWidth(0),
		m_halfFarHeight(0),
		m_halfNearWidth(0),
		m_halfNearHeight(0),
		m_dirty(true)
{
	updateProjection();
}

ViewFrustum::ViewFrustum(const ViewFrustum& other) :
		Frustum(other)
{
	operator=(other);
}

ViewFrustum::~ViewFrustum()
{
}

ViewFrustum& ViewFrustum::operator=(const ViewFrustum& other)
{
	m_fovRadians     = other.m_fovRadians;
	m_aspectRatio    = other.m_aspectRatio;
	m_near           = other.m_near;
	m_far            = other.m_far;
	m_nearHeight     = other.m_nearHeight,
	m_nearWidth      = other.m_nearWidth,
	m_farHeight      = other.m_farHeight,
	m_farWidth       = other.m_farWidth,
	m_halfFarWidth   = other.m_halfFarWidth,
	m_halfFarHeight  = other.m_halfFarHeight,
	m_halfNearWidth  = other.m_halfNearHeight,
	m_halfNearHeight = other.m_halfNearHeight,
	m_dirty          = other.m_dirty;

	Frustum::operator=(other);
	return *this;
}

void ViewFrustum::setFov(float fovRadians)
{
	m_fovRadians = fovRadians;
	m_dirty = true;
}

float ViewFrustum::getFov() const
{
	return m_fovRadians;
}

void ViewFrustum::setAspectRatio(float aspectRatio)
{
	m_aspectRatio = aspectRatio;
	m_dirty = true;
}

float ViewFrustum::getAspectRatio() const
{
	return m_aspectRatio;
}

void ViewFrustum::setNear(float near)
{
	m_near = near;
	m_dirty = true;
}

float ViewFrustum::getNear() const
{
	return m_near;
}

void ViewFrustum::setFar(float far)
{
	m_far = far;
	m_dirty = true;
}

float ViewFrustum::getFar() const
{
	return m_far;
}

const glm::mat4& ViewFrustum::getProjection() const
{
	return m_projection;
}

void ViewFrustum::updateProjection()
{
	if (!m_dirty) {
		return;
	}

	m_nearHeight = 2 * tan(m_fovRadians / 2) * m_near;
	m_nearWidth  = m_nearHeight * m_aspectRatio;
	m_farHeight  = 2 * tan(m_fovRadians / 2) * m_far;
	m_farWidth   = m_farHeight  * m_aspectRatio;

	m_halfFarWidth   = m_farWidth   * 0.5f;
	m_halfFarHeight  = m_farHeight  * 0.5f;
	m_halfNearWidth  = m_nearWidth  * 0.5f;
	m_halfNearHeight = m_nearHeight * 0.5f;

	m_projection = glm::perspective(m_fovRadians, m_aspectRatio, m_near, m_far);
	m_dirty = false;
}

void ViewFrustum::updateCorners(const glm::vec3& position, const glm::vec3& up, const glm::vec3& forward, const glm::vec3& right)
{
	glm::vec3 nearCenter = position + (forward * m_near);
	glm::vec3 farCenter  = position + (forward * m_far);

	updateProjection();

	glm::vec3 nearUp    = up    * m_halfNearHeight;
	glm::vec3 nearRight = right * m_halfNearWidth;
	glm::vec3 farUp     = up    * m_halfFarHeight;
	glm::vec3 farRight  = right * m_halfFarWidth;

	m_corners[CORNER_NLT] = nearCenter - nearRight + nearUp;
	m_corners[CORNER_NLB] = nearCenter - nearRight - nearUp;
	m_corners[CORNER_NRT] = nearCenter + nearRight + nearUp;
	m_corners[CORNER_NRB] = nearCenter + nearRight - nearUp;
	m_corners[CORNER_FLT] = farCenter - farRight + farUp;
	m_corners[CORNER_FLB] = farCenter - farRight - farUp;
	m_corners[CORNER_FRT] = farCenter + farRight + farUp;
	m_corners[CORNER_FRB] = farCenter + farRight - farUp;

	m_boundingBox.clear();
	m_boundingSphere.clear();

	for (const glm::vec3& corner : getCorners()) {
		m_boundingBox.expand(corner);
	}

	m_boundingSphere.setCenter(m_boundingBox.getCenter());
	m_boundingSphere.expand(m_boundingBox);

	m_planes[PLANE_NEAR].setPointNormal(nearCenter, forward);
	m_planes[PLANE_FAR].setPointNormal(farCenter, -forward);

	m_planes[PLANE_TOP].setPoints(m_corners[CORNER_NRT], m_corners[CORNER_NLT], m_corners[CORNER_FLT]);
	m_planes[PLANE_BOTTOM].setPoints(m_corners[CORNER_NLB], m_corners[CORNER_NRB], m_corners[CORNER_FRB]);

	m_planes[PLANE_LEFT].setPoints(m_corners[CORNER_NLT], m_corners[CORNER_NLB], m_corners[CORNER_FLB]);
	m_planes[PLANE_RIGHT].setPoints(m_corners[CORNER_NRB], m_corners[CORNER_NRT], m_corners[CORNER_FRB]);
}

const AABB& ViewFrustum::getBoundingBox() const
{
	return m_boundingBox;
}

const Sphere& ViewFrustum::getBoundingSphere() const
{
	return m_boundingSphere;
}

bool ViewFrustum::isDirty() const
{
	return m_dirty;
}
