/*
 * BaseShader.h
 *
 *  Created on: 7.10.2016
 *      Author: kalle
 */

#ifndef BASESHADER_H_
#define BASESHADER_H_

#include "core/IShader.h"

class BaseShader : public IShader
{
public:
	BaseShader(
			const std::string& vertexShaderPath = std::string(),
			const std::string& fragmentShaderPath = std::string(),
			const std::string& geometryShaderPath = std::string());

	virtual ~BaseShader();

    void bind();
    void unbind();

    bool addVertexShader(const std::string& shaderSource);
    bool addFragmentShader(const std::string& shaderSource);
    bool addGeometryShader(const std::string& shaderSource);

    bool compileShader();

    void setAttribLocation(const std::string& attribName, int location);
    IUniform* getUniform(const std::string& uniformName);

private:
    IShader* m_shader;
};

#endif /* BASESHADER_H_ */
