/*
 * IEventListener.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef IEVENTLISTENER_H_
#define IEVENTLISTENER_H_

class IEventListener
{
public:
	virtual ~IEventListener() {};
};

#endif /* IEVENTLISTENER_H_ */
