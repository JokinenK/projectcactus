/*
 * PluginExports.h
 *
 *  Created on: 7.12.2016
 *      Author: kalle
 */

#ifndef PLUGINEXPORTS_H_
#define PLUGINEXPORTS_H_

#ifdef _WIN32
#  ifdef PLUGIN_EXPORT
#    define PLUGIN_API "C" __declspec(dllexport)
#  else
#    define PLUGIN_API "C" __declspec(dllimport)
#  endif
#else
#  define PLUGIN_API "C"
#endif

#endif /* PLUGINEXPORTS_H_ */
