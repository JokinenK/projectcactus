/*
 * ScrollArea.cpp
 *
 *  Created on: 4.3.2017
 *      Author: kalle
 */

#include <iostream>
#include "ScrollArea.h"
#include "core/IShader.h"
#include "core/gui/GuiRenderer.h"
#include "support/Utils.h"

namespace Gui {

ScrollArea::ScrollArea() :
		m_widgetContainer(new WidgetContainer()),
		m_vertLayout(new BoxLayout(Vertical)),
		m_horizLayout(new BoxLayout(Horizontal)),
		m_vertScrollBar(new ScrollBar(Vertical)),
		m_horizScrollBar(new ScrollBar(Horizontal)),
		m_scrollOffset(0, 0)
{
	m_vertLayout->setParent(this);
	m_vertLayout->addChildren(m_horizLayout);
	m_vertLayout->addChildren(m_horizScrollBar);
	m_vertLayout->setAlignment(Middle | Center);

	m_horizLayout->addChildren(m_widgetContainer);
	m_horizLayout->addChildren(m_vertScrollBar);
	m_horizLayout->setAlignment(Middle | Center);

	m_widgetContainer->setAlignment(Top | Center);

	m_vertScrollBar->addMoveCallback(std::bind(&ScrollArea::scrollBarMoved, this, std::placeholders::_1));
	m_horizScrollBar->addMoveCallback(std::bind(&ScrollArea::scrollBarMoved, this, std::placeholders::_1));
}

ScrollArea::~ScrollArea()
{
	delete m_vertLayout;
}

void ScrollArea::setWidget(Widget* widget)
{
	m_widgetContainer->setWidget(widget);
	invalidate();
}

Widget* ScrollArea::getWidget() const
{
	return m_widgetContainer->getWidget();
}

void ScrollArea::update()
{
	Widget::update();
	m_vertLayout->update();
}

void ScrollArea::invalidate()
{
	Widget::invalidate();
	m_vertLayout->invalidate();
}

void ScrollArea::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		GuiRenderer* guiRenderer = getGuiRenderer();

		bool prevScissorTestEnabledState = guiRenderer->isScissorTestEnabled();
		Rectangle prevScissorRectangle = guiRenderer->getScissorRectangle();

		guiRenderer->setScissorTestEnabled(true);
		guiRenderer->setScissorRectangle(getTransformedGeometry());

		Widget::render(shader, model);
		m_vertLayout->render(shader, model);

		guiRenderer->setScissorRectangle(prevScissorRectangle);
		guiRenderer->setScissorTestEnabled(prevScissorTestEnabledState);
	}
}

bool ScrollArea::mouseEvent(const MouseEvent* mouseEvent)
{
	if (contains(mouseEvent->getX(), mouseEvent->getY())) {
		if (m_horizScrollBar->mouseEvent(mouseEvent)) {
			return true;
		}
		else if (m_vertScrollBar->mouseEvent(mouseEvent)) {
			return true;
		}
		else if (m_widgetContainer->mouseEvent(mouseEvent)) {
			return true;
		}
		else if (mouseEvent->isWheelEvent()) {
			float left = m_horizScrollBar->getPosition() - (m_horizScrollBar->getStep() * mouseEvent->getScrollX());
			float top = m_vertScrollBar->getPosition() - (m_vertScrollBar->getStep() * mouseEvent->getScrollY());

			scroll(left, top);
			return true;
		}
	}

	return false;
}

void ScrollArea::scrollBarMoved(const AbstractSlider* abstractSlider)
{
	/* Unused */
	(void) abstractSlider;

	scroll(m_horizScrollBar->getPosition(), m_vertScrollBar->getPosition());
}

void ScrollArea::scroll(float left, float top)
{
	m_horizScrollBar->setPosition(left);
	m_vertScrollBar->setPosition(top);

	Widget* widget = m_widgetContainer->getWidget();

	if (widget) {
		const Rectangle& rootGeometry = getGeometry();
		const Rectangle widgetGeometry = widget->getPreferredGeometry();

		float scrollableWidth = widgetGeometry.getWidth() - rootGeometry.getWidth();
		float scrollableHeight = widgetGeometry.getHeight() - rootGeometry.getHeight();

		scrollableWidth = (scrollableWidth < 0 ? 0 : scrollableWidth);
		scrollableHeight = (scrollableHeight < 0 ? 0 : scrollableHeight);

		if (m_vertScrollBar->isVisible()) {
			scrollableWidth += m_vertScrollBar->getPreferredGeometry().getWidth();
		}

		if (m_horizScrollBar->isVisible()) {
			scrollableHeight += m_horizScrollBar->getPreferredGeometry().getHeight();
		}

		m_scrollOffset.setPosition(
				Utils::clamp(left * scrollableWidth, 0.0f, scrollableWidth),
				Utils::clamp(top * scrollableHeight, 0.0f, scrollableHeight));

		invalidate();
	}
}

void ScrollArea::reset()
{
	scroll(0.0f, 0.0f);
}

void ScrollArea::onLayoutChanged()
{
	Widget* widget = m_widgetContainer->getWidget();

	const Rectangle& geometry = getGeometry();
	Rectangle widgetGeometry = (widget ? widget->getPreferredGeometry() : Rectangle::EMPTY);
	Rectangle vertScrollBarGeometry = m_vertScrollBar->getPreferredGeometry();
	Rectangle horizScrollBarGeometry = m_horizScrollBar->getPreferredGeometry();

	m_vertScrollBar->setVisibility(widgetGeometry.getHeight() > geometry.getHeight());
	m_horizScrollBar->setVisibility(widgetGeometry.getWidth() > geometry.getWidth());

	widgetGeometry.move(-m_scrollOffset);
	vertScrollBarGeometry.setHeight(geometry.getHeight());
	horizScrollBarGeometry.setWidth(geometry.getWidth());

	m_vertScrollBar->setPreferredGeometry(vertScrollBarGeometry);
	m_horizScrollBar->setPreferredGeometry(horizScrollBarGeometry);
	m_widgetContainer->setPreferredGeometry(geometry);

	if (widget) {
		widget->alignToRectangle(widgetGeometry, Widget::Absolute);
	}

	m_vertLayout->setGeometry(geometry);
	m_horizLayout->setGeometry(geometry);
	m_vertLayout->alignToRectangle(geometry);
}

} /* namespace Gui */
