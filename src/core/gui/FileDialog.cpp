/*
 * FileDialog.cpp
 *
 *  Created on: 8.3.2017
 *      Author: kalle
 */

#include <iostream>
#include "FileDialog.h"
#include "core/TextureManager.h"
#include "support/Rectangle.h"
#include "support/Image.h"
#include "hal/File.h"

namespace Gui {

FileDialog::FileDialog(DialogType dialogType) :
		m_dialogType(dialogType),
		m_vertLayout(new BoxLayout(Vertical)),
		m_horizLayout(new BoxLayout(Horizontal)),
		m_scrollArea(new ScrollArea()),
		m_textField(new TextField()),
		m_buttonOk(new Button("OK")),
		m_buttonCancel(new Button("Cancel")),
		m_clickableGridLayout(new ClickableGridLayout(1, 1))
{
	m_folderTexture = TextureManager::instance()->getTexture2D(
			Hal::File::executableDir() + Hal::File::pathSeparator() + "../res/textures/folder.png",
			Image::DONT_FLIP);

	m_fileTexture = TextureManager::instance()->getTexture2D(
			Hal::File::executableDir() + Hal::File::pathSeparator() + "../res/textures/file.png",
			Image::DONT_FLIP);

	m_vertLayout->setParent(this),
	m_vertLayout->addChildren(m_scrollArea);
	m_vertLayout->addChildren(m_textField);
	m_vertLayout->addChildren(m_horizLayout);

	m_horizLayout->setPadding(2);
	m_horizLayout->addChildren(m_buttonOk);
	m_horizLayout->addChildren(m_buttonCancel);

	m_scrollArea->setWidget(m_clickableGridLayout);

	m_clickableGridLayout->addClickCallback(
			std::bind(
					&FileDialog::itemClicked,
					this,
					std::placeholders::_1,
					std::placeholders::_2,
					std::placeholders::_3));

	m_clickableGridLayout->setEvenColor(Color(0x7f7f7f));
	m_clickableGridLayout->setOddColor(Color(0x5f5f5f));

	m_textField->setPreferredGeometry(Rectangle(0, 0, 0, 30));
	m_textField->setBgColor(Color(0xFFFFFF));

	m_buttonOk->setPreferredGeometry(Rectangle(0, 0, 0, 30));
	m_buttonOk->addClickCallback(std::bind(&FileDialog::buttonOkClicked, this, std::placeholders::_1));

	m_buttonCancel->setPreferredGeometry(Rectangle(0, 0, 0, 30));
	m_buttonCancel->addClickCallback(std::bind(&FileDialog::buttonCancelClicked, this, std::placeholders::_1));

	changeCurrentPath(Hal::File::currentDir());
}

FileDialog::~FileDialog()
{
	delete m_vertLayout;
}

void FileDialog::changeCurrentPath(const std::string& currentPath)
{
	m_currentPath = Hal::File::absolutePath(currentPath);
	m_directoryInfo = Hal::File::readDirectory(m_currentPath);

	int numRows = m_directoryInfo.directories.size() + m_directoryInfo.files.size();
	Rectangle listRectangle(0, 0, 150, numRows * 20);

	m_clickableGridLayout->resize(1, numRows);
	m_clickableGridLayout->setPreferredGeometry(listRectangle);

	m_scrollArea->setPreferredGeometry(listRectangle);

	int currentRow = 0;
	for (const Hal::File::DirectoryEntry& directoryEntry : m_directoryInfo.directories) {
		Label* item = new Label("[" + directoryEntry.name + "]");
		item->setIcon(m_folderTexture);

		m_clickableGridLayout->setWidget(item, 0, currentRow++);
	}

	for (const Hal::File::DirectoryEntry& fileEntry : m_directoryInfo.files) {
		Label* item = new Label(fileEntry.name);
		item->setIcon(m_fileTexture);

		m_clickableGridLayout->setWidget(item, 0, currentRow++);
	}

	m_scrollArea->reset();
	updateGeometry();
}

void FileDialog::changeCurrentFile(const std::string& file)
{
	m_currentFile = file;
	m_textField->setText(Hal::File::fileName(file));
}

void FileDialog::addConfirmCallback(FileDialogCallback callback)
{
	m_confirmCallbacks.push_back(callback);
}

void FileDialog::addCancelCallback(FileDialogCallback callback)
{
	m_cancelCallbacks.push_back(callback);
}

void FileDialog::performConfirmCallbacks(const std::string& filePath) const
{
	for (auto callback : m_confirmCallbacks) {
		callback(this, filePath);
	}
}

void FileDialog::performCancelCallbacks() const
{
	std::string emptyString;

	for (auto callback : m_cancelCallbacks) {
		callback(this, emptyString);
	}
}

void FileDialog::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();
		m_vertLayout->update();
	}
}

void FileDialog::invalidate()
{
	Widget::invalidate();
	m_vertLayout->invalidate();
}

void FileDialog::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		Widget::render(shader, model);
		m_vertLayout->render(shader, model);
	}
}

bool FileDialog::mouseEvent(const MouseEvent* mouseEvent)
{
	if (!isVisible()) {
		return false;
	}

	return m_vertLayout->mouseEvent(mouseEvent);
}

bool FileDialog::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	if (!isVisible()) {
		return false;
	}

	return m_vertLayout->keyboardEvent(keyboardEvent);
}

void FileDialog::updateGeometry()
{
	const Rectangle preferredGeometry = getGeometry().normalized();

	Rectangle buttonOkGeometry = m_buttonOk->getPreferredGeometry();
	buttonOkGeometry.setWidth((preferredGeometry.getWidth() - 2) / 2);
	m_buttonOk->setPreferredGeometry(buttonOkGeometry);

	Rectangle buttonCancelGeometry = m_buttonCancel->getPreferredGeometry();
	buttonCancelGeometry.setWidth((preferredGeometry.getWidth() - 2) / 2);
	m_buttonCancel->setPreferredGeometry(buttonCancelGeometry);

	Rectangle textFieldGeometry = m_textField->getPreferredGeometry();
	textFieldGeometry.setWidth(preferredGeometry.getWidth());
	m_textField->setPreferredGeometry(textFieldGeometry);

	Rectangle clickableGridGeometry = m_clickableGridLayout->getPreferredGeometry();
	clickableGridGeometry.setWidth(preferredGeometry.getWidth());
	m_clickableGridLayout->setPreferredGeometry(clickableGridGeometry);

	Rectangle scrollAreaGeometry = preferredGeometry;
	scrollAreaGeometry.adjustHeight(-textFieldGeometry.getHeight());
	scrollAreaGeometry.adjustHeight(-buttonOkGeometry.getHeight());
	m_scrollArea->setPreferredGeometry(scrollAreaGeometry);
}

void FileDialog::onLayoutChanged()
{
	updateGeometry();
	m_vertLayout->alignToRectangle(getGeometry());
}

void FileDialog::itemClicked(const ClickableGridLayout* clickableGridLayout, int column, int row)
{
	/* Unused */
	(void) clickableGridLayout;
	(void) column;

	int numDirectories = m_directoryInfo.directories.size();
	int numFiles = m_directoryInfo.files.size();

	int directoryIndex = row;
	int fileIndex = row - numDirectories;

	if (directoryIndex >= 0 && directoryIndex < numDirectories) {
		changeCurrentPath(m_currentPath + Hal::File::pathSeparator() + m_directoryInfo.directories[directoryIndex].name);
	}
	else if (fileIndex >= 0 && fileIndex < numFiles) {
		changeCurrentFile(m_currentPath + Hal::File::pathSeparator() + m_directoryInfo.files[fileIndex].name);
	}

	invalidate();
}

const std::string& FileDialog::getSelectedFile() const
{
	return m_currentFile;
}

void FileDialog::buttonOkClicked(const AbstractButton* abstractButton)
{
	/* Unused */
	(void) abstractButton;

	if (isOpenDialog()) {
		if (Hal::File::fileExists(m_currentFile)) {
			performConfirmCallbacks(m_currentFile);
			m_currentFile.clear();
			m_textField->clear();
		}
		else {
			std::cerr << "File doesn't exist!" << std::endl;
		}
	}
	else if (isSaveDialog()) {
		if (!Hal::File::fileExists(m_currentFile)) {
			performConfirmCallbacks(m_currentFile);
		}
		else {
			std::cerr << "File already exist!" << std::endl;
		}
	}
}

void FileDialog::buttonCancelClicked(const AbstractButton* abstractButton)
{
	/* Unused */
	(void) abstractButton;

	performCancelCallbacks();
	m_currentFile.clear();
	m_textField->clear();
}

bool FileDialog::isOpenDialog() const
{
	return (m_dialogType == DIALOG_OPEN);
}

bool FileDialog::isSaveDialog() const
{
	return (m_dialogType == DIALOG_SAVE);
}

} /* namespace Gui */
