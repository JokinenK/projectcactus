/*
 * StackedLayout.cpp
 *
 *  Created on: 12.3.2017
 *      Author: kalle
 */

#include <iterator>
#include <algorithm>
#include "StackedLayout.h"

namespace Gui {

StackedLayout::StackedLayout(StackType stackType) :
		m_stackType(stackType),
		m_activeIndex(0)
{
}

StackedLayout::~StackedLayout()
{
}

Widget* StackedLayout::widgetAt(int index) const
{
	Widget* widget = 0;
	const std::list<Widget*>& children = getChildren();
	int numChildren = children.size();

	if (index >= 0 && index < numChildren) {
		auto iter = children.begin();
		std::advance(iter, index);

		widget = *iter;
	}

	return widget;
}

int StackedLayout::widgetIndex(Widget* widget) const
{
	const std::list<Widget*>& children = getChildren();
	auto iter = std::find(children.begin(), children.end(), widget);

	if (iter != children.end()) {
		return std::distance(children.begin(), iter);
	}

	return -1;
}

void StackedLayout::setActiveIndex(int activeIndex)
{
	const std::list<Widget*>& children = getChildren();
	int numChildren = children.size();

	if (activeIndex >= 0 && activeIndex < numChildren) {
		Widget* activeWidget = widgetAt(activeIndex);

		if (m_stackType == SHOW_ONE) {
			for (Widget* child : children) {
				child->setVisibility(child == activeWidget);
			}
		}

		m_activeIndex = activeIndex;
	}
}

int StackedLayout::getActiveIndex() const
{
	return m_activeIndex;
}

void StackedLayout::setActiveWidget(Widget* activeWidget)
{
	setActiveIndex(widgetIndex(activeWidget));
}

Widget* StackedLayout::getActiveWidget() const
{
	return widgetAt(m_activeIndex);
}

const Rectangle StackedLayout::getPreferredGeometry() const
{
	Rectangle preferredSize;
	Widget* activeWidget = getActiveWidget();

	if (activeWidget) {
		if (m_stackType == SHOW_ALL) {
			const std::list<Widget*>& children = getChildren();

			for (Widget* child : children) {
				Rectangle childSize = child->getPreferredGeometry();

				preferredSize.setWidth(std::max(preferredSize.getWidth(), childSize.getWidth()));
				preferredSize.setHeight(std::max(preferredSize.getHeight(), childSize.getHeight()));
			}
		}
		else if (m_stackType == SHOW_ONE) {
			preferredSize = activeWidget->getPreferredGeometry();
		}
	}
	else {
		preferredSize = Widget::getPreferredGeometry();
	}

	return preferredSize;
}

void StackedLayout::onLayoutChanged()
{
}

} /* namespace Gui */
