/*
 * GuiRenderer.cpp
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#include <glm/gtx/transform.hpp>
#include <vector>
#include <iostream>
#include <glm/glm.hpp>
#include "GuiRenderer.h"
#include "BoxLayout.h"
#include "core/ITexture.h"
#include "core/IModel.h"
#include "core/IUniform.h"
#include "core/RendererFactory.h"

namespace Gui
{

GuiRenderer::GuiRenderer(IRenderer* renderer, float width, float height) :
		m_renderer(renderer),
		m_shader("../res/shaders/gui.vs", "../res/shaders/gui.fs"),
		m_screenSize(width, height),
		m_projection(glm::ortho(0.0f, width, height, 0.0f, -1.0f, 1.0f)),
		m_model(RendererFactory::instance()->createQuad(0.0f, 1.0f, 1.0f, 0.0f, true)),
		m_layout(new BoxLayout(Layout::Horizontal))
{
	m_shader.bind();
	m_shader.getUniform("uniformSamplerDiffuse")->set(0);
	m_shader.getUniform("uniformSamplerFont")->set(1);
	m_shader.getUniform("uniformProjection")->set(m_projection);
	m_shader.unbind();
}

GuiRenderer::~GuiRenderer()
{
	delete m_layout;
}

void GuiRenderer::update()
{
	m_layout->update();
}

void GuiRenderer::render()
{
	bool prevBlendEnabledState = m_renderer->isBlendEnabled();
	bool prevDepthTestEnabledState = m_renderer->isDepthTestEnabled();

	m_renderer->setBlendEnabled(true);
	m_renderer->setDepthTestEnabled(false);
	m_renderer->setBlendFunc(IRenderer::SRC_ALPHA, IRenderer::ONE_MINUS_SRC_ALPHA);

	m_shader.bind();
	m_layout->render(&m_shader, m_model);
	m_shader.unbind();

	m_renderer->setBlendEnabled(prevDepthTestEnabledState);
	m_renderer->setDepthTestEnabled(prevBlendEnabledState);
}

void GuiRenderer::setLayout(Layout* layout)
{
	if (m_layout) {
		delete m_layout;
	}

	m_layout = layout;
	m_layout->setGuiRenderer(this);
}

Layout* GuiRenderer::getLayout() const
{
	return m_layout;
}

void GuiRenderer::setScissorRectangle(const Rectangle& scissorRectangle)
{
	/* This is needed because the OpenGL want's the scissor
	 * rectangle from bottom left (0, 0) corner and we are
	 * calculating everything in top left (0, 0) */

	m_renderer->setScissorRectangle(
			scissorRectangle.getLeft(),
			m_screenSize.getHeight() - scissorRectangle.getBottom(),
			scissorRectangle.getWidth(),
			scissorRectangle.getHeight());
}

const Rectangle GuiRenderer::getScissorRectangle() const
{
	float left;
	float top;
	float width;
	float height;

	m_renderer->getScissorRectangle(left, top, width, height);
	return Rectangle(left, top, width, height);
}

void GuiRenderer::setScissorTestEnabled(bool enabled)
{
	m_renderer->setScissorTestEnabled(enabled);
}

bool GuiRenderer::isScissorTestEnabled() const
{
	return m_renderer->isScissorTestEnabled();
}

bool GuiRenderer::mouseEvent(const MouseEvent* mouseEvent)
{
	return m_layout->mouseEvent(mouseEvent);
}

bool GuiRenderer::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	return m_layout->keyboardEvent(keyboardEvent);
}

}
