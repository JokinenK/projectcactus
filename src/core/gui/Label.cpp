/*
 * Console.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <iostream>
#include <cmath>
#include "core/IShader.h"
#include "core/gui/GuiRenderer.h"
#include "core/gui/Label.h"
#include "hal/File.h"

namespace Gui
{

Label::Label(const std::string& text) :
		m_font(new Font(Hal::File::defaultFont())),
		m_icon(new Picture())
{
	m_text = new TextInstance(m_font, text);
	m_icon->setParent(this);
	m_icon->setVisibility(false);
	m_icon->setPreferredGeometry(Rectangle(0, 0, 24, 24));
}

Label::~Label()
{
	delete m_icon;
	delete m_text;
}

void Label::setText(const std::string& text)
{
	m_text->setText(text);
	invalidate();
}

const std::string& Label::getText() const
{
	return m_text->getText();
}

void Label::setFont(Font* font)
{
	m_text->setFont(font);
	invalidate();
}

Font* Label::getFont() const
{
	return m_text->getFont();
}

void Label::setIcon(ITexture* texture)
{
	m_icon->setTexture(texture);
	m_icon->setVisibility(true);
	invalidate();
}

ITexture* Label::getIcon() const
{
	return m_icon->getTexture();
}

void Label::setIconSize(const Dimensions& iconSize)
{
	m_icon->setPreferredGeometry(Rectangle(0, 0, iconSize.getWidth(), iconSize.getHeight()));
	invalidate();
}

const Dimensions Label::getIconSize() const
{
	return m_icon->getGeometry().getDimensions();
}

const Rectangle Label::getPreferredGeometry() const
{
	Rectangle preferredGeometry(0, 0, m_text->getWidth(), m_text->getHeight());

	if (m_icon->isVisible()) {
		Rectangle iconGeometry = m_icon->getPreferredGeometry();
		preferredGeometry.adjustWidth(iconGeometry.getWidth());
		preferredGeometry.setHeight(std::max(preferredGeometry.getHeight(), iconGeometry.getHeight()));
	}

	return preferredGeometry;
}

void Label::render(IShader* shader, IModel* model) const
{
	Widget::render(shader, model);
	m_icon->render(shader, model);
	m_text->render(shader, model);
}

void Label::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();
		m_icon->update();
	}
}

void Label::onLayoutChanged()
{
	Rectangle position = calcAlignment(
			getPreferredGeometry(),
			getTransformedGeometry(),
			AlignType::Absolute);

	if (m_icon->isVisible()) {
		position.move(m_icon->getPreferredGeometry().getWidth(), 0.0f);
	}

	m_text->setPosition(position.getPosition());
}

}
