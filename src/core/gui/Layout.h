/*
 * Layout.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_LAYOUT_H_
#define GUI_LAYOUT_H_

#include "core/gui/Widget.h"
#include "support/Rectangle.h"

namespace Gui
{

class Layout : public Widget
{
public:
	Layout();
	virtual ~Layout();

	void setPadding(int padding);
	int getPadding() const;

	void addChildren(Widget* widget);
	void removeChildren(Widget* widget);

	bool containsWidget(Widget* widget);

	const std::list<Widget*>& getChildren() const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

	virtual void update();

	virtual void invalidate();

	virtual void render(IShader* shader, IModel* model) const;

	virtual const Rectangle getPreferredGeometry() const = 0;

private:
	int m_padding;
	std::list<Widget*> m_children;
};

}

#endif // GUI_LAYOUT_H_
