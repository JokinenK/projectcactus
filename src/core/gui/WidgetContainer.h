/*
 * WidgetContainer.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_WIDGETCONTAINER_H_
#define GUI_WIDGETCONTAINER_H_

#include <list>
#include "Widget.h"

class IModel;
class IShader;

namespace Gui
{

class WidgetContainer : public Widget
{
public:
	WidgetContainer();
	virtual ~WidgetContainer();

	void setWidget(Widget* widget);
	Widget* getWidget() const;
	void removeWidget();

	void setColSpan(int colSpan);
	int getColSpan() const;

	void setRowSpan(int rowSpan);
	int getRowSpan() const;

	virtual void update();

	virtual void invalidate();

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

	virtual void render(
			IShader* shader,
			IModel* model) const;

	virtual void onLayoutChanged();

private:
	int m_colSpan;
	int m_rowSpan;

	Widget* m_widget;
};

}

#endif /* GUI_WIDGETCONTAINER_H_ */
