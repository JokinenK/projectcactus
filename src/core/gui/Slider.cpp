/*
 * Slider.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <iostream>
#include <sstream>
#include "core/gui/Slider.h"
#include "support/Rectangle.h"
#include "support/Utils.h"

namespace Gui
{

Slider::Slider(const std::string& text) :
		AbstractSlider(Widget::Horizontal),
		m_text(text),
		m_low(0.0f),
		m_high(1.0f),
		m_vertLayout(new BoxLayout(Layout::Vertical)),
		m_horizLayout(new BoxLayout(Layout::Horizontal)),
		m_label(new Label(text)),
		m_left(new Widget()),
		m_slider(new Widget()),
		m_right(new Widget())
{
	setBgGradient(
			Color(0.9f, 0.9f, 0.9f),
			Color(0.5f, 0.5f, 0.5f));

	m_vertLayout->setParent(this);
	m_vertLayout->addChildren(m_label);
	m_vertLayout->addChildren(m_horizLayout);
	m_vertLayout->setPadding(10);
	m_vertLayout->setAlignment(Widget::Middle | Widget::Center);

	m_horizLayout->addChildren(m_left);
	m_horizLayout->addChildren(m_slider);
	m_horizLayout->addChildren(m_right);
	m_horizLayout->setAlignment(Widget::Middle | Widget::Center);

	m_label->setAlignment(Widget::Middle | Widget::Center);

	m_left->setAlignment(Widget::Middle);
	m_slider->setAlignment(Widget::Middle);
	m_right->setAlignment(Widget::Middle);

	m_left->setBgColor(Color(0.4f, 0.4f, 0.4f, 1.0f));
	m_slider->setBgColor(Color(0.15f, 0.15f, 0.15f, 1.0f));
	m_right->setBgColor(Color(0.4f, 0.4f, 0.4f, 1.0f));

	m_slider->setPreferredGeometry(Rectangle(0, 0, 5, 15));
}

Slider::~Slider()
{
	delete m_vertLayout;
}

void Slider::setText(const std::string& text)
{
	m_label->setText(text);
	invalidate();
}

const std::string& Slider::getText() const
{
	return m_label->getText();
}

void Slider::setFont(Font* font)
{
	m_label->setFont(font);
	invalidate();
}

Font* Slider::getFont() const
{
	return m_label->getFont();
}

void Slider::setRange(float low, float high)
{
	m_low = low;
	m_high = high;
}

void Slider::setValue(float value)
{
	setPosition(Utils::map(value, m_low, m_high, 0.0, 1.0));
}

float Slider::getValue() const
{
	return Utils::map(getPosition(), 0.0, 1.0, m_low, m_high);
}

void Slider::render(IShader* shader, IModel* model) const
{
	Widget::render(shader, model);
	m_vertLayout->render(shader, model);
}

void Slider::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();
		m_vertLayout->update();
	}
}

void Slider::invalidate()
{
	Widget::invalidate();
	m_vertLayout->invalidate();
}

void Slider::onLayoutChanged()
{
	const Rectangle& geometry = getGeometry();
	Rectangle sliderGeometry = m_slider->getPreferredGeometry();

	float position = getPosition();
	float marginOffset = getMargin() * 2;
	float availableWidth = geometry.getWidth() - sliderGeometry.getWidth() - marginOffset;

	float leftWidth = availableWidth * position;
	float rightWidth = availableWidth * (1 - position);

	m_left->setPreferredGeometry(Rectangle(0, 0, leftWidth, 1));
	m_right->setPreferredGeometry(Rectangle(0, 0, rightWidth, 1));

	std::stringstream labelText;
	labelText.precision(2);
	labelText << m_text << " (" << getValue() << ")";

	m_label->setText(labelText.str());
	m_vertLayout->alignToRectangle(geometry, Widget::Relative);
}

}
