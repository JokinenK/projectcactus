/*
 * WidgetContainer.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include "WidgetContainer.h"

namespace Gui
{

WidgetContainer::WidgetContainer() :
		m_colSpan(1),
		m_rowSpan(1),
		m_widget(0)
{
}

WidgetContainer::~WidgetContainer()
{
	removeWidget();
}

void WidgetContainer::setWidget(Widget* widget)
{
	removeWidget();

	m_widget = widget;
	m_widget->setParent(getParent());
}

void WidgetContainer::removeWidget()
{
	if (m_widget) {
		delete m_widget;
	}

	m_widget = 0;
}

Widget* WidgetContainer::getWidget() const
{
	return m_widget;
}

void WidgetContainer::setColSpan(int colSpan)
{
	m_colSpan = colSpan;
}

int WidgetContainer::getColSpan() const
{
	return m_colSpan;
}

void WidgetContainer::setRowSpan(int rowSpan)
{
	m_rowSpan = rowSpan;
}

int WidgetContainer::getRowSpan() const
{
	return m_rowSpan;
}

void WidgetContainer::update()
{
	if (m_widget) {
		m_widget->update();
	}
}

void WidgetContainer::invalidate()
{
	if (m_widget) {
		m_widget->invalidate();
	}
}

bool WidgetContainer::mouseEvent(const MouseEvent* mouseEvent)
{
	if (m_widget) {
		return m_widget->mouseEvent(mouseEvent);
	}

	return false;
}

bool WidgetContainer::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	if (m_widget) {
		return m_widget->keyboardEvent(keyboardEvent);
	}

	return false;
}

void WidgetContainer::render(
		IShader* shader,
		IModel* model) const
{
	Widget::render(shader, model);

	if (m_widget) {
		m_widget->render(shader, model);
	}
}

void WidgetContainer::onLayoutChanged()
{
	if (m_widget) {
		m_widget->onLayoutChanged();
	}
}

}
