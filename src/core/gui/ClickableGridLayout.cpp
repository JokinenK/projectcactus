/*
 * ClickableGridLayout.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include <iostream>
#include "ClickableGridLayout.h"

namespace Gui
{

ClickableGridLayout::ClickableGridLayout(int columns, int rows) :
		GridLayout(columns, rows),
		m_state(false)
{
}

ClickableGridLayout::~ClickableGridLayout()
{
}

bool ClickableGridLayout::mouseEvent(const MouseEvent* mouseEvent)
{
	if (!isVisible()) {
		return false;
	}

	if (Layout::mouseEvent(mouseEvent)) {
		return true;
	}

	float x = mouseEvent->getX();
	float y = mouseEvent->getY();

	bool state = mouseEvent->isButtonPressed(1);
	bool prevState = m_state;

	bool stateChanged = state != prevState;
	bool insideButton = contains(x, y);

	if (stateChanged) {
		const Rectangle rectangle = getTransformedGeometry();
		int col = (x - rectangle.getLeft()) / getCellWidth();
		int row = (y - rectangle.getTop())  / getCellHeight();

		if (insideButton) {
			if (state && mouseEvent->isButtonEvent()) {
				m_state = state;
				performPressCallbacks(col, row);
				return true;
			}
			else if (!state) {
				m_state = state;
				performClickCallbacks(col, row);
				performReleaseCallbacks(col, row);
				return true;
			}
		}
		else if (!state) {
			m_state = state;
			performReleaseCallbacks(col, row);
			return true;
		}
	}

	return false;
}

void ClickableGridLayout::addPressCallback(ClickableGridLayoutCallback clickableGridLayoutCallback)
{
	m_pressCallbacks.push_back(clickableGridLayoutCallback);
}

void ClickableGridLayout::addClickCallback(ClickableGridLayoutCallback clickableGridLayoutCallback)
{
	m_clickCallbacks.push_back(clickableGridLayoutCallback);
}

void ClickableGridLayout::addReleaseCallback(ClickableGridLayoutCallback clickableGridLayoutCallback)
{
	m_releaseCallbacks.push_back(clickableGridLayoutCallback);
}

void ClickableGridLayout::performPressCallbacks(int column, int row) const
{
	for (auto callback : m_pressCallbacks) {
		callback(this, column, row);
	}
}

void ClickableGridLayout::performClickCallbacks(int column, int row) const
{
	for (auto callback : m_clickCallbacks) {
		callback(this, column, row);
	}
}

void ClickableGridLayout::performReleaseCallbacks(int column, int row) const
{
	for (auto callback : m_releaseCallbacks) {
		callback(this, column, row);
	}
}

}
