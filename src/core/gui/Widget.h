/*
 * Widget.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_WIDGET_H_
#define GUI_WIDGET_H_

#include <list>
#include "support/Rectangle.h"
#include "support/Dimensions.h"
#include "support/Color.h"
#include "core/events/MouseEvent.h"
#include "core/events/KeyboardEvent.h"

class Camera;
class IModel;
class IShader;

namespace Gui
{

class GuiRenderer;

class Widget
{
public:
	typedef enum {
		Left   = 0x01,
		Center = 0x02,
		Right  = 0x04,
		Top    = 0x08,
		Middle = 0x10,
		Bottom = 0x20
	} Alignment;

	typedef enum {
		Horizontal,
		Vertical
	} Orientation;

	typedef enum {
		Absolute,
		Relative
	} AlignType;

	Widget();
	virtual ~Widget();

	void setVisibility(bool visibility);
	bool isVisible() const;

	void setDirty(bool dirty);
	bool isDirty() const;

	void setPosition(const Point& position);
	const Point getPosition() const;

	virtual void setGeometry(const Rectangle& geometry);
	virtual const Rectangle& getGeometry() const;

	virtual const Rectangle getTransformedGeometry() const;

	virtual void setPreferredGeometry(const Rectangle& preferredGeometry);
	virtual const Rectangle getPreferredGeometry() const;

	void setAlignment(int alignment);
	int getAlignment() const;

	void setMargin(int margin);
	int getMargin() const;

	bool contains(float x, float y) const;
	bool contains(const Point& point) const;

	void setParent(Widget* parent);
	Widget* getParent() const;

	void setBgColor(const Color& bgColor);
	void setBgGradient(const Color& bgColor1, const Color& bgColor2);

	Rectangle calcAlignment(const Rectangle& source, const Rectangle& target, AlignType alignType);
	bool alignToRectangle(const Rectangle& target, AlignType alignType = Relative);

	void setGuiRenderer(GuiRenderer* guiRenderer);
	GuiRenderer* getGuiRenderer() const;

	bool isLeftAligned() const;
	bool isCenterAligned() const;
	bool isRightAligned() const;

	bool isTopAligned() const;
	bool isMiddleAligned() const;
	bool isBottomAligned() const;

	virtual const Color& getBgColor1() const;
	virtual const Color& getBgColor2() const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

	virtual void invalidate();

	virtual void update();

	virtual void render(IShader* shader, IModel* model) const;

	virtual void onLayoutChanged() {};

protected:
	virtual void onMousePressEvent() {};
	virtual void onMouseClickEvent() {};
	virtual void onMouseReleaseEvent() {};

private:
	bool m_visibility;
	bool m_dirty;

	Color m_bgColor1;
	Color m_bgColor2;

	Rectangle m_geometry;
	Rectangle m_preferredGeometry;

	int m_alignment;
	int m_margin;

	Widget* m_parent;
	GuiRenderer* m_guiRenderer;
};

}

#endif /* GUI_WIDGET_H_ */
