/*
 * AbstractButton.cpp
 *
 *  Created on: 28.2.2017
 *      Author: kalle
 */

#include "core/gui/AbstractButton.h"

namespace Gui
{

AbstractButton::AbstractButton() : m_state(false)
{
}

AbstractButton::~AbstractButton()
{
}

void AbstractButton::addClickCallback(ButtonCallback buttonCallback)
{
	m_clickCallbacks.push_back(buttonCallback);
}

void AbstractButton::addPressCallback(ButtonCallback buttonCallback)
{
	m_pressCallbacks.push_back(buttonCallback);
}

void AbstractButton::addReleaseCallback(ButtonCallback buttonCallback)
{
	m_releaseCallbacks.push_back(buttonCallback);
}

void AbstractButton::performClickCallbacks() const
{
	for (auto callback : m_clickCallbacks) {
		callback(this);
	}
}

void AbstractButton::performPressCallbacks() const
{
	for (auto callback : m_pressCallbacks) {
		callback(this);
	}
}

void AbstractButton::performReleaseCallbacks() const
{
	for (auto callback : m_releaseCallbacks) {
		callback(this);
	}
}

void AbstractButton::click() const
{
	performPressCallbacks();
	performClickCallbacks();
	performReleaseCallbacks();
}

bool AbstractButton::getState() const
{
	return m_state;
}

bool AbstractButton::mouseEvent(const MouseEvent* mouseEvent)
{
	float x = mouseEvent->getX();
	float y = mouseEvent->getY();

	bool state = mouseEvent->isButtonPressed(1);
	bool prevState = m_state;

	bool stateChanged = state != prevState;
	bool insideButton = contains(x, y);

	if (stateChanged) {
		if (insideButton) {
			if (state && mouseEvent->isButtonEvent()) {
				m_state = state;
				performPressCallbacks();
				return true;
			}
			else if (!state) {
				m_state = state;
				performClickCallbacks();
				performReleaseCallbacks();
				return true;
			}
		}
		else if (!state) {
			m_state = state;
			performReleaseCallbacks();
			return true;
		}
	}

	return false;
}

}
