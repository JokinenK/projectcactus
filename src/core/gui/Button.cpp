/*
 * Button.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <iostream>
#include "core/gui/Button.h"
#include "support/Rectangle.h"

namespace Gui
{

Button::Button(const std::string& text) :
		AbstractButton(),
		m_horizLayout(new BoxLayout(Layout::Horizontal)),
		m_label(new Label(text))
{
	setBgGradient(
			Color(0.9f, 0.9f, 0.9f),
			Color(0.5f, 0.5f, 0.5f));

	m_horizLayout->setParent(this);
	m_horizLayout->addChildren(m_label);
	m_horizLayout->setAlignment(Widget::Center | Widget::Middle);
}

Button::~Button()
{
	delete m_horizLayout;
}

void Button::setText(const std::string& text)
{
	m_label->setText(text);
	invalidate();
}

const std::string& Button::getText() const
{
	return m_label->getText();
}

void Button::setIcon(ITexture* icon)
{
	m_label->setIcon(icon);
	invalidate();
}

ITexture* Button::getIcon() const
{
	return m_label->getIcon();
}

void Button::setFont(Font* font)
{
	m_label->setFont(font);
	invalidate();
}

Font* Button::getFont() const
{
	return m_label->getFont();
}

void Button::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		Widget::render(shader, model);
		m_horizLayout->render(shader, model);
	}
}

void Button::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();
		m_horizLayout->update();
	}
}

void Button::invalidate()
{
	Widget::invalidate();
	m_horizLayout->invalidate();
}

void Button::onLayoutChanged()
{
	m_horizLayout->alignToRectangle(getGeometry());
}

const Color& Button::getBgColor1() const
{
	return getState() ? Widget::getBgColor2() : Widget::getBgColor1();
}

const Color& Button::getBgColor2() const
{
	return getState() ? Widget::getBgColor1() : Widget::getBgColor2();
}

}
