/*
 * GuiRenderer.h
 *
 *  Created on: 23.9.2016
 *      Author: kalle
 */

#ifndef GUI_GUIRENDERER_H_
#define GUI_GUIRENDERER_H_

#include <list>
#include "core/IRenderer.h"
#include "core/BaseShader.h"
#include "core/model/IndexedVertexModel.h"
#include "core/MessageBus.h"
#include "core/EventListener.h"
#include "core/events/MouseEvent.h"
#include "core/Camera.h"
#include "core/gui/Widget.h"
#include "core/gui/Layout.h"
#include "core/events/MouseEvent.h"
#include "core/events/KeyboardEvent.h"
#include "core/text/TextInstance.h"
#include "support/Singleton.h"
#include "support/Dimensions.h"

namespace Gui
{

class GuiRenderer
{
public:
	GuiRenderer(
			IRenderer* renderer,
			float width,
			float height);

	virtual ~GuiRenderer();

	void update();
	void render();

	void setLayout(Layout* layout);
	Layout* getLayout() const;

	void setScissorRectangle(const Rectangle& scissorRectangle);
	const Rectangle getScissorRectangle() const;

	void setScissorTestEnabled(bool enabled);
	bool isScissorTestEnabled() const;

	bool mouseEvent(const MouseEvent* mouseEvent);
	bool keyboardEvent(const KeyboardEvent* keyboardEvent);

private:
	GuiRenderer& operator=(const GuiRenderer& other);

	IRenderer* m_renderer;
	BaseShader m_shader;

	Dimensions m_screenSize;
	glm::mat4 m_projection;

	IModel* m_model;
	Layout* m_layout;
};

}

#endif /* GUI_GUIRENDERER_H_ */
