/*
 * ColorSelector.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_COLORSELECTOR_H_
#define GUI_COLORSELECTOR_H_

#include "core/gui/BoxLayout.h"
#include "core/gui/Slider.h"
#include "core/gui/Label.h"
#include "support/Color.h"

namespace Gui
{

class ColorSelector : public Widget
{
public:
	typedef std::function<void(const ColorSelector*)> ColorSelectorCallback;

	ColorSelector();
	virtual ~ColorSelector();

	void addColorChangedCallback(ColorSelectorCallback callback);

	void setColor(const Color& color);
	const Color& getColor() const;

	virtual const Color& getBgColor1() const;
	virtual const Color& getBgColor2() const;

	virtual void invalidate();

	virtual void render(IShader* shader, IModel* model) const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual void onLayoutChanged();

private:
	void redChanged(const AbstractSlider* slider);
	void greenChanged(const AbstractSlider* slider);
	void blueChanged(const AbstractSlider* slider);

	void performColorChangedCallbacks() const;

	Color m_color;

	BoxLayout* m_vertLayout;
	Slider* m_red;
	Slider* m_green;
	Slider* m_blue;

	std::list<ColorSelectorCallback> m_colorChangedCallbacks;
};

}

#endif /* GUI_COLORSELECTOR_H_ */
