/*
 * AbstractSlider.h
 *
 *  Created on: 28.2.2017
 *      Author: kalle
 */

#ifndef GUI_ABSTRACTSLIDER_H_
#define GUI_ABSTRACTSLIDER_H_

#include <functional>
#include "core/gui/Widget.h"
#include "support/Point.h"

namespace Gui
{

class AbstractSlider : public Widget
{
public:
	typedef std::function<void(const AbstractSlider*)> SliderCallback;

	explicit AbstractSlider(Orientation orientation);
	virtual ~AbstractSlider();

	void addMoveCallback(SliderCallback sliderCallback);
	void addPressCallback(SliderCallback sliderCallback);
	void addReleaseCallback(SliderCallback sliderCallback);

	virtual void setPosition(float position);
	virtual float getPosition() const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

protected:
	virtual float parsePosition(float x, float y);

private:
	void performMoveCallbacks() const;
	void performPressCallbacks() const;
	void performReleaseCallbacks() const;

	Orientation m_orientation;

	float m_state;
	float m_position;

	std::list<SliderCallback> m_moveCallbacks;
	std::list<SliderCallback> m_pressCallbacks;
	std::list<SliderCallback> m_releaseCallbacks;
};

}

#endif /* GUI_ABSTRACTSLIDER_H_ */
