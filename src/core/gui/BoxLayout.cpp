/*
 * BoxLayout.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include <iostream>
#include "BoxLayout.h"

namespace Gui
{

BoxLayout::BoxLayout(Orientation orientation) :
		m_orientation(orientation)
{
	setAlignment(Widget::Left | Widget::Top);
}

BoxLayout::BoxLayout(Orientation orientation, Alignment alignment) :
		m_orientation(orientation)
{
	setAlignment(alignment);
}

BoxLayout::~BoxLayout()
{
}

void BoxLayout::setOrientation(Orientation orientation)
{
	m_orientation = orientation;
}

Layout::Orientation BoxLayout::getOrientation() const
{
	return m_orientation;
}

const Rectangle BoxLayout::getPreferredGeometry() const
{
	bool firstItem = true;
	float margin = getMargin();
	float padding = getPadding();
	float preferredWidth = margin;
	float preferredHeight = margin;

	for (Widget* child : getChildren()) {
		if (!child->isVisible()) {
			continue;
		}

		const Rectangle& childGeometry = child->getPreferredGeometry();

		if (isHorizontal()) {
			preferredWidth += childGeometry.getWidth() + (firstItem ? 0 : padding);
			preferredHeight = std::max(preferredHeight, childGeometry.getHeight() + margin);
		}
		else {
			preferredWidth = std::max(preferredWidth, childGeometry.getWidth() + margin);
			preferredHeight += childGeometry.getHeight() + (firstItem ? 0 : padding);
		}

		firstItem = false;
	}

	return Rectangle(0, 0, preferredWidth + margin, preferredHeight + margin);
}

void BoxLayout::onLayoutChanged()
{
	float margin = getMargin();
	float padding = getPadding();

	const Rectangle& geometry = getGeometry();
	Rectangle preferredGeometry = getPreferredGeometry();
	Rectangle offset = calcAlignment(preferredGeometry, geometry, Widget::Relative);
	offset.move(margin, margin);

	for (Widget* child : getChildren()) {
		if (!child->isVisible()) {
			continue;
		}

		Rectangle childRectangle = child->getPreferredGeometry();

		if (isHorizontal()) {
			childRectangle.setHeight(preferredGeometry.getHeight());
		}
		else {
			childRectangle.setWidth(preferredGeometry.getWidth());
		}

		childRectangle.move(offset.getLeft(), offset.getTop());
		child->alignToRectangle(childRectangle, Widget::Absolute);

		if (isHorizontal()) {
			offset.move(childRectangle.getWidth() + padding, 0);
		}
		else {
			offset.move(0, childRectangle.getHeight() + padding);
		}
	}
}

bool BoxLayout::isHorizontal() const
{
	return (m_orientation == Layout::Horizontal);
}

bool BoxLayout::isVertical() const
{
	return (m_orientation == Layout::Vertical);
}

}
