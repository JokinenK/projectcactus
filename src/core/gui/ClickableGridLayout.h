/*
 * ClickableGridLayout.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef GUI_CLICKABLEGRIDLAYOUT_H_
#define GUI_CLICKABLEGRIDLAYOUT_H_

#include <functional>
#include "GridLayout.h"

namespace Gui
{

class ClickableGridLayout : public GridLayout
{
public:
	typedef std::function<void(const ClickableGridLayout*, int column, int row)> ClickableGridLayoutCallback;

	ClickableGridLayout(int cols, int rows);
	virtual ~ClickableGridLayout();

	void addPressCallback(ClickableGridLayoutCallback clickableGridLayoutCallback);
	void addClickCallback(ClickableGridLayoutCallback clickableGridLayoutCallback);
	void addReleaseCallback(ClickableGridLayoutCallback clickableGridLayoutCallback);

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

private:
	void performPressCallbacks(int column, int row) const;
	void performClickCallbacks(int column, int row) const;
	void performReleaseCallbacks(int column, int row) const;

	bool m_state;

	std::list<ClickableGridLayoutCallback> m_pressCallbacks;
	std::list<ClickableGridLayoutCallback> m_clickCallbacks;
	std::list<ClickableGridLayoutCallback> m_releaseCallbacks;
};

}

#endif /* GUI_CLICKABLEGRIDLAYOUT_H_ */
