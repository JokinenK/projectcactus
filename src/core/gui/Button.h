/*
 * Button.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_BUTTON_H_
#define GUI_BUTTON_H_

#include "core/gui/BoxLayout.h"
#include "core/gui/AbstractButton.h"
#include "core/gui/Label.h"
#include "Picture.h"

namespace Gui
{

class Button : public AbstractButton
{
public:
	explicit Button(const std::string& text);
	virtual ~Button();

	void setText(const std::string& text);
	const std::string& getText() const;

	void setIcon(ITexture* icon);
	ITexture* getIcon() const;

	void setFont(Font* font);
	Font* getFont() const;

	virtual const Color& getBgColor1() const;
	virtual const Color& getBgColor2() const;

	virtual void render(IShader* shader, IModel* model) const;

	virtual void update();

	virtual void invalidate();

	virtual void onLayoutChanged();

private:
	BoxLayout* m_horizLayout;
	Label* m_label;
};

}

#endif /* GUI_BUTTON_H_ */
