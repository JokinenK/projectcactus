/*
 * ScrollArea.h
 *
 *  Created on: 4.3.2017
 *      Author: kalle
 */

#ifndef GUI_SCROLLAREA_H_
#define GUI_SCROLLAREA_H_

#include "WidgetContainer.h"
#include "ScrollBar.h"
#include "support/Rectangle.h"
#include "support/Point.h"

class GuiRenderer;

namespace Gui {

class ScrollArea : public Widget
{
public:
	ScrollArea();
	virtual ~ScrollArea();

	void setWidget(Widget* widget);
	Widget* getWidget() const;

	void scroll(float left, float top);

	void reset();

	virtual void update();

	virtual void invalidate();

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual void render(IShader* shader, IModel* model) const;

	virtual void onLayoutChanged();

private:
	void scrollBarMoved(const AbstractSlider* abstractSlider);

	WidgetContainer* m_widgetContainer;
	BoxLayout* m_vertLayout;
	BoxLayout* m_horizLayout;
	ScrollBar* m_vertScrollBar;
	ScrollBar* m_horizScrollBar;
	Point m_scrollOffset;
};

} /* namespace Gui */

#endif /* GUI_SCROLLAREA_H_ */
