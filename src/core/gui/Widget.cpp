/*
 * Widget.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <glm/gtx/transform.hpp>
#include <iostream>
#include "Widget.h"
#include "core/gui/GuiRenderer.h"
#include "core/ITexture.h"
#include "core/IModel.h"
#include "core/IUniform.h"

namespace
{
	glm::mat4 createModelMatrix(const Rectangle& geometry)
	{
		float width = geometry.getWidth();
		float height = geometry.getHeight();
		float posX = geometry.getLeft();
		float posY = geometry.getTop();

		glm::mat4 modelMatrix = glm::scale(glm::vec3(width, height, 0));
		glm::mat4 translateMatrix = glm::translate(glm::vec3(posX, posY, 0));

		return translateMatrix * modelMatrix;
	}
}

namespace Gui
{

Widget::Widget() :
		m_visibility(true),
		m_dirty(true),
		m_parent(0),
		m_guiRenderer(0)
{
	setBgColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	setAlignment(Widget::Left | Widget::Top);
	setMargin(0);
}

Widget::~Widget()
{
}

void Widget::setVisibility(bool visibility)
{
	m_visibility = visibility;
	invalidate();
}

void Widget::setDirty(bool dirty)
{
	m_dirty = dirty;
}

bool Widget::isDirty() const
{
	return m_dirty;
}

bool Widget::isVisible() const
{
	return m_visibility;
}

void Widget::setPosition(const Point& position)
{
	m_geometry.setPosition(position);
	invalidate();
}

const Point Widget::getPosition() const
{
	return m_geometry.getPosition();
}

void Widget::setPreferredGeometry(const Rectangle& preferredGeometry)
{
	m_preferredGeometry = preferredGeometry;
	m_geometry = preferredGeometry;
	invalidate();
}

const Rectangle Widget::getPreferredGeometry() const
{
	return m_preferredGeometry;
}

void Widget::setGeometry(const Rectangle& geometry)
{
	m_geometry = geometry;
	invalidate();
}

const Rectangle& Widget::getGeometry() const
{
	return m_geometry;
}

const Rectangle Widget::getTransformedGeometry() const
{
	if (!m_parent) {
		return m_geometry;
	}

	Rectangle parentGeometry = m_parent->getTransformedGeometry();

	return Rectangle(
			m_geometry.getLeft() + parentGeometry.getLeft(),
			m_geometry.getTop() + parentGeometry.getTop(),
			m_geometry.getWidth(),
			m_geometry.getHeight());
}

void Widget::setAlignment(int alignment)
{
	m_alignment = alignment;
	invalidate();
}

int Widget::getAlignment() const
{
	return m_alignment;
}

void Widget::setMargin(int margin)
{
	m_margin = margin;
	invalidate();
}

int Widget::getMargin() const
{
	return m_margin;
}

bool Widget::contains(float x, float y) const
{
	return getTransformedGeometry().contains(x, y);
}

bool Widget::contains(const Point& point) const
{
	return getTransformedGeometry().contains(point);
}

void Widget::setParent(Widget* parent)
{
	m_parent = parent;
}

Widget* Widget::getParent() const
{
	return m_parent;
}

void Widget::setBgColor(const Color& bgColor)
{
	setBgGradient(bgColor, bgColor);
}

void Widget::setBgGradient(const Color& bgColor1, const Color& bgColor2)
{
	m_bgColor1 = bgColor1;
	m_bgColor2 = bgColor2;
}

const Color& Widget::getBgColor1() const
{
	return m_bgColor1;
}

const Color& Widget::getBgColor2() const
{
	return m_bgColor2;
}

Rectangle Widget::calcAlignment(const Rectangle& source, const Rectangle& target, AlignType alignType)
{
	float left = 0;
	float top = 0;

	if ((m_alignment & Center) == Center) {
		left = (target.getWidth() - source.getWidth()) / 2;
	}
	else if ((m_alignment & Right) == Right) {
		left = (target.getWidth() - source.getWidth());
	}
	else {
		left = 0;
	}

	if ((m_alignment & Middle) == Middle) {
		top = (target.getHeight() - source.getHeight()) / 2;
	}
	else if ((m_alignment & Bottom) == Bottom) {
		top = (target.getHeight() - source.getHeight());
	}
	else {
		top = 0;
	}

	if (alignType == Absolute) {
		left += target.getLeft();
		top += target.getTop();
	}

	return Rectangle(left, top, source.getWidth(), source.getHeight());
}

bool Widget::alignToRectangle(const Rectangle& target, AlignType alignType)
{
	setGeometry(calcAlignment(getGeometry(), target, alignType));
	return true;
}

void Widget::setGuiRenderer(GuiRenderer* guiRenderer)
{
	m_guiRenderer = guiRenderer;
}

GuiRenderer* Widget::getGuiRenderer() const
{
	if (m_parent) {
		return m_parent->getGuiRenderer();
	}

	return m_guiRenderer;
}

bool Widget::isLeftAligned() const
{
	return ((m_alignment & Left) == Left);
}

bool Widget::isCenterAligned() const
{
	return ((m_alignment & Center) == Center);
}

bool Widget::isRightAligned() const
{
	return ((m_alignment & Right) == Right);
}

bool Widget::isTopAligned() const
{
	return ((m_alignment & Top) == Top);
}

bool Widget::isMiddleAligned() const
{
	return ((m_alignment & Middle) == Middle);
}

bool Widget::isBottomAligned() const
{
	return ((m_alignment & Bottom) == Bottom);
}

bool Widget::mouseEvent(const MouseEvent* mouseEvent)
{
	/* Unused */
	(void) mouseEvent;

	return false;
}

bool Widget::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	/* Unused */
	(void) keyboardEvent;

	return false;
}

void Widget::invalidate()
{
	setDirty(true);
	update();
}

void Widget::update()
{
	if (isVisible() && isDirty()) {
		onLayoutChanged();
		setDirty(false);
	}
}

void Widget::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		shader->getUniform("uniformBgGradiendColor1")->set(getBgColor1().toVec4());
		shader->getUniform("uniformBgGradiendColor2")->set(getBgColor2().toVec4());
		shader->getUniform("uniformModel")->set(createModelMatrix(getTransformedGeometry()));

		model->render();
	}
}

}
