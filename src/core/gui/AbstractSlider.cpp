/*
 * AbstractSlider.cpp
 *
 *  Created on: 28.2.2017
 *      Author: kalle
 */

#include <iostream>
#include <cmath>
#include "core/gui/AbstractSlider.h"
#include "support/Utils.h"

namespace
{
	const static float EPSILON = 0.0001f;

	bool floatEquals(float lhs, float rhs) {
		return std::abs(lhs - rhs) < EPSILON;
	}
}

namespace Gui
{

AbstractSlider::AbstractSlider(Orientation orientation) :
		m_orientation(orientation),
		m_state(false),
		m_position(0.0f)
{
}

AbstractSlider::~AbstractSlider()
{
}

void AbstractSlider::addMoveCallback(SliderCallback sliderCallback)
{
	m_moveCallbacks.push_back(sliderCallback);
}

void AbstractSlider::addPressCallback(SliderCallback sliderCallback)
{
	m_pressCallbacks.push_back(sliderCallback);
}

void AbstractSlider::addReleaseCallback(SliderCallback sliderCallback)
{
	m_releaseCallbacks.push_back(sliderCallback);
}

void AbstractSlider::performMoveCallbacks() const
{
	for (auto callback : m_moveCallbacks) {
		callback(this);
	}
}

void AbstractSlider::performPressCallbacks() const
{
	for (auto callback : m_pressCallbacks) {
		callback(this);
	}
}

void AbstractSlider::performReleaseCallbacks() const
{
	for (auto callback : m_releaseCallbacks) {
		callback(this);
	}
}

void AbstractSlider::setPosition(float position)
{
	position = Utils::clamp(position, 0.0f, 1.0f);
	bool sliderMoved = !floatEquals(position, m_position);

	m_position = position;
	performPressCallbacks();

	if (sliderMoved) {
		performMoveCallbacks();
	}

	performReleaseCallbacks();
	invalidate();
}

float AbstractSlider::getPosition() const
{
	return m_position;
}

float AbstractSlider::parsePosition(float x, float y)
{
	const Rectangle& geometry = getTransformedGeometry();
	Point normalized(x - geometry.getLeft(), y - geometry.getTop());

	float position = 0.0f;

	if (m_orientation == Horizontal) {
		float width = geometry.getWidth() - getMargin() * 2;
		position = Utils::clamp(normalized.getX() / width, 0.0f, 1.0f);
	}
	else if (m_orientation == Vertical) {
		float height = geometry.getHeight() - getMargin() * 2;
		position = Utils::clamp(normalized.getY() / height, 0.0f, 1.0f);
	}

	return position;
}

bool AbstractSlider::mouseEvent(const MouseEvent* mouseEvent)
{
	float x = mouseEvent->getX();
	float y = mouseEvent->getY();

	bool state = mouseEvent->isButtonPressed(1);
	bool prevState = m_state;

	bool stateChanged = state != prevState;
	bool insideButton = contains(x, y);

	if (stateChanged) {
		if (insideButton && state && mouseEvent->isButtonEvent()) {
			m_state = state;
			m_position = parsePosition(x, y);
			performPressCallbacks();
			performMoveCallbacks();
			invalidate();
			return true;
		}
		else if (!state) {
			m_state = state;
			m_position = parsePosition(x, y);
			performMoveCallbacks();
			performReleaseCallbacks();
			invalidate();
			return true;
		}
	}
	else if (prevState) {
		m_position = parsePosition(x, y);
		performMoveCallbacks();
		invalidate();
		return true;
	}

	return false;
}

}
