/*
 * ScrollBar.cpp
 *
 *  Created on: 6.3.2017
 *      Author: kalle
 */

#include "ScrollBar.h"
#include "support/Utils.h"

namespace Gui {

ScrollBar::ScrollBar(Orientation orientation) :
		AbstractSlider(orientation),
		m_step(0.1f),
		m_orientation(orientation),
		m_layout(new BoxLayout(orientation)),
		m_button1(new Button(orientation == Widget::Horizontal ? "<" : "^")),
		m_spacer1(new Widget()),
		m_thumb(new Widget()),
		m_spacer2(new Widget()),
		m_button2(new Button(orientation == Widget::Horizontal ? ">" : "v"))
{
	m_layout->setParent(this);
	m_layout->addChildren(m_button1);
	m_layout->addChildren(m_spacer1);
	m_layout->addChildren(m_thumb);
	m_layout->addChildren(m_spacer2);
	m_layout->addChildren(m_button2);
	//m_layout->setAlignment(Middle | Center);

	Rectangle geometry = Rectangle(0, 0, 20, 20);
	setPreferredGeometry(geometry);
	m_button1->setPreferredGeometry(geometry);
	m_spacer1->setPreferredGeometry(geometry);
	m_thumb->setPreferredGeometry(geometry);
	m_spacer2->setPreferredGeometry(geometry);
	m_button2->setPreferredGeometry(geometry);

	m_button1->addClickCallback(std::bind(&ScrollBar::button1Clicked, this, std::placeholders::_1));
	m_button2->addClickCallback(std::bind(&ScrollBar::button2Clicked, this, std::placeholders::_1));

	m_button1->setAlignment(Middle | Center);
	m_button2->setAlignment(Middle | Center);

	m_spacer1->setBgColor(Color(0.3f, 0.3f, 0.3f));
	m_thumb->setBgColor(Color(0.5f, 0.5f, 0.5f));
	m_spacer2->setBgColor(Color(0.3f, 0.3f, 0.3f));
}

ScrollBar::~ScrollBar()
{
	delete m_layout;
}

void ScrollBar::setStep(float step)
{
	m_step = step;
}

float ScrollBar::getStep() const
{
	return m_step;
}

float ScrollBar::parsePosition(float x, float y)
{
	float position;
	const Rectangle& rootGeometry = getTransformedGeometry();
	const Rectangle buttonGeometry = m_button1->getPreferredGeometry();
	const Rectangle thumbGeometry = m_thumb->getPreferredGeometry();

	Point normalized(
			x - rootGeometry.getLeft() - (buttonGeometry.getWidth() + thumbGeometry.getWidth() / 2.0f),
			y - rootGeometry.getTop() - (buttonGeometry.getHeight() + thumbGeometry.getHeight() / 2.0f));

	if (m_orientation == Widget::Horizontal) {
		float width = rootGeometry.getWidth() - (buttonGeometry.getWidth() * 2) - thumbGeometry.getWidth();
		position = normalized.getX() / width;

	}
	else {
		float height = rootGeometry.getHeight() - (buttonGeometry.getHeight() * 2) - thumbGeometry.getHeight();
		position = normalized.getY() / height;
	}

	return Utils::clamp(position, 0.0f, 1.0f);
}

void ScrollBar::setThumbSize(float size)
{
	float width;
	float height;

	if (m_orientation == Widget::Horizontal) {
		width = size;
		height = m_thumb->getPreferredGeometry().getHeight();
	}
	else {
		width = m_thumb->getPreferredGeometry().getWidth();
		height = size;
	}

	m_thumb->setPreferredGeometry(Rectangle(0, 0, width, height));
}

void ScrollBar::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		Widget::render(shader, model);
		m_layout->render(shader, model);
	}
}

void ScrollBar::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();
		m_layout->update();
	}
}

void ScrollBar::invalidate()
{
	Widget::invalidate();
	m_layout->invalidate();
}

bool ScrollBar::mouseEvent(const MouseEvent* mouseEvent)
{
	if (isVisible()) {
		if (m_layout->mouseEvent(mouseEvent)) {
			return true;
		}

		return AbstractSlider::mouseEvent(mouseEvent);
	}

	return false;
}

void ScrollBar::onLayoutChanged()
{
	Rectangle rootGeometry = getPreferredGeometry();
	Rectangle thumbGeometry = m_thumb->getPreferredGeometry();
	Rectangle buttonGeometry = m_button1->getPreferredGeometry();

	float position = AbstractSlider::getPosition();
	float availableWidth = rootGeometry.getWidth() - thumbGeometry.getWidth() - (buttonGeometry.getWidth() * 2);
	float availableHeight = rootGeometry.getHeight() - thumbGeometry.getHeight() - (buttonGeometry.getHeight() * 2);

	Dimensions dimensionsSpacer1;
	Dimensions dimensionsSpacer2;

	if (m_orientation == Widget::Horizontal) {
		dimensionsSpacer1.setWidth(availableWidth * position);
		dimensionsSpacer1.setHeight(thumbGeometry.getHeight());

		dimensionsSpacer2.setWidth(availableWidth * (1.0f - position));
		dimensionsSpacer2.setHeight(thumbGeometry.getHeight());
	}
	else {
		dimensionsSpacer1.setWidth(thumbGeometry.getWidth());
		dimensionsSpacer1.setHeight(availableHeight * position);

		dimensionsSpacer2.setWidth(thumbGeometry.getWidth());
		dimensionsSpacer2.setHeight(availableHeight * (1.0f - position));
	}

	m_spacer1->setPreferredGeometry(Rectangle(0, 0, dimensionsSpacer1.getWidth(), dimensionsSpacer1.getHeight()));
	m_spacer2->setPreferredGeometry(Rectangle(0, 0, dimensionsSpacer2.getWidth(), dimensionsSpacer2.getHeight()));

	m_layout->alignToRectangle(getGeometry());
}

void ScrollBar::button1Clicked(const AbstractButton* abstractButton)
{
	/* Unused */
	(void) abstractButton;

	setPosition(Utils::clamp(getPosition() - m_step, 0.0f, 1.0f));
}

void ScrollBar::button2Clicked(const AbstractButton* abstractButton)
{
	/* Unused */
	(void) abstractButton;

	setPosition(Utils::clamp(getPosition() + m_step, 0.0f, 1.0f));
}

} /* namespace Gui */
