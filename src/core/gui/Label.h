/*
 * Label.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_LABEL_H_
#define GUI_LABEL_H_

#include "core/text/Font.h"
#include "core/text/TextInstance.h"
#include "core/gui/Widget.h"
#include "core/gui/Picture.h"

namespace Gui
{

class Label : public Widget
{
public:
	Label(const std::string& text = std::string());
	virtual ~Label();

	void setText(const std::string& text);
	const std::string& getText() const;

	void setFont(Font* font);
	Font* getFont() const;

	void setIcon(ITexture* texture);
	ITexture* getIcon() const;

	void setIconSize(const Dimensions& iconSize);
	const Dimensions getIconSize() const;

	virtual const Rectangle getPreferredGeometry() const;

	virtual void render(IShader* shader, IModel* model) const;

	virtual void update();

	virtual void onLayoutChanged();

private:
	TextInstance* m_text;
	Font* m_font;
	Gui::Picture* m_icon;
};

}

#endif /* GUI_LABEL_H_ */
