/*
 * AbstractButton.h
 *
 *  Created on: 28.2.2017
 *      Author: kalle
 */

#ifndef GUI_ABSTRACTBUTTON_H_
#define GUI_ABSTRACTBUTTON_H_

#include <functional>
#include "core/gui/Widget.h"
#include "core/events/MouseEvent.h"

namespace Gui
{

class AbstractButton : public Widget
{
public:
	typedef std::function<void(const AbstractButton*)> ButtonCallback;

	AbstractButton();
	virtual ~AbstractButton();

	void addClickCallback(ButtonCallback buttonCallback);
	void addPressCallback(ButtonCallback buttonCallback);
	void addReleaseCallback(ButtonCallback buttonCallback);

	void click() const;
	bool getState() const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

private:
	void performClickCallbacks() const;
	void performPressCallbacks() const;
	void performReleaseCallbacks() const;

	bool m_state;

	std::list<ButtonCallback> m_clickCallbacks;
	std::list<ButtonCallback> m_pressCallbacks;
	std::list<ButtonCallback> m_releaseCallbacks;
};

}

#endif /* GUI_ABSTRACTBUTTON_H_ */
