/*
 * TextField.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_TEXTFIELD_H_
#define GUI_TEXTFIELD_H_

#include <string>
#include <functional>
#include "core/gui/Widget.h"
#include "core/gui/Label.h"
#include "core/events/KeyboardEvent.h"

class IShader;
class IModel;

namespace Gui
{

class TextField : public Widget
{
public:
	typedef std::function<void(const TextField*)> TextFieldCallback;

	TextField();
	virtual ~TextField();

	void addChangedCallback(TextFieldCallback callback);
	void addReturnPressedCallback(TextFieldCallback callback);

	void setText(const std::string& text);
	const std::string& getText() const;
	void clear();

	virtual void render(IShader* shader, IModel* model) const;

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

	virtual void onLayoutChanged();

private:
	void performTextChangedCallbacks() const;
	void performReturnPressedCallbacks() const;

	bool m_hasFocus;
	Label* m_label;
	std::string m_text;

	std::list<TextFieldCallback> m_textChangedCallbacks;
	std::list<TextFieldCallback> m_returnPressedCallbacks;
};

}

#endif /* GUI_TEXTFIELD_H_ */
