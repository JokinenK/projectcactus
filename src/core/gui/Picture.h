/*
 * Picture.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_PICTURE_H_
#define GUI_PICTURE_H_

#include "core/gui/Widget.h"

class Camera;
class IModel;
class IShader;
class ITexture;

namespace Gui
{

class Picture : public Widget
{
public:
	explicit Picture(ITexture* texture = 0);
	virtual ~Picture();

	void setTexture(ITexture* texture);
	ITexture* getTexture() const;

	virtual void render(IShader* shader, IModel* model) const;

private:
	ITexture* m_texture;
};

}

#endif /* GUI_PICTURE_H_ */
