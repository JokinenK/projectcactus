/*
 * FileDialog.h
 *
 *  Created on: 8.3.2017
 *      Author: kalle
 */

#ifndef GUI_FILEDIALOG_H_
#define GUI_FILEDIALOG_H_

#include "Widget.h"
#include "Button.h"
#include "TextField.h"
#include "ScrollArea.h"
#include "ClickableGridLayout.h"
#include "hal/File.h"

namespace Gui {

class FileDialog : public Widget
{
public:
	typedef enum {
		DIALOG_OPEN,
		DIALOG_SAVE
	} DialogType;

	typedef std::function<void(const FileDialog*, const std::string& filePath)> FileDialogCallback;

	explicit FileDialog(DialogType dialogType);
	virtual ~FileDialog();

	const std::string& getSelectedFile() const;

	void changeCurrentPath(const std::string& path);
	void changeCurrentFile(const std::string& file);

	void addConfirmCallback(FileDialogCallback callback);
	void addCancelCallback(FileDialogCallback callback);

	void updateGeometry();

	virtual void update();

	virtual void invalidate();

	virtual void render(IShader* shader, IModel* model) const;

	virtual void onLayoutChanged();

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

private:
	void performConfirmCallbacks(const std::string& filePath) const;
	void performCancelCallbacks() const;

	void itemClicked(const ClickableGridLayout* clickableGridLayout, int column, int row);
	void buttonOkClicked(const AbstractButton* abstractButton);
	void buttonCancelClicked(const AbstractButton* abstractButton);

	bool isOpenDialog() const;
	bool isSaveDialog() const;

	DialogType m_dialogType;
	std::string m_currentPath;
	std::string m_currentFile;
	Hal::File::DirectoryInfo m_directoryInfo;

	ITexture* m_folderTexture;
	ITexture* m_fileTexture;

	BoxLayout* m_vertLayout;
	BoxLayout* m_horizLayout;
	ScrollArea* m_scrollArea;
	TextField* m_textField;
	Button* m_buttonOk;
	Button* m_buttonCancel;
	ClickableGridLayout* m_clickableGridLayout;

	std::list<FileDialogCallback> m_confirmCallbacks;
	std::list<FileDialogCallback> m_cancelCallbacks;
};

} /* namespace Gui */

#endif /* GUI_FILEDIALOG_H_ */
