/*
 * Slider.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#ifndef GUI_SLIDER_H_
#define GUI_SLIDER_H_

#include "core/gui/BoxLayout.h"
#include "core/gui/AbstractSlider.h"
#include "core/gui/Label.h"

namespace Gui
{

class Slider : public AbstractSlider
{
public:
	Slider(const std::string& text);
	virtual ~Slider();

	void setText(const std::string& text);
	const std::string& getText() const;

	void setFont(Font* font);
	Font* getFont() const;

	void setRange(float low, float high);

	void setValue(float value);
	float getValue() const;

	virtual void render(IShader* shader, IModel* model) const;

	virtual void update();

	virtual void invalidate();

	virtual void onLayoutChanged();

private:
	std::string m_text;
	float m_low;
	float m_high;

	BoxLayout* m_vertLayout;
	BoxLayout* m_horizLayout;

	Label* m_label;
	Widget* m_left;
	Widget* m_slider;
	Widget* m_right;
};

}

#endif /* GUI_SLIDER_H_ */
