/*
 * Console.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include "TextField.h"
#include "core/KeyMappings.h"
#include <sstream>
#include <locale>

namespace Gui
{

TextField::TextField() :
		m_hasFocus(false),
		m_label(new Label())
{
	m_label->setParent(this);
}

TextField::~TextField()
{
	delete m_label;
}

void TextField::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		Widget::render(shader, model);
		m_label->render(shader, model);
	}
}

void TextField::setText(const std::string& text)
{
	m_text = text;
}

const std::string& TextField::getText() const
{
	return m_text;
}

void TextField::clear()
{
	m_text.clear();
}

bool TextField::mouseEvent(const MouseEvent* mouseEvent)
{
	if (!isVisible()) {
		return false;
	}

	if (contains(mouseEvent->getX(), mouseEvent->getY())
			&& mouseEvent->isButtonEvent()) {

		if (mouseEvent->isButtonPressed(1)) {
			m_hasFocus = true;
		}

		return true;
	}

	return false;
}

bool TextField::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	if (!isVisible() || !m_hasFocus) {
		return false;
	}

	if (keyboardEvent->getType() == KeyboardEvent::KeyPress) {
		switch (keyboardEvent->getChar()) {
			case KeyCode::KEYCODE_BACKSPACE: {
				if (!m_text.empty()) {
					m_text.erase(m_text.size() - 1);
					performTextChangedCallbacks();
					invalidate();
				}
			} break;

			case KeyCode::KEYCODE_RETURN: {
				performReturnPressedCallbacks();
				m_hasFocus = false;
				m_text.clear();
				performTextChangedCallbacks();
				invalidate();
			} break;
		}

		return true;
	}
	else if (keyboardEvent->getType() == KeyboardEvent::TextInput) {
		char ch = keyboardEvent->getChar();
		m_text.append(&ch, 1);
		performTextChangedCallbacks();
		invalidate();
	}

	return true;
}

void TextField::addChangedCallback(TextFieldCallback callback)
{
	m_textChangedCallbacks.push_back(callback);
}

void TextField::addReturnPressedCallback(TextFieldCallback callback)
{
	m_returnPressedCallbacks.push_back(callback);
}

void TextField::performTextChangedCallbacks() const
{
	for (auto callback : m_textChangedCallbacks) {
		callback(this);
	}
}

void TextField::performReturnPressedCallbacks() const
{
	for (auto callback : m_returnPressedCallbacks) {
		callback(this);
	}
}

void TextField::onLayoutChanged()
{
	m_label->setText(m_text);
	m_label->alignToRectangle(getGeometry());
}

}
