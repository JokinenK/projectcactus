/*
 * Picture.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include "Picture.h"

#include <iostream>
#include "core/ITexture.h"
#include "core/IModel.h"
#include "core/IShader.h"
#include "core/IUniform.h"

namespace Gui
{

Picture::Picture(ITexture* texture) : Widget()
{
	setTexture(texture);
}

Picture::~Picture()
{
}

void Picture::setTexture(ITexture* texture)
{
	m_texture = texture;
}

ITexture* Picture::getTexture() const
{
	return m_texture;
}

void Picture::render(IShader* shader, IModel* model) const
{
	if (isVisible() && m_texture) {
		m_texture->bind(0);
		shader->getUniform("uniformHasTexture")->set(true);
		shader->getUniform("uniformSamplerDiffuse")->set(0);

		Widget::render(shader, model);

		shader->getUniform("uniformHasTexture")->set(false);
		m_texture->unbind(0);
	}
}

}
