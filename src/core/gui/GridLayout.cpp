/*
 * GridLayout.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include "GridLayout.h"
#include "support/Rectangle.h"

namespace Gui
{

GridLayout::GridLayout(int columns, int rows) :
		m_evenColor(0, true),
		m_oddColor(0, true),
		m_columns(0),
		m_rows(0)
{
	resize(columns, rows);
}

GridLayout::~GridLayout()
{
}

void GridLayout::resize(int columns, int rows)
{
	int row;
	int column;
	size_t offset;

	for (row = 0; row < m_rows; row++) {
		for (column = 0; column < m_columns; column++) {
			offset = calcOffset(column, row);
			removeChildren(m_widgets[offset]);
		}
	}

	m_columns = columns;
	m_rows = rows;
	m_widgets.resize(columns * rows);

	for (row = 0; row < m_rows; row++) {
		for (column = 0; column < m_columns; column++) {
			offset = calcOffset(column, row);
			m_widgets[offset] = new WidgetContainer();
			addChildren(m_widgets[offset]);
		}
	}
}

void GridLayout::setWidget(Widget* widget, int column, int row, int colSpan, int rowSpan)
{
	size_t offset = calcOffset(column, row);

	m_widgets[offset]->setWidget(widget);
	m_widgets[offset]->setRowSpan(rowSpan);
	m_widgets[offset]->setColSpan(colSpan);
}

void GridLayout::removeWidget(int col, int row)
{
	size_t offset = calcOffset(col, row);

	m_widgets[offset]->removeWidget();
}

float GridLayout::getCellWidth() const
{
	const Rectangle& geometry = getGeometry();
	float availableWidth = geometry.getWidth() - (2 * getMargin()) - ((m_columns - 1) * getPadding());
	return availableWidth / m_columns;
}

float GridLayout::getCellHeight() const
{
	const Rectangle& geometry = getGeometry();
	float availableHeight = geometry.getHeight() - (2 * getMargin()) - ((m_rows - 1) * getPadding());
	return availableHeight / m_rows;
}

void GridLayout::setEvenColor(const Color& evenColor)
{
	m_evenColor = evenColor;
}

const Color& GridLayout::getEventColor() const
{
	return m_evenColor;
}

void GridLayout::setOddColor(const Color& oddColor)
{
	m_oddColor = oddColor;
}

const Color& GridLayout::getOddColor() const
{
	return m_oddColor;
}

const Rectangle GridLayout::getPreferredGeometry() const
{
	return Widget::getPreferredGeometry();
}

size_t GridLayout::calcOffset(int column, int row) const
{
	return (m_columns * column + row);
}

void GridLayout::onLayoutChanged()
{
	float margin = getMargin();
	float padding = getPadding();
	float cellWidth = getCellWidth();
	float cellHeight = getCellHeight();

	float top = margin;

	int colSpan = 0;
	int rowSpan = 0;
	float childWidth = 0;
	float childHeight = 0;

	for (int row = 0; row < m_rows; row++) {
		float left = margin;
		bool isEven = ((row % 2) == 0);

		for (int col = 0; col < m_columns; col++) {
			size_t offset = calcOffset(col, row);

			if (m_widgets[offset]) {
				colSpan = m_widgets[offset]->getColSpan();
				rowSpan = m_widgets[offset]->getRowSpan();

				childWidth = colSpan * cellWidth + (colSpan - 1) * padding;
				childHeight = rowSpan * cellHeight + (rowSpan - 1) * padding;

				Rectangle childGeometry(left, top, childWidth, childHeight);

				m_widgets[offset]->setBgColor(isEven ? m_evenColor : m_oddColor);
				m_widgets[offset]->setGeometry(childGeometry);

				Widget* widget = m_widgets[offset]->getWidget();

				if (widget) {
					widget->setGeometry(childGeometry);
				}
			}

			left += cellWidth + padding;
		}

		top += cellHeight + padding;
	}
}

}
