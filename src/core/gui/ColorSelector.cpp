/*
 * ColorSelector.h
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <iostream>
#include "core/gui/ColorSelector.h"
#include "support/Rectangle.h"
#include "support/Utils.h"

namespace Gui
{

ColorSelector::ColorSelector() :
		Widget(),
		m_color(Color(0x7F7F7F)),
		m_vertLayout(new BoxLayout(Layout::Vertical)),
		m_red(new Slider("Red")),
		m_green(new Slider("Green")),
		m_blue(new Slider("Blue"))
{
	m_vertLayout->setParent(this);
	m_vertLayout->addChildren(m_red);
	m_vertLayout->addChildren(m_green);
	m_vertLayout->addChildren(m_blue);

	m_red->setBgColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	m_green->setBgColor(Color(0.0f, 0.0f, 0.0f, 0.0f));
	m_blue->setBgColor(Color(0.0f, 0.0f, 0.0f, 0.0f));

//	m_red->setAlignment(Widget::Middle | Widget::Center);
//	m_green->setAlignment(Widget::Middle | Widget::Center);
//	m_blue->setAlignment(Widget::Middle | Widget::Center);

	m_red->setPosition(m_color.getRed());
	m_green->setPosition(m_color.getGreen());
	m_blue->setPosition(m_color.getBlue());

	m_red->addMoveCallback(std::bind(&ColorSelector::redChanged, this, std::placeholders::_1));
	m_green->addMoveCallback(std::bind(&ColorSelector::greenChanged, this, std::placeholders::_1));
	m_blue->addMoveCallback(std::bind(&ColorSelector::blueChanged, this, std::placeholders::_1));
}

ColorSelector::~ColorSelector()
{
	delete m_vertLayout;
}

void ColorSelector::setColor(const Color& color)
{
	m_color = color;
	m_red->setPosition(m_color.getRed());
	m_green->setPosition(m_color.getGreen());
	m_blue->setPosition(m_color.getBlue());
}

const Color& ColorSelector::getColor() const
{
	return m_color;
}

const Color& ColorSelector::getBgColor1() const
{
	return m_color;
}

const Color& ColorSelector::getBgColor2() const
{
	return m_color;
}

void ColorSelector::render(IShader* shader, IModel* model) const
{
	Widget::render(shader, model);
	m_vertLayout->render(shader, model);
}

bool ColorSelector::mouseEvent(const MouseEvent* mouseEvent)
{
	if (contains(mouseEvent->getX(), mouseEvent->getY())) {
		if (m_red->mouseEvent(mouseEvent)) {
			invalidate();
			return true;
		}

		if (m_green->mouseEvent(mouseEvent)) {
			invalidate();
			return true;
		}

		if (m_blue->mouseEvent(mouseEvent)) {
			invalidate();
			return true;
		}
	}

	return false;
}

void ColorSelector::invalidate()
{
	Widget::invalidate();
	m_vertLayout->invalidate();
}

void ColorSelector::redChanged(const AbstractSlider* slider)
{
	m_color.setRed(slider->getPosition());
	performColorChangedCallbacks();
}

void ColorSelector::greenChanged(const AbstractSlider* slider)
{
	m_color.setGreen(slider->getPosition());
	performColorChangedCallbacks();
}

void ColorSelector::blueChanged(const AbstractSlider* slider)
{
	m_color.setBlue(slider->getPosition());
	performColorChangedCallbacks();
}

void ColorSelector::addColorChangedCallback(ColorSelectorCallback callback)
{
	m_colorChangedCallbacks.push_back(callback);
}

void ColorSelector::performColorChangedCallbacks() const
{
	for (auto callback : m_colorChangedCallbacks) {
		callback(this);
	}
}

void ColorSelector::onLayoutChanged()
{
	const Rectangle geometry = getGeometry();
	Rectangle childRectangle = geometry.normalized();
	childRectangle.setHeight(geometry.getHeight() / 3);

	m_red->setPreferredGeometry(childRectangle);
	m_green->setPreferredGeometry(childRectangle);
	m_blue->setPreferredGeometry(childRectangle);
	m_vertLayout->alignToRectangle(geometry);
}

}
