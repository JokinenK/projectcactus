/*
 * GridLayout.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef GUI_GRIDLAYOUT_H_
#define GUI_GRIDLAYOUT_H_

#include <vector>
#include "Layout.h"
#include "WidgetContainer.h"

namespace Gui
{

class GridLayout : public Layout
{
public:
	GridLayout(int columns, int rows);
	virtual ~GridLayout();

	void resize(int columns, int rows);

	float getCellWidth() const;
	float getCellHeight() const;

	void setEvenColor(const Color& evenColor);
	const Color& getEventColor() const;

	void setOddColor(const Color& oddColor);
	const Color& getOddColor() const;

	virtual void setWidget(Widget* widget, int column, int row, int colSpan = 1, int rowSpan = 1);

	virtual void removeWidget(int column, int row);

	virtual const Rectangle getPreferredGeometry() const;

	virtual void onLayoutChanged();

private:
	size_t calcOffset(int col, int row) const;

	Color m_evenColor;
	Color m_oddColor;

	int m_columns;
	int m_rows;

	std::vector<WidgetContainer*> m_widgets;
};

}

#endif /* GUI_GRIDLAYOUT_H_ */
