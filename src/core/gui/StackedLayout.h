/*
 * StackedLayout.h
 *
 *  Created on: 12.3.2017
 *      Author: kalle
 */

#ifndef GUI_STACKEDLAYOUT_H_
#define GUI_STACKEDLAYOUT_H_

#include "Layout.h"

namespace Gui
{

class StackedLayout : public Layout
{
public:
	typedef enum {
		SHOW_ONE,
		SHOW_ALL
	} StackType;

	explicit StackedLayout(StackType stackType);
	virtual ~StackedLayout();

	Widget* widgetAt(int index) const;
	int widgetIndex(Widget* widget) const;

	void setActiveIndex(int activeIndex);
	int getActiveIndex() const;

	void setActiveWidget(Widget* activeWidget);
	Widget* getActiveWidget() const;

	virtual const Rectangle getPreferredGeometry() const;

	virtual void onLayoutChanged();

private:
	StackType m_stackType;
	int m_activeIndex;
};

} /* namespace Gui */

#endif /* GUI_STACKEDLAYOUT_H_ */
