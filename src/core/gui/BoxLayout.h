/*
 * BoxLayout.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef GUI_BOXLAYOUT_H_
#define GUI_BOXLAYOUT_H_

#include "Layout.h"
#include "support/Rectangle.h"

namespace Gui
{

class BoxLayout : public Layout
{
public:
	explicit BoxLayout(Orientation orientation);
	explicit BoxLayout(Orientation orientation, Alignment alignment);
	virtual ~BoxLayout();

	void setOrientation(Orientation orientation);
	Orientation getOrientation() const;

	virtual const Rectangle getPreferredGeometry() const;

	virtual void onLayoutChanged();

private:
	bool isHorizontal() const;
	bool isVertical() const;

	Orientation m_orientation;
};

}

#endif /* GUI_BOXLAYOUT_H_ */
