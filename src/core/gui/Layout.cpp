/*
 * Layout.cpp
 *
 *  Created on: 22.9.2016
 *      Author: kalle
 */

#include <algorithm>
#include "core/gui/Layout.h"

namespace Gui
{

Layout::Layout() : m_padding(0)
{
}

Layout::~Layout()
{
	for (Widget* child : m_children) {
		delete child;
	}
}

void Layout::setPadding(int padding)
{
	m_padding = padding;
	invalidate();
}

int Layout::getPadding() const
{
	return m_padding;
}

void Layout::addChildren(Widget* widget)
{
	m_children.push_back(widget);
	widget->setParent(this);
	widget->invalidate();

	invalidate();
}

void Layout::removeChildren(Widget* widget)
{
	if (containsWidget(widget)) {
		m_children.remove(widget);
	}
}

bool Layout::containsWidget(Widget* widget)
{
	return (std::find(m_children.begin(), m_children.end(), widget) != m_children.end());
}

const std::list<Widget*>& Layout::getChildren() const
{
	return m_children;
}

void Layout::update()
{
	if (isVisible() && isDirty()) {
		Widget::update();

		for (Widget* child : m_children) {
			child->update();
		}
	}
}

void Layout::invalidate()
{
	Widget::invalidate();

	for (Widget* child : m_children) {
		child->invalidate();
	}
}

void Layout::render(IShader* shader, IModel* model) const
{
	if (isVisible()) {
		Widget::render(shader, model);

		for (Widget* child : m_children) {
			child->render(shader, model);
		}
	}
}

bool Layout::mouseEvent(const MouseEvent* mouseEvent)
{
	for (Widget* child : m_children) {
		if (child->isVisible() && child->mouseEvent(mouseEvent)) {
			return true;
		}
	}

	return false;
}

bool Layout::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	for (Widget* child : m_children) {
		if (child->isVisible() && child->keyboardEvent(keyboardEvent)) {
			return true;
		}
	}

	return false;
}

}
