/*
 * ScrollBar.h
 *
 *  Created on: 6.3.2017
 *      Author: kalle
 */

#ifndef GUI_SCROLLBAR_H_
#define GUI_SCROLLBAR_H_

#include "AbstractSlider.h"
#include "Button.h"

namespace Gui {

class ScrollBar : public AbstractSlider
{
public:
	explicit ScrollBar(Orientation orientation);
	virtual ~ScrollBar();

	void setThumbSize(float size);

	void setStep(float step);

	float getStep() const;

	virtual void render(IShader* shader, IModel* model) const;

	virtual void update();

	virtual void invalidate();

	virtual bool mouseEvent(const MouseEvent* mouseEvent);

	virtual void onLayoutChanged();

private:
	virtual float parsePosition(float x, float y);

	void button1Clicked(const AbstractButton* abstractButton);

	void button2Clicked(const AbstractButton* abstractButton);

	float m_step;

	Orientation m_orientation;
	BoxLayout* m_layout;
	Button* m_button1;
	Widget* m_spacer1;
	Widget* m_thumb;
	Widget* m_spacer2;
	Button* m_button2;
};

} /* namespace Gui */

#endif /* GUI_SCROLLBAR_H_ */
