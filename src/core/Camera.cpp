#include <glm/gtx/transform.hpp>
#include <iostream>
#include "core/Camera.h"
#include "core/Transform.h"
#include "support/Utils.h"

namespace
{
	static const glm::mat4 f_biasMatrix(
			0.5f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.5f, 0.5f, 0.5f, 1.0f);
}

/* Pair up force vectors with actual directions */
const glm::vec3 Camera::Left           =  glm::vec3(-1.0f,  0.0f,  0.0f);
const glm::vec3 Camera::Right          =  glm::vec3( 1.0f,  0.0f,  0.0f);
const glm::vec3 Camera::Up             =  glm::vec3( 0.0f,  1.0f,  0.0f);
const glm::vec3 Camera::Down           =  glm::vec3( 0.0f, -1.0f,  0.0f);
const glm::vec3 Camera::Forward        =  glm::vec3( 0.0f,  0.0f, -1.0f);
const glm::vec3 Camera::Backward       =  glm::vec3( 0.0f,  0.0f,  1.0f);

/* Pair up rotation vectors with actual directions */
const glm::vec3 Camera::RotateForward  = Camera::Right;
const glm::vec3 Camera::RotateBackward = Camera::Left;
const glm::vec3 Camera::RotateLeft     = Camera::Up;
const glm::vec3 Camera::RotateRight    = Camera::Down;
const glm::vec3 Camera::RollLeft       = Camera::Forward;
const glm::vec3 Camera::RollRight      = Camera::Backward;

Camera::Camera(float fovRadians, float aspectRatio, float near, float far) :
		m_frustum(fovRadians, aspectRatio, near, far),
		m_splitFrustum(m_frustum),
		m_transform(0)
{
}

Camera::~Camera()
{
}

void Camera::lookAt(const glm::vec3& to)
{
    const glm::vec3& from = m_transform->getPosition();
    glm::quat lookAt = glm::conjugate(glm::toQuat(glm::lookAt(from, to, Camera::Up)));

    m_transform->setRotation(lookAt);
}

void Camera::start()
{
	m_transform = getParent()->getTransform();
}

void Camera::update()
{
	const glm::vec3& cameraPosition = getCameraPosition();
	const glm::vec3& cameraForward = getCameraForward();
	const glm::vec3& cameraUp = getCameraUp();
	const glm::vec3& cameraRight = getCameraRight();

    m_view = glm::lookAt(cameraPosition, cameraPosition + cameraForward, cameraUp);
    m_viewProjectionInverse = glm::inverse(getProjection() * m_view);

    m_splitFrustum.updateCorners(cameraPosition, cameraUp, cameraForward, cameraRight);
}

const glm::mat4& Camera::getView() const
{
	return m_view;
}

const glm::mat4& Camera::getProjection() const
{
	return m_frustum.getProjection();
}

const glm::vec2 Camera::getRange() const
{
	return glm::vec2(m_frustum.getNear(), m_frustum.getFar());
}

void Camera::setNear(float near)
{
	m_frustum.setNear(near);
}

float Camera::getNear() const
{
	return m_frustum.getNear();
}

void Camera::setFar(float far)
{
	m_frustum.setFar(far);
}

float Camera::getFar() const
{
	return m_frustum.getFar();
}

void Camera::setFov(float fovDegrees)
{
	m_frustum.setFov(fovDegrees);
}

float Camera::getFov() const
{
	return m_frustum.getFov();
}

void Camera::setAspectRatio(float aspectRatio)
{
	m_frustum.setAspectRatio(aspectRatio);
}

float Camera::getAspectRatio() const
{
	return m_frustum.getAspectRatio();
}

const ViewFrustum& Camera::getFrustum() const
{
	return m_frustum;
}

const std::vector<ViewFrustum>& Camera::getFrustumSplits() const
{
	return m_splitFrustum.getFrustumSplits();
}

const std::vector<float>& Camera::getFrustumEnds() const
{
	return m_splitFrustum.getFrustumEnds();
}

glm::mat4 Camera::calculateShadowMatrix(const glm::mat4& lightViewProjection, ShadowType shadowType) const
{
	if (shadowType == SHADOW_ORTHO) {
		return f_biasMatrix * lightViewProjection * m_viewProjectionInverse;
	}

	return f_biasMatrix * lightViewProjection;
}

Transform* Camera::getTransform()
{
	return m_transform;
}

const glm::vec3& Camera::getCameraPosition() const
{
	return m_transform->getTransformedPosition();
}

const glm::vec3& Camera::getCameraUp() const
{
	return m_transform->getUp();
}

const glm::vec3& Camera::getCameraForward() const
{
	return m_transform->getForward();
}

const glm::vec3& Camera::getCameraRight() const
{
	return m_transform->getRight();
}
