/*
 * UniformDirectionalLight.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformDirectionalLight.h"
#include "core/light/DirectionalLight.h"

UniformDirectionalLight::UniformDirectionalLight() :
		m_uniformDirection(0)
{
}

UniformDirectionalLight::UniformDirectionalLight(IShader* shader, const std::string& uniformName) :
		UniformBaseLight(shader, uniformName + ".light"),
		m_uniformDirection(shader->getUniform(uniformName + ".direction"))
{
}

UniformDirectionalLight::UniformDirectionalLight(const UniformDirectionalLight& other) :
		UniformBaseLight(other)
{
	operator=(other);
}

UniformDirectionalLight::~UniformDirectionalLight()
{
}

void UniformDirectionalLight::operator=(const UniformDirectionalLight& other)
{
	m_uniformDirection = other.m_uniformDirection;

	UniformBaseLight::operator=(other);
}

void UniformDirectionalLight::set(const DirectionalLight& light)
{
	m_uniformDirection->set(light.getDirection());

	UniformBaseLight::set(light);
}
