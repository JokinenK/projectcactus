/*
 * UniformSpotLight.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformSpotLight.h"
#include "core/light/SpotLight.h"

UniformSpotLight::UniformSpotLight() :
		m_uniformCutoff(0),
		m_uniformDirection(0)
{
}

UniformSpotLight::UniformSpotLight(IShader* shader, const std::string& uniformName) :
		UniformPointLight(shader, uniformName + ".pointLight"),
		m_uniformCutoff(shader->getUniform(uniformName + ".cutoff")),
		m_uniformDirection(shader->getUniform(uniformName + ".direction"))
{
}

UniformSpotLight::UniformSpotLight(const UniformSpotLight& other) :
		UniformPointLight(other)
{
	operator=(other);
}

UniformSpotLight::~UniformSpotLight()
{
}

void UniformSpotLight::operator=(const UniformSpotLight& other)
{
	m_uniformCutoff = other.m_uniformCutoff;
	m_uniformDirection = other.m_uniformDirection;

	UniformPointLight::operator=(other);
}

void UniformSpotLight::set(const SpotLight& light)
{
	m_uniformCutoff->set(light.getCutoff());
	m_uniformDirection->set(light.getDirection());

	UniformPointLight::set(light);
}
