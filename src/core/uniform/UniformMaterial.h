/*
 * UniformMaterial.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMMATERIAL_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMMATERIAL_H_

#include "core/IShader.h"
#include "core/IUniform.h"

class Material;

class UniformMaterial
{
public:
	UniformMaterial();
	UniformMaterial(IShader* shader, const std::string& uniformName);
	UniformMaterial(const UniformMaterial& other);

	virtual ~UniformMaterial();

	void operator=(const UniformMaterial& other);

	void set(const Material& material);

private:
	IUniform* m_uniformHasAmbientTexture;
	IUniform* m_uniformHasDiffuseTexture;
	IUniform* m_uniformHasSpecularTexture;
	IUniform* m_uniformHasShininessTexture;
	IUniform* m_uniformHasDissolveTexture;
	IUniform* m_uniformHasNormalTexture;
	IUniform* m_uniformHasDisplacementTexture;
	IUniform* m_uniformColorAmbient;
	IUniform* m_uniformColorDiffuse;
	IUniform* m_uniformColorSpecular;
	IUniform* m_uniformShininess;
	IUniform* m_uniformDissolve;
	IUniform* m_uniformSamplerAmbient;
	IUniform* m_uniformSamplerDiffuse;
	IUniform* m_uniformSamplerSpecular;
	IUniform* m_uniformSamplerShininess;
	IUniform* m_uniformSamplerDissolve;
	IUniform* m_uniformSamplerNormal;
	IUniform* m_uniformSamplerDisplacement;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMMATERIAL_H_ */
