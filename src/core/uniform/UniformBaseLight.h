/*
 * UniformBaseLight.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMBASELIGHT_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMBASELIGHT_H_

#include "core/IShader.h"
#include "core/IUniform.h"

class BaseLight;

class UniformBaseLight
{
public:
	UniformBaseLight();
	UniformBaseLight(IShader* shader, const std::string& uniformName);
	UniformBaseLight(const UniformBaseLight& other);
	virtual ~UniformBaseLight();

	void operator=(const UniformBaseLight& other);

	void set(const BaseLight& light);

private:
	IUniform* m_uniformShadow;
	IUniform* m_uniformColor;
	IUniform* m_uniformIntensity;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMBASELIGHT_H_ */
