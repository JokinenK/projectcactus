/*
 * UniformPointLight.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformPointLight.h"
#include "core/light/PointLight.h"

UniformPointLight::UniformPointLight() :
		m_uniformAttenuation(0),
		m_uniformRadius(0),
		m_uniformPosition(0)
{
}

UniformPointLight::UniformPointLight(IShader* shader, const std::string& uniformName) :
		UniformBaseLight(shader, uniformName + ".light"),
		m_uniformAttenuation(shader->getUniform(uniformName + ".attenuation")),
		m_uniformRadius(shader->getUniform(uniformName + ".radius")),
		m_uniformPosition(shader->getUniform(uniformName + ".position"))
{
}

UniformPointLight::~UniformPointLight()
{
}

UniformPointLight::UniformPointLight(const UniformPointLight& other) :
		UniformBaseLight(other)
{
	operator=(other);
}

void UniformPointLight::operator=(const UniformPointLight& other)
{
	m_uniformAttenuation = other.m_uniformAttenuation;
	m_uniformRadius = other.m_uniformRadius;
	m_uniformPosition = other.m_uniformPosition;

	UniformBaseLight::operator=(other);
}

void UniformPointLight::set(const PointLight& light)
{
	m_uniformAttenuation->set(light.getAttenuation().toVec3());
	m_uniformRadius->set(light.getRadius());
	m_uniformPosition->set(light.getPosition());

	UniformBaseLight::set(light);
}
