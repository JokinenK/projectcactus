/*
 * BaseLightUniform.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMCAMERA_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMCAMERA_H_

#include "core/IShader.h"
#include "core/IUniform.h"

class Camera;

class UniformCamera
{
public:
	UniformCamera();
	UniformCamera(IShader* shader, const std::string& uniformName);
	UniformCamera(const UniformCamera& other);

	virtual ~UniformCamera();

	void operator=(const UniformCamera& other);

	void set(const Camera& camera);

private:
	IUniform* m_uniformPosition;
	IUniform* m_uniformForward;
	IUniform* m_uniformRangeNear;
	IUniform* m_uniformRangeFar;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMCAMERA_H_ */
