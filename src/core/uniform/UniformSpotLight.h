/*
 * UniformSpotLight.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMSPOTLIGHT_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMSPOTLIGHT_H_

#include "core/IShader.h"
#include "core/IUniform.h"
#include "core/uniform/UniformPointLight.h"

class SpotLight;

class UniformSpotLight : public UniformPointLight
{
public:
	UniformSpotLight();
	UniformSpotLight(IShader* shader, const std::string& uniformName);
	UniformSpotLight(const UniformSpotLight& other);
	virtual ~UniformSpotLight();

	void operator=(const UniformSpotLight& other);

	void set(const SpotLight& light);

private:
	IUniform* m_uniformCutoff;
	IUniform* m_uniformDirection;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMSPOTLIGHT_H_ */
