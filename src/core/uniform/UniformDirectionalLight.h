/*
 * UniformDirectionalLight.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMDIRECTIONALLIGHT_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMDIRECTIONALLIGHT_H_

#include "core/IShader.h"
#include "core/IUniform.h"
#include "core/uniform/UniformBaseLight.h"

class DirectionalLight;

class UniformDirectionalLight : public UniformBaseLight
{
public:
	UniformDirectionalLight();
	UniformDirectionalLight(IShader* shader, const std::string& uniformName);
	UniformDirectionalLight(const UniformDirectionalLight& other);

	virtual ~UniformDirectionalLight();

	void operator=(const UniformDirectionalLight& other);

	void set(const DirectionalLight& light);

private:
	IUniform* m_uniformDirection;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMDIRECTIONALLIGHT_H_ */
