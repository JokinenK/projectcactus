/*
 * UniformPipeline.cpp
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#include "UniformPipeline.h"
#include "core/Pipeline.h"

UniformPipeline::UniformPipeline() :
		m_uniformModel(0),
		m_uniformInverseModel(0),
		m_uniformView(0),
		m_uniformInverseView(0),
		m_uniformProjection(0),
		m_uniformInverseProjection(0),
		m_uniformMV(0),
		m_uniformInverseMV(0),
		m_uniformVP(0),
		m_uniformInverseVP(0),
		m_uniformMVP(0),
		m_uniformInverseMVP(0)
{
}

UniformPipeline::UniformPipeline(IShader* shader, const std::string& uniformName) :
		m_uniformModel(shader->getUniform(uniformName + ".model")),
		m_uniformInverseModel(shader->getUniform(uniformName + ".inverseModel")),
		m_uniformView(shader->getUniform(uniformName + ".view")),
		m_uniformInverseView(shader->getUniform(uniformName + ".inverseView")),
		m_uniformProjection(shader->getUniform(uniformName + ".projection")),
		m_uniformInverseProjection(shader->getUniform(uniformName + ".inverseProjection")),
		m_uniformMV(shader->getUniform(uniformName + ".MV")),
		m_uniformInverseMV(shader->getUniform(uniformName + ".inverseMV")),
		m_uniformVP(shader->getUniform(uniformName + ".VP")),
		m_uniformInverseVP(shader->getUniform(uniformName + ".inverseVP")),
		m_uniformMVP(shader->getUniform(uniformName + ".MVP")),
		m_uniformInverseMVP(shader->getUniform(uniformName + ".inverseMVP"))
{
}

UniformPipeline::UniformPipeline(const UniformPipeline& other)
{
	operator=(other);
}

UniformPipeline::~UniformPipeline()
{
}

void UniformPipeline::operator=(const UniformPipeline& other)
{
	m_uniformModel = other.m_uniformModel;
	m_uniformInverseModel = other.m_uniformInverseModel;
	m_uniformView = other.m_uniformView;
	m_uniformInverseView = other.m_uniformInverseView;
	m_uniformProjection = other.m_uniformProjection;
	m_uniformInverseProjection = other.m_uniformInverseProjection;
	m_uniformMV = other.m_uniformMV;
	m_uniformInverseMV = other.m_uniformInverseMV;
	m_uniformVP = other.m_uniformVP;
	m_uniformInverseVP = other.m_uniformInverseVP;
	m_uniformMVP = other.m_uniformMVP;
	m_uniformInverseMVP = other.m_uniformInverseMVP;
}

void UniformPipeline::set(const Pipeline& pipeline)
{
	m_uniformModel->set(pipeline.getModel());
	m_uniformInverseModel->set(pipeline.getInverseModel());
	m_uniformView->set(pipeline.getView());
	m_uniformInverseView->set(pipeline.getInverseView());
	m_uniformProjection->set(pipeline.getProjection());
	m_uniformInverseProjection->set(pipeline.getInverseProjection());
	m_uniformMV->set(pipeline.getMV());
	m_uniformInverseMV->set(pipeline.getInverseMV());
	m_uniformVP->set(pipeline.getVP());
	m_uniformInverseVP->set(pipeline.getInverseVP());
	m_uniformMVP->set(pipeline.getMVP());
	m_uniformInverseMVP->set(pipeline.getInverseMVP());
}
