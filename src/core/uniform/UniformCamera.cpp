/*
 * UniformCamera.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformCamera.h"
#include "core/Camera.h"

UniformCamera::UniformCamera() :
		m_uniformPosition(0),
		m_uniformForward(0),
		m_uniformRangeNear(0),
		m_uniformRangeFar(0)
{
}

UniformCamera::UniformCamera(IShader* shader, const std::string& uniformName) :
		m_uniformPosition(shader->getUniform(uniformName + ".position")),
		m_uniformForward(shader->getUniform(uniformName + ".forward")),
		m_uniformRangeNear(shader->getUniform(uniformName + ".range.near")),
		m_uniformRangeFar(shader->getUniform(uniformName + ".range.far"))
{
}

UniformCamera::~UniformCamera()
{
}

UniformCamera::UniformCamera(const UniformCamera& other)
{
	operator=(other);
}

void UniformCamera::operator=(const UniformCamera& other)
{
	m_uniformPosition = other.m_uniformPosition;
	m_uniformForward = other.m_uniformForward;
	m_uniformRangeNear = other.m_uniformRangeNear;
	m_uniformRangeFar = other.m_uniformRangeFar;
}

void UniformCamera::set(const Camera& camera)
{
	m_uniformPosition->set(camera.getCameraPosition());
	m_uniformForward->set(camera.getCameraForward());
	m_uniformRangeNear->set(camera.getNear());
	m_uniformRangeFar->set(camera.getFar());
}
