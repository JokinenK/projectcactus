/*
 * UniformBaseLight.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformBaseLight.h"
#include "core/light/BaseLight.h"

UniformBaseLight::UniformBaseLight() :
	m_uniformShadow(0),
	m_uniformColor(0),
	m_uniformIntensity(0)
{
}

UniformBaseLight::UniformBaseLight(
		IShader* shader,
		const std::string& uniformName) :
				m_uniformShadow(shader->getUniform(uniformName + ".shadow")),
				m_uniformColor(shader->getUniform(uniformName + ".color")),
				m_uniformIntensity(shader->getUniform(uniformName + ".intensity"))
{
}

UniformBaseLight::~UniformBaseLight()
{
}

UniformBaseLight::UniformBaseLight(const UniformBaseLight& other)
{
	operator=(other);
}

void UniformBaseLight::operator=(const UniformBaseLight& other)
{
	m_uniformShadow = other.m_uniformShadow;
	m_uniformColor = other.m_uniformColor;
	m_uniformIntensity = other.m_uniformIntensity;
}

void UniformBaseLight::set(const BaseLight& light)
{
	m_uniformShadow->set(light.isShadowEnabled());
	m_uniformColor->set(light.getColor().toVec4());
	m_uniformIntensity->set(light.getIntensity());
}
