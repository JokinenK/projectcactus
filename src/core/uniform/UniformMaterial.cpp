/*
 * UniformMaterial.cpp
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#include "UniformMaterial.h"
#include "core/Material.h"

UniformMaterial::UniformMaterial() :
		m_uniformHasAmbientTexture(0),
		m_uniformHasDiffuseTexture(0),
		m_uniformHasSpecularTexture(0),
		m_uniformHasShininessTexture(0),
		m_uniformHasDissolveTexture(0),
		m_uniformHasNormalTexture(0),
		m_uniformHasDisplacementTexture(0),
		m_uniformColorAmbient(0),
		m_uniformColorDiffuse(0),
		m_uniformColorSpecular(0),
		m_uniformShininess(0),
		m_uniformDissolve(0),
		m_uniformSamplerAmbient(0),
		m_uniformSamplerDiffuse(0),
		m_uniformSamplerSpecular(0),
		m_uniformSamplerShininess(0),
		m_uniformSamplerDissolve(0),
		m_uniformSamplerNormal(0),
		m_uniformSamplerDisplacement(0)
{
}

UniformMaterial::UniformMaterial(IShader* shader, const std::string& uniformName) :
		m_uniformHasAmbientTexture(shader->getUniform(uniformName + ".hasAmbientTexture")),
		m_uniformHasDiffuseTexture(shader->getUniform(uniformName + ".hasDiffuseTexture")),
		m_uniformHasSpecularTexture(shader->getUniform(uniformName + ".hasSpecularTexture")),
		m_uniformHasShininessTexture(shader->getUniform(uniformName + ".hasShininessTexture")),
		m_uniformHasDissolveTexture(shader->getUniform(uniformName + ".hasDissolveTexture")),
		m_uniformHasNormalTexture(shader->getUniform(uniformName + ".hasNormalTexture")),
		m_uniformHasDisplacementTexture(shader->getUniform(uniformName + ".hasDisplacementTexture")),
		m_uniformColorAmbient(shader->getUniform(uniformName + ".colorAmbient")),
		m_uniformColorDiffuse(shader->getUniform(uniformName + ".colorDiffuse")),
		m_uniformColorSpecular(shader->getUniform(uniformName + ".colorSpecular")),
		m_uniformShininess(shader->getUniform(uniformName + ".shininess")),
		m_uniformDissolve(shader->getUniform(uniformName + ".dissolve")),
		m_uniformSamplerAmbient(shader->getUniform(uniformName + ".samplerAmbient")),
		m_uniformSamplerDiffuse(shader->getUniform(uniformName + ".samplerDiffuse")),
		m_uniformSamplerSpecular(shader->getUniform(uniformName + ".samplerSpecular")),
		m_uniformSamplerShininess(shader->getUniform(uniformName + ".samplerShininess")),
		m_uniformSamplerDissolve(shader->getUniform(uniformName + ".samplerDissolve")),
		m_uniformSamplerNormal(shader->getUniform(uniformName + ".samplerNormal")),
		m_uniformSamplerDisplacement(shader->getUniform(uniformName + ".samplerDisplacement"))
{
	// To set constant values right after init, we need to bind shader
	shader->bind();

	m_uniformSamplerAmbient->set(Material::TEXTURE_AMBIENT);
	m_uniformSamplerDiffuse->set(Material::TEXTURE_DIFFUSE);
	m_uniformSamplerSpecular->set(Material::TEXTURE_SPECULAR);
	m_uniformSamplerShininess->set(Material::TEXTURE_SHININESS);
	m_uniformSamplerDissolve->set(Material::TEXTURE_DISSOLVE);
	m_uniformSamplerNormal->set(Material::TEXTURE_NORMAL);
	m_uniformSamplerDisplacement->set(Material::TEXTURE_DISPLACEMENT);

	shader->unbind();
}

UniformMaterial::UniformMaterial(const UniformMaterial& other)
{
	operator=(other);
}

UniformMaterial::~UniformMaterial()
{
}

void UniformMaterial::operator=(const UniformMaterial& other)
{
	m_uniformHasAmbientTexture = other.m_uniformHasAmbientTexture;
	m_uniformHasDiffuseTexture = other.m_uniformHasDiffuseTexture;
	m_uniformHasSpecularTexture = other.m_uniformHasSpecularTexture;
	m_uniformHasShininessTexture = other.m_uniformHasShininessTexture;
	m_uniformHasDissolveTexture = other.m_uniformHasDissolveTexture;
	m_uniformHasNormalTexture = other.m_uniformHasNormalTexture;
	m_uniformHasDisplacementTexture = other.m_uniformHasDisplacementTexture;
	m_uniformColorAmbient = other.m_uniformColorAmbient;
	m_uniformColorDiffuse = other.m_uniformColorDiffuse;
	m_uniformColorSpecular = other.m_uniformColorSpecular;
	m_uniformShininess = other.m_uniformShininess;
	m_uniformDissolve = other.m_uniformDissolve;
	m_uniformSamplerAmbient = other.m_uniformSamplerAmbient;
	m_uniformSamplerDiffuse = other.m_uniformSamplerDiffuse;
	m_uniformSamplerSpecular = other.m_uniformSamplerSpecular;
	m_uniformSamplerShininess = other.m_uniformSamplerShininess;
	m_uniformSamplerDissolve = other.m_uniformSamplerDissolve;
	m_uniformSamplerNormal = other.m_uniformSamplerNormal;
	m_uniformSamplerDisplacement = other.m_uniformSamplerDisplacement;
}

void UniformMaterial::set(const Material& material)
{
	m_uniformHasAmbientTexture->set(material.hasAmbientTexture());
	m_uniformHasDiffuseTexture->set(material.hasDiffuseTexture());
	m_uniformHasSpecularTexture->set(material.hasSpecularTexture());
	m_uniformHasShininessTexture->set(material.hasShininessTexture());
	m_uniformHasDissolveTexture->set(material.hasDissolveTexture());
	m_uniformHasNormalTexture->set(material.hasNormalTexture());
	m_uniformHasDisplacementTexture->set(material.hasDisplacementTexture());
	m_uniformColorAmbient->set(material.getAmbientColor().toVec4());
	m_uniformColorDiffuse->set(material.getDiffuseColor().toVec4());
	m_uniformColorSpecular->set(material.getSpecularColor().toVec4());
	m_uniformShininess->set(material.getShininess());
	m_uniformDissolve->set(material.getDissolve());
}
