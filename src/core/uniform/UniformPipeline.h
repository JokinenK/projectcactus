/*
 * UniformPipeline.h
 *
 *  Created on: 1.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPIPELINE_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPIPELINE_H_

#include "core/IShader.h"
#include "core/IUniform.h"

class Pipeline;

class UniformPipeline
{
public:
	UniformPipeline();
	UniformPipeline(IShader* shader, const std::string& uniformName);
	UniformPipeline(const UniformPipeline& other);

	virtual ~UniformPipeline();

	void operator=(const UniformPipeline& other);

	void set(const Pipeline& pipeline);

private:
	IUniform* m_uniformModel;
	IUniform* m_uniformInverseModel;
	IUniform* m_uniformView;
	IUniform* m_uniformInverseView;
	IUniform* m_uniformProjection;
	IUniform* m_uniformInverseProjection;
	IUniform* m_uniformMV;
	IUniform* m_uniformInverseMV;
	IUniform* m_uniformVP;
	IUniform* m_uniformInverseVP;
	IUniform* m_uniformMVP;
	IUniform* m_uniformInverseMVP;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPIPELINE_H_ */
