/*
 * UniformPointLight.h
 *
 *  Created on: 5.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPOINTLIGHT_H_
#define PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPOINTLIGHT_H_

#include "core/IShader.h"
#include "core/IUniform.h"
#include "core/uniform/UniformBaseLight.h"

class PointLight;

class UniformPointLight : public UniformBaseLight
{
public:
	UniformPointLight();
	UniformPointLight(IShader* shader, const std::string& uniformName);
	UniformPointLight(const UniformPointLight& other);

	virtual ~UniformPointLight();

	void operator=(const UniformPointLight& other);

	void set(const PointLight& light);

private:
	IUniform* m_uniformAttenuation;
	IUniform* m_uniformRadius;
	IUniform* m_uniformPosition;
};

#endif /* PROJECTCACTUS_SRC_CORE_UNIFORM_UNIFORMPOINTLIGHT_H_ */
