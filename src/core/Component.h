#ifndef COMPONENT_H
#define COMPONENT_H

class GameObject;
class MouseEvent;
class KeyboardEvent;
class RenderQueue;
class IRenderPrerequisite;

class Component
{
public:
    Component();
    virtual ~Component() {}

    void setParent(GameObject* parent);
	GameObject* getParent() const;

	void setActive(bool active);
	bool isActive() const;

    virtual void start() {}
    virtual void stop() {}
    virtual void update() {}
    virtual void fixedUpdate() {}
    virtual void lateUpdate() {}
    virtual void pause() {}
    virtual void unpause() {}
    virtual void addToRenderQueue(RenderQueue& /*queue*/) {};

    virtual bool mouseEvent(const MouseEvent* mouseEvent);
	virtual bool keyboardEvent(const KeyboardEvent* keyboardEvent);

private:
    GameObject* m_parent;
    bool m_active;
};

#endif // COMPONENT_H
