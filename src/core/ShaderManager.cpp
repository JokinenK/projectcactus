/*
 * ShaderManager.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include "ShaderManager.h"
#include "core/RendererFactory.h"
#include "support/MapIterator.h"

ShaderManager::ShaderManager()
{
}

ShaderManager::~ShaderManager()
{
	MapIterator<std::string, IShader*> iter(m_cachedShaders);

	while (iter.hasNext()) {
		iter.next();
		RendererFactory::instance()->releaseShader(iter.value());
	}
}

IShader* ShaderManager::getShader(const std::string& shaderName)
{
	IShader* shader = 0;

	if (!shaderName.empty()) {
		auto iterator = m_cachedShaders.find(shaderName);

		if (iterator == m_cachedShaders.end()) {
			std::cout << "Loading shader " << shaderName << "..." << std::endl;

			shader = RendererFactory::instance()->createShader();
			m_cachedShaders[shaderName] = shader;
		}
		else {
			shader = iterator->second;
		}
	}

	return shader;
}

