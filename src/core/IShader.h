#ifndef ISHADER_H_
#define ISHADER_H_

#include <string>

class IUniform;

class IShader
{
public:
    virtual ~IShader() {};

    virtual void bind() = 0;
    virtual void unbind() = 0;

    virtual bool addVertexShader(const std::string& shaderSource) = 0;
    virtual bool addFragmentShader(const std::string& shaderSource) = 0;
    virtual bool addGeometryShader(const std::string& shaderSource) = 0;

    virtual bool compileShader() = 0;

    virtual void setAttribLocation(const std::string& attribName, int location) = 0;
    virtual IUniform* getUniform(const std::string& uniformName) = 0;
};

#endif // ISHADER_H_
