/*
 * IPrimitiveRenderer.h
 *
 *  Created on: 2.3.2017
 *      Author: kalle
 */

#ifndef IPRIMITIVERENDERER_H_
#define IPRIMITIVERENDERER_H_

#include "support/Color.h"

class IPrimitiveRenderer {
public:
	IPrimitiveRenderer();
	virtual ~IPrimitiveRenderer();

	virtual void setColor(const Color& color) = 0;
	virtual const Color& getColor() const = 0;

	virtual void drawRectangle(
			float x1,
			float y1,
			float x2,
			float y2) const = 0;

	virtual void drawRectangle(
			float x1,
			const Color& colorX1,
			float y1,
			const Color& colorY1,
			float x2,
			const Color& colorX2,
			float y2,
			const Color& colorY2) const = 0;
};

#endif /* IPRIMITIVERENDERER_H_ */
