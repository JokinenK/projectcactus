/*
 * AlwaysTrueRenderPrerequisite.h
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_ALWAYSTRUERENDERPREREQUISITE_H_
#define PROJECTCACTUS_SRC_CORE_ALWAYSTRUERENDERPREREQUISITE_H_

#include "IRenderPrerequisite.h"

class BoundingBox;

class AlwaysTrueRenderPrerequisite : public IRenderPrerequisite
{
public:
	virtual bool shouldRender(const BoundingBox* boundingBox) const;
};

#endif /* PROJECTCACTUS_SRC_CORE_ALWAYSTRUERENDERPREREQUISITE_H_ */
