#ifndef MESH_H_
#define MESH_H_

#include "core/Component.h"
#include "core/model/VertexModel.h"
#include "core/model/IndexedVertexModel.h"

class IModel;
class IRenderer;
class Transform;
class Material;
class BoundingBox;

class Mesh : public Component
{
public:
	Mesh(const VertexModel& modelData, Material* material = 0);
	Mesh(const IndexedVertexModel& modelData, Material* material = 0);
	Mesh(IModel* model, Material* material = 0);
    virtual ~Mesh();

    virtual void start();
    virtual void stop();
    virtual void addToRenderQueue(RenderQueue& queue);

    IModel* getModel() const;
    Material* getMaterial() const;

private:
    void initFromIndexedVertexModel(const IndexedVertexModel& model);

    Mesh();
    Mesh(const Mesh& other);

    IModel* m_model;
    Material* m_material;
    Transform* m_transform;
    IRenderer* m_renderer;
    BoundingBox* m_boundingBox;

    bool m_shouldDealloc;
};

#endif // MESH_H_
