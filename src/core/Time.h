/*
 * Time.h
 *
 *  Created on: 30.9.2016
 *      Author: kalle
 */

#ifndef CACTUS_TIME_H_
#define CACTUS_TIME_H_

#include <vector>
#include "support/Singleton.h"

class Time : public Singleton<Time>
{
public:
    Time(int frameRate = 60, int maxFrameSkip = 5);
    virtual ~Time();

    void setTargetFrameRate(int frameRate);
    void startFrame();
    void endFrame();
    bool doFixedUpdate();

    float fps() const;
    float frameTime() const;
    float t() const;
    float interp() const;

private:
    void addSample(float fps);

    int m_sampleIndex;
    float m_samplesTotal;
    float m_numSamples;
    std::vector<float> m_samples;

    unsigned long long m_targetFrameTime_ms;
    unsigned long long m_prevTime_ms;
    unsigned long long m_nextGameTick_ms;

    int m_maxFrameSkip;
    int m_loops;

    float m_frameTime_ms;
    float m_t;
};

#endif // CACTUS_TIME_H_
