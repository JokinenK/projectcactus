/*
 * MessageBus.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef MESSAGEBUS_H_
#define MESSAGEBUS_H_

#include <map>
#include <list>
#include <iostream>
#include <algorithm>
#include <typeinfo>
#include "core/EventListener.h"
#include "support/Singleton.h"

class MessageBus : public Singleton<MessageBus>
{
public:
	template <class T>
	bool subscribe(EventListener<T>* listener)
	{
		const std::type_info& typeInfo = typeid(T);
		std::list<IEventListener*>& listeners = m_listeners[typeInfo.hash_code()];

		if (!contains<IEventListener*>(listeners, listener)) {
			listeners.push_back(listener);
			return true;
		}

		return false;
	}

	template <class T>
	bool unsubscribe(EventListener<T>* listener)
	{
		const std::type_info& typeInfo = typeid(T);
		std::list<IEventListener*>& listeners = m_listeners[typeInfo.hash_code()];

		if (contains<IEventListener*>(listeners, listener)) {
			listeners.remove(listener);
			return true;
		}

		return false;
	}

	template <class T>
	void publish(const T* event)
	{
		const std::type_info& typeInfo = typeid(T);
		std::list<IEventListener*>& listeners = m_listeners[typeInfo.hash_code()];

		for (IEventListener* base : listeners) {
			EventListener<T>* listener = dynamic_cast<EventListener<T>*>(base);
			listener->receiveEvent(event);
		}
	}

private:
	template <class T>
	bool contains(const std::list<T>& list, T elem) {
		return (std::find(list.begin(), list.end(), elem) != list.end());
	}

	std::map<size_t, std::list<IEventListener*> > m_listeners;
};

#endif /* MESSAGEBUS_H_ */
