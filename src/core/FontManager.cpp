/*
 * FontManager.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include <iostream>
#include "FontManager.h"
#include "hal/File.h"
#include "text/FontAtlas.h"

FontManager::FontManager()
{
}

FontManager::~FontManager()
{
}

FontAtlas* FontManager::defaultFontAtlas()
{
	return getFontAtlas(Hal::File::resolveFont(Hal::File::defaultFont()));
}

FontAtlas* FontManager::getFontAtlas(const std::string& fontName)
{
	FontAtlas* font;

	if (!fontName.empty()) {
		std::string fontPath = Hal::File::resolveFont(fontName);
		auto iterator = m_cachedFonts.find(fontPath);

		if (iterator == m_cachedFonts.end()) {
			std::cout << "Loading " << fontPath << "..." << std::endl;

			font = new FontAtlas(fontPath);
			m_cachedFonts[fontPath] = font;
		}
		else {
			font = iterator->second;
		}
	}

	return font;
}

