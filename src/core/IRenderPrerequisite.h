/*
 * IRenderPrerequisite.h
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_IRENDERPREREQUISITE_H_
#define PROJECTCACTUS_SRC_CORE_IRENDERPREREQUISITE_H_

class BoundingBox;

class IRenderPrerequisite
{
public:
	virtual ~IRenderPrerequisite() {};

	virtual bool shouldRender(const BoundingBox* boundingBox) const = 0;
};

#endif /* PROJECTCACTUS_SRC_CORE_IRENDERPREREQUISITE_H_ */
