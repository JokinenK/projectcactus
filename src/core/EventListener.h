/*
 * EventListener.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef EVENTLISTENER_H_
#define EVENTLISTENER_H_

#include "IEventListener.h"

template <class T>
class EventListener : public IEventListener
{
public:
	virtual ~EventListener() {};

	virtual bool receiveEvent(const T* event) = 0;
};

#endif /* EVENTLISTENER_H_ */
