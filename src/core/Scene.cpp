/*
 * Scene.cpp
 *
 *  Created on: 5.2.2017
 *      Author: kalle
 */

#include "Scene.h"
#include "core/MessageBus.h"
#include "core/light/BaseLight.h"
#include "core/light/SpotLight.h"
#include "core/light/PointLight.h"
#include "core/light/DirectionalLight.h"
#include "support/Utils.h"

namespace
{
	static const Scene::EventHandler s_eventHandlers[ComponentEvent::NUM_EVENTS] = {
			/* EVENT_START        */ &Scene::start,
			/* EVENT_STOP         */ &Scene::stop,
			/* EVENT_UPDATE       */ &Scene::update,
			/* EVENT_FIXED_UPDATE */ &Scene::fixedUpdate,
			/* EVENT_LATE_UPDATE  */ &Scene::lateUpdate,
			/* EVENT_PAUSE        */ &Scene::pause,
			/* EVENT_UNPAUSE      */ &Scene::unpause
	};
}

Scene::Scene() : GameObject("Scene")
{
	MessageBus::instance()->subscribe(this);
}

Scene::~Scene()
{
	MessageBus::instance()->unsubscribe(this);
}

bool Scene::receiveEvent(const ComponentEvent* event)
{
	EventHandler handler = s_eventHandlers[event->getType()];
	INVOKE_MEMBER_FN(this, handler)();

	return true;
}

void Scene::addChildren(GameObject* children)
{
	std::list<BaseLight*> lights = children->findComponents<BaseLight>();

	for (BaseLight* light : lights) {
		if (SpotLight* spotLight = dynamic_cast<SpotLight*>(light)) {
			addSpotLight(spotLight);
		}
		else if (PointLight* pointLight = dynamic_cast<PointLight*>(light)) {
			addPointLight(pointLight);
		}
		else if (DirectionalLight* directionalLight = dynamic_cast<DirectionalLight*>(light)) {
			addDirectionalLight(directionalLight);
		}
	}

	std::cout << "Number of lights total: " << numberOfLights() << std::endl;
	GameObject::addChildren(children);
}

void Scene::removeChildren(GameObject* children)
{
	std::list<BaseLight*> lights = children->findComponents<BaseLight>();

	for (BaseLight* light : lights) {
		if (SpotLight* spotLight = dynamic_cast<SpotLight*>(light)) {
			removeSpotLight(spotLight);
		}
		else if (PointLight* pointLight = dynamic_cast<PointLight*>(light)) {
			removePointLight(pointLight);
		}
		else if (DirectionalLight* directionalLight = dynamic_cast<DirectionalLight*>(light)) {
			removeDirectionalLight(directionalLight);
		}
	}

	std::cout << "Number of lights total: " << numberOfLights() << std::endl;
	GameObject::removeChildren(children);
}

size_t Scene::numberOfLights() const
{
	return m_spotLights.size() + m_pointLights.size() + m_directionalLights.size();
}

bool Scene::addSpotLight(SpotLight* spotLight)
{
	if (std::find(
			m_spotLights.begin(),
			m_spotLights.end(),
			spotLight) == m_spotLights.end()) {

		std::cout << "Added spot light" << std::endl;
		m_spotLights.push_back(spotLight);
		return true;
	}

	return false;
}

bool Scene::removeSpotLight(SpotLight* spotLight)
{
	if (std::find(
			m_spotLights.begin(),
			m_spotLights.end(),
			spotLight) != m_spotLights.end()) {

		std::cout << "Removed spot light" << std::endl;
		m_spotLights.remove(spotLight);
		return true;
	}

	return false;
}

bool Scene::addPointLight(PointLight* pointLight)
{
	if (std::find(
			m_pointLights.begin(),
			m_pointLights.end(),
			pointLight) == m_pointLights.end()) {

		std::cout << "Added point light" << std::endl;
		m_pointLights.push_back(pointLight);
		return true;
	}

	return false;
}

bool Scene::removePointLight(PointLight* pointLight)
{
	if (std::find(
			m_pointLights.begin(),
			m_pointLights.end(),
			pointLight) != m_pointLights.end()) {

		std::cout << "Removed point light" << std::endl;
		m_pointLights.remove(pointLight);
		return true;
	}

	return false;
}


bool Scene::addDirectionalLight(DirectionalLight* directionalLight)
{
	if (std::find(
			m_directionalLights.begin(),
			m_directionalLights.end(),
			directionalLight) == m_directionalLights.end()) {

		std::cout << "Added directional light" << std::endl;
		m_directionalLights.push_back(directionalLight);
		return true;
	}

	return false;
}

bool Scene::removeDirectionalLight(DirectionalLight* directionalLight)
{
	if (std::find(
			m_directionalLights.begin(),
			m_directionalLights.end(),
			directionalLight) != m_directionalLights.end()) {

		std::cout << "Removed directional light" << std::endl;
		m_directionalLights.remove(directionalLight);
		return true;
	}

	return false;
}

const std::list<SpotLight*>& Scene::getSpotLights() const
{
	return m_spotLights;
}

const std::list<PointLight*>& Scene::getPointLights() const
{
	return m_pointLights;
}

const std::list<DirectionalLight*>& Scene::getDirectionalLights() const
{
	return m_directionalLights;
}
