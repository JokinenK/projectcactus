/*
 * GraphicsFactory.h
 *
 *  Created on: 7.10.2016
 *      Author: kalle
 */

#ifndef RENDERERFACTORY_H_
#define RENDERERFACTORY_H_

#include <string>
#include "support/TypeFactory.h"
#include "support/Singleton.h"
#include "core/Plugin.h"

class IRenderer;
class IShader;
class ITexture;
class IModel;
class IFramebuffer;

class RendererFactory : public Singleton<RendererFactory> {
public:
	RendererFactory();
	virtual ~RendererFactory();

	bool setRenderer(IRenderer* renderer);
	IRenderer* getRenderer() const;

	IShader* createShader() const;
	void releaseShader(IShader* shader);

	ITexture* createTexture() const;
	void releaseTexture(ITexture* texture);

	IModel* createModel() const;
	void releaseModel(IModel* model);

	IFramebuffer* createFramebuffer() const;
	void releaseFramebuffer(IFramebuffer* framebuffer);

	IModel* createQuad(
			float left = -1.0f,
			float right = 1.0f,
			float top = 1.0f,
			float bottom = -1.0f,
			bool inverted = false);

private:
	IRenderer* m_renderer;
};

#endif /* RENDERERFACTORY_H_ */
