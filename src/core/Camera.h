#ifndef CAMERA_H
#define CAMERA_H

#include <glm/glm.hpp>
#include <vector>
#include "Component.h"
#include "core/GameObject.h"
#include "core/SplitFrustum.h"

class Transform;

class Camera : public Component
{
public:
	typedef enum {
		SHADOW_ORTHO,
		SHADOW_PERSPECTIVE
	} ShadowType;

    // Force vectors
    static const glm::vec3 Left;
    static const glm::vec3 Right;
    static const glm::vec3 Up;
    static const glm::vec3 Down;
    static const glm::vec3 Forward;
    static const glm::vec3 Backward;

    // Rotation vectors
    static const glm::vec3 RotateForward;
    static const glm::vec3 RotateBackward;
    static const glm::vec3 RollLeft;
    static const glm::vec3 RollRight;
    static const glm::vec3 RotateLeft;
	static const glm::vec3 RotateRight;

	Camera(float fovRadians, float aspectRatio, float near = 1.0f, float far = 1000.0f);
	virtual ~Camera();

	void start();
	void update();

	const glm::mat4& getView() const;
	const glm::mat4& getProjection() const;

	void lookAt(const glm::vec3& dest);

	void updateProjection();
	const glm::vec2 getRange() const;

	void setNear(float near);
	float getNear() const;

	void setFar(float far);
	float getFar() const;

	void setFov(float fov);
	float getFov() const;

	void setAspectRatio(float aspectRatio);
	float getAspectRatio() const;

	const ViewFrustum& getFrustum() const;
	const std::vector<ViewFrustum>& getFrustumSplits() const;
	const std::vector<float>& getFrustumEnds() const;

	glm::mat4 calculateShadowMatrix(const glm::mat4& lightViewProjection, ShadowType shadowType) const;
	Transform* getTransform();

	const glm::vec3& getCameraPosition() const;
	const glm::vec3& getCameraUp() const;
	const glm::vec3& getCameraForward() const;
	const glm::vec3& getCameraRight() const;

private:
	ViewFrustum m_frustum;
	SplitFrustum m_splitFrustum;

	Transform* m_transform;

	glm::mat4 m_view;
	glm::mat4 m_viewProjectionInverse;
};

#endif // CAMERA_H
