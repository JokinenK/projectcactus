#include "Component.h"

Component::Component() :
		m_parent(0),
		m_active(true)
{
}

void Component::setParent(GameObject* parent)
{
	m_parent = parent;
}

GameObject* Component::getParent() const
{
    return m_parent;
}

void Component::setActive(bool active)
{
    m_active = active;
}

bool Component::isActive() const
{
    return m_active;
}

bool Component::mouseEvent(const MouseEvent* mouseEvent)
{
	/* Unused */
	(void) mouseEvent;

	return false;
}

bool Component::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	/* Unused */
	(void) keyboardEvent;

	return false;
}
