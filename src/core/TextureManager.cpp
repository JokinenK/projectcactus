/*
 * TextureCache.cpp
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#include "TextureManager.h"
#include "hal/File.h"
#include "core/RendererFactory.h"
#include "core/ITexture.h"
#include "support/MapIterator.h"
#include "support/Utils.h"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
	MapIterator<std::string, ITexture*> iter(m_cachedTextures);

	while (iter.hasNext()) {
		iter.next();
		RendererFactory::instance()->releaseTexture(iter.value());
	}
}

ITexture* TextureManager::getTexture2D(const std::string& filePath, int flipMask)
{
	ITexture* texture = 0;

	if (!filePath.empty()) {
		std::string absolutePath = Hal::File::absolutePath(filePath);

		if (Hal::File::fileExists(absolutePath)) {
			auto iterator = m_cachedTextures.find(absolutePath);

			if (iterator == m_cachedTextures.end()) {
				std::cout << "Loading " << absolutePath << "..." << std::endl;

				Image textureImage(absolutePath, flipMask);
				texture = RendererFactory::instance()->createTexture();

				ITexture::DataFormat dataFormat = ITexture::determineImageFormat(textureImage.getChannels());

				texture->initTexture(
						textureImage.getWidth(),
						textureImage.getHeight());

				texture->setTextureData(
						textureImage.getData().data(),
						dataFormat,
						dataFormat);

				texture->setWrapS(ITexture::REPEAT);
				texture->setWrapT(ITexture::REPEAT);
				texture->setWrapR(ITexture::REPEAT);

				texture->setFilterMin(ITexture::LINEAR);
				texture->setFilterMag(ITexture::LINEAR);

				m_cachedTextures[absolutePath] = texture;
			}
			else {
				texture = iterator->second;
			}
		}
		else {
			std::cerr << "File: " << filePath << " does not exist" << std::endl;
		}
	}

	return texture;
}

