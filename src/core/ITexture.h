#ifndef ITEXTURE_H_
#define ITEXTURE_H_

class ITexture
{
public:
	typedef enum {
		TEXTURE_SOURCE,
		TEXTURE_2D,
		TEXTURE_CUBE_MAP,
		TEXTURE_CUBE_MAP_POSITIVE_X,
		TEXTURE_CUBE_MAP_NEGATIVE_X,
		TEXTURE_CUBE_MAP_POSITIVE_Y,
		TEXTURE_CUBE_MAP_NEGATIVE_Y,
		TEXTURE_CUBE_MAP_POSITIVE_Z,
		TEXTURE_CUBE_MAP_NEGATIVE_Z,
		NUM_TEXTURE_TYPES
	} TextureType;

	typedef enum {
		TEXTURE_WRAP_S,
		TEXTURE_WRAP_T,
		TEXTURE_WRAP_R,
		NUM_WRAP_TYPES
	} WrapType;

	typedef enum {
		REPEAT,
		CLAMP_TO_EDGE,
		MIRRORED_REPEAT,
		MIRROR_CLAMP_TO_EDGE,
		NUM_WRAP_VALUES
	} WrapValue;

	typedef enum {
		TEXTURE_MIN_FILTER,
		TEXTURE_MAG_FILTER,
		NUM_FILTER_TYPES
	} FilterType;

	typedef enum {
		NEAREST,
		LINEAR,
		NEAREST_MIPMAP_NEAREST,
		LINEAR_MIPMAP_NEAREST,
		NEAREST_MIPMAP_LINEAR,
		LINEAR_MIPMAP_LINEAR,
		NUM_FILTER_VALUES
	} FilterValue;

	typedef enum {
		RGB,
		RGB16F,
		RGB32F,
		RGBA,
		RGBA16F,
		RGBA32F,
		RED,
		RG,
		LUMINANCE_ALPHA,
		DEPTH_COMPONENT,
		DEPTH_COMPONENT16,
		DEPTH_COMPONENT24,
		DEPTH_COMPONENT32,
		DEPTH_COMPONENT32F,
		DEPTH_STENCIL,
		DEPTH24_STENCIL8,
		NUM_DATA_FORMATS
	} DataFormat;

	typedef enum {
		BYTE,
		UNSIGNED_BYTE,
		SHORT,
		UNSIGNED_SHORT,
		INT,
		UNSIGNED_INT,
		FLOAT,
		TWO_BYTES,
		THREE_BYTES,
		FOUR_BYTES,
		DOUBLE,
		UNSIGNED_INT_24_8,
		NUM_DATA_TYPES
	} StorageType;

    virtual ~ITexture() {}

    virtual bool initTexture(
			int textureWidth,
			int textureHeight,
			StorageType storageType = UNSIGNED_BYTE,
			TextureType textureType = TEXTURE_2D) = 0;

    virtual bool setTextureData(
			const unsigned char* textureData,
			DataFormat sourceFormat = RGBA,
			DataFormat targetFormat = RGBA,
			TextureType textureType = TEXTURE_SOURCE) = 0;

    virtual bool updateTextureData(
    		int x,
			int y,
    		int width,
    		int height,
			const unsigned char* data,
			DataFormat sourceFormat = RGBA,
			TextureType textureType = TEXTURE_SOURCE) = 0;

    virtual bool bind(int textureUnit) = 0;
    virtual bool unbind(int textureUnit) = 0;

    virtual int getWidth() const = 0;
    virtual int getHeight() const = 0;

    virtual bool setFilter(FilterType filterType, FilterValue filterValue) = 0;
    virtual bool setFilterMin(FilterValue filterValue) = 0;
    virtual bool setFilterMag(FilterValue filterValue) = 0;

    virtual bool setWrap(WrapType wrapType, WrapValue wrapValue) = 0;
    virtual bool setWrapS(WrapValue wrapValue) = 0;
    virtual bool setWrapT(WrapValue wrapValue) = 0;
    virtual bool setWrapR(WrapValue wrapValue) = 0;

    virtual int getTextureHandle() const = 0;
    virtual TextureType getTextureType() const = 0;

	static inline DataFormat determineImageFormat(int numChannels)
	{
		DataFormat dataFormat;

		switch (numChannels) {
			case 1: dataFormat = ITexture::RED;  break;
			case 2: dataFormat = ITexture::RG;   break;
			case 3: dataFormat = ITexture::RGB;  break;
			case 4: dataFormat = ITexture::RGBA; break;
		}

		return dataFormat;
	}
};

#endif // ITEXTURE_H_
