/*
 * IBoundingVolume.h
 *
 *  Created on: 12.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_ICOLLIDER_H_
#define PROJECTCACTUS_SRC_CORE_ICOLLIDER_H_

#include <glm/glm.hpp>

class AABB;
class Sphere;
class Plane;
class Frustum;

class ICollider
{
public:
	typedef enum {
		NEGATIVE = -1,
		ON_PLANE = 0,
		POSITIVE = +1,
	} HalfSpace;

	typedef enum {
		TYPE_AABB,
		TYPE_SPHERE,
		TYPE_PLANE,
		TYPE_FRUSTUM
	} ColliderType;

	virtual ~ICollider() {};

	virtual HalfSpace intersectsVec3(const glm::vec3& other) const = 0;
	virtual HalfSpace intersectsAABB(const AABB& other) const = 0;
	virtual HalfSpace intersectsSphere(const Sphere& other) const = 0;
	virtual HalfSpace intersectsPlane(const Plane& other) const = 0;
	virtual HalfSpace intersectsFrustum(const Frustum& other) const = 0;

	virtual ColliderType getColliderType() const = 0;
};

#endif /* PROJECTCACTUS_SRC_CORE_PHYSICS_ICOLLIDER_H_ */
