#include "core/GameObject.h"
#include "core/RenderQueue.h"

GameObject::GameObject(const std::string& tag) :
	m_tag(tag),
	m_parent(0),
	m_renderer(0),
	m_transform(new Transform())
{
}

GameObject::~GameObject()
{
	for (GameObject* child : m_children) {
		delete child;
	}

	for (Component* component : m_components) {
		delete component;
	}

	delete m_transform;
}

void GameObject::setTag(const std::string& tag)
{
	m_tag = tag;
}

const std::string& GameObject::getTag() const
{
	return m_tag;
}

void GameObject::setRenderer(IRenderer* renderer)
{
	m_renderer = renderer;
}

IRenderer* GameObject::getRenderer() const
{
	if (m_renderer) {
		return m_renderer;
	}

	if (m_parent) {
		return m_parent->getRenderer();
	}

	return 0;
}

void GameObject::setParent(GameObject* parent)
{
	m_parent = parent;
	m_transform->setParent(parent);
}

GameObject* GameObject::getParent() const
{
	return m_parent;
}

void GameObject::addComponent(Component* component)
{
    component->setParent(this);
	m_components.push_back(component);
}

void GameObject::removeComponent(Component* component)
{
	m_components.remove(component);
	delete component;
}

void GameObject::addChildren(GameObject* children)
{
    children->setParent(this);
	m_children.push_back(children);
}

void GameObject::removeChildren(GameObject* children)
{
	m_children.remove(children);
	delete children;
}

const std::list<GameObject*>& GameObject::getChildren() const
{
	return m_children;
}

bool GameObject::mouseEvent(const MouseEvent* mouseEvent)
{
	for (Component* component : m_components) {
		if (component->mouseEvent(mouseEvent)) {
			return true;
		}
	}

	for (GameObject* child : m_children) {
		if (child->mouseEvent(mouseEvent)) {
			return true;
		}
	}

	return false;
}

bool GameObject::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	for (GameObject* child : m_children) {
		if (child->keyboardEvent(keyboardEvent)) {
			return true;
		}
	}

	for (Component* component : m_components) {
		if (component->keyboardEvent(keyboardEvent)) {
			return true;
		}
	}

	return false;
}

GameObject* GameObject::getRoot()
{
	if (m_parent) {
		return m_parent->getRoot();
	}

    return this;
}

void GameObject::start()
{
	for (GameObject* child : m_children) {
		child->start();
	}

	for (Component* component : m_components) {
		component->start();
	}
}

void GameObject::stop()
{
	for (GameObject* child : m_children) {
		child->stop();
	}

	for (Component* component : m_components) {
		component->stop();
	}
}

void GameObject::update()
{
	m_transform->update();

	for (GameObject* child : m_children) {
		child->update();
	}

	for (Component* component : m_components) {
		component->update();
	}
}

void GameObject::fixedUpdate()
{
	for (GameObject* child : m_children) {
		child->fixedUpdate();
	}

	for (Component* component : m_components) {
		component->fixedUpdate();
	}
}

void GameObject::lateUpdate()
{
	for (GameObject* child : m_children) {
		child->lateUpdate();
	}

	for (Component* component : m_components) {
		component->lateUpdate();
	}
}

void GameObject::pause()
{
	for (GameObject* child : m_children) {
		child->pause();
	}

	for (Component* component : m_components) {
		component->pause();
	}
}

void GameObject::unpause()
{
	for (GameObject* child : m_children) {
		child->unpause();
	}

	for (Component* component : m_components) {
		component->unpause();
	}
}

void GameObject::addToRenderQueue(RenderQueue& queue)
{
	for (GameObject* child : m_children) {
		child->addToRenderQueue(queue);
	}

	for (Component* component : m_components) {
		component->addToRenderQueue(queue);
	}
}

void GameObject::setTransform(Transform* transform)
{
	if (m_transform) {
		delete m_transform;
	}

	m_transform = transform;
}

Transform* GameObject::getTransform() const
{
	return m_transform;
}

std::list<GameObject*> GameObject::findGameObjectsWithTag(const std::string& tag)
{
	if (m_parent) {
		return m_parent->findGameObjectsWithTag(tag);
	}

    std::list<GameObject*> results;
	findGameObjectsWithTag(tag, results);

	return results;
}

void GameObject::findGameObjectsWithTag(const std::string& tag, std::list<GameObject*>& results)
{
	if (tag.compare(m_tag) == 0) {
        results.push_back(this);
	}

	for (GameObject* child : m_children) {
		child->findGameObjectsWithTag(tag, results);
	}
}
