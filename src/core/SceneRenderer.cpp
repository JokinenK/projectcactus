/*
 * SceneRenderer.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include <iostream>
#include <glm/glm.hpp>
#include "core/GameObject.h"
#include "core/Mesh.h"
#include "core/Scene.h"
#include "core/SceneRenderer.h"
#include "core/Material.h"
#include "core/Camera.h"
#include "core/model/IndexedVertexModel.h"
#include "core/IRenderer.h"
#include "core/IFramebuffer.h"
#include "core/ITexture.h"
#include "core/light/BaseLight.h"
#include "core/light/DirectionalLight.h"
#include "core/light/PointLight.h"
#include "core/light/SpotLight.h"
#include "core/physics/AABB.h"
#include "core/physics/Sphere.h"
#include "core/RendererFactory.h"
#include "model/assimp/AssimpLoader.h"
#include "support/Utils.h"
#include "hal/File.h"

SceneRenderer::SceneRenderer(IRenderer* renderer, int screenWidth, int screenHeight) :
		m_renderer(renderer),
		m_scene(0),
		m_objectSphere(new GameObject("Sphere")),
		m_objectQuad(new GameObject("Quad")),
		m_objectCone(new GameObject("Cone")),
		m_gBufferTechnique(renderer, screenWidth, screenHeight),
		m_geometryRenderQueue(m_viewFrustumRenderPrerequisite),
		m_shadowRenderQueue(m_alwaysTrueRenderPrerequisite),
		m_directionalLightRenderQueue(m_alwaysTrueRenderPrerequisite),
		m_directionalLightShadow(renderer, 1024),
		m_directionalLightTechnique(
				renderer,
				screenWidth,
				screenHeight,
				m_gBufferTechnique,
				m_directionalLightShadow),
		m_pointLightRenderQueue(m_alwaysTrueRenderPrerequisite),
		m_pointLightShadow(renderer, 512),
		m_pointLightTechnique(
				renderer,
				screenWidth,
				screenHeight,
				m_gBufferTechnique,
				m_pointLightShadow),
		m_spotLightRenderQueue(m_alwaysTrueRenderPrerequisite),
		m_spotLightShadow(renderer, 512),
		m_spotLightTechnique(
				renderer,
				screenWidth,
				screenHeight,
				m_gBufferTechnique,
				m_spotLightShadow)
{
	std::string spherePath = Hal::File::executableDir() + Hal::File::pathSeparator() + "../res/models/sphere.obj";
	std::string conePath = Hal::File::executableDir() + Hal::File::pathSeparator() + "../res/models/lightcone.obj";

	m_objectSphere->addComponent(new Mesh(AssimpLoader::parse(spherePath).begin()->toIndexedVertexModel()));
	m_objectSphere->setRenderer(renderer);
	m_objectSphere->start();
	m_objectSphere->addToRenderQueue(m_pointLightRenderQueue);

	m_objectQuad->addComponent(new Mesh(RendererFactory::instance()->createQuad(-1, 1, 1, -1)));
	m_objectQuad->setRenderer(renderer);
	m_objectQuad->start();
	m_objectQuad->addToRenderQueue(m_directionalLightRenderQueue);

	m_objectCone->addComponent(new Mesh(AssimpLoader::parse(conePath).begin()->toIndexedVertexModel()));
	m_objectCone->setRenderer(renderer);
	m_objectCone->start();
	m_objectCone->addToRenderQueue(m_spotLightRenderQueue);

	glm::vec2 screenSize(screenWidth, screenHeight);

    m_gBufferTechnique.bind();
    m_gBufferTechnique.updateScreenSize(screenSize);
    m_gBufferTechnique.unbind();

    m_directionalLightTechnique.bind();
    m_directionalLightTechnique.updateScreenSize(screenSize);
    m_directionalLightTechnique.unbind();

    m_pointLightTechnique.bind();
    m_pointLightTechnique.updateScreenSize(screenSize);
    m_pointLightTechnique.unbind();

    m_spotLightTechnique.bind();
    m_spotLightTechnique.updateScreenSize(screenSize);
    m_spotLightTechnique.unbind();
}

SceneRenderer::~SceneRenderer()
{
	if (m_scene) {
		delete m_scene;
	}

	delete m_objectSphere;
	delete m_objectQuad;
	delete m_objectCone;
}

void SceneRenderer::setScene(Scene* scene)
{
	if (m_scene) {
		delete m_scene;
	}

	m_scene = scene;
	m_scene->setRenderer(m_renderer);
}

Scene* SceneRenderer::getScene() const
{
	return m_scene;
}

void SceneRenderer::render(Camera* camera)
{
	m_geometryRenderQueue.clear();
	m_shadowRenderQueue.clear();
	m_viewFrustumRenderPrerequisite.setFrustum(camera->getFrustum());

	m_scene->addToRenderQueue(m_geometryRenderQueue);
	m_scene->addToRenderQueue(m_shadowRenderQueue);

	prepareCamera(*camera);
	geometryPass(*camera);

    for (DirectionalLight* light : m_scene->getDirectionalLights()) {
    	directionalShadowPass(*camera, *light);
    	directionalLightPass(*camera, *light);
	}

    for (PointLight* light : m_scene->getPointLights()) {
    	pointShadowPass(*camera, *light);
    	pointLightPass(*camera, *light);
	}

    for (SpotLight* light : m_scene->getSpotLights()) {
    	spotShadowPass(*camera, *light);
    	spotLightPass(*camera, *light);
    }

    m_gBufferTechnique.copyDepthToActiveFramebuffer();
}

void SceneRenderer::prepareCamera(const Camera& camera)
{
	//	m_directionalLightShadow.bind();
	//	m_directionalLightShadow.updateCamera(*camera);

	m_gBufferTechnique.bind();
	m_gBufferTechnique.updateCamera(camera);
	m_gBufferTechnique.unbind();

	m_directionalLightTechnique.bind();
	m_directionalLightTechnique.updateCamera(camera);
	m_directionalLightTechnique.unbind();

	m_pointLightTechnique.bind();
	m_pointLightTechnique.updateCamera(camera);
	m_pointLightTechnique.unbind();

	m_spotLightTechnique.bind();
	m_spotLightTechnique.updateCamera(camera);
	m_spotLightTechnique.unbind();

	m_pointLightShadow.bind();
	m_pointLightShadow.updateCamera(camera);
	m_pointLightShadow.unbind();
}

void SceneRenderer::geometryPass(const Camera& camera)
{
	UNUSED(camera);

	m_gBufferTechnique.bind();
	m_gBufferTechnique.render(m_geometryRenderQueue);
	m_gBufferTechnique.unbind();
}

void SceneRenderer::directionalShadowPass(const Camera& camera, const DirectionalLight& light)
{
	m_directionalLightShadow.bind();
	m_directionalLightShadow.updateLightMatrices(camera, light);
	m_directionalLightShadow.render(m_shadowRenderQueue);
	m_directionalLightShadow.unbind();
}

void SceneRenderer::directionalLightPass(const Camera& camera, const DirectionalLight& light)
{
	UNUSED(camera);

	m_directionalLightTechnique.bind();
	m_directionalLightTechnique.updateDirectionalLight(light);
	m_directionalLightTechnique.render(m_directionalLightRenderQueue);
	m_directionalLightTechnique.unbind();
}

void SceneRenderer::pointShadowPass(const Camera& camera, const PointLight& light)
{
	UNUSED(camera);

	m_pointLightShadow.bind();
	m_pointLightShadow.updateLightMatrices(camera, light);
	m_pointLightShadow.updatePointLight(light);
	m_pointLightShadow.render(m_shadowRenderQueue);
	m_pointLightShadow.unbind();
}

void SceneRenderer::pointLightPass(const Camera& camera, const PointLight& light)
{
	UNUSED(camera);

	m_objectSphere->getTransform()->setPosition(light.getPosition());
	m_objectSphere->getTransform()->setScale(glm::vec3(light.getRadius()));
	m_objectSphere->getTransform()->update();

	m_pointLightTechnique.bind();
	m_pointLightTechnique.updatePointLight(light);
	m_pointLightTechnique.render(m_pointLightRenderQueue);
	m_pointLightTechnique.unbind();
}

void SceneRenderer::spotShadowPass(const Camera& camera, const SpotLight& light)
{
	UNUSED(camera);

	m_spotLightShadow.bind();
	m_spotLightShadow.updateLightMatrices(camera, light);
	m_spotLightShadow.render(m_shadowRenderQueue);
	m_spotLightShadow.unbind();
}

void SceneRenderer::spotLightPass(const Camera& camera, const SpotLight& light)
{
	UNUSED(camera);

	float radius = light.getRadius();
	float cutoff = light.getCutoff();
	float xyScale = radius * std::tan(cutoff);

	m_objectCone->getTransform()->setPosition(light.getTransform()->getTransformedPosition());
	m_objectCone->getTransform()->setRotation(light.getTransform()->getTransformedRotation());
	m_objectCone->getTransform()->setScale(glm::vec3(xyScale, xyScale, radius));
	m_objectCone->update();

	m_spotLightTechnique.bind();
	m_spotLightTechnique.updateSpotLight(light);
	m_spotLightTechnique.render(m_spotLightRenderQueue);
	m_spotLightTechnique.unbind();
}

bool SceneRenderer::mouseEvent(const MouseEvent* mouseEvent)
{
	return m_scene->mouseEvent(mouseEvent);
}

bool SceneRenderer::keyboardEvent(const KeyboardEvent* keyboardEvent)
{
	return m_scene->keyboardEvent(keyboardEvent);
}
