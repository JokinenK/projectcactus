/*
 * GameLoop.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include "core/GameLoop.h"
#include "core/Camera.h"
#include "core/GameObject.h"
#include "core/Scene.h"
#include "core/MessageBus.h"
#include "core/events/ComponentEvent.h"
#include "core/SceneRenderer.h"
#include "core/SkyBoxRenderer.h"
#include "core/gui/GuiRenderer.h"
#include "core/Time.h"
#include "core/IInput.h"
#include "core/IDisplay.h"
#include "core/RendererFactory.h"

namespace
{
	void sendEvent(MessageBus* messageBus, ComponentEvent* event, ComponentEvent::EventType eventType)
	{
		event->setType(eventType);
		messageBus->publish(event);
	}
}

GameLoop::GameLoop(
		IDisplay* display,
		IInput* input,
		SceneRenderer* sceneRenderer,
		SkyBoxRenderer* skyBoxRenderer,
		Gui::GuiRenderer* guiRenderer) :
				m_running(false),
				m_display(display),
				m_renderer(RendererFactory::instance()->getRenderer()),
				m_sceneRenderer(sceneRenderer),
				m_skyBoxRenderer(skyBoxRenderer),
				m_guiRenderer(guiRenderer),
				m_input(input),
				m_time(Time::instance()),
				m_primaryCamera(0),
				m_messageBus(MessageBus::instance())
{
	MessageBus::instance()->subscribe<MouseEvent>(this);
	MessageBus::instance()->subscribe<KeyboardEvent>(this);
}

GameLoop::~GameLoop()
{
	MessageBus::instance()->unsubscribe<MouseEvent>(this);
	MessageBus::instance()->unsubscribe<KeyboardEvent>(this);
}

bool GameLoop::isRunning() const
{
	return m_running;
}

void GameLoop::start()
{
	if (!m_running) {
		// Enable running
		m_running = true;

		initCameras();
		gameLoop();
	}
}

void GameLoop::stop()
{
	if (m_running) {
		m_running = false;
	}
}

void GameLoop::initCameras()
{
	std::list<Camera*> cameraList = m_sceneRenderer->getScene()->findComponents<Camera>();

	if (!cameraList.empty()) {
		if (cameraList.size() > 1) {
			std::cout << "Multiple cameras found, using first of the list" << std::endl;
		}

		m_primaryCamera = cameraList.front();
	}
	else {
		std::cerr << "Unable to find primary camera!" << std::endl;
	}
}

void GameLoop::gameLoop()
{
	ComponentEvent event;
	sendEvent(m_messageBus, &event, ComponentEvent::EVENT_START);

    while (m_running && !m_display->isClosed()) {
    	// Start frame calculation
		m_time->startFrame();

        // Request input update
        m_input->update();
        m_guiRenderer->update();

        // Send update to components
        sendEvent(m_messageBus, &event, ComponentEvent::EVENT_UPDATE);

        while (m_time->doFixedUpdate()) {
        	// Happends less frequent than actual update
        	sendEvent(m_messageBus, &event, ComponentEvent::EVENT_FIXED_UPDATE);
        }

		// Render new frame
		m_renderer->clear();
		m_renderer->setViewPort(m_display->getWidth(), m_display->getHeight());
		m_sceneRenderer->render(m_primaryCamera);
		m_skyBoxRenderer->render(m_primaryCamera);
		m_guiRenderer->render();

		// Send late update to components
		sendEvent(m_messageBus, &event, ComponentEvent::EVENT_LATE_UPDATE);

        // Swap buffers
        m_display->swapBuffers();

        // End frame so we can calculate fps
		m_time->endFrame();
	}

	// Send update to components
    sendEvent(m_messageBus, &event, ComponentEvent::EVENT_STOP);
}

Camera* GameLoop::getPrimaryCamera() const
{
	return m_primaryCamera;
}

bool GameLoop::receiveEvent(const MouseEvent* mouseEvent)
{
	if (m_guiRenderer->mouseEvent(mouseEvent)) {
		return true;
	}

	if (m_sceneRenderer->mouseEvent(mouseEvent)) {
		return true;
	}

	return false;
}

bool GameLoop::receiveEvent(const KeyboardEvent* keyboardEvent)
{
	if (m_guiRenderer->keyboardEvent(keyboardEvent)) {
		return true;
	}

	if (m_sceneRenderer->keyboardEvent(keyboardEvent)) {
		return true;
	}

	return false;
}
