/*
 * Scene.h
 *
 *  Created on: 5.2.2017
 *      Author: kalle
 */

#ifndef SCENE_H_
#define SCENE_H_

#include <list>
#include "core/GameObject.h"
#include "core/EventListener.h"
#include "core/events/ComponentEvent.h"

class SpotLight;
class PointLight;
class DirectionalLight;

class Scene : public GameObject,
              public EventListener<ComponentEvent>
{
public:
	typedef void (Scene::*EventHandler)(void);

	Scene();
	virtual ~Scene();

	bool receiveEvent(const ComponentEvent* event);

	virtual void addChildren(GameObject* children);
	virtual void removeChildren(GameObject* children);

	size_t numberOfLights() const;

	const std::list<SpotLight*>& getSpotLights() const;
	const std::list<PointLight*>& getPointLights() const;
	const std::list<DirectionalLight*>& getDirectionalLights() const;

private:
	bool addSpotLight(SpotLight* spotLight);
	bool removeSpotLight(SpotLight* spotLight);

	bool addPointLight(PointLight* pointLight);
	bool removePointLight(PointLight* pointLight);

	bool addDirectionalLight(DirectionalLight* directionalLight);
	bool removeDirectionalLight(DirectionalLight* directionalLight);

	std::list<SpotLight*> m_spotLights;
	std::list<PointLight*> m_pointLights;
	std::list<DirectionalLight*> m_directionalLights;
};

#endif /* SCENE_H_ */
