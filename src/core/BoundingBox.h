/*
 * BoundingBox.h
 *
 *  Created on: 14.5.2017
 *      Author: Kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_BOUNDINGBOX_H_
#define PROJECTCACTUS_SRC_CORE_BOUNDINGBOX_H_

#include "core/Component.h"
#include "core/physics/AABB.h"

class GameObject;
class Transform;

class BoundingBox : public Component
{
public:
	BoundingBox(const AABB& boundingBox);
	virtual ~BoundingBox();

	virtual void start();
	virtual void stop();
	virtual void update();

	const AABB& getTransformedBoundingBox() const;

private:
	GameObject* m_parent;
	Transform* m_transform;

	AABB m_boundingBox;
	AABB m_transformedBoundingBox;
};

#endif /* PROJECTCACTUS_SRC_CORE_BOUNDINGBOX_H_ */
