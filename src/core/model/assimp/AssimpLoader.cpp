/*
 * AssimpLoader.cpp
 *
 *  Created on: 22.3.2017
 *      Author: kalle
 */

#include "AssimpLoader.h"
#include <iostream>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include "core/MaterialManager.h"
#include "hal/File.h"

AssimpLoader::AssimpLoader(const std::string& fileName) :
		m_modelFile(Hal::File::absolutePath(fileName)),
		m_modelDirectory(Hal::File::directoryName(m_modelFile))
{
	Assimp::Importer importer;
	importer.SetPropertyBool(AI_CONFIG_PP_PTV_KEEP_HIERARCHY, true);

	m_scene = importer.ReadFile(fileName, aiProcess_Triangulate | aiProcess_PreTransformVertices);

	if (m_scene && m_scene->mRootNode) {
		parseNode(m_scene->mRootNode);
	}
	else {
		std::cerr << importer.GetErrorString() << std::endl;
	}
}

AssimpLoader::~AssimpLoader()
{
}

const std::list<VertexModel> AssimpLoader::parse(const std::string& name)
{
	return AssimpLoader(name).getModels();
}

const std::list<VertexModel>& AssimpLoader::getModels() const
{
	return m_models;
}

const VertexModel& AssimpLoader::findModel(const std::string& modelName) const
{
	for (const VertexModel& model : m_models) {
		if (modelName == model.getName()) {
			return model;
		}
	}

	return VertexModel::EMPTY;
}

void AssimpLoader::parseNode(const aiNode* node)
{
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		parseMesh(node->mMeshes[i]);
	}

    for (unsigned int i = 0; i < node->mNumChildren; i++) {
    	parseNode(node->mChildren[i]);
    }
}

void AssimpLoader::parseMesh(unsigned int meshIndex)
{
	VertexModel model;
	const aiMesh* mesh = m_scene->mMeshes[meshIndex];

	model.setName(mesh->mName.C_Str());
	const aiVector3D empty(0.0f, 0.0f, 0.0f);

    for (unsigned int i = 0; i < mesh->mNumFaces ; i++) {
    	const aiFace& face = mesh->mFaces[i];

    	bool hasNormals = mesh->HasNormals();
    	bool hasTexCoords = mesh->HasTextureCoords(0);

    	for (int j = 0; j < 3; j++) {
    		unsigned int verticeIndex = face.mIndices[j];

            const aiVector3D& pPosition = mesh->mVertices[verticeIndex];
            model.addPosition(glm::vec3(pPosition.x, pPosition.y, pPosition.z));

            if (hasNormals) {
            	const aiVector3D& pNormal = mesh->mNormals[verticeIndex];
            	model.addNormal(glm::vec3(pNormal.x, pNormal.y, pNormal.z));
            }

            if (hasTexCoords) {
				const aiVector3D& pTexCoord = mesh->mTextureCoords[0][verticeIndex];
				model.addTexCoord(glm::vec2(pTexCoord.x, pTexCoord.y));
            }
    	}
    }

    model.setMaterial(parseMaterial(mesh->mMaterialIndex));
    m_models.push_back(model);
}

Material* AssimpLoader::parseMaterial(unsigned int materialIndex)
{
	auto iter = m_materialCache.find(materialIndex);
	if (iter != m_materialCache.end()) {
		return iter->second;
	}

	Material material;
	const aiMaterial* assimpMaterial = m_scene->mMaterials[materialIndex];

	float floatBuffer;
	aiString stringBuffer;
	aiColor3D colorBuffer;

//	if (AI_SUCCESS == material->Get(AI_MATKEY_NAME, stringBuffer)) {
//		std::cout << stringBuffer.C_Str();
//	}

	if (AI_SUCCESS == assimpMaterial->Get(AI_MATKEY_SHININESS, floatBuffer)) {
		material.setShininess(floatBuffer);
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &stringBuffer)) {
		material.setDiffuseTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_SPECULAR, 0, &stringBuffer)) {
		material.setSpecularTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_AMBIENT, 0, &stringBuffer)) {
		material.setAmbientTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_NORMALS, 0, &stringBuffer)) {
		material.setNormalTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_SHININESS, 0, &stringBuffer)) {
		material.setShininessTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_OPACITY, 0, &stringBuffer)) {
		material.setDissolveTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	if (AI_SUCCESS == assimpMaterial->GetTexture(aiTextureType_DISPLACEMENT, 0, &stringBuffer)) {
		material.setDissolveTexture(m_modelDirectory + Hal::File::pathSeparator() + stringBuffer.C_Str());
	}

	//	material->GetTexture(aiTextureType_EMISSIVE, 0, &textureBuffer);
	//	destination.setEmissiveTexture(textureBuffer);

	//	material->GetTexture(aiTextureType_HEIGHT, 0, &stringBuffer);
	//	dest.setH

	if (AI_SUCCESS == assimpMaterial->Get(AI_MATKEY_COLOR_AMBIENT, colorBuffer)) {
		material.setAmbientColor(Color(colorBuffer.r, colorBuffer.g, colorBuffer.b));
	}

	if (AI_SUCCESS == assimpMaterial->Get(AI_MATKEY_COLOR_DIFFUSE, colorBuffer)) {
		material.setDiffuseColor(Color(colorBuffer.r, colorBuffer.g, colorBuffer.b));
	}

	if (AI_SUCCESS == assimpMaterial->Get(AI_MATKEY_COLOR_SPECULAR, colorBuffer)) {
		material.setSpecularColor(Color(colorBuffer.r, colorBuffer.g, colorBuffer.b));
	}

	//AI_MATKEY_COLOR_EMISSIVE

	material.finalize();
	Material* cachedMatrial = MaterialManager::instance()->cacheMaterial(material);

	m_materialCache[materialIndex] = cachedMatrial;
	return cachedMatrial;
}
