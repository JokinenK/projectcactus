/*
 * AssimpLoader.h
 *
 *  Created on: 22.3.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_MODEL_ASSIMP_ASSIMPLOADER_H_
#define PROJECTCACTUS_SRC_CORE_MODEL_ASSIMP_ASSIMPLOADER_H_

#include <map>
#include <list>
#include "core/Material.h"
#include "core/model/VertexModel.h"

class aiScene;
class aiNode;
class aiMesh;
class aiMaterial;

class AssimpLoader
{
public:
	AssimpLoader(const std::string& fileName);
	virtual ~AssimpLoader();

	const std::list<VertexModel>& getModels() const;
	const VertexModel& findModel(const std::string& modelName) const;

	static const std::list<VertexModel> parse(const std::string& name);

private:
	void parseNode(const aiNode* node);
	void parseMesh(unsigned int meshIndex);
	Material* parseMaterial(unsigned int materialIndex);

	const aiScene* m_scene;

	std::string m_modelFile;
	std::string m_modelDirectory;

	std::list<VertexModel> m_models;
	std::map<unsigned int, Material*> m_materialCache;
};

#endif /* PROJECTCACTUS_SRC_CORE_MODEL_ASSIMP_ASSIMPLOADER_H_ */
