#ifndef PACKEDVERTEXMODEL_H_
#define PACKEDVERTEXMODEL_H_

#include <glm/glm.hpp>
#include <cstring>

class PackedVertexModel
{
public:
    PackedVertexModel(
    		const glm::vec3& position,
			const glm::vec2& texCoords,
			const glm::vec3& normal) :
				m_position(position),
				m_texCoords(texCoords),
				m_normal(normal)
    {
    }

    bool operator<(const PackedVertexModel& that) const
    {
        return (memcmp(this, &that, sizeof(PackedVertexModel)) > 0);
    }

private:
    glm::vec3 m_position;
    glm::vec2 m_texCoords;
    glm::vec3 m_normal;
};

#endif /* PACKEDVERTEXMODEL_H_ */
