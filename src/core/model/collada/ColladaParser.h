#ifndef COLLADAPARSER_H_
#define COLLADAPARSER_H_

#include <iostream>
#include "xmlparser/XMLData.h"
#include "xmlparser/XMLList.h"
#include "xmlparser/XMLParsable.h"
#include "xmlparser/XMLParser.h"
#include "core/model/collada/ColladaParser_consts.h"
#include "ColladaLibraryGeometries.h"
#include "ColladaLibraryImages.h"
#include "ColladaLibraryEffects.h"

namespace ColladaDOM
{

struct Model : public XMLParsable
{
	Model() :
			XMLParsable(std::string()),
			libraryGeometries(new Geometry::LibraryGeometries()),
			libraryImages(new Images::LibraryImages()),
			libraryEffects(new Effects::LibraryEffects())
	{
		bindChildren(libraryGeometries);
		bindChildren(libraryImages);
		bindChildren(libraryEffects);
	}

	~Model() {
		delete libraryGeometries;
		delete libraryImages;
		delete libraryEffects;
	}

	static Model* parseDom(const std::string& fileName)
	{
		bool success = false;
		Model* model;
		XMLNode* rootNode = XMLParser::parse(fileName);

		if (rootNode) {
			model = new Model();
			success = model->parse(rootNode);
		}

		if (!success) {
			std::cout << "Error occurred while parsing file: " << fileName << std::endl;
		}

		return model;
	}

	Geometry::LibraryGeometries* libraryGeometries;
	Images::LibraryImages* libraryImages;
	Effects::LibraryEffects* libraryEffects;
};

} // namespace ColladaDOM

#endif // COLLADAPARSER_H_
