#ifndef COLLADAPARSER_CONSTS_H_
#define COLLADAPARSER_CONSTS_H_

namespace ColladaDOM {

static const char* XML_ELEM_ROOT = "COLLADA";
static const char* XML_ELEM_INPUT = "input";
static const char* XML_ELEM_ASSET = "asset";
static const char* XML_ELEM_GEOMETRY = "geometry";
static const char* XML_ELEM_MESH = "mesh";
static const char* XML_ELEM_SOURCE = "source";
static const char* XML_ELEM_SCENE = "scene";
static const char* XML_ELEM_TEHCNIQUECOMMON = "technique_common";
static const char* XML_ELEM_ACCESSOR = "accessor";
static const char* XML_ELEM_VERTICES = "vertices";
static const char* XML_ELEM_POLYLIST = "polylist";
static const char* XML_ELEM_TRIANGLES = "triangles";
static const char* XML_ELEM_P = "p";
static const char* XML_ELEM_VCOUNT = "vcount";
static const char* XML_ELEM_PARAM = "param";
static const char* XML_ELEM_IMAGE = "image";
static const char* XML_ELEM_INIT_FROM = "init_from";
static const char* XML_ELEM_FORMAT = "init_from";

static const char* XML_ELEM_FLOAT_ARRAY = "float_array";
static const char* XML_ELEM_INT_ARRAY = "int_array";
static const char* XML_ELEM_BOOL_ARRAY = "bool_array";
static const char* XML_ELEM_NAME_ARRAY = "Name_array";
static const char* XML_ELEM_IDREF_ARRAY = "IDREF_array";
static const char* XML_ELEM_SIDREF_ARRAY = "SIDREF_array";
static const char* XML_ELEM_TOKEN_ARRAY = "token_array";

static const char* XML_ELEM_LIBRARY_IMAGES = "library_images";
static const char* XML_ELEM_LIBRARY_GEOMETRIES = "library_geometries";
static const char* XML_ELEM_LIBRARY_CONTROLLERS = "library_controllers";
static const char* XML_ELEM_LIBRARY_VISUALSCENES = "library_visual_scenes";

static const char* XML_ELEM_LIBRARY_EFFECTS = "library_effects";
static const char* XML_ELEM_EFFECT = "effect";
static const char* XML_ELEM_PROFILE_COMMON = "profile_COMMON";
static const char* XML_ELEM_NEWPARAM = "newparam";

static const char* XML_ELEM_SEMANTIC = "semantic";
static const char* XML_ELEM_FLOAT1 = "float";
static const char* XML_ELEM_FLOAT2 = "float2";
static const char* XML_ELEM_FLOAT3 = "float3";
static const char* XML_ELEM_FLOAT4 = "float4";
static const char* XML_ELEM_SURFACE = "surface";
static const char* XML_ELEM_SAMPLER2D = "sampler2D";

static const char* XML_ATTR_VERSION = "version";
static const char* XML_ATTR_SEMANTIC = "semantic";
static const char* XML_ATTR_SOURCE = "source";
static const char* XML_ATTR_OFFSET = "offset";
static const char* XML_ATTR_ID = "id";
static const char* XML_ATTR_NAME = "name";
static const char* XML_ATTR_TYPE = "type";
static const char* XML_ATTR_COUNT = "count";
static const char* XML_ATTR_STRIDE = "stride";
static const char* XML_ATTR_SID = "sid";

static const char* COLLADA_SEMANTIC_POSITION = "POSITION";
static const char* COLLADA_SEMANTIC_NORMAL = "NORMAL";
static const char* COLLADA_SEMANTIC_TEXCOORD = "TEXCOORD";
static const char* COLLADA_SEMANTIC_INTERPOLATION = "INTERPOLATION";
static const char* COLLADA_SEMANTIC_LINEAR_STEPS = "LINEAR_STEPS";
static const char* COLLADA_SEMANTIC_INPUT = "INPUT";
static const char* COLLADA_SEMANTIC_OUTPUT = "OUTPUT";
static const char* COLLADA_SEMANTIC_IN_TANGENT = "IN_TANGENT";
static const char* COLLADA_SEMANTIC_OUT_TANGENT = "OUT_TANGENT";
static const char* COLLADA_SEMANTIC_CONTINUITY = "CONTINUITY";
static const char* COLLADA_SEMANTIC_BEZIER = "BEZIER";
static const char* COLLADA_SEMANTIC_LINEAR = "LINEAR";
static const char* COLLADA_SEMANTIC_BSPLINE = "BSPLINE";
static const char* COLLADA_SEMANTIC_HERMITE = "HERMITE";


} // namespace ColladaModel

#endif // COLLADAPARSER_CONSTS_H_
