/*
 * ColladaGeometry.h
 *
 *  Created on: 17.9.2016
 *      Author: kalle
 */

#ifndef COLLADALIBRARYGEOMETRIES_H_
#define COLLADALIBRARYGEOMETRIES_H_

#include "xmlparser/XMLParsable.h"
#include "ColladaParser_consts.h"

namespace ColladaDOM
{

namespace Geometry
{

struct Input : public XMLParsable
{
	Input() :
			XMLParsable(XML_ELEM_INPUT),
			offset(0)
	{
		bindData(XMLData<std::string>::requiredAttr(semantic, XML_ATTR_SEMANTIC));
		bindData(XMLData<std::string>::requiredAttr(source, XML_ATTR_SOURCE));
		bindData(XMLData<int>::optionalAttr(offset, XML_ATTR_OFFSET));
	}

	std::string semantic;
	std::string source;
	int offset;
};

struct Vertices : public XMLParsable
{
	Vertices() : XMLParsable(XML_ELEM_VERTICES)
	{
		bindList(XMLList<Input>::requiredList(inputs));
		bindData(XMLData<std::string>::requiredAttr(id, XML_ATTR_ID));
	}

	~Vertices()
	{
		for (Input* input : inputs) {
			delete input;
		}
	}

	std::string id;
	std::list<Input*> inputs;
};

struct VCount : public XMLParsable
{
	VCount() : XMLParsable(XML_ELEM_VCOUNT)
	{
		bindData(XMLData<std::string>::requiredValue(value));
	}

	std::string value;
};

struct P : public XMLParsable
{
	P() : XMLParsable(XML_ELEM_P)
	{
		bindData(XMLData<std::string>::requiredValue(raw));
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success;
	}

	std::vector<int> elements;

private:
	std::string raw;
};

struct Polylist : public XMLParsable
{
	Polylist() :
			XMLParsable(XML_ELEM_POLYLIST),
			count(0),
			vcount(new VCount()),
			p(new P())
	{
		bindChildren(vcount);
		bindChildren(p);
		bindData(XMLData<int>::requiredAttr(count, XML_ATTR_COUNT));
		bindList(XMLList<Input>::requiredList(inputs));
	}

	~Polylist()
	{
		for (Input* input : inputs) {
			delete input;
		}

		delete p;
		delete vcount;
	}

	int count;
	VCount* vcount;
	P* p;
	std::list<Input*> inputs;
};

struct Triangles : public XMLParsable
{
	Triangles() :
			XMLParsable(XML_ELEM_TRIANGLES),
			count(0),
			p(new P())
	{
		bindChildren(p);
		bindData(XMLData<int>::requiredAttr(count, XML_ATTR_COUNT));
		bindList(XMLList<Input>::requiredList(inputs));
	}

	~Triangles()
	{
		for (Input* input : inputs) {
			delete input;
		}

		delete p;
	}

	int count;
	P* p;
	std::list<Input*> inputs;
};

struct Param : public XMLParsable
{
	Param() : XMLParsable(XML_ELEM_PARAM)
	{
		bindData(XMLData<std::string>::requiredAttr(name, XML_ATTR_NAME));
		bindData(XMLData<std::string>::requiredAttr(type, XML_ATTR_TYPE));
	}

	std::string name;
	std::string type;
};

struct Accessor : public XMLParsable
{
	Accessor() :
			XMLParsable(XML_ELEM_ACCESSOR),
			count(0),
			stride(0)
	{
		bindData(XMLData<std::string>::requiredAttr(source, XML_ATTR_SOURCE));
		bindData(XMLData<int>::requiredAttr(count, XML_ATTR_COUNT));
		bindData(XMLData<int>::requiredAttr(stride, XML_ATTR_STRIDE));
		bindList(XMLList<Param>::requiredList(params));
	}

	~Accessor()
	{
		for (Param* param : params) {
			delete param;
		}
	}

	std::string source;
	int count;
	int stride;
	std::list<Param*> params;
};

struct TechniqueCommon : public XMLParsable
{
	TechniqueCommon() :
			XMLParsable(XML_ELEM_TEHCNIQUECOMMON),
			accessor(new Accessor())
	{
		bindChildren(accessor);
	}

	~TechniqueCommon() {
		delete accessor;
	}

	Accessor* accessor;
};

struct DataArray : public XMLParsable
{
	DataArray(const std::string& tag) :
			XMLParsable(tag),
			count(0)
	{
		bindData(XMLData<std::string>::requiredAttr(id, XML_ATTR_ID));
		bindData(XMLData<unsigned int>::requiredAttr(count, XML_ATTR_COUNT));
		bindData(XMLData<std::string>::requiredValue(raw));
	}

	std::string id;
	unsigned int count;
	std::string raw;
};

struct FloatArray : public DataArray
{
	FloatArray() : DataArray(XML_ELEM_FLOAT_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<float> elements;
};

struct IntArray : public DataArray
{
	IntArray() : DataArray(XML_ELEM_INT_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<int> elements;
};

struct BoolArray : public DataArray
{
	BoolArray() : DataArray(XML_ELEM_BOOL_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<bool> elements;
};

struct NameArray : public DataArray
{
	NameArray() : DataArray(XML_ELEM_NAME_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<std::string> elements;
};

struct IdRefArray : public DataArray
{
	IdRefArray() : DataArray(XML_ELEM_IDREF_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<std::string> elements;
};

struct SidRefArray : public DataArray
{
	SidRefArray() : DataArray(XML_ELEM_SIDREF_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<std::string> elements;
};

struct TokenArray : public DataArray
{
	TokenArray() : DataArray(XML_ELEM_TOKEN_ARRAY)
	{
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, elements);
		raw.clear();
		return success && (count == elements.size());
	}

	std::vector<std::string> elements;
};

struct Source : public XMLParsable
{
	typedef enum {
		INVALID,
		FLOAT,
		INTEGER,
		BOOLEAN,
		NAME,
		IDREF,
		SIDREF,
		TOKEN
	} DataType;

	Source() :
			XMLParsable(XML_ELEM_SOURCE),
			type(INVALID),
			floatArray(new FloatArray()),
			intArray(new IntArray()),
			boolArray(new BoolArray()),
			nameArray(new NameArray()),
			idRefArray(new IdRefArray()),
			sidRefArray(new SidRefArray()),
			tokenArray(new TokenArray()),
			techniqueCommon(new TechniqueCommon())
	{
		bindData(XMLData<std::string>::requiredAttr(id, XML_ATTR_ID));
		bindChildren(floatArray);
		bindChildren(intArray);
		bindChildren(boolArray);
		bindChildren(nameArray);
		bindChildren(idRefArray);
		bindChildren(sidRefArray);
		bindChildren(tokenArray);
		bindChildren(techniqueCommon);
	}

	~Source()
	{
		delete floatArray;
		delete intArray;
		delete boolArray;
		delete nameArray;
		delete idRefArray;
		delete sidRefArray;
		delete tokenArray;
		delete techniqueCommon;
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		if (floatArray->isParsed()) {
			type = FLOAT;
		}
		else if (intArray->isParsed()) {
			type = INTEGER;
		}
		else if (boolArray->isParsed()) {
			type = BOOLEAN;
		}
		else if (nameArray->isParsed()) {
			type = NAME;
		}
		else if (idRefArray->isParsed()) {
			type = IDREF;
		}
		else if (sidRefArray->isParsed()) {
			type = SIDREF;
		}
		else if (tokenArray->isParsed()) {
			type = TOKEN;
		}

		return (type != INVALID);
	}

	std::string id;
	DataType type;
	FloatArray* floatArray;
	IntArray* intArray;
	BoolArray* boolArray;
	NameArray* nameArray;
	IdRefArray* idRefArray;
	SidRefArray* sidRefArray;
	TokenArray* tokenArray;
	TechniqueCommon* techniqueCommon;
};

struct Mesh : public XMLParsable
{
	typedef enum {
		INVALID,
		POLYGONS,
		TRIANGLES
	} DataType;

	Mesh() :
			XMLParsable(XML_ELEM_MESH),
			type(INVALID),
			vertices(new Vertices()),
			polylist(new Polylist()),
			triangles(new Triangles())
	{
		bindList(XMLList<Source>::requiredList(sources));
		bindChildren(vertices);
		bindChildren(polylist);
		bindChildren(triangles);
	}

	~Mesh()
	{
		for (Source* source : sources) {
			delete source;
		}

		delete vertices;
		delete polylist;
		delete triangles;
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		if (polylist->isParsed()) {
			type = POLYGONS;
		}
		else if (triangles->isParsed()) {
			type = TRIANGLES;
		}

		return (type != INVALID);
	}

	DataType type;
	std::list<Source*> sources;
	Vertices* vertices;
	Polylist* polylist;
	Triangles* triangles;
};

struct Geometry : public XMLParsable
{
	Geometry() :
			XMLParsable(XML_ELEM_GEOMETRY),
			mesh(new Mesh())
	{
		bindData(XMLData<std::string>::requiredAttr(id, XML_ATTR_ID));
		bindData(XMLData<std::string>::requiredAttr(name, XML_ATTR_NAME));
		bindChildren(mesh);
	}

	~Geometry()
	{
		delete mesh;
	}

	std::string id;
	std::string name;
	Mesh* mesh;
};

struct LibraryGeometries : public XMLParsable
{
	LibraryGeometries() : XMLParsable(XML_ELEM_LIBRARY_GEOMETRIES)
	{
		bindList(XMLList<Geometry>::requiredList(geometries));
	}

	~LibraryGeometries()
	{
		for (Geometry* geometry : geometries) {
			delete geometry;
		}
	}

	std::list<Geometry*> geometries;
};

} // namespace Geometry

} // namespace ColladaModel

#endif /* COLLADALIBRARYGEOMETRIES_H_ */
