/*
 * ColladaModel.h
 *
 *  Created on: 14.9.2016
 *      Author: kalle
 */

#ifndef COLLADAMODEL_H_
#define COLLADAMODEL_H_

#include "core/model/IndexedVertexModel.h"
#include "core/model/collada/ColladaParser.h"
#include "support/MapIterator.h"

namespace Collada
{
	class BaseSource;
}

namespace
{
	static std::list<ColladaDOM::Geometry::Input*> generateInputs(
			const ColladaDOM::Geometry::Vertices* vertices,
			const std::list<ColladaDOM::Geometry::Input*>& specialized);

    static std::map<std::string, Collada::BaseSource*> generateSources(
    		const std::list<ColladaDOM::Geometry::Source*>& sources);

    static std::map<std::string, Collada::BaseSource*> generateSourceMap(
    		const ColladaDOM::Geometry::Vertices* vertices,
			const std::list<ColladaDOM::Geometry::Input*>& listInputs,
			const std::list<ColladaDOM::Geometry::Source*>& listSources);
}

namespace Collada
{

class BaseSource
{
public:
	BaseSource()
	{
	}

	virtual ~BaseSource()
	{
	}

	virtual void setIndiceOffset(int offset) = 0;

	virtual int getIndiceOffset() const = 0;

	virtual bool parseIndex(int index) = 0;

	virtual int getNumIndices() const = 0;
};

template <class T>
class Source : public BaseSource
{
public:
	Source(ColladaDOM::Geometry::Accessor* accessor, const std::vector<T>& rawData) :
			m_stride(accessor->stride),
			m_indiceOffset(0),
			m_rawData(rawData)
	{
	}

	void setIndiceOffset(int indiceOffset)
	{
		m_indiceOffset = indiceOffset;
	}

	int getIndiceOffset() const
	{
		return m_indiceOffset;
	}

	bool parseIndex(int index)
	{
		bool success = true;
		unsigned int offset = (index * m_stride);

		for (unsigned int i = 0; i < m_stride; i++) {
			if (offset >= 0 && offset <= m_rawData.size()) {
				m_parsedData.push_back(m_rawData.at(offset + i));
			}
			else {
				success = false;
			}
		}

		return success;
	}

	const std::vector<T>& getParsedData() const
	{
		return m_parsedData;
	}

	int getNumIndices() const
	{
		return m_rawData.size();
	}

private:
	unsigned int m_stride;
	unsigned int m_indiceOffset;
	const std::vector<T>& m_rawData;
	std::vector<T> m_parsedData;
};

class Mesh
{
public:
	Mesh(const ColladaDOM::Geometry::Mesh* mesh)
	{
		if (!parseMesh(mesh)) {
			std::cout << "Unable to parse mesh" << std::endl;
		}
	}

	bool hasPositions() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_POSITION) !=
				m_sources.end());
	}

	bool hasNormals() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_NORMAL) !=
				m_sources.end());
	}

	bool hasTexCoords() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_TEXCOORD) !=
				m_sources.end());
	}

	bool hasInterpolations() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_INTERPOLATION) !=
				m_sources.end());
	}

	bool hasLinearSteps() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_LINEAR_STEPS) !=
				m_sources.end());
	}

	bool hasInputs() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_INPUT) !=
				m_sources.end());
	}

	bool hasOutputs() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_OUTPUT) !=
				m_sources.end());
	}

	bool hasInTangents() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_IN_TANGENT) !=
				m_sources.end());
	}

	bool hasOutTangents() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_OUT_TANGENT) !=
				m_sources.end());
	}

	bool hasContinuities() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_CONTINUITY) !=
				m_sources.end());
	}

	bool hasBeziers() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_BEZIER) !=
				m_sources.end());
	}

	bool hasLinears() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_LINEAR) !=
				m_sources.end());
	}

	bool hasBSplines() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_BSPLINE) !=
				m_sources.end());
	}

	bool hasHermites() const
	{
		return (m_sources.find(ColladaDOM::COLLADA_SEMANTIC_HERMITE) !=
				m_sources.end());
	}

	const std::vector<float>& getPositions() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_POSITION));
		return source->getParsedData();
	}

	const std::vector<float>& getNormals() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_NORMAL));
		return source->getParsedData();
	}

	const std::vector<float>& getTexCoords() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_TEXCOORD));
		return source->getParsedData();
	}

	const std::vector<float>& getInterpolations() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_INTERPOLATION));
		return source->getParsedData();
	}

	const std::vector<float>& getLinearSteps() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_LINEAR_STEPS));
		return source->getParsedData();
	}

	const std::vector<float>& getInputs() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_INPUT));
		return source->getParsedData();
	}

	const std::vector<float>& getOutputs() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_OUTPUT));
		return source->getParsedData();
	}

	const std::vector<float>& getInTangents() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_IN_TANGENT));
		return source->getParsedData();
	}

	const std::vector<float>& getOutTangents() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_OUT_TANGENT));
		return source->getParsedData();
	}

	const std::vector<float>& getContinuities() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_CONTINUITY));
		return source->getParsedData();
	}

	const std::vector<float>& getBeziers() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_BEZIER));
		return source->getParsedData();
	}

	const std::vector<float>& getLinears() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_LINEAR));
		return source->getParsedData();
	}

	const std::vector<float>& getBSplines() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_BSPLINE));
		return source->getParsedData();
	}

	const std::vector<float>& getHermites() const
	{
		Source<float>* source = dynamic_cast<Source<float>* >(m_sources.at(ColladaDOM::COLLADA_SEMANTIC_HERMITE));
		return source->getParsedData();
	}

	IndexedVertexModel toIndexedVertexModel() const {
		VertexModel vertexModel;

		if (hasPositions()) {
			glm::vec3 vec;
			const std::vector<float>& meshPositions = getPositions();

			for (unsigned int i = 0; i < meshPositions.size(); i += 3) {
				vec.x = meshPositions.at(i + 0);
				vec.y = meshPositions.at(i + 1);
				vec.z = meshPositions.at(i + 2);
				vertexModel.addPosition(vec);
			}
		}

		if (hasTexCoords()) {
			glm::vec2 vec;
			const std::vector<float>& meshTexCoords = getTexCoords();

			for (unsigned int i = 0; i < meshTexCoords.size(); i += 2) {
				vec.x = meshTexCoords.at(i + 0);
				vec.y = meshTexCoords.at(i + 1);
				vertexModel.addTexCoord(vec);
			}
		}

		if (hasNormals()) {
			glm::vec3 vec;
			const std::vector<float>& meshNormals = getNormals();

			for (unsigned int i = 0; i < meshNormals.size(); i += 3) {
				vec.x = meshNormals.at(i + 0);
				vec.y = meshNormals.at(i + 1);
				vec.z = meshNormals.at(i + 2);
				vertexModel.addNormal(vec);
			}
		}

		if (hasInTangents()) {
			glm::vec4 vec;
			const std::vector<float>& meshTangents = getInTangents();

			for (unsigned int i = 0; i < meshTangents.size(); i += 4) {
				vec.x = meshTangents.at(i + 0);
				vec.y = meshTangents.at(i + 1);
				vec.z = meshTangents.at(i + 2);
				vec.w = meshTangents.at(i + 3);
				vertexModel.addTangent(vec);
			}
		}

		return IndexedVertexModel(vertexModel);
	}

private:
	bool parseMesh(const ColladaDOM::Geometry::Mesh* mesh)
	{
		bool success = false;

		switch (mesh->type) {
			case ColladaDOM::Geometry::Mesh::TRIANGLES: {
				m_sources = generateSourceMap(
						mesh->vertices,
						mesh->triangles->inputs,
						mesh->sources);

				success = parseMeshWithIndices(mesh->triangles->p->elements);
			} break;

			case ColladaDOM::Geometry::Mesh::POLYGONS: {
				m_sources = generateSourceMap(
						mesh->vertices,
						mesh->polylist->inputs,
						mesh->sources);

				success = parseMeshWithIndices(mesh->polylist->p->elements);
			} break;

			default: {
				success = false;
			}
		}

		return success;
	}

	bool parseMeshWithIndices(const std::vector<int>& indices)
	{
		bool success = true;

		int index = 0;
		int indice = 0;
		int offset = 0;
		int step = getStepSize();
		int primitives = indices.size() / step;

		BaseSource* source;
		MapIterator<std::string, BaseSource* > iter(m_sources);

		for (index = 0; index < primitives; index++) {
			iter.toBegin();

			while (iter.hasNext() && success) {
				iter.next();
				source = iter.value();

				offset = source->getIndiceOffset();
				indice = indices.at(index * step + offset);
				success &= source->parseIndex(indice);
			}
		}

		return success;
	}

	int getStepSize() const
	{
		int offset = 0;
		int stepSize = 0;

		MapIterator<std::string, BaseSource* > iter(m_sources);

		while (iter.hasNext()) {
			iter.next();
			offset = iter.value()->getIndiceOffset();

			if (offset > stepSize) {
				stepSize = offset;
			}
		}

		/* Always one more than largest offset */
		return (stepSize + 1);
	}

	std::map<std::string, BaseSource*> m_sources;
};

class Model
{
public:
    Model(const std::string& fileName)
    {
        ColladaDOM::Model* model = ColladaDOM::Model::parseDom(fileName);
        parseLibraryGeometries(model->libraryGeometries->geometries);
        delete model;
    }

    ~Model()
    {
    	MapIterator<std::string, Mesh*> iter(m_geometries);

    	while (iter.hasNext()) {
    		iter.next();
    		delete iter.value();
    	}
    }

    bool parseLibraryGeometries(
    		const std::list<ColladaDOM::Geometry::Geometry*>& libraryGeometries)
    {
    	bool success = true;

    	for (ColladaDOM::Geometry::Geometry* geometry : libraryGeometries) {
    		m_geometries[geometry->name] = new Mesh(geometry->mesh);
    	}

    	return success;
    }

    const Mesh* findModel(const std::string& name) const
	{
    	return m_geometries.find(name)->second;
	}

    const std::map<std::string, Mesh*>& getModels() const
	{
    	return m_geometries;
	}

private:
    std::map<std::string, Mesh*> m_geometries;
};

} // namespace Collada

namespace
{
	static std::list<ColladaDOM::Geometry::Input* > generateInputs(
			const ColladaDOM::Geometry::Vertices* vertices,
			const std::list<ColladaDOM::Geometry::Input* >& specialized)
    {
    	std::list<ColladaDOM::Geometry::Input* > results(vertices->inputs);

    	for (ColladaDOM::Geometry::Input* input : specialized) {
    		std::string sourceName = input->source.substr(1);

    		if (sourceName.compare(vertices->id) != 0) {
    			results.push_back(input);
    		}
		}

    	return results;
    }

    static std::map<std::string, Collada::BaseSource* > generateSources(
    		const std::list<ColladaDOM::Geometry::Source* >& sources)
    {
    	Collada::BaseSource* result;
    	std::map<std::string, Collada::BaseSource* > results;

        for (ColladaDOM::Geometry::Source* source : sources) {

        	switch (source->type) {
				case ColladaDOM::Geometry::Source::FLOAT: {
					result = new Collada::Source<float>(
									source->techniqueCommon->accessor,
									source->floatArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::INTEGER: {
					result = new Collada::Source<int>(
									source->techniqueCommon->accessor,
									source->intArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::BOOLEAN: {
					result = new Collada::Source<bool>(
									source->techniqueCommon->accessor,
									source->boolArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::NAME: {
					result = new Collada::Source<std::string>(
									source->techniqueCommon->accessor,
									source->nameArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::IDREF: {
					result = new Collada::Source<std::string>(
									source->techniqueCommon->accessor,
									source->idRefArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::SIDREF: {
					result = new Collada::Source<std::string>(
									source->techniqueCommon->accessor,
									source->sidRefArray->elements);
				} break;

				case ColladaDOM::Geometry::Source::TOKEN: {
					result = new Collada::Source<std::string>(
									source->techniqueCommon->accessor,
									source->tokenArray->elements);
				} break;

				default: {
					result = 0;
				}
        	}

        	if (result) {
				results.insert(std::make_pair(source->id, result));
				std::cout << source->id << " " << result->getNumIndices() << std::endl;
			}
        }

        return results;
    }

    static std::map<std::string, Collada::BaseSource* > generateSourceMap(
    		const ColladaDOM::Geometry::Vertices* vertices,
			const std::list<ColladaDOM::Geometry::Input* >& listInputs,
			const std::list<ColladaDOM::Geometry::Source* >& listSources)

	{
    	std::map<std::string, Collada::BaseSource*> results;
    	std::map<std::string, Collada::BaseSource*> sources = generateSources(listSources);
    	std::list<ColladaDOM::Geometry::Input*> inputs = generateInputs(vertices, listInputs);

    	Collada::BaseSource* source;

    	for (ColladaDOM::Geometry::Input* input : inputs) {
    		source = sources[input->source.substr(1)];
    		source->setIndiceOffset(input->offset);
    		results[input->semantic] = source;
		}

    	return results;
	}
}

#endif /* COLLADAMODEL_H_ */
