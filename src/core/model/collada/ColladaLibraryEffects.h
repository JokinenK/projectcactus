#ifndef COLLADALIBRARYEFFECTS_H_
#define COLLADALIBRARYEFFECTS_H_

#include "xmlparser/XMLParsable.h"
#include "ColladaLibraryImages.h"
#include "core/model/collada/ColladaParser_consts.h"

namespace ColladaDOM
{

namespace Effects
{

struct Semantic : public XMLParsable
{
	Semantic() : XMLParsable(XML_ELEM_SEMANTIC)
	{
		bindData(XMLData<std::string>::requiredValue(value));
	}

	std::string value;
};

struct Float1 : public XMLParsable
{
	Float1() : XMLParsable(XML_ELEM_FLOAT1)
	{
		bindData(XMLData<float>::requiredValue(value));
	}

	float value;
};

struct Float2 : public XMLParsable
{
	Float2() : XMLParsable(XML_ELEM_FLOAT2)
	{
		bindData(XMLData<std::string>::requiredValue(raw));
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, value);
		raw.clear();
		return success;
	}

	std::vector<float> value;

private:
	std::string raw;
};

struct Float3 : public XMLParsable
{
	Float3() : XMLParsable(XML_ELEM_FLOAT3)
	{
		bindData(XMLData<std::string>::requiredValue(raw));
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, value);
		raw.clear();
		return success;
	}

	std::vector<float> value;

private:
	std::string raw;
};

struct Float4 : public XMLParsable
{
	Float4() : XMLParsable(XML_ELEM_FLOAT4)
	{
		bindData(XMLData<std::string>::requiredValue(raw));
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		bool success = Utils::stringToList(raw, value);
		raw.clear();
		return success;
	}

	std::vector<float> value;

private:
	std::string raw;
};

struct Surface : public XMLParsable
{
	Surface() :
			XMLParsable(XML_ELEM_SURFACE),
			format(new Images::Format()),
			initFrom(new Images::InitFrom())
	{
		bindChildren(format);
		bindChildren(initFrom);
	}

	~Surface()
	{
		delete format;
		delete initFrom;
	}

	std::string id;
	Images::Format* format;
	Images::InitFrom* initFrom;
};

struct Sampler2D : public XMLParsable
{
	Sampler2D() : XMLParsable(XML_ELEM_SAMPLER2D)
	{

	}
};

struct NewParam : public XMLParsable
{
	typedef enum {
		INVALID,
		SEMANTIC,
		FLOAT1,
		FLOAT2,
		FLOAT3,
		FLOAT4,
		SURFACE,
		SAMPLER2D
	} Type;

	NewParam() :
			XMLParsable(XML_ELEM_NEWPARAM),
			type(INVALID),
			semantic(new Semantic()),
			float1(new Float1()),
			float2(new Float2()),
			float3(new Float3()),
			float4(new Float4()),
			surface(new Surface()),
			sampler2d(new Sampler2D())
	{
		bindData(XMLData<std::string>::requiredAttr(sid, XML_ATTR_SID));
		bindChildren(semantic);
		bindChildren(float1);
		bindChildren(float2);
		bindChildren(float3);
		bindChildren(float4);
		bindChildren(surface);
		bindChildren(sampler2d);
	}

	~NewParam()
	{
		delete semantic;
		delete float1;
		delete float2;
		delete float3;
		delete float4;
		delete surface;
		delete sampler2d;
	}

	bool postParse(XMLNode* xmlElement)
	{
		/* Unused */
		(void) xmlElement;

		if (semantic->isParsed()) {
			type = SEMANTIC;
		}
		else if (float1->isParsed()) {
			type = FLOAT1;
		}
		else if (float2->isParsed()) {
			type = FLOAT2;
		}
		else if (float3->isParsed()) {
			type = FLOAT3;
		}
		else if (float4->isParsed()) {
			type = FLOAT4;
		}
		else if (surface->isParsed()) {
			type = SURFACE;
		}
		else if (sampler2d->isParsed()) {
			type = SAMPLER2D;
		}

		return (type != INVALID);
	}

	Type type;
	std::string sid;
	Semantic* semantic;
	Float1* float1;
	Float2* float2;
	Float3* float3;
	Float4* float4;
	Surface* surface;
	Sampler2D* sampler2d;
};

struct ProfileCommon : public XMLParsable
{
	ProfileCommon() : XMLParsable(XML_ELEM_PROFILE_COMMON)
	{
		bindList(XMLList<NewParam>::optionalList(newParams));
	}

	~ProfileCommon()
	{
		for (NewParam* newParam : newParams)
		{
			delete newParam;
		}
	}

	std::list<NewParam*> newParams;
};

struct Effect : public XMLParsable
{
	Effect() :
			XMLParsable(XML_ELEM_EFFECT),
			profileCommon(new ProfileCommon())
	{
		bindData(XMLData<std::string>::requiredAttr(id, XML_ATTR_ID));
		bindData(XMLData<std::string>::optionalAttr(name, XML_ATTR_NAME));
		bindChildren(profileCommon);
	}

	~Effect()
	{
		delete profileCommon;
	}

	std::string id;
	std::string name;
	ProfileCommon* profileCommon;
};

struct LibraryEffects : public XMLParsable
{
	LibraryEffects() : XMLParsable(XML_ELEM_LIBRARY_EFFECTS)
	{
		bindList(XMLList<Effect>::requiredList(effects));
	}

	~LibraryEffects()
	{
		for (Effect* effect : effects) {
			delete effect;
		}
	}

	std::list<Effect*> effects;
};

} // namespace Effects

} // namespace ColladaDOM

#endif // COLLADALIBRARYEFFECTS_H_
