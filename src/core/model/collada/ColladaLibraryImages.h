#ifndef COLLADALIBRARYIMAGES_H_
#define COLLADALIBRARYIMAGES_H_

#include "xmlparser/XMLParsable.h"
#include "core/model/collada/ColladaParser_consts.h"

namespace ColladaDOM
{

namespace Images
{

struct Format : public XMLParsable
{
	Format() : XMLParsable(XML_ELEM_FORMAT)
	{
		bindData(XMLData<std::string>::requiredValue(value));
	}

	std::string value;
};

struct InitFrom : public XMLParsable
{
	InitFrom() : XMLParsable(XML_ELEM_INIT_FROM)
	{
		bindData(XMLData<std::string>::requiredValue(value));
	}

	std::string value;
};

struct Image : public XMLParsable
{
	Image() :
			XMLParsable(XML_ELEM_IMAGE),
			initFrom(new InitFrom())
	{
		bindChildren(initFrom);
	}

	~Image()
	{
		delete initFrom;
	}

	InitFrom* initFrom;
};

struct LibraryImages : public XMLParsable
{
	LibraryImages() : XMLParsable(XML_ELEM_LIBRARY_IMAGES)
	{
		bindList(XMLList<Image>::requiredList(images));
	}

	~LibraryImages()
	{
		for (Image* image : images) {
			delete image;
		}
	}

	std::list<Image*> images;
};

} // namespace Images

} // namespace ColladaDOM

#endif // COLLADALIBRARYIMAGES_H_
