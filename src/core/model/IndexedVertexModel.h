#ifndef INDEXEDVERTEXMODEL_H_
#define INDEXEDVERTEXMODEL_H_

#include <vector>
#include <glm/glm.hpp>
#include "core/physics/AABB.h"

class VertexModel;

class IndexedVertexModel
{
public:
	explicit IndexedVertexModel(const VertexModel& vertexModel);
	explicit IndexedVertexModel(const VertexModel* vertexModel);

	IndexedVertexModel(const IndexedVertexModel& other);
	~IndexedVertexModel();

	IndexedVertexModel& operator=(const IndexedVertexModel& other);

	void init(const VertexModel* vertexModel);

    const std::vector<unsigned short>& getIndices() const;
    const std::vector<glm::vec3>& getPositions() const;
    const std::vector<glm::vec2>& getTexCoords() const;
    const std::vector<glm::vec3>& getNormals() const;
    const std::vector<glm::vec4>& getTangents() const;

    const AABB& getBoundingBox() const;

private:
    void computeNormals();
	void computeTangents();

    std::vector<unsigned short> m_indices;
    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec2> m_texCoords;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec4> m_tangents;

    AABB m_boundingBox;
};

#endif // INDEXEDVERTEXMODEL_H_

