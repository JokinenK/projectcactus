#include <iostream>
#include <map>
#include "IndexedVertexModel.h"
#include "PackedVertexModel.h"
#include "VertexModel.h"

namespace
{
	static ssize_t findVertexIndex(
			const PackedVertexModel& vertex,
			std::map<PackedVertexModel, ssize_t>& lookupMap)
	{
		std::map<PackedVertexModel, ssize_t>::iterator iter = lookupMap.find(vertex);

		if (iter != lookupMap.end()) {
			return iter->second;
		}

		return -1;
	}
}

IndexedVertexModel::IndexedVertexModel(const VertexModel& vertexModel)
{
	init(&vertexModel);
}

IndexedVertexModel::IndexedVertexModel(const VertexModel* vertexModel)
{
	init(vertexModel);
}

IndexedVertexModel::IndexedVertexModel(const IndexedVertexModel& other) :
		m_indices(other.getIndices()),
		m_positions(other.getPositions()),
		m_texCoords(other.getTexCoords()),
		m_normals(other.getNormals()),
		m_tangents(other.getTangents()),
		m_boundingBox(other.getBoundingBox())
{
}

IndexedVertexModel::~IndexedVertexModel()
{
}

IndexedVertexModel& IndexedVertexModel::operator=(const IndexedVertexModel& other)
{
    m_indices = other.getIndices();
    m_positions = other.getPositions();
    m_texCoords = other.getTexCoords();
    m_normals = other.getNormals();
    m_tangents = other.getTangents();
    m_boundingBox = other.getBoundingBox();

    return *this;
}

void IndexedVertexModel::init(const VertexModel* vertexModel)
{
	m_boundingBox = vertexModel->getBoundingBox();

	const std::vector<glm::vec3>& positions = vertexModel->getPositions();
	const std::vector<glm::vec2>& texCoords = vertexModel->getTexCoords();
	const std::vector<glm::vec3>& normals = vertexModel->getNormals();

	m_indices.clear();
	m_positions.clear();
	m_texCoords.clear();
	m_normals.clear();
	m_tangents.clear();

	std::map<PackedVertexModel, ssize_t> lookupMap;

	const glm::vec2 emptyVec2;
	const glm::vec3 emptyVec3;

	for (unsigned int i = 0; i < positions.size(); i++) {
		const glm::vec3& position  = (i < positions.size()  ? positions[i]  : emptyVec3);
		const glm::vec2& texCoord  = (i < texCoords.size()  ? texCoords[i]  : emptyVec2);
		const glm::vec3& normal    = (i < normals.size()    ? normals[i]    : emptyVec3);

		PackedVertexModel packedModel(position, texCoord, normal);
		ssize_t index = findVertexIndex(packedModel, lookupMap);

		if (index < 0) {
			index = m_positions.size();
			lookupMap.insert(std::make_pair(packedModel, index));
			m_indices.push_back(index);

			m_positions.push_back(position);
			m_texCoords.push_back(texCoord);
		}
		else {
			m_indices.push_back(index);
		}
	}

	computeNormals();
	computeTangents();
}

void IndexedVertexModel::computeNormals()
{
	m_normals.resize(m_indices.size());

	for (unsigned int i = 0; i < m_indices.size(); i += 3) {
		int i1 = m_indices[i + 0];
		int i2 = m_indices[i + 1];
		int i3 = m_indices[i + 2];

		glm::vec3 v1 = m_positions[i2] - m_positions[i1];
		glm::vec3 v2 = m_positions[i3] - m_positions[i1];
		glm::vec3 normal = glm::normalize(glm::cross(v1, v2));

		m_normals[i1] += normal;
		m_normals[i2] += normal;
		m_normals[i3] += normal;
	}

	for (unsigned int i = 0; i < m_normals.size(); i++) {
		m_normals[i] = glm::normalize(m_normals[i]);
	}
}

void IndexedVertexModel::computeTangents()
{
	m_tangents.resize(m_indices.size());

	std::vector<glm::vec3> tan1;
	std::vector<glm::vec3> tan2;

	tan1.resize(m_indices.size());
	tan2.resize(m_indices.size());

	for (unsigned int i = 0; i < m_indices.size(); i += 3) {
		int i1 = m_indices[i + 0];
		int i2 = m_indices[i + 1];
		int i3 = m_indices[i + 2];

		const glm::vec3& v1 = m_positions[i1];
		const glm::vec3& v2 = m_positions[i2];
		const glm::vec3& v3 = m_positions[i3];

		const glm::vec2& w1 = m_texCoords[i1];
		const glm::vec2& w2 = m_texCoords[i2];
		const glm::vec2& w3 = m_texCoords[i3];

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;
		
		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;
		
		float r = 1.0f / (s1 * t2 - s2 * t1);
		
		glm::vec3 sdir(
				(t2 * x1 - t1 * x2) * r, 
				(t2 * y1 - t1 * y2) * r,
				(t2 * z1 - t1 * z2) * r);
		
		glm::vec3 tdir(
				(s1 * x2 - s2 * x1) * r, 
				(s1 * y2 - s2 * y1) * r,
				(s1 * z2 - s2 * z1) * r);
		
		tan1[i1] += sdir;
		tan1[i2] += sdir;
		tan1[i3] += sdir;
		
		tan2[i1] += tdir;
		tan2[i2] += tdir;
		tan2[i3] += tdir;
	}

	for (unsigned int i = 0; i < m_indices.size(); i++) {
		int index = m_indices[i];
		const glm::vec3& normal = m_normals[index];
		
		const glm::vec3& t1 = tan1[index];
		const glm::vec3& t2 = tan2[index];
		
		// Gram-Schmidt orthogonalize
		glm::vec3 tangent = glm::normalize(t1 - normal * glm::dot(normal, t1));

		// Calculate handedness
		float handedness = (glm::dot(glm::cross(normal, t1), t2) < 0.0f) ? -1.0f : 1.0f;

		m_tangents[index] = glm::vec4(tangent, handedness);
	}
}

const std::vector<unsigned short>& IndexedVertexModel::getIndices() const
{
	return m_indices;
}

const std::vector<glm::vec3>& IndexedVertexModel::getPositions() const
{
	return m_positions;
}

const std::vector<glm::vec2>& IndexedVertexModel::getTexCoords() const
{
	return m_texCoords;
}

const std::vector<glm::vec3>& IndexedVertexModel::getNormals() const
{
	return m_normals;
}

const std::vector<glm::vec4>& IndexedVertexModel::getTangents() const
{
	return m_tangents;
}

const AABB& IndexedVertexModel::getBoundingBox() const
{
	return m_boundingBox;
}
