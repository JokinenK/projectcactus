/*
 * MTLLoader.cpp
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#include <cstring>
#include <cstdio>
#include <cstdarg>
#include <iostream>
#include "MTLLoader.h"
#include "support/Utils.h"
#include "WavefrontUtils.h"
#include "hal/File.h"

MTLLoader::MTLLoader(const std::string& fileName, ssize_t bufLen) :
		m_modelFile(Hal::File::absolutePath(fileName)),
		m_materialPath(Hal::File::directoryName(m_modelFile))
{
	m_handle = fopen(m_modelFile.c_str(), "rb");
	m_bufLen = bufLen;
	m_buffer.resize(bufLen);

	char bufLenFormat[32];
	snprintf(bufLenFormat, sizeof(bufLenFormat), "%%%ds", static_cast<int>(bufLen - 1));

	if (m_handle != 0) {
		std::cout << "Parsing material: " << m_modelFile << std::endl;

		bool success = false;

		while (!feof(m_handle)) {
			memset(m_buffer.data(), 0, m_bufLen);
			fscanf(m_handle, bufLenFormat, m_buffer.data());

			success = false;
			char* prefix = trim(m_buffer.data());

			if (strlen(prefix) == 0) {
				continue;
			}

			else if (stringEquals(prefix, "newmtl")) {
				success = parseMaterialName();
			}
			else if (stringEquals(prefix, "Ka")) {
				success = parseAmbientColor();
			}
			else if (stringEquals(prefix, "Kd")) {
				success = parseDiffuseColor();
			}
			else if (stringEquals(prefix, "Ks")) {
				success = parseSpecularColor();
			}
			else if (stringEquals(prefix, "Ns")) {
				success = parseShininess();
			}
			else if (stringEquals(prefix, "d")) {
				success = parseDissolve(VALUE_NORMAL);
			}
			else if (stringEquals(prefix, "Tr")) {
				success = parseDissolve(VALUE_INVERTED);
			}
			else if (stringEquals(prefix, "illum")) {
				success = parseIlluminationType();
			}
			else if (stringEquals(prefix, "map_Ka")) {
				success = parseAmbientTexture();
			}
			else if (stringEquals(prefix, "map_Kd")) {
				success = parseDiffuseTexture();
			}
			else if (stringEquals(prefix, "map_Ks")) {
				success = parseSpecularTexture();
			}
			else if (stringEquals(prefix, "map_Ns")) {
				success = parseShininessTexture();
			}
			else if (stringEquals(prefix, "map_d")) {
				success = parseDissolveTexture();
			}
			else if (stringEquals(prefix, "map_bump") || stringEquals(prefix, "bump")) {
				success = parseNormalTexture();
			}
			else if (stringEquals(prefix, "map_Disp") || stringEquals(prefix, "disp")) {
				success = parseDisplacementTexture();
			}
			else if (stringBeginsWith(prefix, "#")) {
				handleCommentLine();
			}
			else {
				handleUnknownPrefix();
			}
		}

		(void) success;

		materialEnd();
		m_buffer.resize(0);
		fclose(m_handle);
	}
	else {
		std::cerr << "Unable to parse material: " << m_modelFile << std::endl;
	}
}

MTLLoader::~MTLLoader()
{
}

const std::map<std::string, Material> MTLLoader::parse(const std::string& name)
{
	return MTLLoader(name).getMaterials();
}

void MTLLoader::materialBegin()
{
	m_activeMaterial.clear();
}

void MTLLoader::materialEnd()
{
	const std::string& materialName = m_activeMaterial.getMaterialName();

	if (!materialName.empty()) {
		m_activeMaterial.finalize();
		m_materials[materialName] = m_activeMaterial;
	}
}

bool MTLLoader::parseMaterialName()
{
	materialEnd();
	materialBegin();

	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setMaterialName(trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseAmbientColor()
{
	float r;
	float g;
	float b;

	if (fscanf(m_handle, "%f %f %f", &r, &g, &b) == 3) {
		m_activeMaterial.setAmbientColor(Color(r, g, b));
		return true;
	}

	return false;
}

bool MTLLoader::parseDiffuseColor()
{
	float r;
	float g;
	float b;

	if (fscanf(m_handle, "%f %f %f", &r, &g, &b) == 3) {
		m_activeMaterial.setDiffuseColor(Color(r, g, b));
		return true;
	}

	return false;
}

bool MTLLoader::parseSpecularColor()
{
	float r;
	float g;
	float b;

	if (fscanf(m_handle, "%f %f %f", &r, &g, &b) == 3) {
		m_activeMaterial.setSpecularColor(Color(r, g, b));
		return true;
	}

	return false;
}

bool MTLLoader::parseShininess()
{
	float value;

	if (fscanf(m_handle, "%f", &value) == 1) {
		m_activeMaterial.setShininess(value);
		return true;
	}

	return false;
}

bool MTLLoader::parseDissolve(ValueType valueType)
{
	float value;

	if (fscanf(m_handle, "%f", &value) == 1) {
		if (valueType == VALUE_INVERTED) {
			value = 1 - value;
		}

		m_activeMaterial.setDissolve(value);
		return true;
	}

	return false;
}

bool MTLLoader::parseIlluminationType()
{
	int value;

	if (fscanf(m_handle, "%d", &value) == 1) {
		//m_activeMaterial.setIlluminationType(value);
		// Not supported by the material system
		return true;
	}

	return false;
}

bool MTLLoader::parseAmbientTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setAmbientTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseDiffuseTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setDiffuseTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseSpecularTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setSpecularTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseShininessTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setShininessTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseDissolveTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setDissolveTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseNormalTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setNormalTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool MTLLoader::parseDisplacementTexture()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeMaterial.setDisplacementTexture(m_materialPath + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

void MTLLoader::handleCommentLine()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
}

void MTLLoader::handleUnknownPrefix() {
	std::string prefix = std::string(trim(m_buffer.data()));

	fgets(m_buffer.data(), m_bufLen, m_handle);
	std::string line = std::string(trim(m_buffer.data()));

	std::cerr << "Unhandled line (" << prefix << " " << line << ")" << std::endl;
}

const std::map<std::string, Material>& MTLLoader::getMaterials() const
{
	return m_materials;
}

const Material& MTLLoader::findMaterial(const std::string& modelName) const
{
	auto iter = m_materials.find(modelName);

	if (iter != m_materials.end()) {
		return iter->second;
	}

	return Material::EMPTY;
}
