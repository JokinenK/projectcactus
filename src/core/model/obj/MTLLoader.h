/*
 * MTLLoader.h
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#ifndef MTLLOADER_H_
#define MTLLOADER_H_

#include <string>
#include <vector>
#include <map>
#include <list>
#include "core/Material.h"

class MTLLoader {
public:
	MTLLoader(const std::string& fileName, ssize_t bufLen = 0xFF);
	virtual ~MTLLoader();

	const std::map<std::string, Material>& getMaterials() const;
	const Material& findMaterial(const std::string& modelName) const;

	static const std::map<std::string, Material> parse(const std::string& name);

private:
	typedef enum {
		VALUE_NORMAL,
		VALUE_INVERTED
	} ValueType;

	void materialBegin();
	void materialEnd();

	bool parseMaterialName();
	bool parseAmbientTexture();
	bool parseDiffuseColor();
	bool parseSpecularColor();
	bool parseAmbientColor();

	bool parseShininess();
	bool parseDissolve(ValueType valueType);
	bool parseIlluminationType();
	bool parseDiffuseTexture();
	bool parseSpecularTexture();
	bool parseShininessTexture();
	bool parseDissolveTexture();
	bool parseNormalTexture();
	bool parseDisplacementTexture();

	void handleCommentLine();
	void handleUnknownPrefix();

	std::string m_modelFile;
	std::string m_materialPath;

	FILE* m_handle;
	std::vector<char> m_buffer;
	ssize_t m_bufLen;

	Material m_activeMaterial;
	std::map<std::string, Material> m_materials;
};

#endif /* MTLLOADER_H_ */
