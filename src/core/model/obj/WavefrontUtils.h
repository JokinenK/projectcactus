/*
 * WavefrontUtils.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef WAVEFRONTUTILS_H_
#define WAVEFRONTUTILS_H_

namespace
{
	static bool stringEquals(const char* lhs, const char* rhs) {
		return (strcmp(lhs, rhs) == 0);
	}

	static bool stringBeginsWith(const char* lhs, const char* rhs) {
		return (strncmp(lhs, rhs, strlen(rhs)) == 0);
	}

	static char* trimLeft(char* str) {
		while (isspace((unsigned char)*str)) {
			str++;
		}

		return str;
	}

	static char* trimRight(char* str) {
		char* end = str + strlen(str) - 1;

		while (end > str && isspace((unsigned char)*end)) {
			end--;
		}

		*(end + 1) = 0;
		return str;
	}

	static char* trim(char* str) {
		return trimRight(trimLeft(str));
	}
}

#endif /* WAVEFRONTUTILS_H_ */
