/*
 * OBJLoader.h
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#ifndef OBJLOADER_H_
#define OBJLOADER_H_

#include <string>
#include <vector>
#include <map>
#include <list>
#include "MTLLoader.h"
#include "core/model/VertexModel.h"

class OBJLoader {
public:
	OBJLoader(const std::string& fileName, ssize_t bufLen = 0xFF);
	virtual ~OBJLoader();

	const std::list<VertexModel>& getModels() const;
	const VertexModel& findModel(const std::string& modelName) const;

	static const std::list<VertexModel> parse(const std::string& name);

private:
	struct FaceIndices {
		int position;
		int texCoord;
		int normal;
	};

	void modelBegin();
	void modelEnd();

	bool parseModelName();
	bool parseModelMaterial();
	bool parseModelFace();

	bool parseSharedMaterialLibrary();
	bool parseSharedPosition();
	bool parseSharedTexCoord();
	bool parseSharedNormal();

	bool parseUnusedSmoothing();

	void handleCommentLine();
	void handleUnknownPrefix();

	bool parseFaceIndices(const char* faceBuffer, FaceIndices& faceIndices);
	void triangulateFaceIndices(std::list<FaceIndices>& listFaceIndices);
	void addFaceToModel(const FaceIndices& i0, const FaceIndices& i1, const FaceIndices& i2);

	FILE* m_handle;
	std::vector<char> m_buffer;
	ssize_t m_bufLen;

	std::string m_modelFile;
	std::string m_modelDirectory;

	std::vector<glm::vec3> m_sharedPositions;
	std::vector<glm::vec2> m_sharedTexCoords;
	std::vector<glm::vec3> m_sharedNormals;

	VertexModel m_activeModel;
	MTLLoader* m_materialLoader;
	std::list<VertexModel> m_models;
};

#endif /* OBJLOADER_H_ */
