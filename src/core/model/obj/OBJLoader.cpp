/*
 * OBJLoader.cpp
 *
 *  Created on: 16.2.2017
 *      Author: kalle
 */

#include <cstring>
#include <cstdio>
#include <cstdarg>
#include <iostream>
#include "OBJLoader.h"
#include "support/Utils.h"
#include "WavefrontUtils.h"
#include "hal/File.h"
#include "core/MaterialManager.h"

OBJLoader::OBJLoader(const std::string& fileName, ssize_t bufLen) :
		m_modelFile(Hal::File::absolutePath(fileName)),
		m_modelDirectory(Hal::File::directoryName(m_modelFile)),
		m_materialLoader(0)
{
	m_handle = fopen(m_modelFile.c_str(), "rb");
	m_bufLen = bufLen;
	m_buffer.resize(bufLen);

	char bufLenFormat[32];
	snprintf(bufLenFormat, sizeof(bufLenFormat), "%%%ds", static_cast<int>(bufLen - 1));

	if (m_handle != 0) {
		std::cout << "Parsing model: " << m_modelFile << std::endl;

		bool success = false;

		while (!feof(m_handle)) {
			memset(m_buffer.data(), 0, m_bufLen);
			fscanf(m_handle, bufLenFormat, m_buffer.data());

			success = false;
			char* prefix = trim(m_buffer.data());

			if (strlen(prefix) == 0) {
				continue;
			}

			else if (stringEquals(prefix, "v")) {
				success = parseSharedPosition();
			}
			else if (stringEquals(prefix, "vt")) {
				success = parseSharedTexCoord();
			}
			else if (stringEquals(prefix, "vn")) {
				success = parseSharedNormal();
			}
			else if (stringEquals(prefix, "mtllib")) {
				success = parseSharedMaterialLibrary();
			}
			else if (stringEquals(prefix, "o") || stringEquals(prefix, "g")) {
				success = parseModelName();
			}
			else if (stringEquals(prefix, "usemtl")) {
				success = parseModelMaterial();
			}
			else if (stringEquals(prefix, "f")) {
				success = parseModelFace();
			}
			else if (stringEquals(prefix, "s")) {
				success = parseUnusedSmoothing();
			}
			else if (stringBeginsWith(prefix, "#")) {
				handleCommentLine();
			}
			else {
				handleUnknownPrefix();
			}
		}

		(void) success;

		modelEnd();
		m_buffer.resize(0);
		m_sharedPositions.clear();
		m_sharedTexCoords.clear();
		m_sharedNormals.clear();

		fclose(m_handle);
	}
	else {
		std::cerr << "Unable to parse model: " << m_modelFile << std::endl;
	}
}

OBJLoader::~OBJLoader()
{
	delete m_materialLoader;
}

const std::list<VertexModel> OBJLoader::parse(const std::string& name)
{
	return OBJLoader(name).getModels();
}

void OBJLoader::modelBegin()
{
	m_activeModel.clear();
}

void OBJLoader::modelEnd()
{
	const std::string& modelName = m_activeModel.getName();

	if (!modelName.empty()) {
		m_models.push_back(m_activeModel);
	}
}

bool OBJLoader::parseModelName()
{
	modelEnd();
	modelBegin();

	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_activeModel.setName(trim(m_buffer.data()));

	return true;
}

bool OBJLoader::parseModelMaterial()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);

	const Material& material = m_materialLoader->findMaterial(trim(m_buffer.data()));
	Material* cachedMaterial = MaterialManager::instance()->cacheMaterial(material);

	m_activeModel.setMaterial(cachedMaterial);
	return true;
}

bool OBJLoader::parseModelFace()
{
	FaceIndices indice;
	std::list<FaceIndices> listIndices;

	memset(m_buffer.data(), 0, m_bufLen);
	fgets(m_buffer.data(), m_bufLen, m_handle);
	char* token = strtok(trim(m_buffer.data()), " ");

	while (token) {
		if (!parseFaceIndices(token, indice)) {
			std::cerr << "Unable to parse indice" << std::endl;
			return false;
		}

		listIndices.push_back(indice);
		token = strtok(NULL, " ");
	}

	if (listIndices.size() < 3) {
		std::cerr << "Not enough indices parsed to triangulate" << std::endl;
		return false;
	}

	triangulateFaceIndices(listIndices);
	return true;
}

bool OBJLoader::parseSharedMaterialLibrary()
{
	if (m_materialLoader) {
		delete m_materialLoader;
	}

	fgets(m_buffer.data(), m_bufLen, m_handle);
	m_materialLoader = new MTLLoader(m_modelDirectory + Hal::File::pathSeparator() + trim(m_buffer.data()));

	return true;
}

bool OBJLoader::parseSharedPosition()
{
	float x;
	float y;
	float z;

	if (fscanf(m_handle, "%f %f %f", &x, &y, &z) == 3) {
		m_sharedPositions.push_back(glm::vec3(x, y, z));
		return true;
	}

	return false;
}

bool OBJLoader::parseSharedTexCoord()
{
	float x;
	float y;
	float z;

	/* Some formats have the Z available too, we can ignore it */
	if (fscanf(m_handle, "%f %f %f", &x, &y, &z) >= 2) {
		m_sharedTexCoords.push_back(glm::vec2(x, y));
		return true;
	}

	return false;
}

bool OBJLoader::parseSharedNormal()
{
	float x;
	float y;
	float z;

	if (fscanf(m_handle, "%f %f %f", &x, &y, &z) == 3) {
		m_sharedNormals.push_back(glm::vec3(x, y, z));
		return true;
	}

	return false;
}

bool OBJLoader::parseUnusedSmoothing()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
	return true;
}

void OBJLoader::handleCommentLine()
{
	fgets(m_buffer.data(), m_bufLen, m_handle);
}

void OBJLoader::handleUnknownPrefix() {
	std::string prefix = std::string(trim(m_buffer.data()));

	fgets(m_buffer.data(), m_bufLen, m_handle);
	std::string line = std::string(trim(m_buffer.data()));

	std::cerr << "Unhandled line (" << prefix << " " << line << ")" << std::endl;
}

bool OBJLoader::parseFaceIndices(const char* buffer, FaceIndices& indices)
{
	bool success = false;

	if (sscanf(buffer, "%d/%d/%d", &indices.position, &indices.texCoord, &indices.normal) == 3) {
		success = true;
	}
	else if (sscanf(buffer, "%d//%d", &indices.position, &indices.normal) == 2) {
		indices.texCoord = -1;
		success = true;
	}
	else if (sscanf(buffer, "%d/%d", &indices.position, &indices.texCoord) == 2) {
		indices.normal = -1;
		success = true;
	}

	return success;
}

void OBJLoader::triangulateFaceIndices(std::list<FaceIndices>& listIndices)
{
	FaceIndices i0 = listIndices.front();
	listIndices.pop_front();

	FaceIndices i1;
	FaceIndices i2 = listIndices.front();
	listIndices.pop_front();

	while (listIndices.size() > 0) {
		i1 = i2;
		i2 = listIndices.front();
		listIndices.pop_front();

		addFaceToModel(i0, i1, i2);
	}
}

void OBJLoader::addFaceToModel(const FaceIndices& i0, const FaceIndices& i1, const FaceIndices& i2)
{
    m_activeModel.addPosition(m_sharedPositions[i0.position - 1]);
    m_activeModel.addPosition(m_sharedPositions[i1.position - 1]);
    m_activeModel.addPosition(m_sharedPositions[i2.position - 1]);

    if (i0.texCoord > 0 && i1.texCoord > 0 && i2.texCoord > 0) {
    	m_activeModel.addTexCoord(m_sharedTexCoords[i0.texCoord - 1]);
    	m_activeModel.addTexCoord(m_sharedTexCoords[i1.texCoord - 1]);
    	m_activeModel.addTexCoord(m_sharedTexCoords[i2.texCoord - 1]);
    }

    if (i0.normal > 0 && i1.normal > 0 && i2.normal > 0) {
    	m_activeModel.addNormal(m_sharedNormals[i0.normal - 1]);
    	m_activeModel.addNormal(m_sharedNormals[i1.normal - 1]);
    	m_activeModel.addNormal(m_sharedNormals[i2.normal - 1]);
    }
}

const std::list<VertexModel>& OBJLoader::getModels() const
{
	return m_models;
}

const VertexModel& OBJLoader::findModel(const std::string& modelName) const
{
	for (const VertexModel& model : m_models) {
		if (modelName == model.getName()) {
			return model;
		}
	}

	return VertexModel::EMPTY;
}
