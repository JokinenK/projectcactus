#ifndef VERTEXMODEL_H_
#define VERTEXMODEL_H_

#include <vector>
#include <glm/glm.hpp>
#include "core/model/IndexedVertexModel.h"
#include "core/Material.h"
#include "core/physics/AABB.h"

class VertexModel
{
public:
	static const VertexModel EMPTY;

	VertexModel(
				const std::vector<glm::vec3>& positions = std::vector<glm::vec3>(),
				const std::vector<glm::vec2>& texCoords = std::vector<glm::vec2>(),
				const std::vector<glm::vec3>& normals = std::vector<glm::vec3>(),
				const std::vector<glm::vec4>& tangents = std::vector<glm::vec4>());

	virtual ~VertexModel();

	VertexModel& operator=(const VertexModel& other);

	virtual void clear();

	VertexModel& computeMissingValues();
	bool computeNormals();
	bool computeTangents();

	void setName(const std::string& name);
	const std::string& getName() const;
	void clearName();

	void setMaterial(Material* material);
	Material* getMaterial() const;
	void clearMaterial();

	void addPosition(const glm::vec3& position);
	void setPositions(const std::vector<glm::vec3>& positions);
	bool hasPositions() const;
    const std::vector<glm::vec3>& getPositions() const;
    void clearPositions();

    void addTexCoord(const glm::vec2& texCoord);
    void setTexCoords(const std::vector<glm::vec2>& texCoords);
    bool hasTexCoords() const;
    const std::vector<glm::vec2>& getTexCoords() const;
    void clearTexCoords();

    void addNormal(const glm::vec3& normal);
    void setNormals(const std::vector<glm::vec3>& normals);
    bool hasNormals() const;
    const std::vector<glm::vec3>& getNormals() const;
    void clearNormals();

    void addTangent(const glm::vec4& tangent);
    void setTangents(const std::vector<glm::vec4>& tangents);
    bool hasTangents() const;
    const std::vector<glm::vec4>& getTangents() const;
    void clearTangents();

    const AABB& getBoundingBox() const;
    void clearBoundingBox();

    IndexedVertexModel toIndexedVertexModel() const;

private:
    std::string m_name;
    Material* m_material;

    std::vector<glm::vec3> m_positions;
    std::vector<glm::vec2> m_texCoords;
    std::vector<glm::vec3> m_normals;
    std::vector<glm::vec4> m_tangents;

    AABB m_boundingBox;
};

#endif // VERTEXMODEL_H_

