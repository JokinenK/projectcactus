#include <iostream>
#include <map>
#include <limits>
#include "VertexModel.h"

const VertexModel VertexModel::EMPTY;

VertexModel::VertexModel(
		const std::vector<glm::vec3>& positions,
		const std::vector<glm::vec2>& texCoords,
		const std::vector<glm::vec3>& normals,
		const std::vector<glm::vec4>& tangents) :
				m_material(0),
				m_positions(positions),
				m_texCoords(texCoords),
				m_normals(normals),
				m_tangents(tangents)
{
}

VertexModel::~VertexModel()
{
}

VertexModel& VertexModel::operator=(const VertexModel& other)
{
	m_name = other.getName();
	m_positions = other.getPositions();
	m_texCoords = other.getTexCoords();
	m_normals = other.getNormals();
	m_tangents = other.getTangents();

	return *this;
}

VertexModel& VertexModel::computeMissingValues()
{
	if (!hasNormals() && !computeNormals()) {
		std::cerr << "Unable to calculate normals" << std::endl;
	}

	if (!hasTangents() && !computeTangents()) {
		std::cerr << "Unable to calculate tangents" << std::endl;
	}

	return *this;
}

bool VertexModel::computeNormals()
{
	m_normals.resize(m_positions.size());

	for (unsigned int i = 0; i < m_positions.size(); i += 3) {
		glm::vec3 v1 = m_positions[i + 1] - m_positions[i + 0];
		glm::vec3 v2 = m_positions[i + 2] - m_positions[i + 0];
		glm::vec3 normal = glm::normalize(glm::cross(v1, v2));

		m_normals[i + 0] = normal;
		m_normals[i + 1] = normal;
		m_normals[i + 2] = normal;
	}

	return true;
}

bool VertexModel::computeTangents()
{
	if (m_texCoords.empty()) {
		return false;
	}

	std::vector<glm::vec3> tan1;
	std::vector<glm::vec3> tan2;

	tan1.resize(m_positions.size());
	tan2.resize(m_positions.size());

	for (unsigned int i = 0; i < m_positions.size(); i += 3) {
		const glm::vec3& v1 = m_positions[i + 0];
		const glm::vec3& v2 = m_positions[i + 1];
		const glm::vec3& v3 = m_positions[i + 2];

		const glm::vec2& w1 = m_texCoords[i + 0];
		const glm::vec2& w2 = m_texCoords[i + 1];
		const glm::vec2& w3 = m_texCoords[i + 2];

		float x1 = v2.x - v1.x;
		float x2 = v3.x - v1.x;
		float y1 = v2.y - v1.y;
		float y2 = v3.y - v1.y;
		float z1 = v2.z - v1.z;
		float z2 = v3.z - v1.z;
		
		float s1 = w2.x - w1.x;
		float s2 = w3.x - w1.x;
		float t1 = w2.y - w1.y;
		float t2 = w3.y - w1.y;
		
		float r = 1.0f / (s1 * t2 - s2 * t1);
		
		glm::vec3 sdir(
				(t2 * x1 - t1 * x2) * r, 
				(t2 * y1 - t1 * y2) * r,
				(t2 * z1 - t1 * z2) * r);
		
		glm::vec3 tdir(
				(s1 * x2 - s2 * x1) * r, 
				(s1 * y2 - s2 * y1) * r,
				(s1 * z2 - s2 * z1) * r);
		
		tan1[i + 0] = sdir;
		tan1[i + 1] = sdir;
		tan1[i + 2] = sdir;
		
		tan2[i + 0] = tdir;
		tan2[i + 1] = tdir;
		tan2[i + 2] = tdir;
	}

	m_tangents.resize(m_positions.size());

	for (unsigned int i = 0; i < m_positions.size(); i++) {
		const glm::vec3& normal = m_normals[i];
		const glm::vec3& t1 = tan1[i];
		const glm::vec3& t2 = tan2[i];
		
		// Gram-Schmidt orthogonalize
		glm::vec3 tangent = glm::normalize(t1 - normal * glm::dot(normal, t1));

		// Calculate handedness
		float handedness = (glm::dot(glm::cross(normal, t1), t2) < 0.0f) ? -1.0f : 1.0f;

		m_tangents[i] = glm::vec4(tangent, handedness);
	}

	return true;
}

void VertexModel::clear()
{
	clearName();
	clearPositions();
	clearTexCoords();
	clearNormals();
	clearTangents();
	clearBoundingBox();
}

void VertexModel::setName(const std::string& name)
{
	m_name = name;
}

const std::string& VertexModel::getName() const
{
	return m_name;
}

void VertexModel::clearName()
{
	m_name.clear();
}

void VertexModel::setMaterial(Material* material)
{
	m_material = material;
}

Material* VertexModel::getMaterial() const
{
	return m_material;
}

void VertexModel::clearMaterial()
{
	m_material = 0;
}

void VertexModel::addPosition(const glm::vec3& position)
{
	m_positions.push_back(position);
	m_boundingBox.expand(position);
}

void VertexModel::setPositions(const std::vector<glm::vec3>& positions)
{
	m_positions = positions;
}

bool VertexModel::hasPositions() const
{
	return !m_positions.empty();
}

const std::vector<glm::vec3>& VertexModel::getPositions() const
{
	return m_positions;
}

void VertexModel::clearPositions()
{
	m_positions.clear();
}

void VertexModel::addTexCoord(const glm::vec2& texCoord)
{
	m_texCoords.push_back(texCoord);
}

void VertexModel::setTexCoords(const std::vector<glm::vec2>& texCoords)
{
	m_texCoords = texCoords;
}

bool VertexModel::hasTexCoords() const
{
	return !m_texCoords.empty();
}

const std::vector<glm::vec2>& VertexModel::getTexCoords() const
{
	return m_texCoords;
}

void VertexModel::clearTexCoords()
{
	m_texCoords.clear();
}

void VertexModel::addNormal(const glm::vec3& normal)
{
	m_normals.push_back(normal);
}

void VertexModel::setNormals(const std::vector<glm::vec3>& normals)
{
	m_normals = normals;
}

bool VertexModel::hasNormals() const
{
	return !m_normals.empty();
}

const std::vector<glm::vec3>& VertexModel::getNormals() const
{
	return m_normals;
}

void VertexModel::clearNormals()
{
	m_normals.clear();
}

void VertexModel::addTangent(const glm::vec4& tangent)
{
	m_tangents.push_back(tangent);
}

void VertexModel::setTangents(const std::vector<glm::vec4>& tangents)
{
	m_tangents = tangents;
}

bool VertexModel::hasTangents() const
{
	return !m_tangents.empty();
}

const std::vector<glm::vec4>& VertexModel::getTangents() const
{
	return m_tangents;
}

void VertexModel::clearTangents()
{
	m_tangents.clear();
}

const AABB& VertexModel::getBoundingBox() const
{
	return m_boundingBox;
}

void VertexModel::clearBoundingBox()
{
	m_boundingBox.clear();
}

IndexedVertexModel VertexModel::toIndexedVertexModel() const
{
	return IndexedVertexModel(*this);
}
