#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#include <list>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

class GameObject;

class Transform
{
public:
    Transform(
    		const glm::vec3& position = glm::vec3(),
			const glm::quat& rotation = glm::quat(),
			const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f));

    void update();

    void rotate(const glm::vec3& axis, float angleRadians);
    void rotate(const glm::quat& rotation);

    void translate(const glm::vec3& dir, float amount);

    const glm::mat4& getTransformation() const;
    const glm::mat4& getTranslateMatrix() const;
	const glm::mat4& getRotateMatrix() const;
    const glm::mat4& getScaleMatrix() const;

    const glm::vec3& getTransformedPosition();
    const glm::quat& getTransformedRotation();

    const glm::vec3& getUp();
    const glm::vec3& getRight();
    const glm::vec3& getForward();

    void setParent(GameObject* parent);
    GameObject* getParent() const;

    void setPosition(const glm::vec3& position);
    const glm::vec3& getPosition() const;

    void setRotation(const glm::quat& rotation);
    const glm::quat& getRotation() const;

    void setScale(const glm::vec3& scale);
    const glm::vec3& getScale() const;

private:
    void addChild(Transform* transform);
    void removeChild(Transform* transform);

    void invalidate();

	const glm::mat4& getParentTransformation() const;
	const glm::quat& getParentRotation() const;

    GameObject* m_parent;
    Transform* m_parentTransform;

    glm::mat4 m_transformation;

    glm::mat4 m_translateMatrix;
	glm::mat4 m_rotateMatrix;
    glm::mat4 m_scaleMatrix;

    glm::vec3 m_transformedPosition;
    glm::quat m_transformedRotation;

    glm::mat4 m_parentTransformation;
    glm::quat m_parentRotation;

    // Variables
    glm::vec3 m_position;
    glm::quat m_rotation;
    glm::vec3 m_scale;

    glm::vec3 m_up;
    glm::vec3 m_right;
    glm::vec3 m_forward;

    std::list<Transform*> m_children;

    bool m_dirty;
};

#endif // TRANSFORM_H_
