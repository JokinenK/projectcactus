/*
 * MaterialManager.cpp
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#include "MaterialManager.h"

MaterialManager::MaterialManager()
{
}

MaterialManager::~MaterialManager()
{
}

Material* MaterialManager::cacheMaterial(const Material& material)
{
	Material::Id materialId = material.getMaterialId();

	if (m_materials.find(materialId) == m_materials.end()) {
		m_materials[materialId] = material;
	}

	return findMaterial(materialId);
}

Material* MaterialManager::findMaterial(Material::Id materialId)
{
	return &m_materials[materialId];
}
