/*
 * SceneRenderer.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef SCENERENDERER_H_
#define SCENERENDERER_H_

#include <list>
#include "core/Pipeline.h"
#include "core/RenderQueue.h"
#include "core/ViewFrustumRenderPrerequisite.h"
#include "core/AlwaysTrueRenderPrerequisite.h"
#include "core/events/MouseEvent.h"
#include "core/events/KeyboardEvent.h"
#include "core/techniques/GBufferTechnique.h"
#include "core/techniques/DirectionalLightTechnique.h"
#include "core/techniques/DirectionalLightShadowTechnique.h"
#include "core/techniques/PointLightTechnique.h"
#include "core/techniques/PointLightShadowTechnique.h"
#include "core/techniques/SpotLightTechnique.h"
#include "core/techniques/SpotLightShadowTechnique.h"

class IRenderer;
class IFramebuffer;
class IRenderTarget;
class IShader;
class ITexture;
class GameObject;
class Camera;
class Scene;
class BaseLight;
class DirectionalLight;
class PointLight;
class SpotLight;
class Transform;
class Material;
class IndexedVertexModel;

class SceneRenderer {
public:
	SceneRenderer(IRenderer* renderer, int screenWidth, int screenHeight);
	virtual ~SceneRenderer();

	void setScene(Scene* scene);
	Scene* getScene() const;

    void render(Camera* camera);

	bool mouseEvent(const MouseEvent* mouseEvent);
	bool keyboardEvent(const KeyboardEvent* keyboardEvent);

private:
	void prepareCamera(const Camera& camera);

	void geometryPass(const Camera& camera);

	void directionalShadowPass(const Camera& camera, const DirectionalLight& light);
	void directionalLightPass(const Camera& camera, const DirectionalLight& light);

	void pointShadowPass(const Camera& camera, const PointLight& light);
	void pointLightPass(const Camera& camera, const PointLight& light);

	void spotShadowPass(const Camera& camera, const SpotLight& light);
	void spotLightPass(const Camera& camera, const SpotLight& light);

	IRenderer* m_renderer;
	Scene* m_scene;

	GameObject* m_objectSphere;
	GameObject* m_objectQuad;
	GameObject* m_objectCone;

	ViewFrustumRenderPrerequisite m_viewFrustumRenderPrerequisite;
	AlwaysTrueRenderPrerequisite m_alwaysTrueRenderPrerequisite;

	GBufferTechnique m_gBufferTechnique;
	RenderQueue m_geometryRenderQueue;
	RenderQueue m_shadowRenderQueue;

	RenderQueue m_directionalLightRenderQueue;
	DirectionalLightShadowTechnique m_directionalLightShadow;
	DirectionalLightTechnique m_directionalLightTechnique;

	RenderQueue m_pointLightRenderQueue;
	PointLightShadowTechnique m_pointLightShadow;
	PointLightTechnique m_pointLightTechnique;

	RenderQueue m_spotLightRenderQueue;
	SpotLightShadowTechnique m_spotLightShadow;
	SpotLightTechnique m_spotLightTechnique;
};

#endif /* SCENERENDERER_H_ */
