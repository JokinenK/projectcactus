/*
 * SplitFrustum.cpp
 *
 *  Created on: 3.5.2017
 *      Author: kalle
 */

#include "SplitFrustum.h"

SplitFrustum::SplitFrustum(
		ViewFrustum& frustum,
		int numSplits,
		float lambda,
		float overlap) :
			m_frustum(frustum),
			m_numSplits(numSplits),
			m_lambda(lambda),
			m_overlap(overlap)
{
	splitFrustum();
}

SplitFrustum::~SplitFrustum()
{
}

void SplitFrustum::splitFrustum()
{
	m_frustumEnds.resize(m_numSplits);
	m_frustumSplits.resize(m_numSplits);

	float cameraNear = m_frustum.getNear();
	float cameraFar = m_frustum.getFar();
	float frustumRatio = cameraFar / cameraNear;
	float constant = cameraNear + (cameraFar - cameraNear);
	float overlapCoeff = 1.0f + m_overlap;

	m_frustumSplits[0].setNear(cameraNear);

	for(int i = 1 ; i < m_numSplits; ++i) {
		float split = i / static_cast<float>(m_numSplits);
		float frustumNear = glm::mix(constant * split, cameraNear * powf(frustumRatio, split), m_lambda);
		float frustumFar = frustumNear * overlapCoeff;

		m_frustumSplits[i].setNear(frustumNear);
		m_frustumSplits[i - 1].setFar(frustumFar);
	}

	m_frustumSplits[m_numSplits - 1].setFar(cameraFar);

	for(int i = 0 ; i < m_numSplits; ++i) {
		m_frustumSplits[i].setFov(m_frustum.getFov());
		m_frustumSplits[i].setAspectRatio(m_frustum.getAspectRatio());
		m_frustumEnds[i] = m_frustumSplits[i].getFar();
	}
}

void SplitFrustum::updateCorners(const glm::vec3& position, const glm::vec3& up, const glm::vec3& forward, const glm::vec3& right)
{
	if (m_frustum.isDirty()) {
		splitFrustum();
	}

	m_frustum.updateCorners(position, up, forward, right);

	for (ViewFrustum& frustum : m_frustumSplits) {
		frustum.updateCorners(position, up, forward, right);
	}
}

const std::vector<ViewFrustum>& SplitFrustum::getFrustumSplits() const
{
	return m_frustumSplits;
}

const std::vector<float>& SplitFrustum::getFrustumEnds() const
{
	return m_frustumEnds;
}
