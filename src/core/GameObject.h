#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <string>
#include <list>
#include "core/Component.h"
#include "core/Transform.h"
#include "core/events/MouseEvent.h"
#include "core/events/KeyboardEvent.h"

class IRenderer;
class IRenderPrerequisite;

class GameObject
{
public:
    GameObject(const std::string& tag = std::string());

    virtual ~GameObject();

    virtual GameObject* getRoot();

    virtual void setTag(const std::string& tag);
    virtual const std::string& getTag() const;

    virtual void setRenderer(IRenderer* renderer);
    virtual IRenderer* getRenderer() const;

    virtual void setTransform(Transform* transform);
    virtual Transform* getTransform() const;

    virtual void setParent(GameObject* parent);
    virtual GameObject* getParent() const;

    virtual void addComponent(Component* component);
    virtual void removeComponent(Component* component);

    virtual void addChildren(GameObject* children);
    virtual void removeChildren(GameObject* children);

    virtual bool mouseEvent(const MouseEvent* mouseEvent);
	virtual bool keyboardEvent(const KeyboardEvent* mouseEvent);

    const std::list<GameObject*>& getChildren() const;
    std::list<GameObject*> findGameObjectsWithTag(const std::string& tag);

    void start();
    void stop();
    void update();
    void fixedUpdate();
    void lateUpdate();
    void pause();
    void unpause();
    void addToRenderQueue(RenderQueue& queue);

    template <class T>
    std::list<T*> getComponents()
    {
    	std::list<T*> results;

        for (Component* component : m_components) {
        	if (T* object = dynamic_cast<T*>(component)) {
            	results.push_back(object);
            }
        }

        return results;
    }

    template <class T>
    std::list<T*> findComponents()
    {
    	std::list<T*> results;
    	findComponents<T>(results);
        return results;
    }

private:
    void findGameObjectsWithTag(const std::string& tag, std::list<GameObject*>& results);

    template <class T>
    void findComponents(std::list<T*>& results)
    {
        for (Component* component : m_components) {
        	if (T* object = dynamic_cast<T*>(component)) {
            	results.push_back(object);
            }
        }

        for (GameObject* children : m_children) {
        	children->findComponents(results);
        }
    }

    std::string m_tag;
    GameObject* m_parent;

    IRenderer* m_renderer;
    Transform* m_transform;
    std::list<GameObject*> m_children;
    std::list<Component*> m_components;
};

#endif // GAMEOBJECT_H

