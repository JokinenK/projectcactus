/*
 * Sphere.cpp
 *
 *  Created on: 25.4.2017
 *      Author: kalle
 */

#include <algorithm>
#include "Sphere.h"
#include "AABB.h"
#include "Plane.h"
#include "Frustum.h"

Sphere::Sphere() :
		m_center(glm::vec3(0)),
		m_radius(0.0f)
{
}

Sphere::Sphere(const glm::vec3& center, float radius) :
		m_center(center),
		m_radius(radius)
{
}

Sphere::Sphere(const Sphere& other) :
		m_center(other.m_center),
		m_radius(other.m_radius)
{
}

Sphere::Sphere(const AABB& other) :
		m_center(other.getCenter()),
		m_radius(0.0f)
{
	expand(other);
}

Sphere::~Sphere()
{
}

ICollider::HalfSpace Sphere::intersectsVec3(const glm::vec3& other) const
{
	float distance = sqrtf(
			(m_center.x - other.x) * (m_center.x - other.x) +
			(m_center.y - other.y) * (m_center.y - other.y) +
			(m_center.z - other.z) * (m_center.z - other.z));

	if (distance < m_radius) {
		return ON_PLANE;
	}

	return NEGATIVE;
}

ICollider::HalfSpace Sphere::intersectsSphere(const Sphere& other) const
{
	const glm::vec3& otherCenter = other.getCenter();

	float distance = sqrtf(
			(m_center.x - otherCenter.x) * (m_center.x - otherCenter.x) +
			(m_center.y - otherCenter.y) * (m_center.y - otherCenter.y) +
			(m_center.z - otherCenter.z) * (m_center.z - otherCenter.z));

	if (distance < (m_radius + other.getRadius())) {
		return ON_PLANE;
	}

	return NEGATIVE;
}

ICollider::HalfSpace Sphere::intersectsAABB(const AABB& other) const
{
	const glm::vec3& otherMin = other.getMin();
	const glm::vec3& otherMax = other.getMax();

	float x = std::max(otherMin.x, std::min(otherMax.x, m_center.x));
	float y = std::max(otherMin.y, std::min(otherMax.y, m_center.y));
	float z = std::max(otherMin.z, std::min(otherMax.z, m_center.z));

	return intersectsVec3(glm::vec3(x, y, z));
}

ICollider::HalfSpace Sphere::intersectsPlane(const Plane& other) const
{
	return other.intersectsSphere(*this);
}

ICollider::HalfSpace Sphere::intersectsFrustum(const Frustum& other) const
{
	return other.intersectsSphere(*this);
}

void Sphere::setCenter(const glm::vec3& center)
{
	m_center = center;
}

const glm::vec3& Sphere::getCenter() const
{
	return m_center;
}

void Sphere::setRadius(float radius)
{
	m_radius = radius;
}

float Sphere::getRadius() const
{
	return m_radius;
}

void Sphere::expand(float radius)
{
	m_radius = std::max(radius, m_radius);
}

void Sphere::expand(const glm::vec3& point)
{
    expand(glm::length(point - m_center));
}

void Sphere::expand(const AABB& other)
{
	expand(other.getMin());
	expand(other.getMax());
}

void Sphere::clear()
{
	m_center = glm::vec3(0);
	m_radius = 0.0f;
}
