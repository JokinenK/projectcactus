/*
 * Sphere.h
 *
 *  Created on: 25.4.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_PHYSICS_SPHERE_H_
#define PROJECTCACTUS_SRC_CORE_PHYSICS_SPHERE_H_

#include <glm/glm.hpp>
#include "core/ICollider.h"

class Sphere : public ICollider
{
public:
	Sphere();
	Sphere(const glm::vec3& center, float radius);
	Sphere(const Sphere& other);
	Sphere(const AABB& other);

	virtual ~Sphere();

	virtual HalfSpace intersectsVec3(const glm::vec3& other) const;
	virtual HalfSpace intersectsAABB(const AABB& other) const;
	virtual HalfSpace intersectsSphere(const Sphere& other) const;
	virtual HalfSpace intersectsPlane(const Plane& other) const;
	virtual HalfSpace intersectsFrustum(const Frustum& frustum) const;

	inline virtual ColliderType getColliderType() const
	{
		return TYPE_SPHERE;
	}

	void setCenter(const glm::vec3& center);
	const glm::vec3& getCenter() const;

	void setRadius(float radius);
	float getRadius() const;

	void expand(float radius);
	void expand(const glm::vec3& point);
	void expand(const AABB& other);

	void clear();
private:
	glm::vec3 m_center;
	float m_radius;
};

#endif /* PROJECTCACTUS_SRC_CORE_PHYSICS_SPHERE_H_ */
