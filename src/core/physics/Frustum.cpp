/*
 * Frustum.cpp
 *
 *  Created on: 3.5.2017
 *      Author: kalle
 */

#include <iostream>
#include "Frustum.h"

Frustum::Frustum()
{
	m_corners.resize(NUM_CORNERS);
	m_planes.resize(NUM_PLANES);
}

Frustum::Frustum(const std::vector<glm::vec3> corners)
{
	if (corners.size() == NUM_CORNERS) {
		m_corners = corners;
		updatePlanes();
	}
	else {
		std::cerr << "Invalid number of corners" << std::endl;
	}
}

Frustum::Frustum(const Frustum& other)
{
	operator=(other);
}

Frustum::~Frustum()
{
}

Frustum& Frustum::operator=(const Frustum& other)
{
	m_corners = other.m_corners;
	m_planes = other.m_planes;

	return *this;
}

void Frustum::setCorner(CornerName cornerName, const glm::vec3& corner)
{
	m_corners[cornerName] = corner;
}

const glm::vec3& Frustum::getCorner(CornerName cornerName) const
{
	return m_corners[cornerName];
}

const std::vector<glm::vec3>& Frustum::getCorners() const
{
	return m_corners;
}

void Frustum::setPlane(PlaneName planeName, const Plane& plane)
{
	m_planes[planeName] = plane;
}

const Plane& Frustum::getPlane(PlaneName planeName) const
{
	return m_planes[planeName];
}

const std::vector<Plane>& Frustum::getPlanes() const
{
	return m_planes;
}

void Frustum::updatePlanes()
{
	m_planes[PLANE_TOP].setPoints(
			m_corners[CORNER_NRT],
			m_corners[CORNER_NLT],
			m_corners[CORNER_FLT]);

	m_planes[PLANE_BOTTOM].setPoints(
			m_corners[CORNER_NLB],
			m_corners[CORNER_NRB],
			m_corners[CORNER_FRB]);

	m_planes[PLANE_LEFT].setPoints(
			m_corners[CORNER_NLT],
			m_corners[CORNER_NLB],
			m_corners[CORNER_FLB]);

	m_planes[PLANE_RIGHT].setPoints(
			m_corners[CORNER_NRB],
			m_corners[CORNER_NRT],
			m_corners[CORNER_FRB]);

	m_planes[PLANE_NEAR].setPoints(
			m_corners[CORNER_NLT],
			m_corners[CORNER_NRT],
			m_corners[CORNER_NRB]);

	m_planes[PLANE_FAR].setPoints(
			m_corners[CORNER_FRT],
			m_corners[CORNER_FLT],
			m_corners[CORNER_FLB]);
}

ICollider::HalfSpace Frustum::intersectsVec3(const glm::vec3& point) const
{
	HalfSpace result = POSITIVE;

	for (const Plane& plane : m_planes) {
		HalfSpace intersection = plane.intersectsVec3(point);

		if (intersection == NEGATIVE) {
			// If plane is considered outside, then don't loop anymore
			result = intersection;
			break;
		}
		else if (intersection == ON_PLANE) {
			result = intersection;
		}
	}

	return result;
}

ICollider::HalfSpace Frustum::intersectsSphere(const Sphere& sphere) const
{
	HalfSpace result = POSITIVE;

	for (const Plane& plane : m_planes) {
		HalfSpace intersection = plane.intersectsSphere(sphere);

		if (intersection == NEGATIVE) {
			// If plane is considered outside, then don't loop anymore
			result = intersection;
			break;
		}
		else if (intersection == ON_PLANE) {
			result = intersection;
		}
	}

	return result;
}

ICollider::HalfSpace Frustum::intersectsAABB(const AABB& aabb) const
{
	HalfSpace result = POSITIVE;

	int index = 0;
	for (const Plane& plane : m_planes) {
		HalfSpace intersection = plane.intersectsAABB(aabb);

		if (intersection == NEGATIVE) {
			result = intersection;
			break;
		}
		else if (intersection == ON_PLANE) {
			result = intersection;
		}
	}

	return result;
}

ICollider::HalfSpace Frustum::intersectsPlane(const Plane& plane) const
{
	// Some of the frustum's planes are always intersecting
	return POSITIVE;
}

ICollider::HalfSpace Frustum::intersectsFrustum(const Frustum& frustum) const
{
	return POSITIVE;
}

