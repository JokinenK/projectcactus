/*
 * Frustum.h
 *
 *  Created on: 3.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_PHYSICS_FRUSTUM_H_
#define PROJECTCACTUS_SRC_CORE_PHYSICS_FRUSTUM_H_

#include <vector>
#include <glm/glm.hpp>
#include "core/ICollider.h"
#include "core/physics/Plane.h"

class Frustum : public ICollider
{
public:
	typedef enum {
		FIRST_CORNER,
		CORNER_NLT = FIRST_CORNER,
		CORNER_NLB,
		CORNER_NRT,
		CORNER_NRB,
		CORNER_FLT,
		CORNER_FLB,
		CORNER_FRT,
		CORNER_FRB,
		NUM_CORNERS
	} CornerName;

	typedef enum {
		FIRST_PLANE,
		PLANE_LEFT = FIRST_PLANE,
		PLANE_RIGHT,
		PLANE_TOP,
		PLANE_BOTTOM,
		PLANE_NEAR,
		PLANE_FAR,
		NUM_PLANES
	} PlaneName;

	Frustum();
	Frustum(const std::vector<glm::vec3> corners);
	Frustum(const Frustum& other);
	virtual ~Frustum();

	Frustum& operator=(const Frustum& other);

	virtual HalfSpace intersectsVec3(const glm::vec3& point) const;
	virtual HalfSpace intersectsSphere(const Sphere& sphere) const;
	virtual HalfSpace intersectsAABB(const AABB& aabb) const;
	virtual HalfSpace intersectsPlane(const Plane& plane) const;
	virtual HalfSpace intersectsFrustum(const Frustum& frustum) const;

	inline virtual ColliderType getColliderType() const
	{
		return TYPE_FRUSTUM;
	}

	void setCorner(CornerName cornerName, const glm::vec3& corner);
	const glm::vec3& getCorner(CornerName cornerName) const;
	const std::vector<glm::vec3>& getCorners() const;

	void setPlane(PlaneName planeName, const Plane& plane);
	const Plane& getPlane(PlaneName planeName) const;
	const std::vector<Plane>& getPlanes() const;

	void updatePlanes();

protected:
	std::vector<glm::vec3> m_corners;
	std::vector<Plane> m_planes;
};

#endif /* PROJECTCACTUS_SRC_CORE_PHYSICS_FRUSTUM_H_ */
