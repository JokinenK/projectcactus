/*
 * Plane.h
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_PHYSICS_PLANE_H_
#define PROJECTCACTUS_SRC_CORE_PHYSICS_PLANE_H_

#include <glm/glm.hpp>
#include "core/ICollider.h"

class Plane : public ICollider
{
public:
	Plane();
	Plane(float a, float b, float c, float d);
	Plane(const glm::vec3& normal, float d);
	Plane(const Plane& other);

	virtual ~Plane();

	Plane& operator=(const Plane& other);

	virtual HalfSpace intersectsVec3(const glm::vec3& other) const;
	virtual HalfSpace intersectsAABB(const AABB& other) const;
	virtual HalfSpace intersectsSphere(const Sphere& other) const;
	virtual HalfSpace intersectsPlane(const Plane& other) const;
	virtual HalfSpace intersectsFrustum(const Frustum& other) const;

	inline virtual ColliderType getColliderType() const
	{
		return TYPE_PLANE;
	}

	static Plane FromPointNormal(const glm::vec3& point, const glm::vec3& normal);
	static Plane FromPointVectors(const glm::vec3& point, const glm::vec3& v1, const glm::vec3& v2);
	static Plane FromPoints(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2);

	void normalize();
	Plane normalized() const;

	float distanceToPoint(const glm::vec3& point) const;

	void set(float a, float b, float c, float d);
	void set(const glm::vec3& normal, float d);
	void setPointNormal(const glm::vec3& point, const glm::vec3& normal);
	void setPointVectors(const glm::vec3& point, const glm::vec3& v1, const glm::vec3& v2);
	void setPoints(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2);

	float getA() const;
	float getB() const;
	float getC() const;
	float getD() const;

	const glm::vec3& getNormal() const;

private:
	glm::vec3 m_normal;
	float m_d;
};

#endif /* PROJECTCACTUS_SRC_CORE_PHYSICS_PLANE_H_ */
