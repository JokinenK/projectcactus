/*
 * Plane.cpp
 *
 *  Created on: 4.5.2017
 *      Author: kalle
 */

#include "Plane.h"
#include "AABB.h"
#include "Sphere.h"
#include "Frustum.h"

Plane::Plane() :
		m_normal(0),
		m_d(0)
{
}

Plane::Plane(float a, float b, float c, float d) :
		m_normal(a, b, c),
		m_d(d)
{
}

Plane::Plane(const glm::vec3& normal, float d) :
		m_normal(normal),
		m_d(d)
{
}

Plane::Plane(const Plane& other) :
		m_normal(0),
		m_d(0)
{
	operator=(other);
}

Plane::~Plane()
{
}

Plane& Plane::operator=(const Plane& other)
{
	m_normal = other.m_normal;
	m_d = other.m_d;

	return *this;
}

void Plane::set(float a, float b, float c, float d)
{
	m_normal.x = a;
	m_normal.y = b;
	m_normal.z = c;
	m_d = d;
}

void Plane::set(const glm::vec3& normal, float d)
{
	m_normal = normal;
	m_d = d;
}

void Plane::setPointNormal(const glm::vec3& point, const glm::vec3& normal)
{
    m_normal = glm::normalize(normal);
    m_d = -glm::dot(point, m_normal);
}

void Plane::setPointVectors(const glm::vec3& point, const glm::vec3& v1, const glm::vec3& v2)
{
	glm::vec3 normal = glm::cross(v1, v2);
	setPointNormal(point, normal);
}

void Plane::setPoints(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2)
{
    glm::vec3 normal = glm::normalize(glm::cross(v1 - v0, v2 - v0));
    setPointNormal(v0, normal);
}

Plane Plane::FromPointNormal(const glm::vec3& point, const glm::vec3& normal)
{
	Plane result;
	result.setPointNormal(point, normal);
	return result;
}

Plane Plane::FromPointVectors(const glm::vec3& point, const glm::vec3& v1, const glm::vec3& v2)
{
	Plane result;
	result.setPointVectors(point, v1, v2);
	return result;
}

Plane Plane::FromPoints(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2)
{
	Plane result;
	result.setPoints(v0, v1, v2);
	return result;
}

float Plane::distanceToPoint(const glm::vec3& point) const
{
    return (m_normal.x * point.x +
			m_normal.y * point.y +
			m_normal.z * point.z +
			m_d);
}

void Plane::normalize()
{
	float distance = sqrtf(
			m_normal.x * m_normal.x +
			m_normal.y * m_normal.y +
			m_normal.z * m_normal.z);

    m_normal = m_normal / distance;
    m_d = m_d / distance;
}

Plane Plane::normalized() const
{
	Plane plane(*this);
	plane.normalize();

	return plane;
}

float Plane::getA() const
{
	return m_normal.x;
}

float Plane::getB() const
{
	return m_normal.y;
}

float Plane::getC() const
{
	return m_normal.z;
}

float Plane::getD() const
{
	return m_d;
}

const glm::vec3& Plane::getNormal() const
{
	return m_normal;
}

ICollider::HalfSpace Plane::intersectsVec3(const glm::vec3& other) const
{
	float distance = distanceToPoint(other);

	if (distance < 0) {
		return NEGATIVE;
	}
	else if (distance > 0) {
		return POSITIVE;
	}

	return ON_PLANE;
}

ICollider::HalfSpace Plane::intersectsSphere(const Sphere& other) const
{
	float radius = other.getRadius();
	float distance = distanceToPoint(other.getCenter());

	if (distance < -radius) {
		return NEGATIVE;
	}
	else if (distance > radius) {
		return POSITIVE;
	}

	return ON_PLANE;
}

ICollider::HalfSpace Plane::intersectsAABB(const AABB& other) const
{
	const glm::vec3 center = other.getCenter();
	const glm::vec3 extent = other.getExtent();

	float d =
			center.x * m_normal.x +
			center.y * m_normal.y +
			center.z * m_normal.z;

	float r =
			extent.x * fabsf(m_normal.x) +
	        extent.y * fabsf(m_normal.y) +
			extent.z * fabsf(m_normal.z);

	if ((d + r) < -m_d) {
		return NEGATIVE;
	}
	else if ((d - r) < -m_d) {
		return ON_PLANE;
	}

	return POSITIVE;
}

ICollider::HalfSpace Plane::intersectsPlane(const Plane& other) const
{
	float distance = glm::length(glm::cross(getNormal(), other.getNormal()));

	if (distance < 0) {
		return NEGATIVE;
	}
	else if (distance > 0) {
		return POSITIVE;
	}

	return ON_PLANE;
}

ICollider::HalfSpace Plane::intersectsFrustum(const Frustum& other) const
{
	return other.intersectsPlane(*this);
}
