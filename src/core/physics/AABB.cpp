/*
 * AABB.cpp
 *
 *  Created on: 25.4.2017
 *      Author: kalle
 */

#include <algorithm>
#include "AABB.h"
#include "Sphere.h"
#include "Plane.h"
#include "Frustum.h"

AABB::AABB() :
		m_min(glm::vec3(INFINITY, INFINITY, INFINITY)),
		m_max(glm::vec3(-INFINITY, -INFINITY, -INFINITY))
{
}

AABB::AABB(const glm::vec3& min, const glm::vec3& max) :
		m_min(min),
		m_max(max)
{
}

AABB::AABB(const AABB& other)
{
	operator=(other);
}

AABB::~AABB()
{
}

AABB& AABB::operator=(const AABB& other)
{
	m_min = other.getMin();
	m_max = other.getMax();

	return *this;
}

void AABB::expand(const AABB& other)
{
	expand(other.getMin());
	expand(other.getMax());
}

void AABB::expand(const glm::vec3& other)
{
	m_min.x = std::min(m_min.x, other.x);
	m_max.x = std::max(m_max.x, other.x);

	m_min.y = std::min(m_min.y, other.y);
	m_max.y = std::max(m_max.y, other.y);

	m_min.z = std::min(m_min.z, other.z);
	m_max.z = std::max(m_max.z, other.z);
}

ICollider::HalfSpace AABB::intersectsAABB(const AABB& other) const
{
	const glm::vec3& otherMin = other.getMin();
	const glm::vec3& otherMax = other.getMax();

	if ((m_min.x <= otherMax.x && otherMin.x <= m_max.x) &&
	       (m_min.y <= otherMax.y && otherMin.y <= m_max.y) &&
	       (m_min.z <= otherMax.z && otherMin.z <= m_max.z)) {

		return ON_PLANE;
	}

	return NEGATIVE;
}

ICollider::HalfSpace AABB::intersectsVec3(const glm::vec3& other) const
{
	if ((m_min.x <= other.x && other.x <= m_max.x) &&
		   (m_min.y <= other.y && other.y <= m_max.y) &&
		   (m_min.z <= other.z && other.z <= m_max.z)) {

		return ON_PLANE;
	}

	return NEGATIVE;
}

ICollider::HalfSpace AABB::intersectsSphere(const Sphere& other) const
{
	return other.intersectsAABB(*this);
}

ICollider::HalfSpace AABB::intersectsPlane(const Plane& other) const
{
	return other.intersectsAABB(*this);
}

ICollider::HalfSpace AABB::intersectsFrustum(const Frustum& other) const
{
	return other.intersectsAABB(*this);
}

const glm::vec3& AABB::getMin() const
{
	return m_min;
}

const glm::vec3& AABB::getMax() const
{
	return m_max;
}

const glm::vec3 AABB::getCenter() const
{
	return 0.5f * (m_min + m_max);
}

const glm::vec3 AABB::getExtent() const
{
	return 0.5f * (m_max - m_min);
}

float AABB::width() const
{
	return m_max.x - m_min.x;
}

float AABB::height() const
{
	return m_max.y - m_min.y;
}

float AABB::length() const
{
	return m_max.z - m_min.z;
}

void AABB::clear()
{
	m_min = glm::vec3(INFINITY, INFINITY, INFINITY);
	m_max = glm::vec3(-INFINITY, -INFINITY, -INFINITY);
}
