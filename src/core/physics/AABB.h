/*
 * AABB.h
 *
 *  Created on: 25.4.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_PHYSICS_AABB_H_
#define PROJECTCACTUS_SRC_CORE_PHYSICS_AABB_H_

#include <glm/glm.hpp>
#include "core/ICollider.h"

class AABB : public ICollider
{
public:
	AABB();
	AABB(const glm::vec3& min, const glm::vec3& max);
	AABB(const AABB& other);

	virtual ~AABB();

	AABB& operator=(const AABB& other);

	virtual HalfSpace intersectsVec3(const glm::vec3& other) const;
	virtual HalfSpace intersectsAABB(const AABB& other) const;
	virtual HalfSpace intersectsSphere(const Sphere& other) const;
	virtual HalfSpace intersectsPlane(const Plane& other) const;
	virtual HalfSpace intersectsFrustum(const Frustum& frustum) const;

	inline virtual ColliderType getColliderType() const
	{
		return TYPE_AABB;
	}

	void expand(const AABB& other);
	void expand(const glm::vec3& other);

	const glm::vec3& getMin() const;
	const glm::vec3& getMax() const;
	const glm::vec3 getCenter() const;
	const glm::vec3 getExtent() const;

	float width() const;
	float height() const;
	float length() const;

	void clear();

private:
	glm::vec3 m_min;
	glm::vec3 m_max;
};

#endif /* PROJECTCACTUS_SRC_CORE_PHYSICS_AABB_H_ */
