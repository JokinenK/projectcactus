/*
 * IFramebuffer.h
 *
 *  Created on: 24.1.2017
 *      Author: kalle
 */

#ifndef IFRAMEBUFFER_H_
#define IFRAMEBUFFER_H_

class ITexture;

class IFramebuffer
{
public:
	typedef enum {
		NONE,
		COLOR_ATTACHMENT0,
		COLOR_ATTACHMENT1,
		COLOR_ATTACHMENT2,
		COLOR_ATTACHMENT3,
		COLOR_ATTACHMENT4,
		DEPTH_ATTACHMENT,
		STENCIL_ATTACHMENT,
		NUM_ATTACHMENTS
	} Attachment;

	virtual ~IFramebuffer() {};

	virtual void init(int width, int height) = 0;

	virtual int getWidth() const = 0;
	virtual int getHeight() const = 0;

	virtual void bindWriteBuffer() = 0;
	virtual void unbindWriteBuffer() = 0;

	virtual void bindReadBuffer() = 0;
	virtual void unbindReadBuffer() = 0;

	virtual void clear() = 0;
	virtual void attachRenderTarget(ITexture* texture, Attachment attachment) = 0;
	virtual void drawBuffers(Attachment* attachments, int numAttachments) = 0;
	virtual void copyColorToActiveFramebuffer() = 0;
	virtual void copyDepthToActiveFramebuffer() = 0;
};

#endif /* IFRAMEBUFFER_H_ */
