/*
 * CoreEvent.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef COREEVENT_H_
#define COREEVENT_H_

class CoreEvent
{
public:
	typedef enum {
		EVENT_NONE,
		EVENT_QUIT,
		EVENT_FULLSCREEN,
		EVENT_WINDOWED,
		EVENT_SHOW_CONSOLE,
		EVENT_HIDE_CONSOLE,
		EVENT_CAPTURE_MOUSE,
		EVENT_RELEASE_MOUSE,
		EVENT_ADD_LIGHT,
		NUM_EVENTS
	} EventType;

	CoreEvent(EventType type = EVENT_NONE) :
		m_type(type)
	{
	}

	void setType(EventType type)
	{
		m_type = type;
	}

	EventType getType() const
	{
		return m_type;
	}

private:
	EventType m_type;
};

#endif /* COREEVENT_H_ */
