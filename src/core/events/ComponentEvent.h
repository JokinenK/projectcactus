/*
 * ComponentEvent.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef COMPONENTEVENT_H_
#define COMPONENTEVENT_H_

class ComponentEvent
{
public:
	typedef enum {
		EVENT_START,
		EVENT_STOP,
		EVENT_UPDATE,
		EVENT_FIXED_UPDATE,
		EVENT_LATE_UPDATE,
		EVENT_PAUSE,
		EVENT_UNPAUSE,
		NUM_EVENTS
	} EventType;

	ComponentEvent(EventType type = EVENT_START) :
		m_type(type)
	{
	}

	void setType(EventType type)
	{
		m_type = type;
	}

	EventType getType() const
	{
		return m_type;
	}

private:
	EventType m_type;
};

#endif /* COMPONENTEVENT_H_ */
