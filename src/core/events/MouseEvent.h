/*
 * MouseEvent.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef MOUSEEVENT_H_
#define MOUSEEVENT_H_

#define BUTTON_MASK(X) (1 << ((X)-1))

class MouseEvent
{
public:
	typedef enum {
		MouseButton,
		MouseWheel,
		MouseMove
	} EventType;

	MouseEvent(EventType type, int buttons, short x, short y, short scrollX = 0, short scrollY = 0) :
		m_type(type),
		m_buttons(buttons),
		m_x(x),
		m_y(y),
		m_scrollX(scrollX),
		m_scrollY(scrollY)
	{
	}

	EventType getType() const
	{
		return m_type;
	}

	int getButtons() const
	{
		return m_buttons;
	}

	bool isButtonPressed(char button) const
	{
		return (m_buttons & BUTTON_MASK(button));
	}

	short getX() const
	{
		return m_x;
	}

	short getY() const
	{
		return m_y;
	}

	short getScrollX() const
	{
		return m_scrollX;
	}

	short getScrollY() const
	{
		return m_scrollY;
	}

	bool isButtonEvent() const
	{
		return (m_type == MouseEvent::MouseButton);
	}

	bool isWheelEvent() const
	{
		return (m_type == MouseEvent::MouseWheel);
	}

	bool isMoveEvent() const
	{
		return (m_type == MouseEvent::MouseMove);
	}

private:
	EventType m_type;
	int m_buttons;
	short m_x;
	short m_y;
	short m_scrollX;
	short m_scrollY;
};

#endif /* MOUSEEVENT_H_ */
