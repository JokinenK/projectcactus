/*
 * KeyboardEvent.h
 *
 *  Created on: 13.9.2016
 *      Author: kalle
 */

#ifndef KEYBOARDEVENT_H_
#define KEYBOARDEVENT_H_

#include <string>

class KeyboardEvent
{
public:
	typedef enum {
		KeyPress,
		KeyRelease,
		TextInput
	} EventType;

	typedef enum {
		None   = 0x00,
		LAlt   = 0x01,
		RAlt   = 0x02,
		LCtrl  = 0x04,
		RCtrl  = 0x08,
		LShift = 0x10,
		RShift = 0x20,
		LWin   = 0x40,
		RWin   = 0x80,
		Alt    = (LAlt   | RAlt),
		Ctrl   = (LCtrl  | RCtrl),
		Shift  = (LShift | RShift),
		Win    = (LWin   | RWin),
	} ModifierKey;

	KeyboardEvent(EventType type, int keyCode, int scanCode, int keyModifiers) :
		m_type(type),
		m_keyCode(keyCode),
		m_scanCode(scanCode),
		m_keyModifiers(keyModifiers)
	{
	}

	EventType getType() const
	{
		return m_type;
	}

	int getKeyCode() const
	{
		return m_keyCode;
	}

	int getScanCode() const
	{
		return m_scanCode;
	}

	char getChar() const
	{
		if (isShift()) {
			return std::toupper(m_keyCode);
		}

		return m_keyCode;
	}

	bool getKeyModifier() const
	{
		return m_keyModifiers;
	}

	bool isAlt() const
	{
		return (m_keyModifiers & Alt);
	}

	bool isCtrl() const
	{
		return (m_keyModifiers & Ctrl);
	}

	bool isShift() const
	{
		return (m_keyModifiers & Shift);
	}

	bool isWin() const
	{
		return (m_keyModifiers & Win);
	}

private:
	EventType m_type;
	int m_keyCode;
	int m_scanCode;
	int m_keyModifiers;
};

#endif /* KEYBOARDEVENT_H_ */
