/*
 * ShaderManager.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef SHADERMANAGER_H_
#define SHADERMANAGER_H_

#include <map>
#include <string>
#include "support/Singleton.h"

class IShader;

class ShaderManager : public Singleton<ShaderManager>
{
public:
	ShaderManager();
	virtual ~ShaderManager();

	IShader* getShader(const std::string& shaderName);

private:
	std::map<std::string, IShader*> m_cachedShaders;
};

#endif /* SHADERMANAGER_H_ */
