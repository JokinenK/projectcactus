/*
 * FontManager.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef FONTMANAGER_H_
#define FONTMANAGER_H_

#include <map>
#include <string>
#include "support/Singleton.h"
#include "text/FontAtlas.h"

class ITexture;

class FontManager : public Singleton<FontManager>
{
public:
	FontManager();
	virtual ~FontManager();

	FontAtlas* defaultFontAtlas();
	FontAtlas* getFontAtlas(const std::string& fontPath);

private:
	std::map<std::string, FontAtlas*> m_cachedFonts;
};

#endif /* FONTMANAGER_H_ */
