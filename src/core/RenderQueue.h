/*
 * RenderQueue.h
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#ifndef PROJECTCACTUS_SRC_CORE_RENDERQUEUE_H_
#define PROJECTCACTUS_SRC_CORE_RENDERQUEUE_H_

#include <map>
#include <glm/glm.hpp>
#include "core/Material.h"

class IModel;
class ITechnique;
class Transform;
class BoundingBox;
class IRenderPrerequisite;

class RenderQueue
{
public:
	typedef uint64_t Key;

	typedef struct {
		IModel* model;
		Material* material;
		Transform* transform;
	} Value;

	typedef std::multimap<Key, Value> Queue;
	typedef Queue::iterator Iterator;

	RenderQueue(const IRenderPrerequisite& renderPrerequisite);
	virtual ~RenderQueue();

	bool shouldRender(const BoundingBox* boundingBox);
	void addToQueue(Key key, Value entry);
	void sort();
	void clear();

	Iterator begin();
	Iterator end();

private:
	Queue m_queue;
	const IRenderPrerequisite& m_renderPrerequisite;
};

#endif /* PROJECTCACTUS_SRC_CORE_RENDERQUEUE_H_ */
