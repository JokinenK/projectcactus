/*
 * BoundingBox.cpp
 *
 *  Created on: 14.5.2017
 *      Author: Kalle
 */

#include <iostream>
#include "BoundingBox.h"
#include "core/GameObject.h"
#include "core/Transform.h"

BoundingBox::BoundingBox(const AABB& boundingBox) :
		m_parent(0),
		m_transform(0),
		m_boundingBox(boundingBox)
{
}

BoundingBox::~BoundingBox()
{
}

void BoundingBox::start()
{
	m_parent = getParent();
	m_transform = m_parent->getTransform();
}

void BoundingBox::stop()
{

}

void BoundingBox::update()
{
	const glm::mat4& transform = m_transform->getTransformation();
	const glm::vec3& min = m_boundingBox.getMin();
	const glm::vec3& max = m_boundingBox.getMax();

	const glm::vec3 translation = glm::vec3(transform[3]);

	glm::vec3 xa = transform[0] * min.x;
	glm::vec3 xb = transform[0] * max.x;

	glm::vec3 ya = transform[1] * min.y;
	glm::vec3 yb = transform[1] * max.y;

	glm::vec3 za = transform[2] * min.z;
	glm::vec3 zb = transform[2] * max.z;

	glm::vec3 newMin = glm::min(xa, xb) + glm::min(ya, yb) + glm::min(za, zb) + translation;
	glm::vec3 newMax = glm::max(xa, xb) + glm::max(ya, yb) + glm::max(za, zb) + translation;

	m_transformedBoundingBox.clear();
	m_transformedBoundingBox.expand(newMin);
	m_transformedBoundingBox.expand(newMax);

//	std::cout << "Min: X: " << newMin.x << ", Y: " << newMin.y << ", Z: " << newMin.z << std::endl;
//	std::cout << "Max: X: " << newMax.x << ", Y: " << newMax.y << ", Z: " << newMax.z << std::endl;
}

const AABB& BoundingBox::getTransformedBoundingBox() const
{
	return m_transformedBoundingBox;
}
