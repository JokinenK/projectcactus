/*
 * ViewFrustumRenderPrequisite.cpp
 *
 *  Created on: 7.5.2017
 *      Author: kalle
 */

#include "AlwaysTrueRenderPrerequisite.h"

bool AlwaysTrueRenderPrerequisite::shouldRender(const BoundingBox* boundingBox) const
{
	return true;
}
