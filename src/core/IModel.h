/*
 * IModelHandle.h
 *
 *  Created on: 18.2.2017
 *      Author: Kalle
 */

#ifndef IMODEL_H_
#define IMODEL_H_

#include <glm/glm.hpp>
#include <vector>

class IModel
{
public:
	typedef enum {
		BUFFER_INDICES,
		BUFFER_POSITION,
	    BUFFER_TEXCOORD,
	    BUFFER_NORMAL,
		BUFFER_TANGENT,
	    NUM_BUFFERS
	} BufferName;

	virtual ~IModel() {};

    virtual void setIndices(const std::vector<unsigned short>& indices) = 0;
    virtual void setPositions(const std::vector<glm::vec3>& positions) = 0;
    virtual void setTexCoords(const std::vector<glm::vec2>& texCoords) = 0;
    virtual void setNormals(const std::vector<glm::vec3>& normals) = 0;
    virtual void setTangents(const std::vector<glm::vec4>& tangents) = 0;

	virtual void render() = 0;
};

#endif /* IMODELHANDLE_H_ */
