#include <iostream>
#include <string>
#include "ExampleGame.h"

int main(int argc, char* argv[])
{
	ExampleGame exampleGame(argc, argv);
	return exampleGame.run();
}
