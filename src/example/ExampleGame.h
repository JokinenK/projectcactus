#ifndef EXAMPLEGAME_H_
#define EXAMPLEGAME_H_

#include "core/EventListener.h"
#include "core/Plugin.h"
#include "core/OptionParser.h"
#include "support/Color.h"

class MessageBus;
class GameObject;
class RendererFactory;
class TextureManager;
class SkyBoxRenderer;
class SceneRenderer;
class IDisplay;
class IInput;
class IRenderer;
class GameLoop;
class CoreEvent;
class Camera;

namespace Gui
{
	class AbstractButton;
	class AbstractSlider;
	class StackedLayout;
	class BoxLayout;
	class GridLayout;
	class Button;
	class GuiRenderer;
	class ColorSelector;
	class FileDialog;
	class Widget;
	class FileDialog;
	class Slider;
}

class ExampleGame : public EventListener<CoreEvent> {
public:
	ExampleGame(int argc, char** argv);
	~ExampleGame();

	int run();
	bool receiveEvent(const CoreEvent* event);

private:
	typedef enum {
		TYPE_POINT,
		TYPE_SPOT
	} LightType;

	GameObject* createGameObject(const std::string& modelPath, const std::string& modelTag = std::string());
	void removeObjectsWithTag(const std::string& tag);

	void colorChanged(const Gui::ColorSelector* colorSelector);
	void sliderMoved(const Gui::AbstractSlider* abstractSlider);
	void buttonPressed(const Gui::AbstractButton* abstractButton);
	void dialogCallback(const Gui::FileDialog* fileDialog, const std::string& filePath);

	void loadPlugins();
	void initRenderers();
	void loadModels();
	void createWidgets();
	void runGame();

	void addLight(LightType lightType);

	OptionParser m_optionParser;

	int m_width;
	int m_height;
	float m_drawDistance;

	Color m_createLightColor;
	float m_createModelScale;

	MessageBus* m_messageBus;
	RendererFactory* m_graphicsFactory;
	TextureManager* m_textureManager;

	Plugin<IDisplay>* m_displayPlugin;
	Plugin<IInput>* m_inputPlugin;
	Plugin<IRenderer>* m_rendererPlugin;

	SkyBoxRenderer* m_skyBoxRenderer;
	SceneRenderer* m_sceneRenderer;
	Gui::GuiRenderer* m_guiRenderer;

	IDisplay* m_display;
	IInput* m_input;
	IRenderer* m_renderer;
	Camera* m_camera;

	Gui::StackedLayout* m_mainLayout;
	Gui::BoxLayout* m_widgetLayout;
	Gui::GridLayout* m_dialogLayout;
	Gui::ColorSelector* m_colorSelector;
	Gui::Slider* m_sceneScaleSlider;
	Gui::Slider* m_importScaleSlider;
	Gui::Button* m_addPointLightButton;
	Gui::Button* m_addSpotLightButton;
	Gui::Button* m_loadModelButton;
	Gui::Button* m_clearModelsButton;
	Gui::Button* m_clearLightsButton;
	Gui::FileDialog* m_fileDialog;

	GameLoop* m_gameLoop;
};

#endif /* EXAMPLEGAME_H_ */
