#ifndef MOVE_H
#define MOVE_H

#include <iostream>
#include "core/Component.h"
#include "core/Transform.h"
#include "core/MessageBus.h"
#include "core/events/KeyboardEvent.h"
#include "core/events/MouseEvent.h"
#include "core/events/CoreEvent.h"
#include "core/Time.h"
#include "core/KeyMappings.h"

class MoveBehavior : public Component
{
public:
	typedef enum {
		Forward     = 0x01,
		Backward    = 0x02,
		StrafeLeft  = 0x04,
		StrafeRight = 0x08,
		RollLeft    = 0x10,
		RollRight   = 0x20
	} MoveDirection;

	typedef enum {
		Fullscreen   = 0x01,
		Quit         = 0x02,
		AddLight     = 0x04,
		Console      = 0x08,
		CaptureMouse = 0x10,
	} Actions;

    MoveBehavior() :
			m_prevX(0),
			m_prevY(0),
			m_direction(0),
			m_flags(0),
			m_time(Time::instance()),
			m_messageBus(MessageBus::instance()),
			m_transform(0)
    {
    }

    void start()
    {
        m_transform = getParent()->getTransform();
    }

    void fixedUpdate()
    {
        float rollFactor = 2.0f;
        float moveAmount = 0.5f;

        if (m_direction & Forward) {
            m_transform->translate(m_transform->getForward(), moveAmount);
        }
        else if (m_direction & Backward) {
            m_transform->translate(m_transform->getForward(), -moveAmount);
        }

        if (m_direction & StrafeLeft) {
            m_transform->translate(m_transform->getRight(), -moveAmount);
        }
        else if (m_direction & StrafeRight) {
            m_transform->translate(m_transform->getRight(), moveAmount);
        }

        if (m_direction & RollLeft) {
            m_transform->rotate(m_transform->getForward(), -glm::radians(moveAmount * rollFactor));
        }
        else if (m_direction & RollRight) {
            m_transform->rotate(m_transform->getForward(), glm::radians(moveAmount * rollFactor));
        }
    }

    bool mouseEvent(const MouseEvent* event)
    {
    	if (event->getType() == MouseEvent::MouseButton) {
			if (event->isButtonPressed(1)) {
				m_flags |= CaptureMouse;

				CoreEvent event(CoreEvent::EVENT_CAPTURE_MOUSE);
				m_messageBus->publish(&event);
			}
			else {
				m_flags &= ~CaptureMouse;

				CoreEvent event(CoreEvent::EVENT_RELEASE_MOUSE);
				m_messageBus->publish(&event);
			}

			return true;
		}
    	else if (event->getType() == MouseEvent::MouseMove) {
    		if (m_flags & CaptureMouse) {
    			// Read mouse delta
    			float deltaX = m_prevX - event->getX();
    			float deltaY = m_prevY - event->getY();

    			// Rotate horizontally
    			m_transform->rotate(Camera::RotateLeft, glm::radians(deltaX));
    			m_transform->rotate(Camera::RotateBackward, glm::radians(deltaY));
    		}

    		m_prevX = event->getX();
    		m_prevY = event->getY();

    		return true;
    	}
    	else if (event->getType() == MouseEvent::MouseWheel) {
    		/* TODO */
    	}

    	return false;
    }

    bool keyboardEvent(const KeyboardEvent* event)
    {
    	if (event->getType() == KeyboardEvent::TextInput) {
    		return false;
    	}

        bool state = (event->getType() == KeyboardEvent::KeyPress);

        switch (event->getScanCode()) {
			case ScanCode::SCANCODE_W: {
				state ? m_direction |= Forward
					  : m_direction &= ~Forward;
			} break;

			case ScanCode::SCANCODE_S: {
				state ? m_direction |= Backward
					  : m_direction &= ~Backward;
			} break;

			case ScanCode::SCANCODE_A: {
				state ? m_direction |= StrafeLeft
					  : m_direction &= ~StrafeLeft;
			} break;

			case ScanCode::SCANCODE_D: {
				state ? m_direction |= StrafeRight
					  : m_direction &= ~StrafeRight;
			} break;

			case ScanCode::SCANCODE_COMMA: {
				state ? m_direction |= RollLeft
					  : m_direction &= ~RollLeft;
			} break;

			case ScanCode::SCANCODE_PERIOD: {
				state ? m_direction |= RollRight
					  : m_direction &= ~RollRight;
			} break;

			case ScanCode::SCANCODE_E: {
				if (state) {
					CoreEvent event(CoreEvent::EVENT_ADD_LIGHT);
					m_messageBus->publish(&event);
				}
			} break;

			case ScanCode::SCANCODE_F: {
				if (state) {
					m_flags ^= Fullscreen;
					CoreEvent event((m_flags & Fullscreen) ? CoreEvent::EVENT_FULLSCREEN : CoreEvent::EVENT_WINDOWED);
					m_messageBus->publish(&event);
				}
			} break;

			case ScanCode::SCANCODE_GRAVE: {
				if (state) {
					m_flags ^= Console;
					CoreEvent event((m_flags & Console) ? CoreEvent::EVENT_SHOW_CONSOLE : CoreEvent::EVENT_HIDE_CONSOLE);
					m_messageBus->publish(&event);
				}
			} break;

			case ScanCode::SCANCODE_ESCAPE: {
				if (state) {
					m_flags &= ~CaptureMouse;
					CoreEvent event(CoreEvent::EVENT_RELEASE_MOUSE);
					m_messageBus->publish(&event);
				}
			} break;
        }

        return true;
    }

private:
    short m_prevX;
    short m_prevY;
    unsigned char m_direction;
    unsigned char m_flags;

    Time* m_time;
	MessageBus* m_messageBus;
	Transform* m_transform;
};

#endif // MOVE_H
