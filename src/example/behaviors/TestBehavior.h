#ifndef TESTBEHAVIOR_H
#define TESTBEHAVIOR_H

#include "core/Transform.h"
#include "core/Time.h"
#include "core/Component.h"
#include "core/FontManager.h"
#include "support/Utils.h"
#include "hal/File.h"

class TestBehavior : public Component
{
public:
	TestBehavior() :
		m_time(0),
		m_transform(0),
		m_height(0.0f),
		m_sec(0.0f)
	{
	}

    void start()
    {
        // Read time
        m_time = Time::instance();

        // Cache transform
        m_transform = getParent()->getTransform();

        // Read reference height
        m_height = m_transform->getPosition().y;
    }

    void fixedUpdate()
    {
        // Add delta to sec
        m_sec += m_time->frameTime();

        if (m_sec >= 1.0f) {
            // Print FPS for debug
            std::cout << "Calculated FPS: " << m_time->fps() << ", deltaTime: " << m_time->frameTime() << ", t: " << m_time->t() << std::endl;

            // Clear counter
            m_sec -= 1.0f;
        }

        // Helpers to test out
        float sin = sinf(m_time->t());
        float cos = cosf(m_time->t());

        // Apply transform
        m_transform->rotate(Camera::RotateLeft, 1);
        m_transform->rotate(glm::vec3(sin, cos, sin), 1);
        m_transform->setPosition(glm::vec3(cos * 4, m_height + sin, cos * 4));
    }

private:
    Time* m_time;
    Transform* m_transform;
    float m_height;
    float m_sec;
};

#endif // TESTBEHAVIOR_H
