#include <iostream>
#include <string>
#include <glm/glm.hpp>
#include "ExampleGame.h"
#include "core/IDisplay.h"
#include "core/IInput.h"
#include "core/GameObject.h"
#include "core/BoundingBox.h"
#include "core/Mesh.h"
#include "core/GameLoop.h"
#include "core/MessageBus.h"
#include "core/Material.h"
#include "core/EventListener.h"
#include "core/TextureManager.h"
#include "core/events/CoreEvent.h"
#include "core/events/KeyboardEvent.h"
#include "core/events/MouseEvent.h"
#include "core/gui/GuiRenderer.h"
#include "core/gui/Widget.h"
#include "core/gui/Label.h"
#include "core/gui/Button.h"
#include "core/gui/Slider.h"
#include "core/gui/Picture.h"
#include "core/gui/ColorSelector.h"
#include "core/gui/TextField.h"
#include "core/gui/ScrollArea.h"
#include "core/gui/BoxLayout.h"
#include "core/gui/GridLayout.h"
#include "core/gui/StackedLayout.h"
#include "core/gui/FileDialog.h"
#include "core/gui/GuiRenderer.h"
#include "core/Plugin.h"
#include "core/IRenderer.h"
#include "core/SceneRenderer.h"
#include "core/Scene.h"
#include "core/SkyBoxRenderer.h"
#include "core/RendererFactory.h"
#include "core/light/DirectionalLight.h"
#include "core/light/PointLight.h"
#include "core/light/SpotLight.h"
#include "behaviors/MoveBehavior.h"
#include "behaviors/TestBehavior.h"
#include "core/model/obj/OBJLoader.h"
#include "core/model/collada/ColladaModel.h"
#include "core/model/assimp/AssimpLoader.h"
#include "hal/File.h"

namespace
{
	static const std::string DYNAMIC_MODEL_TAG("DynamicModel");
	static const std::string DYNAMIC_LIGHT_TAG("DynamicLight");
}

ExampleGame::ExampleGame(int argc, char** argv) :
		m_optionParser(argc, argv),
		m_width(1280),
		m_height(720),
		m_drawDistance(500.0f),
		m_createLightColor(0xFFFFFF),
		m_createModelScale(1.0f),
		m_messageBus(MessageBus::instance()),
		m_graphicsFactory(RendererFactory::instance()),
		m_textureManager(TextureManager::instance())
{
	m_messageBus->subscribe<CoreEvent>(this);

	m_width = m_optionParser.readValue("-width", 1280);
	m_height = m_optionParser.readValue("-height", 720);
	m_drawDistance = m_optionParser.readValue("-drawDistance", 250.0f);

	loadPlugins();
	initRenderers();
	loadModels();
	createWidgets();

	if (m_optionParser.isDefined("-model")) {
		createGameObject(m_optionParser.readValue("-model", std::string()), DYNAMIC_MODEL_TAG);
	}
	else if (m_optionParser.numTokens() == 1) {
		createGameObject(m_optionParser.readIndex(0, std::string()), DYNAMIC_MODEL_TAG);
	}
}

ExampleGame::~ExampleGame()
{
	delete m_gameLoop;

	delete m_guiRenderer;
	delete m_sceneRenderer;
	delete m_skyBoxRenderer;

	m_rendererPlugin->releaseInstance(m_renderer);
	m_inputPlugin->releaseInstance(m_input);
	m_displayPlugin->releaseInstance(m_display);

	delete m_rendererPlugin;
	delete m_inputPlugin;
	delete m_displayPlugin;
}

int ExampleGame::run()
{
	runGame();
	return 0;
}

GameObject* ExampleGame::createGameObject(const std::string& modelPath, const std::string& modelTag)
{
	GameObject* model = 0;
	AssimpLoader assimpLoader(modelPath);

	if (assimpLoader.getModels().size() > 0) {
		model = new GameObject(modelTag.empty() ? modelPath : modelTag);
		model->getTransform()->setScale(glm::vec3(m_createModelScale));

		for (const VertexModel& mesh : assimpLoader.getModels()) {
			GameObject* meshContainer = new GameObject();
			meshContainer->addComponent(new Mesh(mesh, mesh.getMaterial()));
			meshContainer->addComponent(new BoundingBox(mesh.getBoundingBox()));

			model->addChildren(meshContainer);
		}

		m_sceneRenderer->getScene()->addChildren(model);

		if (m_gameLoop->isRunning()) {
			model->start();
		}
	}
	else {
		std::cerr << "Unable to parse model: " << modelPath << std::endl;
	}

	return model;
}

void ExampleGame::removeObjectsWithTag(const std::string& modelTag)
{
	GameObject* root = m_sceneRenderer->getScene();

	std::list<GameObject*> objectsToRemove =
			root->findGameObjectsWithTag(modelTag);

	for (GameObject* children : objectsToRemove) {
		root->removeChildren(children);
	}
}

bool ExampleGame::receiveEvent(const CoreEvent* event)
{
	bool success = true;

	switch (event->getType()) {
		case CoreEvent::EVENT_FULLSCREEN: {
			//m_display->setFullscreen(true);
			m_camera->lookAt(glm::vec3(0, 0, 0));
		} break;

		case CoreEvent::EVENT_WINDOWED: {
			//m_display->setFullscreen(false);
			m_camera->lookAt(glm::vec3(0, 0, 0));
		} break;

		case CoreEvent::EVENT_CAPTURE_MOUSE: {
			m_display->captureMouse(true);
		} break;

		case CoreEvent::EVENT_RELEASE_MOUSE: {
			m_display->captureMouse(false);
		} break;

		case CoreEvent::EVENT_SHOW_CONSOLE: {
			/* TODO */
		} break;

		case CoreEvent::EVENT_HIDE_CONSOLE: {
			/* TODO */
		} break;

		case CoreEvent::EVENT_ADD_LIGHT: {
			addLight(TYPE_POINT);
		} break;

		case CoreEvent::EVENT_QUIT: {
			m_gameLoop->stop();
		} break;

		default: {
			success = false;
		}
	}

	return success;
}

void ExampleGame::addLight(LightType lightType) {
	GameObject* lightContainer = new GameObject(DYNAMIC_LIGHT_TAG);
	BaseLight* light = 0;

	if (lightType == TYPE_POINT) {
		light = new PointLight(m_createLightColor, Attenuation(0.1f, 0.03f, 0.003f), 0.7f);
		light->setShadowEnabled(true);
	}
	else if (lightType == TYPE_SPOT) {
		light = new SpotLight(m_createLightColor, Attenuation(0.1f, 0.03f, 0.003f), 0.7f, 45.0f);
		light->setShadowEnabled(true);
	}

	lightContainer->addComponent(light);
	lightContainer->getTransform()->setPosition(m_camera->getParent()->getTransform()->getTransformedPosition());
	lightContainer->getTransform()->setRotation(m_camera->getParent()->getTransform()->getTransformedRotation());

	m_sceneRenderer->getScene()->addChildren(lightContainer);

	if (m_gameLoop->isRunning()) {
		lightContainer->start();
	}
}

void ExampleGame::loadPlugins() {
	// Create display object
	m_displayPlugin = new Plugin<IDisplay>("libCactusSDLDisplay.dll");
	m_inputPlugin = new Plugin<IInput>("libCactusSDLInput.dll");
	m_rendererPlugin = new Plugin<IRenderer>("libCactusOpenGLRenderer.dll");

	m_display = m_displayPlugin->createInstance();
	m_display->init("CactusSDK Example", m_width, m_height);

	m_input = m_inputPlugin->createInstance();
	m_input->init(m_messageBus);

	m_renderer = m_rendererPlugin->createInstance();

	m_graphicsFactory->setRenderer(m_renderer);
}

void ExampleGame::initRenderers() {

	m_renderer = m_graphicsFactory->getRenderer();
	m_renderer->setClearColor(0.0f, 0.15f, 0.3f);

	std::string skyboxBaseName = Hal::File::executableDir() + Hal::File::pathSeparator() + "../res/textures/sky";

	m_skyBoxRenderer = new SkyBoxRenderer(m_renderer, m_drawDistance / 2.0f, skyboxBaseName, "png");
	m_sceneRenderer = new SceneRenderer(m_renderer, m_width, m_height);
	m_guiRenderer = new Gui::GuiRenderer(m_renderer, m_width, m_height);

	m_gameLoop = new GameLoop(m_display, m_input, m_sceneRenderer, m_skyBoxRenderer, m_guiRenderer);
}

void ExampleGame::createWidgets() {
	m_mainLayout = new Gui::StackedLayout(Gui::StackedLayout::SHOW_ALL);
	m_widgetLayout = new Gui::BoxLayout(Gui::Layout::Vertical);
	m_dialogLayout = new Gui::GridLayout(5, 5);

	m_colorSelector = new Gui::ColorSelector();
	m_colorSelector->setPreferredGeometry(Rectangle(0, 0, 200, 200));
	m_colorSelector->addColorChangedCallback(std::bind(&ExampleGame::colorChanged, this, std::placeholders::_1));
	m_colorSelector->setColor(m_createLightColor);

	m_sceneScaleSlider = new Gui::Slider("Scene Scale");
	m_sceneScaleSlider->setPreferredGeometry(Rectangle(0, 0, 200, 50));
	m_sceneScaleSlider->addMoveCallback(std::bind(&ExampleGame::sliderMoved, this, std::placeholders::_1));
	m_sceneScaleSlider->setRange(0.01f, 10.0f);
	m_sceneScaleSlider->setValue(m_createModelScale);

	m_importScaleSlider = new Gui::Slider("Model Scale");
	m_importScaleSlider->setPreferredGeometry(Rectangle(0, 0, 200, 50));
	m_importScaleSlider->addMoveCallback(std::bind(&ExampleGame::sliderMoved, this, std::placeholders::_1));
	m_importScaleSlider->setRange(0.01f, 10.0f);
	m_importScaleSlider->setValue(m_createModelScale);

	m_addPointLightButton = new Gui::Button("Add point light...");
	m_addPointLightButton->setPreferredGeometry(Rectangle(0, 0, 200, 30));
	m_addPointLightButton->addClickCallback(std::bind(&ExampleGame::buttonPressed, this, std::placeholders::_1));

	m_addSpotLightButton = new Gui::Button("Add spot light...");
	m_addSpotLightButton->setPreferredGeometry(Rectangle(0, 0, 200, 30));
	m_addSpotLightButton->addClickCallback(std::bind(&ExampleGame::buttonPressed, this, std::placeholders::_1));

	m_loadModelButton = new Gui::Button("Load model...");
	m_loadModelButton->setPreferredGeometry(Rectangle(0, 0, 200, 30));
	m_loadModelButton->addClickCallback(std::bind(&ExampleGame::buttonPressed, this, std::placeholders::_1));

	m_clearModelsButton = new Gui::Button("Clear models");
	m_clearModelsButton->setPreferredGeometry(Rectangle(0, 0, 200, 30));
	m_clearModelsButton->addClickCallback(std::bind(&ExampleGame::buttonPressed, this, std::placeholders::_1));

	m_clearLightsButton = new Gui::Button("Clear lights");
	m_clearLightsButton->setPreferredGeometry(Rectangle(0, 0, 200, 30));
	m_clearLightsButton->addClickCallback(std::bind(&ExampleGame::buttonPressed, this, std::placeholders::_1));

	m_widgetLayout->setPreferredGeometry(Rectangle(0, 0, m_width, m_height));
	m_widgetLayout->setMargin(3);
	m_widgetLayout->setPadding(3);
	m_widgetLayout->setAlignment(Gui::Widget::Top | Gui::Widget::Right);
	m_widgetLayout->addChildren(m_colorSelector);
	m_widgetLayout->addChildren(m_sceneScaleSlider);
	m_widgetLayout->addChildren(m_importScaleSlider);
	m_widgetLayout->addChildren(m_addPointLightButton);
	m_widgetLayout->addChildren(m_addSpotLightButton);
	m_widgetLayout->addChildren(m_loadModelButton);
	m_widgetLayout->addChildren(m_clearModelsButton);
	m_widgetLayout->addChildren(m_clearLightsButton);

	m_fileDialog = new Gui::FileDialog(Gui::FileDialog::DIALOG_OPEN);
	m_fileDialog->setPreferredGeometry(Rectangle(0, 0, 200, 200));
	m_fileDialog->addConfirmCallback(std::bind(&ExampleGame::dialogCallback, this, std::placeholders::_1, std::placeholders::_2));
	m_fileDialog->addCancelCallback(std::bind(&ExampleGame::dialogCallback, this, std::placeholders::_1, std::placeholders::_2));
	m_fileDialog->setAlignment(Gui::Widget::Center | Gui::Widget::Middle);
	m_fileDialog->setVisibility(false);

	m_dialogLayout->setPreferredGeometry(Rectangle(0, 0, m_width, m_height));
	m_dialogLayout->setMargin(0);
	m_dialogLayout->setPadding(0);
	m_dialogLayout->setWidget(m_fileDialog, 1, 1, 3, 3);

	m_mainLayout->addChildren(m_widgetLayout);
	m_mainLayout->addChildren(m_dialogLayout);

	m_guiRenderer->setLayout(m_mainLayout);
}

void ExampleGame::loadModels() {
	Scene* scene = new Scene();
	scene->setRenderer(m_renderer);
	m_sceneRenderer->setScene(scene);

	// Main camera
	m_camera = new Camera(glm::radians(70.0f), m_display->aspectRatio(), 0.1f, m_drawDistance);

	GameObject* cameraContainer = new GameObject("MainCamera");
	cameraContainer->getTransform()->setPosition(glm::vec3(0, 5, 10));
	cameraContainer->addComponent(new MoveBehavior());
	cameraContainer->addComponent(m_camera);

	// Create directional light
	GameObject* dirLightContainer = new GameObject("DirectionalLight");

	DirectionalLight* dirLight = new DirectionalLight(0xFFFFFF, 0.5f);
	dirLight->setShadowEnabled(true);

	dirLightContainer->addComponent(dirLight);
	dirLightContainer->getTransform()->rotate(Camera::RotateForward, glm::radians(90.0f));

	// Append objects to the scene
	scene->addChildren(cameraContainer);
	scene->addChildren(dirLightContainer);
}

void ExampleGame::runGame() {
    m_gameLoop->start();
}

void ExampleGame::colorChanged(const Gui::ColorSelector* colorSelector)
{
	m_createLightColor = colorSelector->getColor();
}

void ExampleGame::sliderMoved(const Gui::AbstractSlider* abstractSlider)
{
	if (abstractSlider == m_sceneScaleSlider) {
		std::cout << "Scene slider" << std::endl;
		m_sceneRenderer->getScene()->getTransform()->setScale(glm::vec3(m_sceneScaleSlider->getValue()));
		std::cout << "Scale: " << m_sceneRenderer->getScene()->getTransform()->getScale().x << std::endl;
	}
	else if (abstractSlider == m_importScaleSlider) {
		m_createModelScale = m_importScaleSlider->getValue();
	}
}

void ExampleGame::buttonPressed(const Gui::AbstractButton* abstractButton)
{
	if (abstractButton == m_loadModelButton) {
		m_fileDialog->setVisibility(true);
	}
	else if (abstractButton == m_addPointLightButton) {
		addLight(TYPE_POINT);
	}
	else if (abstractButton == m_addSpotLightButton) {
		addLight(TYPE_SPOT);
	}
	else if (abstractButton == m_clearModelsButton) {
		removeObjectsWithTag(DYNAMIC_MODEL_TAG);
	}
	else if (abstractButton == m_clearLightsButton) {
		removeObjectsWithTag(DYNAMIC_LIGHT_TAG);
	}
}

void ExampleGame::dialogCallback(const Gui::FileDialog* fileDialog, const std::string& filePath)
{
	/* Unused */
	(void) fileDialog;

	if (!filePath.empty()) {
		createGameObject(filePath, DYNAMIC_MODEL_TAG);
	}

	m_fileDialog->setVisibility(false);
}
