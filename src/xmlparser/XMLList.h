#ifndef XMLLIST_H
#define XMLLIST_H

#include <string>
#include <list>

// Forward declare
class XMLParsable;

class XMLAbstractList
{
public:
    virtual ~XMLAbstractList() {}
    virtual bool parseList(XMLNode* xmlElement, XMLParsable* parent) = 0;
};

template <class T>
class XMLList : public XMLAbstractList
{
public:
    virtual ~XMLList()
    {
    }

    static XMLAbstractList* optionalList(std::list<T*>& list)
    {
        return new XMLList(list, false);
    }

    static XMLAbstractList* requiredList(std::list<T*>& list)
    {
        return new XMLList(list, true);
    }

    bool parseList(XMLNode* xmlElement, XMLParsable* parent)
    {
        bool success = true;

        for (XMLNode* xmlChild : xmlElement->getChildren()) {
            T* instance = new T();

            if (instance->tagMatches(xmlChild->getTag())) {
                success = instance->parse(xmlChild, parent);

                if (!success) { break;}
                m_list.push_back(instance);
            }

            if (!success) {
            	delete instance;
            }
        }

        return success || !m_required;
    }

private:
    XMLList(std::list<T*>& list, bool required) :
        m_list(list),
        m_required(required)
    {
    }

    std::list<T*>& m_list;
    bool m_required;
};

#endif // XMLLIST_H
