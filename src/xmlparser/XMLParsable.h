#ifndef XMLPARSABLE_H
#define XMLPARSABLE_H

#include <list>
#include <string>
#include "xmlparser/XMLNode.h"

class XMLAbstractData;
class XMLAbstractList;
class XMLAbstractMap;

class XMLParsable
{
public:
    XMLParsable(
    		const std::string& tag1,
			const std::string& tag2 = std::string(),
			const std::string& tag3 = std::string(),
			const std::string& tag4 = std::string(),
			const std::string& tag5 = std::string(),
			const std::string& tag6 = std::string(),
			const std::string& tag7 = std::string(),
			const std::string& tag8 = std::string(),
			const std::string& tag9 = std::string());

    virtual ~XMLParsable();

    bool parse(XMLNode* xmlElement, XMLParsable* parent = 0);
    bool tagMatches(const std::string& tag) const;
    bool isParsed() const;
    XMLParsable* getParent() const;

protected:
    void bindData(XMLAbstractData* data);
    void bindList(XMLAbstractList* list);

    template <class T>
    void bindChildren(T* child)
    {
        m_children.push_back(child);
    }

private:
    virtual bool preParse(XMLNode* xmlElement);
    virtual bool postParse(XMLNode* xmlElement);
    bool parseData(XMLNode* xmlElement);
    bool parseLists(XMLNode* xmlElement);
    bool parseChildren(XMLNode* xmlElement);

    bool m_parsed;
    XMLParsable* m_parent;
    std::list<std::string> m_tags;
    std::list<XMLAbstractData*> m_data;
    std::list<XMLAbstractList*> m_lists;
    std::list<XMLParsable*> m_children;
};

#endif // XMLPARSABLE_H
