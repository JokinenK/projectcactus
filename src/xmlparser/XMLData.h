#ifndef XMLDATA_H
#define XMLDATA_H

#include <string>
#include <vector>
#include <map>
#include "support/Utils.h"
#include "xmlparser/XMLNode.h"

typedef enum {
    DATASOURCE_ATTR,
    DATASOURCE_VALUE,
	DATASOURCE_CDATA
} XMLDataSource;

class XMLAbstractData
{
public:
    XMLAbstractData() {}
    virtual ~XMLAbstractData() {}
    virtual bool parse(XMLNode* element) = 0;
};

template <class T>
class XMLData : public XMLAbstractData
{
public:
    virtual ~XMLData() {}

    static XMLAbstractData* optionalValue(T& target)
    {
        return new XMLData<T>(DATASOURCE_VALUE, target, std::string(), false);
    }

    static XMLAbstractData* requiredValue(T& target)
    {
        return new XMLData<T>(DATASOURCE_VALUE, target, std::string(), true);
    }

    static XMLAbstractData* optionalCDATA(T& target)
    {
        return new XMLData<T>(DATASOURCE_CDATA, target, std::string(), false);
    }

    static XMLAbstractData* requiredCDATA(T& target)
    {
        return new XMLData<T>(DATASOURCE_CDATA, target, std::string(), true);
    }

    static XMLAbstractData* optionalAttr(T& target, const std::string& attrName)
    {
        return new XMLData<T>(DATASOURCE_ATTR, target, attrName, false);
    }

    static XMLAbstractData* requiredAttr(T& target, const std::string& attrName)
    {
        return new XMLData<T>(DATASOURCE_ATTR, target, attrName, true);
    }

    bool parse(XMLNode* xmlElement)
    {
        bool success = false;
        T value;

        if (m_dataSource == DATASOURCE_VALUE) {
        	value = xmlElement->getValueAs<T>(&success);
        }
        else if (m_dataSource == DATASOURCE_CDATA) {
        	value = xmlElement->getCDATAAs<T>(&success);
        }
        else {
        	value = xmlElement->getAttrAs<T>(m_attrName, &success);
        }

        if (success) {
        	m_target = value;
        }

        return success || !m_required;
    }

private:
    XMLData(XMLDataSource dataSource,
    		T& target,
    		const std::string& attrName,
			bool required) :
				m_dataSource(dataSource),
				m_target(target),
				m_attrName(attrName),
				m_required(required)
    {
    }

    XMLDataSource m_dataSource;
    T& m_target;
    std::string m_attrName;
    bool m_required;
};

#endif // XMLDATA_H
