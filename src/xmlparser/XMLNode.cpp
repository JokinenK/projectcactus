/*
 * XMLNode.cpp
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#include "xmlparser/XMLNode.h"
#include "support/MapIterator.h"

XMLNode::XMLNode(const std::string& tag) : m_tag(tag)
{
}

XMLNode::~XMLNode()
{
	for (XMLNode* child : m_children) {
		delete child;
	}
}

const std::string& XMLNode::getTag() const
{
	return m_tag;
}

void XMLNode::setValue(const std::string& value)
{
	m_value = value;
}

const std::string& XMLNode::getValue() const
{
	return m_value;
}

void XMLNode::setCDATA(const std::string& cdata)
{
	m_cdata = cdata;
}

const std::string& XMLNode::getCDATA() const
{
	return m_cdata;
}

void XMLNode::setAttrs(const std::map<std::string, std::string>& attrs)
{
	m_attrs = attrs;
}

const std::map<std::string, std::string>& XMLNode::getAttrs() const
{
	return m_attrs;
}

const std::string XMLNode::getAttr(const std::string& key, bool* success) const
{
	std::string result;
	auto iter = m_attrs.find(key);

	if (iter != m_attrs.end()) {
		result = iter->second;
		*success = true;
	}
	else {
		*success = false;
	}

	return result;
}

void XMLNode::addChildren(XMLNode* children)
{
	m_children.push_back(children);
}

const std::list<XMLNode*>& XMLNode::getChildren() const
{
	return m_children;
}

const std::list<XMLNode*> XMLNode::findChildren(const std::string& tag) const
{
	std::list<XMLNode*> results;

	for (XMLNode* child : m_children) {
		if (child->getTag() == tag) {
			results.push_back(child);
		}
	}

	return results;
}
