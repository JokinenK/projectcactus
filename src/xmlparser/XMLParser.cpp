#include <iostream>
#include <sstream>
#include <tinyxml2/tinyxml2.h>
#include "xmlparser/XMLParser.h"
#include "xmlparser/XMLNode.h"
#include "support/Utils.h"

namespace
{
	static XMLNode* parseNode(const tinyxml2::XMLElement* xmlElement);
	static void parseAttrs(const tinyxml2::XMLElement* element, XMLNode* node);
}

XMLNode* XMLParser::parse(const std::string& fileName)
{
	XMLNode* result = 0;
	tinyxml2::XMLDocument doc;

	if (doc.LoadFile(fileName.c_str()) == tinyxml2::XML_SUCCESS) {
		result = parseNode(doc.RootElement());
	}
	else {
		std::cerr << "Unable to parse file: " << fileName << std::endl;
	}

	return result;
}

namespace
{
	XMLNode* parseNode(const tinyxml2::XMLElement* xmlElem)
	{
		XMLNode* node = 0;

		if (xmlElem) {
			std::stringstream value;
			std::stringstream cdata;

			node = new XMLNode(Utils::trimmed(xmlElem->Value()));
			parseAttrs(xmlElem, node);

			const tinyxml2::XMLNode* childXmlNode = xmlElem->FirstChild();

			while (childXmlNode != NULL) {
				const tinyxml2::XMLText* childXmlText = childXmlNode->ToText();
				const tinyxml2::XMLElement* childXmlElem = childXmlNode->ToElement();

				if (childXmlText) {
					if (childXmlText->CData()) {
						cdata << Utils::trimmed(childXmlText->Value());
					}
					else {
						value << Utils::trimmed(childXmlText->Value());
					}
				}
				else if (childXmlElem) {
					node->addChildren(parseNode(childXmlElem));
				}

				childXmlNode = childXmlNode->NextSibling();
			}

			node->setValue(value.str());
			node->setCDATA(cdata.str());
		}

		return node;
	}

    static void parseAttrs(const tinyxml2::XMLElement* element, XMLNode* node)
    {
    	std::map<std::string, std::string> attrs;
        std::string key;
        std::string value;

        const tinyxml2::XMLAttribute* xmlAttr = element->FirstAttribute();

        while (xmlAttr != NULL) {
			key = Utils::trimmed(xmlAttr->Name());
			value = Utils::trimmed(xmlAttr->Value());

			if (!key.empty() && !value.empty()) {
				attrs[key] = value;
			}

        	xmlAttr = xmlAttr->Next();
        }

        node->setAttrs(attrs);
    }
}
