#include <map>
#include <algorithm>
#include <iostream>
#include "xmlparser/XMLParsable.h"
#include "xmlparser/XMLData.h"
#include "xmlparser/XMLList.h"

XMLParsable::XMLParsable(
		const std::string& tag1,
		const std::string& tag2,
		const std::string& tag3,
		const std::string& tag4,
		const std::string& tag5,
		const std::string& tag6,
		const std::string& tag7,
		const std::string& tag8,
		const std::string& tag9) :
				m_parsed(false),
				m_parent(NULL)
{
	// Always atleast one tag
	m_tags.push_back(tag1);

	// In other cases, multiple tags
	if (!tag2.empty()) { m_tags.push_back(tag2); }
	if (!tag3.empty()) { m_tags.push_back(tag3); }
	if (!tag4.empty()) { m_tags.push_back(tag4); }
	if (!tag5.empty()) { m_tags.push_back(tag5); }
	if (!tag6.empty()) { m_tags.push_back(tag6); }
	if (!tag7.empty()) { m_tags.push_back(tag7); }
	if (!tag8.empty()) { m_tags.push_back(tag8); }
	if (!tag9.empty()) { m_tags.push_back(tag9); }
}

XMLParsable::~XMLParsable()
{
}

bool XMLParsable::parse(XMLNode* xmlElement, XMLParsable* parent)
{
	bool success = true;

	if (parent) {
		m_parent = parent;
	}

	success &= preParse(xmlElement);
	success &= parseData(xmlElement);
	success &= parseLists(xmlElement);
	success &= parseChildren(xmlElement);
	success &= postParse(xmlElement);

	if  (!success) {
		std::cerr << "Failed to parse: " << xmlElement->getTag() << std::endl;
	}

	m_parsed = success;
	return success;
}

bool XMLParsable::tagMatches(const std::string& tag) const
{
	return (std::find(m_tags.begin(), m_tags.end(), tag) != m_tags.end());
}

bool XMLParsable::isParsed() const
{
	return m_parsed;
}

XMLParsable* XMLParsable::getParent() const
{
	return m_parent;
}

void XMLParsable::bindData(XMLAbstractData* data)
{
	m_data.push_back(data);
}

void XMLParsable::bindList(XMLAbstractList* list)
{
	m_lists.push_back(list);
}

bool XMLParsable::preParse(XMLNode* xmlElement)
{
	/* Unused */
	(void) xmlElement;

	return true;
}

bool XMLParsable::postParse(XMLNode* xmlElement)
{
	/* Unused */
	(void) xmlElement;

	return true;
}

bool XMLParsable::parseData(XMLNode* xmlElement)
{
	bool success = true;

	for (XMLAbstractData* data : m_data) {
		if (!data->parse(xmlElement)) {
			std::cerr << "parseData failed for element: " << xmlElement->getTag() << std::endl;
			success = false;
			break;
		}
	}

	return success;
}

bool XMLParsable::parseLists(XMLNode* xmlElement)
{
	bool success = true;

	for (XMLAbstractList* list : m_lists) {
		if (!list->parseList(xmlElement, this)) {
			std::cerr << "parseLists failed for element: " << xmlElement->getTag() << std::endl;
			success = false;
			break;
		}
	}

	return success;
}

bool XMLParsable::parseChildren(XMLNode* xmlElement)
{
	bool success = true;

	for (XMLNode* xmlChild : xmlElement->getChildren()) {
		for (XMLParsable* child : m_children) {
			if (child->tagMatches(xmlChild->getTag()) && !child->parse(xmlChild, this)) {
				std::cerr << "parseChildren failed for element: " << xmlChild->getTag() << std::endl;
				success = false;
				break;
			}
		}
	}

	return success;
}
