#ifndef XMLPARSER_H_
#define XMLPARSER_H_

#include "xmlparser/XMLNode.h"

class XMLParser
{
public:
	static XMLNode* parse(const std::string& fileName);
};

#endif // XMLPARSER_H_
