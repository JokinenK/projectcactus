/*
 * XMLNode.h
 *
 *  Created on: 2.10.2016
 *      Author: kalle
 */

#ifndef XMLNODE_H_
#define XMLNODE_H_

#include <list>
#include <string>
#include <map>
#include "support/Utils.h"

class XMLNode {
public:
	XMLNode(const std::string& tag);

	virtual ~XMLNode();

	const std::string& getTag() const;

	void setValue(const std::string& value);
	const std::string& getValue() const;

	void setCDATA(const std::string& cdata);
	const std::string& getCDATA() const;

	void setAttrs(const std::map<std::string, std::string>& attrs);
	const std::map<std::string, std::string>& getAttrs() const;
	const std::string getAttr(const std::string& key, bool* success = 0) const;

	void addChildren(XMLNode* children);
	const std::list<XMLNode*>& getChildren() const;
	const std::list<XMLNode*> findChildren(const std::string& tag) const;

	template <class T>
	T getValueAs(bool* success) const
	{
		T result;
		*success = Utils::fromString(getValue(), result);
		return result;
	}

	template <class T>
	T getCDATAAs(bool* success) const
	{
		T result;
		*success = Utils::fromString(getCDATA(), result);
		return result;
	}

	template <class T>
	T getAttrAs(const std::string& key, bool* success) const
	{
		T result;
		std::string value = getAttr(key, success);

		if (*success) {
			*success = Utils::fromString(value, result);
		}

		return result;
	}

private:
	std::string m_tag;
	std::string m_value;
	std::string m_cdata;
	std::map<std::string, std::string> m_attrs;
	std::list<XMLNode*> m_children;
};

#endif /* XMLNODE_H_ */
