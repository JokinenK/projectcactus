#version 330 core
#include "gbuffer.inc"
#include "material.inc"

layout (location = 0) out vec4 gDiffuse;
layout (location = 1) out vec4 gNormal;

in VS_OUT {
    vec2 texCoords;	
    vec3 worldPosition;
	vec3 worldNormal;
    mat3 tangentToWorldSpace;
} fs_in;

uniform mat4 uniformModel;
uniform mat4 uniformView;

uniform Material uniformMaterial;

vec3 getNormal(vec2 texCoords)
{
	if (uniformMaterial.hasNormalTexture) {
		return normalize(fs_in.tangentToWorldSpace * (texture(uniformMaterial.samplerNormal, texCoords).xyz * 2.0 - 1.0));
	}
	
	return normalize(fs_in.worldNormal);
}

vec4 getAmbientColor(vec2 texCoords)
{
	if (uniformMaterial.hasAmbientTexture) {
		return uniformMaterial.colorAmbient * texture(uniformMaterial.samplerAmbient, texCoords);
	}
	
	return uniformMaterial.colorAmbient;
}

vec4 getDiffuseColor(vec2 texCoords)
{
	if (uniformMaterial.hasDiffuseTexture) {
		return uniformMaterial.colorDiffuse * texture(uniformMaterial.samplerDiffuse, texCoords);
	}
	
	return uniformMaterial.colorDiffuse;
}

vec4 getSpecularColor(vec2 texCoords)
{
	if (uniformMaterial.hasSpecularTexture) {
		return uniformMaterial.colorSpecular * texture(uniformMaterial.samplerSpecular, texCoords);
	}
	
	return uniformMaterial.colorSpecular;
}

float getShininess(vec2 texCoords)
{
	if (uniformMaterial.hasShininessTexture) {
		return uniformMaterial.shininess * texture(uniformMaterial.samplerShininess, texCoords).r; // TODO: Check actual color channel
	}
	
	return uniformMaterial.shininess;
}

float getDissolve(vec2 texCoords)
{
	if (uniformMaterial.hasDissolveTexture) {
		return uniformMaterial.dissolve * texture(uniformMaterial.samplerDissolve, texCoords).r; // TODO: Check actual color channel
	}
	
	return uniformMaterial.dissolve;
}

vec2 getTexCoords()
{
	vec2 texCoords = fs_in.texCoords;
	
	if (false && uniformMaterial.hasDisplacementTexture) {
		//vec3 viewDir = normalize(fs_in.worldDirectionToCamera);
        vec3 viewDir;
		float parallaxScale = 0.15;
		
		// number of depth layers
		const float minLayers = 5;
		const float maxLayers = 25;
		float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
		  
		// calculate the size of each layer
		float layerDepth = 1.0 / numLayers;
		
		// depth of current layer
		float currentLayerDepth = 0.0;
		
		// the amount to shift the texture coordinates per layer (from vector P)
		vec2 P = viewDir.xy / viewDir.z * parallaxScale; 
		vec2 deltaTexCoords = P / numLayers;
	  
		// get initial values
		vec2 currentTexCoords = texCoords;
		float currentDepthMapValue = texture(uniformMaterial.samplerDisplacement, currentTexCoords).r;
		  
		while (currentLayerDepth < currentDepthMapValue) {
			// shift texture coordinates along direction of P
			currentTexCoords -= deltaTexCoords;
			// get depthmap value at current texture coordinates
			currentDepthMapValue = texture(uniformMaterial.samplerDisplacement, currentTexCoords).r;  
			// get depth of next layer
			currentLayerDepth += layerDepth;  
		}
		
		// -- parallax occlusion mapping interpolation from here on
		// get texture coordinates before collision (reverse operations)
		vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

		// get depth after and before collision for linear interpolation
		float afterDepth  = currentDepthMapValue - currentLayerDepth;
		float beforeDepth = texture(uniformMaterial.samplerDisplacement, prevTexCoords).r - currentLayerDepth + layerDepth;
	 
		// interpolation of texture coordinates
		float weight = afterDepth / (afterDepth - beforeDepth);
		texCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);
        
        if (texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0) {
            discard;
        }
	}

    return texCoords;
}

void main()
{    
	vec2 texCoords = getTexCoords();
	vec4 ambientColor = getAmbientColor(texCoords);
	vec4 diffuseColor = getDiffuseColor(texCoords);
	vec4 specularColor = getSpecularColor(texCoords);
	float glossiness = getShininess(texCoords);
	float dissolve = getDissolve(texCoords);
	vec3 normal = getNormal(texCoords);
	
	if (dissolve < 0.95) {
		discard;
	}
	
	// Diffuse & Specular
    vec3 finalDiffuseColor = mix(ambientColor.rgb, diffuseColor.rgb, ambientColor.a);
	gDiffuse.xy = GBUFFER_EncodeInterleavedYCoCg(finalDiffuseColor);
	gDiffuse.zw = GBUFFER_EncodeInterleavedYCoCg(specularColor.rgb);
    
    // Normal & glossiness
    gNormal.xy = GBUFFER_EncodeNormal(normal);
    gNormal.z = glossiness;
    gNormal.w = 1.0;
} 