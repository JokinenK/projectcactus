#version 130

in vec3 attrPosition;
out vec3 varyingTexCoord;

uniform mat4 uniformModel;
uniform mat4 uniformVP;

void main()
{
	varyingTexCoord = attrPosition;
	gl_Position = uniformVP * vec4(attrPosition, 1.0);
}