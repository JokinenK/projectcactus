struct Pipeline {
	mat4 model;
	mat4 inverseModel;

	mat4 view;
	mat4 inverseView;

	mat4 projection;
	mat4 inverseProjection;

	mat4 MV;
	mat4 inverseMV;

	mat4 VP;
	mat4 inverseVP;

	mat4 MVP;
	mat4 InverseMVP;
};
