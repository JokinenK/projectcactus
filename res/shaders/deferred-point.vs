#version 150
#include "pipeline.inc"

in vec3 attrPosition;

uniform Pipeline uniformPipeline;

void main()
{    
    gl_Position = uniformPipeline.MVP * vec4(attrPosition, 1.0);
}
