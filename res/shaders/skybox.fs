#version 130

in vec3 varyingTexCoord;

out vec4 FragColor;

uniform samplerCube uniformSamplerDiffuse;

void main()
{
	FragColor = texture(uniformSamplerDiffuse, varyingTexCoord);
}