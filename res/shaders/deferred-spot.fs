#version 150
#include "gbuffer.inc"
#include "light.inc"
#include "shadows.inc"
#include "camera.inc"
#include "pipeline.inc"

out vec4 FragColor;

uniform sampler2D uniformSamplerDiffuse;
uniform sampler2D uniformSamplerNormal;
uniform sampler2D uniformSamplerDepth;
uniform sampler2D uniformSamplerShadow;

uniform mat4 uniformShadowMatrix;

uniform vec2 uniformScreenSize;
uniform Camera uniformCamera;
uniform Pipeline uniformPipeline;
uniform SpotLight uniformSpotLight;

void main()
{    
    // Retrieve data from gbuffer
    vec2 texCoords          = GBUFFER_GetTexCoords(uniformScreenSize);
    
    vec3 clipPosition       = GBUFFER_GetClipSpacePosition(uniformSamplerDepth, texCoords);
    vec3 viewPosition       = GBUFFER_ClipSpaceToViewSpace(clipPosition, uniformPipeline.inverseProjection);
    vec3 worldPosition      = GBUFFER_ViewSpaceToWorldSpace(viewPosition, uniformPipeline.inverseView);
        
    float depth             = texture(uniformSamplerDepth, texCoords).x;
    float linearDepth       = GBUFFER_LinearizeDepth(depth, uniformCamera.range.near, uniformCamera.range.far);
    
    vec4 normalTexture      = texture(uniformSamplerNormal, texCoords);
    vec3 normal             = GBUFFER_DecodeNormal(normalTexture.xy);
    float glossiness        = normalTexture.z;
    float specularIntensity = normalTexture.w;
    float shadowFactor      = SHADOWS_DirectionalLightShadowFactor(uniformSamplerShadow, worldPosition, uniformShadowMatrix);
    
    vec3 diffuseColor       = GBUFFER_DecodeInterleavedYCoCg(uniformSamplerDiffuse, texCoords, true, uniformScreenSize);
	vec3 specularColor      = GBUFFER_DecodeInterleavedYCoCg(uniformSamplerDiffuse, texCoords, false, uniformScreenSize);

    FragColor = vec4(diffuseColor, 1.0) * LIGHT_CalcSpotLight(uniformCamera.position, uniformSpotLight, worldPosition, normal, specularColor, specularIntensity, glossiness, shadowFactor);
}