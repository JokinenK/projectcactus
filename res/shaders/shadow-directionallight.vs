#version 130

in vec3 attrPosition;

uniform mat4 uniformMVP;

void main()
{
	gl_Position = uniformMVP * vec4(attrPosition, 1.0f);
}