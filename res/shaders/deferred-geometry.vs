#version 150

in vec3 attrPosition;
in vec2 attrTexCoord;
in vec3 attrNormal;
in vec4 attrTangent;

out VS_OUT {
    vec2 texCoords;	
    vec3 worldPosition;
	vec3 worldNormal;
    mat3 tangentToWorldSpace;
} vs_out;

uniform mat4 uniformMVP;
uniform mat4 uniformNormalMatrix;
uniform vec3 uniformCameraPos;

void main()
{
    mat3 normalMatrix3x3 = mat3(uniformNormalMatrix);
    
    vec3 worldNormal = normalize(normalMatrix3x3 * attrNormal);
	vec3 worldTangent = normalize(normalMatrix3x3 * attrTangent.xyz);
	vec3 worldBitangent = normalize(normalMatrix3x3 * cross(attrNormal, attrTangent.xyz) * attrTangent.w);

    vs_out.texCoords           = (attrTexCoord);
    vs_out.worldPosition       = (uniformNormalMatrix * vec4(attrPosition, 1.0)).xyz;
    vs_out.worldNormal         = (worldNormal);
    vs_out.tangentToWorldSpace = (mat3(worldTangent, worldBitangent, worldNormal));
	
    //vec3 worldDirectionToCamera	= uniformCameraPos - worldPosition.xyz;
    //vs_out.worldDirectionToCamera = worldDirectionToCamera;
    
    gl_Position = uniformMVP * vec4(attrPosition, 1.0);
}