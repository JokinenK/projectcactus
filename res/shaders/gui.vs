#version 130

in vec3 attrPosition;
out vec2 varyingTexCoord;

uniform bool uniformIsText;
uniform mat4 uniformModel;
uniform mat4 uniformProjection;
uniform vec4 uniformTexCoords;

void main()
{
	vec2 texCoordsLow = uniformTexCoords.xy;
	vec2 texCoordsHigh = uniformTexCoords.zw;
	vec2 charTexCoord = vec2(
		(attrPosition.x < 0.5) ? texCoordsLow.x : texCoordsHigh.x,
		(attrPosition.y < 0.5) ? texCoordsLow.y : texCoordsHigh.y);
        
    if (uniformIsText) {
        varyingTexCoord = charTexCoord;
    }
    else {
        varyingTexCoord = attrPosition.xy;
    }
        
	gl_Position = uniformProjection * uniformModel * vec4(attrPosition, 1.0);
}