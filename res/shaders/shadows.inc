float SHADOWS_DirectionalLightShadowFactor(sampler2D sampler, vec3 fragPosWorldSpace, mat4 shadowMatrix)
{
    const float shadowBias = 0.0005;
    
    vec4 lightSpacePos = shadowMatrix * vec4(fragPosWorldSpace, 1);
    
    // perform perspective divide
    vec3 shadowCoords = lightSpacePos.xyz / lightSpacePos.w;
    float currentDepth = shadowCoords.z;
    float closestDepth = texture(sampler, shadowCoords.xy).z + shadowBias;
    
    float shadowFactor = 1.0;
    
    // Points outside the light volume are in shadow.
    if (shadowCoords.x < 0.0 || shadowCoords.x > 1.0 || shadowCoords.y < 0.0 || shadowCoords.y > 1.0 || shadowCoords.z < 0.0) {
        shadowFactor = 0.5;
    }
    else if (closestDepth < currentDepth) {
        shadowFactor = 0.5;
    }

    return shadowFactor;
}

float SHADOWS_PointLightShadowFactor(samplerCube sampler, vec3 fragPosWorldSpace, vec3 lightPosWorldSpace, float lightRadius)
{
    const float shadowBias = 0.0005;
    
    // Get vector between fragment position and light position
    vec3 fragToLight = fragPosWorldSpace - lightPosWorldSpace;
    
    float currentDepth = length(fragToLight);
    float closestDepth = texture(sampler, fragToLight).r + shadowBias;
    
    // It is currently in linear range between [0,1]. Re-transform back to original value
    closestDepth *= lightRadius;
    
    float shadowFactor = 1.0;
    
    if (closestDepth < currentDepth) {
        shadowFactor = 0.5;
    }
    
    return shadowFactor;
}