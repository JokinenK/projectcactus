#version 130

in vec2 varyingTexCoord;

out vec4 FragColor;

uniform sampler2D uniformSamplerDiffuse;
uniform sampler2D uniformSamplerFont;

uniform bool uniformIsText;
uniform bool uniformHasTexture;
uniform vec4 uniformBgGradiendColor1;
uniform vec4 uniformBgGradiendColor2;
uniform vec4 uniformTextColor;
uniform vec4 uniformOutlineColor;
uniform vec2 uniformShadowOffset;

const float smoothWidth = 0.5;
const float smoothEdge = 0.15;

const float borderWidth = 0.5;
const float borderEdge = 0.4;

vec4 gui()
{
	vec2 position = varyingTexCoord.xy;
	
	if (uniformHasTexture) {
		return texture(uniformSamplerDiffuse, position);
	}
    
    return vec4(mix(uniformBgGradiendColor1, uniformBgGradiendColor2, position.y));
}

vec4 text() {
	float totalAlpha;
	vec3 totalColor;
	
    float smoothDistance = 1 - texture(
    		uniformSamplerFont, 
    		varyingTexCoord).r;
        
    float smoothAlpha = 1 - smoothstep(
			smoothWidth,
			smoothWidth + smoothEdge,
			smoothDistance);
			
	if (uniformOutlineColor.a > 0 && borderWidth > 0.0) {
	    float borderDistance = 1 - texture(
	    		uniformSamplerFont, 
	    		varyingTexCoord + uniformShadowOffset).r;
	    		
	    float borderAlpha = 1 - smoothstep(
				borderWidth,
				borderWidth + borderEdge,
				borderDistance);
		
		totalAlpha = smoothAlpha + (1.0 - smoothAlpha) * borderAlpha;
		totalColor = mix(uniformOutlineColor.rgb, uniformTextColor.rgb, smoothAlpha / totalAlpha);
	}
	else {
		totalAlpha = smoothAlpha;
		totalColor = uniformTextColor.rgb;
	}
	
    return vec4(totalColor, totalAlpha);
}

void main()
{
    FragColor = uniformIsText ? text() : gui();
}
