#version 150
#include "gbuffer.inc"
#include "light.inc"
#include "shadows.inc"
#include "camera.inc"
#include "pipeline.inc"

out vec4 FragColor;

const int NUM_CASCADES = 4;

const vec4 DEBUG_COLORS[NUM_CASCADES] = vec4[](
    vec4(0.5, 0.0, 0.0, 1.0),
    vec4(0.0, 0.5, 0.0, 1.0),
    vec4(0.0, 0.0, 0.5, 1.0),
    vec4(0.5, 0.0, 0.5, 1.0));

uniform sampler2D uniformSamplerDiffuse;
uniform sampler2D uniformSamplerNormal;
uniform sampler2D uniformSamplerDepth;

uniform sampler2D uniformSamplerShadow[NUM_CASCADES];
uniform mat4 uniformShadowMatrices[NUM_CASCADES];
uniform float uniformCascadeLengths[NUM_CASCADES];

uniform vec2 uniformScreenSize;
uniform Camera uniformCamera;
uniform Pipeline uniformPipeline;
uniform DirectionalLight uniformDirectionalLight;

uniform bool uniformUseDebugColor = false;
    
void main()
{    
    // Retrieve data from gbuffer
    vec2 texCoords          = GBUFFER_GetTexCoords(uniformScreenSize);
    
    vec3 clipPosition       = GBUFFER_GetClipSpacePosition(uniformSamplerDepth, texCoords);
    vec3 viewPosition       = GBUFFER_ClipSpaceToViewSpace(clipPosition, uniformPipeline.inverseProjection);
    vec3 worldPosition      = GBUFFER_ViewSpaceToWorldSpace(viewPosition, uniformPipeline.inverseView);
        
    float depth             = texture(uniformSamplerDepth, texCoords).x;
    float linearDepth       = GBUFFER_LinearizeDepth(depth, uniformCamera.range.near, uniformCamera.range.far);
    
    vec4 normalTexture      = texture(uniformSamplerNormal, texCoords);
    vec3 normal             = GBUFFER_DecodeNormal(normalTexture.xy);
    float glossiness        = normalTexture.z;
    float specularIntensity = normalTexture.w;
    float shadowFactor      = 1.0;
    
    vec3 diffuseColor       = GBUFFER_DecodeInterleavedYCoCg(uniformSamplerDiffuse, texCoords, true, uniformScreenSize);
	vec3 specularColor      = GBUFFER_DecodeInterleavedYCoCg(uniformSamplerDiffuse, texCoords, false, uniformScreenSize);
    vec4 debugColor;
    
    for (int i = 0 ; i < NUM_CASCADES; i++) {
        if (linearDepth < uniformCascadeLengths[i]) {
            shadowFactor = SHADOWS_DirectionalLightShadowFactor(uniformSamplerShadow[i], worldPosition, uniformShadowMatrices[i]);
            debugColor = DEBUG_COLORS[i];
            break;
        }
    }
    
    FragColor = vec4(diffuseColor, 1.0) * LIGHT_CalcDirectionalLight(uniformCamera.position, uniformDirectionalLight, worldPosition, normal, specularColor, specularIntensity, glossiness, shadowFactor);
    
    if (uniformUseDebugColor) {
        FragColor += debugColor;
    }
}
