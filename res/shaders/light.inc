struct Light {
    bool shadow;
    vec4 color;
    float intensity;
};

struct DirectionalLight {
    Light light;
    vec3 direction;
};

struct PointLight {
    Light light;
    vec3 attenuation;
    vec3 position;
    float radius;
};

struct SpotLight {
    PointLight pointLight;
    vec3 direction;
    float cutoff;
};

vec4 LIGHT_CalcLight(
        vec3 cameraPos,        
        Light light, 
        vec3 lightDirection, 
        vec3 fragPos, 
        vec3 normal, 
        vec3 specularColor,
        float specularIntensity, 
        float glossiness,
        float shadowFactor)
{
    float diffuseFactor  = clamp(dot(normal, -lightDirection), 0.0, 1.0);
    vec4 diffusePart     = light.color * light.intensity * diffuseFactor;
    vec3 directionToEye  = normalize(cameraPos - fragPos);
    vec3 lightReflect    = normalize(reflect(lightDirection, normal));
    float specularFactor = pow(dot(directionToEye, lightReflect), glossiness);
    
    vec4 specularPart = (specularFactor > 0.0) 
                      ? (vec4(specularColor * specularIntensity * specularFactor, 1)) 
                      : (vec4(0, 0, 0, 0));
    
    shadowFactor = light.shadow ? shadowFactor : 1.0;
    return shadowFactor * (diffusePart + specularPart);
}

vec4 LIGHT_CalcDirectionalLight(
        vec3 cameraPos,
        DirectionalLight directionalLight, 
        vec3 fragPos, 
        vec3 normal, 
        vec3 specularColor,
        float specularIntensity, 
        float glossiness,
        float shadowFactor)
{
    vec3 lightDirection = normalize(directionalLight.direction);
    
    return LIGHT_CalcLight(
            cameraPos,
            directionalLight.light,
            lightDirection,
            fragPos,
            normal,
            specularColor,
            specularIntensity,
            glossiness,
            shadowFactor);
}

vec4 LIGHT_CalcPointLight(
        vec3 cameraPos,      
        PointLight pointLight, 
        vec3 fragPos, 
        vec3 normal, 
        vec3 specularColor,
        float specularIntensity, 
        float glossiness,
        float shadowFactor)
{
    vec3 lightDirection = fragPos - pointLight.position;
    float ligthDistance = length(lightDirection);
    
    vec4 lightColor = LIGHT_CalcLight(
            cameraPos,
            pointLight.light, 
            normalize(lightDirection), 
            fragPos, 
            normal, 
            specularColor,
            specularIntensity, 
            glossiness,
            shadowFactor);
    
    float lightAttenuation = 
            pointLight.attenuation.x + 
            pointLight.attenuation.y * ligthDistance + 
            pointLight.attenuation.z * ligthDistance * ligthDistance;
      
    return lightColor / lightAttenuation;
}

vec4 LIGHT_CalcSpotLight(
        vec3 cameraPos,      
        SpotLight spotLight, 
        vec3 fragPos, 
        vec3 normal, 
        vec3 specularColor,
        float specularIntensity, 
        float glossiness,
        float shadowFactor)
{
    vec4 color = vec4(0, 0, 0, 0);
	vec3 lightDirection = normalize(fragPos - spotLight.pointLight.position);
    float spotFactor = dot(lightDirection, spotLight.direction);
    
    if (spotFactor > spotLight.cutoff) {
        float spotCoefficient = (1.0 - (1.0 - spotFactor) / (1.0 - spotLight.cutoff));
        
        color = LIGHT_CalcPointLight(
            cameraPos, 
            spotLight.pointLight, 
            fragPos, 
            normal, 
            specularColor, 
            specularIntensity, 
            glossiness,
            shadowFactor) * spotCoefficient;
    }
    
    return color; 
}
