#version 330 core
#include "light.inc"
#include "camera.inc"

in vec4 FragPos;

uniform PointLight uniformPointLight;
uniform Camera uniformCamera;

void main()
{
    // get distance between fragment and light source
    float lightDistance = length(FragPos - vec4(uniformPointLight.position, 1));
    
    // map to [0;1] range by dividing by far_plane
    gl_FragDepth = lightDistance / uniformPointLight.radius;
}  