#version 330 core
in vec3 attrPosition;

uniform mat4 uniformModel;

void main()
{
	gl_Position = uniformModel * vec4(attrPosition, 1.0f);
}