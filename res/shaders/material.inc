struct Material {
    bool hasAmbientTexture;
    bool hasDiffuseTexture;
    bool hasSpecularTexture;
    bool hasShininessTexture;
    bool hasDissolveTexture;
    bool hasNormalTexture;
    bool hasDisplacementTexture;
    
    sampler2D samplerAmbient;
    sampler2D samplerDiffuse;
    sampler2D samplerSpecular;
    sampler2D samplerShininess;
    sampler2D samplerDissolve;
    sampler2D samplerNormal;
    sampler2D samplerDisplacement;

    vec4 colorAmbient;
    vec4 colorDiffuse;
    vec4 colorSpecular;
    float shininess;
    float dissolve;
};