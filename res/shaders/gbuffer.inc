const float PI = 3.1415926536;
const float GRADIENT_THRESHOLD = 30.0 / 255.0;

vec2 GBUFFER_GetTexCoords(vec2 screenSize)
{
   return gl_FragCoord.xy / screenSize;
}

float GBUFFER_LinearizeDepth(float depth, float near, float far)
{
    float z = depth * 2.0 - 1.0;
    return (2.0 * near * far) / (far + near - z * (far - near));   
}

vec3 GBUFFER_GetClipSpacePosition(sampler2D sampler, vec2 texCoords)
{
    float depth = texture2D(sampler, texCoords).x;
    vec4 clipSpacePosition = vec4(texCoords * 2.0 - 1.0, depth * 2.0 - 1.0, 1.0);
    
    return clipSpacePosition.xyz;
}

vec3 GBUFFER_ClipSpaceToViewSpace(vec3 clipSpacePosition, mat4 projectionInverse)
{
    vec4 viewSpacePosition = projectionInverse * vec4(clipSpacePosition, 1.0);

    // Perspective division
    return viewSpacePosition.xyz / viewSpacePosition.w;
}

vec3 GBUFFER_ViewSpaceToWorldSpace(vec3 viewSpacePosition, mat4 viewInverse) {
    vec4 worldSpacePosition = viewInverse * vec4(viewSpacePosition, 1.0);
    
    return worldSpacePosition.xyz;
}

float GBUFFER_ApplyFilter(vec2 center, vec2 a1, vec2 a2, vec2 a3, vec2 a4)
{
	vec4 luminance = vec4(a1.x, a2.x , a3.x, a4.x);
	vec4 weight = 1.0 - step(GRADIENT_THRESHOLD, abs(luminance - center.x));
	
	float totalWeight = weight.x + weight.y + weight.z + weight.w;
	
	// Special case where all the weights are zero
	weight.x    = (totalWeight == 0.0) ? 1.0 : weight.x; 
	totalWeight = (totalWeight == 0.0) ? 1.0 : totalWeight;
	
	return (weight.x * a1.y + weight.y * a2.y + weight.z * a3.y + weight.w * a4.y) / totalWeight;
}

vec2 GBUFFER_SelectNibble(vec4 source, bool isLowerNibble)
{
	return isLowerNibble ? source.xy : source.zw;
}

float GBUFFER_FindMissingValue(sampler2D sampler, vec2 centerPixelCoord, bool isLowerNibble, vec2 screenSize)
{
	vec2 offsetX = vec2(1.0 / screenSize.x, 0.0);
	vec2 offsetY = vec2(0.0, 1.0 / screenSize.y);
	
	vec4 rawCenter = texture2D(sampler, centerPixelCoord);
	vec4 rawLeft   = texture2D(sampler, centerPixelCoord - offsetX);
	vec4 rawRight  = texture2D(sampler, centerPixelCoord + offsetX);
	vec4 rawUp     = texture2D(sampler, centerPixelCoord + offsetY);
	vec4 rawDown   = texture2D(sampler, centerPixelCoord - offsetY);
	
	vec2 center = GBUFFER_SelectNibble(rawCenter, isLowerNibble);
	vec2 left   = GBUFFER_SelectNibble(rawLeft,   isLowerNibble);
	vec2 right  = GBUFFER_SelectNibble(rawRight,  isLowerNibble);
	vec2 up     = GBUFFER_SelectNibble(rawUp,     isLowerNibble);
	vec2 down   = GBUFFER_SelectNibble(rawDown,   isLowerNibble);

	return GBUFFER_ApplyFilter(center, left, right, up, down);
}

bool GBUFFER_IsEven()
{
	return (mod(gl_FragCoord.x, 2.0) == mod(gl_FragCoord.y, 2.0));
}

vec2 GBUFFER_EncodeNormal(vec3 normal)
{
    float phi = (atan(normal.x, normal.z) / PI) * 0.5 + 0.5;
    float theta = acos(normal.y) / PI;
    return vec2(phi, theta);
}

vec3 GBUFFER_DecodeNormal(vec2 encoded)
{
    float phi = encoded.x * (2.0 * PI) - PI;
    float theta = encoded.y * PI;

    float sintheta = sin(theta);
    return vec3(sintheta * sin(phi), cos(theta), sintheta * cos(phi));
}

vec3 GBUFFER_EncodeYCoCg(vec3 rgb)
{
    float Co = rgb.r - rgb.b;
    float t = rgb.b + Co * 0.5;
    float Cg = rgb.g - t;
    float Y = t + Cg * 0.5;
    return vec3(Y, Co + 0.5, Cg + 0.5);
}

vec3 GBUFFER_DecodeYCoCg(vec3 yCoCg)
{
    float Y = yCoCg.r;
    float Co = yCoCg.g - 0.5;
    float Cg = yCoCg.b - 0.5;
    
    float t = Y - Cg * 0.5;
    float G = Cg + t;
    float B = t - Co * 0.5;
    float R = Co + B;
    
    return vec3(R, G, B);
}

vec3 GBUFFER_EncodeYCbCr(vec3 rgb)
{
	float Y  = dot(rgb, vec3( 0.299000,  0.587000,  0.114000));
	float Cb = dot(rgb, vec3(-0.168736, -0.331264,  0.500000));
	float Cr = dot(rgb, vec3( 0.500000, -0.418688, -0.081312));
	return vec3(Y, Cb + 0.5, Cr + 0.5);
}

vec3 GBUFFER_DecodeYCbCr(vec3 yCbCr)
{
	float Y = yCbCr.x;
    float Cb = yCbCr.y - 0.5;
	float Cr = yCbCr.z - 0.5;
	
    float R =  1.402 * Cr;
	float G = -0.344 * Cb - 0.714 * Cr;
	float B =  1.772 * Cb;

	return vec3(R, G, B) + Y;
}

vec2 GBUFFER_EncodeInterleavedYCoCg(vec3 rgb)
{
	vec3 yCoCg = GBUFFER_EncodeYCoCg(rgb);
	return GBUFFER_IsEven() ? yCoCg.xy : yCoCg.xz;
}

vec3 GBUFFER_DecodeInterleavedYCoCg(sampler2D sampler, vec2 texCoords, bool isLowerNibble, vec2 screenSize)
{
	vec3 yCoCg;
	vec2 known = GBUFFER_SelectNibble(texture2D(sampler, texCoords), isLowerNibble);
	float missing = GBUFFER_FindMissingValue(sampler, texCoords, isLowerNibble, screenSize);
	
	if (GBUFFER_IsEven()) {
		yCoCg.xy = known;
		yCoCg.z = missing;
	}
	else {
		yCoCg.xz = known;
		yCoCg.y = missing;
	}
	
	return GBUFFER_DecodeYCoCg(yCoCg);
}

vec2 GBUFFER_EncodeInterleavedYCbCr(vec3 rgb)
{
	vec3 yCbCr = GBUFFER_EncodeYCbCr(rgb);
	return GBUFFER_IsEven() ? yCbCr.xy : yCbCr.xz;
}

vec3 GBUFFER_DecodeInterleavedYCbCr(sampler2D sampler, vec2 texCoords, bool isLowerNibble, vec2 screenSize)
{
	vec3 yCbCr;
	vec2 known = GBUFFER_SelectNibble(texture2D(sampler, texCoords), isLowerNibble);
	float missing = GBUFFER_FindMissingValue(sampler, texCoords, isLowerNibble, screenSize);
	
	if (GBUFFER_IsEven()) {
		yCbCr.xy = known;
		yCbCr.z = missing;
	}
	else {
		yCbCr.xz = known;
		yCbCr.y = missing;
	}
	
	return GBUFFER_DecodeYCbCr(yCbCr);
}