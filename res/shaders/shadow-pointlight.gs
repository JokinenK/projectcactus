#version 330 core
layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

uniform mat4 uniformShadowMatrices[6];

out vec4 FragPos; // FragPos from GS (output per emitvertex)

void main()
{
    // built-in variable that specifies to which face we render.
    for (gl_Layer = 0; gl_Layer < 6; ++gl_Layer) {
        // for each triangle's vertices
        for (int i = 0; i < 3; ++i) {
            FragPos = gl_in[i].gl_Position;
            gl_Position = uniformShadowMatrices[gl_Layer] * FragPos;
            
            EmitVertex();
        } 
        
        EndPrimitive();
    }
}  