struct Range {
	float near;
	float far;
};

struct Camera {
	vec3 position;
	vec3 forward;
	Range range;
};
